<?php
include 'AppManager.php';
include('AppConst.php');

class infographic {

    function __construct() {
        
    }

    function LoadData($appid) {
        $appmng = new AppManager();
        //$appid = 'A170226194409096066'; //$_GET['testerid'];
//$numofexam = 6;
        $appobj = new ResponseHeader();
        $datalists = new ResponseHeader();
//$tmpresp = $appmng->GetPrintInfoGraphicByAppID($appid, AppConst::PARAM_PRINT_APP);
        $datalists = $appmng->GetPrintInfoGraphicByAppIDNew($appid);
        $examlist = array();
        $examlist = $appmng->GetShowExamListInfographic();
        $dataset = array();
//service::printr($examlist);
        foreach ($datalists->ResponseDetail as $key => $value) {
            if ($key === AppConst::PARAM_TESTER) {
                continue;
            }
            //echo "print app[nam]" . $key;
            $exam = new ExamObj();
            $exam = $datalists->ResponseDetail[$key];
            /*
             * unset examlist
             */
            if(!in_array($exam->GetExamID(), $examlist)){
                //echo $exam->GetExamID();
                unset($datalists->ResponseDetail[$key]); 
                //continue;
            }
            if (AppConst::EXAM_TYPE_BMI == $exam->GetExamID()) {
                $dataset[AppConst::EXAM_TYPE_BMI] = $exam;
            } else if (AppConst::EXAM_TYPE_EXCERCISE == $exam->GetExamID()) {
                $dataset[AppConst::EXAM_TYPE_EXCERCISE] = $exam;
            } else if (AppConst::EXAM_TYPE_WAIST_CIRC == $exam->GetExamID()) {
                $dataset[AppConst::EXAM_TYPE_WAIST_CIRC] = $exam;
            } else if (AppConst::EXAM_TYPE_PULSE == $exam->GetExamID()) {
                $dataset[AppConst::EXAM_TYPE_PULSE] = $exam;
            } else if (AppConst::EXAM_TYPE_BP_D == $exam->GetExamID()) {
                $dataset[AppConst::EXAM_TYPE_BP_D] = $exam;
            } else if (AppConst::EXAM_TYPE_BP_U == $exam->GetExamID()) {
                $dataset[AppConst::EXAM_TYPE_BP_U] = $exam;
            } else if (AppConst::EXAM_TYPE_OMRON_BODYFAT == $exam->GetExamID()) {
                $dataset[AppConst::EXAM_TYPE_OMRON_BODYFAT] = $exam;
            } else if (AppConst::EXAM_TYPE_SKIN_FOLD == $exam->GetExamID()) {
                $dataset[AppConst::EXAM_TYPE_SKIN_FOLD] = $exam;
            }
            if ($exam->GetExamCat() === AppConst::EXAM_CAT_BAL) {
                if ($exam->GetExamID() === AppConst::EXAM_TYPE_ONE_LEG_BAL_L) {
                    $dataset[AppConst::EXAM_CAT_BAL][0] = $exam;
                } else if ($exam->GetExamID() === AppConst::EXAM_TYPE_ONE_LEG_BAL_R) {
                    $dataset[AppConst::EXAM_CAT_BAL][1] = $exam;
                } else {
                    $dataset[AppConst::EXAM_CAT_BAL][] = $exam;
                }
            } else if ($exam->GetExamCat() === AppConst::EXAM_CAT_STR) {
                if ($exam->GetExamID() === AppConst::EXAM_TYPE_HANDGRIP) {
                    $dataset[AppConst::EXAM_CAT_STR][0] = $exam;
                } else {
                    $dataset[AppConst::EXAM_CAT_STR][] = $exam;
                }
            } else if ($exam->GetExamCat() === AppConst::EXAM_CAT_FLX) {
                $dataset[AppConst::EXAM_CAT_FLX][] = $exam;
            } else if ($exam->GetExamCat() === AppConst::EXAM_CAT_CAR) {
                $dataset[AppConst::EXAM_CAT_CAR][] = $exam;
            } else if ($exam->GetExamCat() === AppConst::EXAM_CAT_END) {
                $dataset[AppConst::EXAM_CAT_END][] = $exam;
            } else if ($exam->GetExamCat() === AppConst::EXAM_CAT_AGI) {
                $dataset[AppConst::EXAM_CAT_AGI][] = $exam;
            } else if ($exam->GetExamCat() === AppConst::EXAM_CAT_PER) {
                if ($exam->GetExamID() !== AppConst::EXAM_TYPE_OMRON_BMI && $exam->GetExamID() !== AppConst::EXAM_TYPE_WAIST_CIRC && $exam->GetExamID() !== AppConst::EXAM_TYPE_BMI ) {
                    $dataset[AppConst::EXAM_CAT_PER][] = $exam;
                }
                
            }
        }
        /* if($dataset[AppConst::EXAM_TYPE_EXCERCISE] == null){
          $dataset[AppConst::EXAM_TYPE_EXCERCISE] =
          } */

//service::printr($dataset);
        //echo "------------------------------------------------------------------------------------";
        usort($dataset[AppConst::EXAM_CAT_BAL], "cmp_desc");
        usort($dataset[AppConst::EXAM_CAT_STR], "cmp_desc");
        usort($dataset[AppConst::EXAM_CAT_FLX], "cmp_desc");
        usort($dataset[AppConst::EXAM_CAT_CAR], "cmp_desc");
        usort($dataset[AppConst::EXAM_CAT_END], "cmp_desc");
        usort($dataset[AppConst::EXAM_CAT_AGI], "cmp_desc");
        usort($dataset[AppConst::EXAM_CAT_PER], "cmp_desc");
//service::printr($dataset);
        $dataset[AppConst::PARAM_TESTER] = $datalists->ResponseDetail[AppConst::PARAM_TESTER];
        return $dataset;
    }

    function cmp_desc($a, $b) {
        return strcmp($b->GetExamPriority(), $a->GetExamPriority());
    }

    function RenderHTML($appid) {
        ob_start();
        $appconst = new AppConst();
        $datasets = $this->LoadData($appid);
        $categorylist = array('examcat01', 'examcat02', 'examcat03', 'examcat04', 'examcat05', 'examcat06', 'examcat00');
        ?>
        <!DOCTYPE html>
        <html>
            <header>
                <link rel="stylesheet" href="resources/bootstrap-3.3.7-dist/css/bootstrap.min.css"/>
                <script src="jquery/jquery-3.1.1.min.js"></script>
                <script src="resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
                <link rel="stylesheet" href="css/infographic.css">
            </header>
            <body>
                <div class="container-fluid">
                    <table>
                        <tr>
                            <td width="250px;">&nbsp;</td>
                            <td width="155px;"><img style="text-align: center;" width="140px" height="40px" src="images/mu_logo3.png"></td>
                            <td width="160px"></td>
                            <td width="80px;"></td>
                        </tr>
                    </table>
                    <br>
                    <div class="normal-size">
                        <div class="row-print">
                            <div class="col-print-6">
                                <div class="col-print-6">
                                    หมายเลขประจำตัวผู้ทดสอบ
                                </div>
                                <div class="col-print-6">
                                    <?= $datasets[AppConst::PARAM_TESTER]->GetTesterID(); ?> 
                                </div>
                            </div>
                            <div class="col-print-6">
                                <div class="col-print-6">
                                    กลุ่มการทดสอบ
                                </div>
                                <div class="col-print-6">
                                    <?= $datasets[AppConst::PARAM_TESTER]->GetTesterGrpDesc(); ?>
                                </div>

                            </div>
                        </div>
                        <div class="row-print">
                            <div class="col-print-6">
                                <div class="col-print-6">
                                    ชื่อ-นามสกุล
                                </div>
                                <div class="col-print-6">
                                    <?= $datasets[AppConst::PARAM_TESTER]->GetNameTH(); ?> <?= $datasets[AppConst::PARAM_TESTER]->GetSurnameTH(); ?> 
                                </div>
                            </div>
                            <div class="col-print-6">
                                <div class="col-print-6">
                                    เพศ <?= ($datasets[AppConst::PARAM_TESTER]->GetSex() == "M" ? "ชาย":"หญิง"); ?>  
                                </div>
                                <div class="col-print-6">
                                    การออกกำลังกาย 
                                    <?php
                                    $value = " -";
                                    if ($datasets[AppConst::EXAM_TYPE_EXCERCISE] != null) {
                                        $value = $datasets[AppConst::EXAM_TYPE_EXCERCISE]->GetTestValue();
                                        if ($value == "0") {
                                            $value = " ไม่เคย";
                                        } else if ($value == "1") {
                                            $value = " 1-2 ครั้ง ต่อสัปดาห์";
                                        } else if ($value == "2") {
                                            $value = " 2-5 ครั้ง ต่อสัปดาห์";
                                        } else {
                                            $value = " -";
                                        }
                                    }
                                    echo $value;
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="row-print">
                            <div class="col-print-6">
                                <div class="col-print-6">
                                    วัน/เดือน/ปี เกิด(พ.ศ.)
                                </div>
                                <div class="col-print-6">
                                    <?php
                                    if ($datasets[AppConst::PARAM_TESTER] != null) {
                                        $dob = explode("/", $datasets[AppConst::PARAM_TESTER]->GetDOB());
                                        //print_r($dob);
                                        $year = $dob[2] + 543;
                                        $birthday = $dob[0] . "/" . $dob[1] . "/" . $year;
                                    }
                                    echo $birthday;
                                    ?>
                                    (อายุ <?= $datasets[AppConst::PARAM_TESTER]->GetAge(); ?> ปี)
                                </div>
                            </div>
                            <div class="col-print-6">
                                <div class="col-print-6">
                                    น้ำหนัก <?= $datasets[AppConst::PARAM_TESTER]->GetWeight(); ?> กก.
                                </div>
                                <div class="col-print-6">
                                    ส่วนสูง <?= $datasets[AppConst::PARAM_TESTER]->GetHeight(); ?> ซม.
                                </div>
                            </div>
                        </div>
                        <div class="row-print">
                            <div class="col-print-6">
                                <div class="col-print-6">
                                    BMI 
                                </div>
                                <div class="col-print-6">
                                    <?= $datasets[AppConst::EXAM_TYPE_BMI]->GetTestValue(); ?> kg/m<sup>2</sup> (<?= $datasets[AppConst::EXAM_TYPE_BMI]->GetGradeTDescription(); ?>)
                                </div>
                            </div>
                            <div class="col-print-6">
                                <div class="col-print-6">
                                    <?php
                                    $value = "-";
                                    if ($datasets[AppConst::EXAM_TYPE_PULSE] != null) {
                                        $value = $datasets[AppConst::EXAM_TYPE_PULSE]->GetTestValue();
                                    }
                                    ?>
                                    ชีพจร <?= $value; ?> ครั้ง/นาที
                                </div>
                                <div class="col-print-6">
                                    <?php
                                    $value = "-";
                                    if ($datasets[AppConst::EXAM_TYPE_WAIST_CIRC] != null) {
                                        $value = $datasets[AppConst::EXAM_TYPE_WAIST_CIRC]->GetTestValue();
                                    }
                                    ?>
                                    รอบเอว <?= $value; ?> ซม. 
                                </div>
                            </div>
                        </div>
                        <div class="row-print">
                            <div class="col-print-6">
                                <div class="col-print-6">
                                    ความดันโลหิต
                                </div>
                                <div class="col-print-6">
                                    <?php
                                    $value = "-";
                                    if ($datasets[AppConst::EXAM_TYPE_BP_U] != null && $datasets[AppConst::EXAM_TYPE_BP_D] != null) {
                                        $value = $datasets[AppConst::EXAM_TYPE_BP_U]->GetTestValue() . " / " . $datasets[AppConst::EXAM_TYPE_BP_D]->GetTestValue();
                                    }
                                    ?>
                                    <?= $value; ?> มม.ปรอท
                                </div>
                            </div>
                            <div class="col-print-6">
                                
                            </div>
                        </div>
                    </div>
                    <div class="large-size">
                        <div class="row-print">
                            <div class="col-print-8">
                                <div class="col-print-6">
                                    หมายเลขประจำตัวผู้ทดสอบ
                                </div>
                                <div class="col-print-6">
                                    <?= $datasets[AppConst::PARAM_TESTER]->GetTesterID(); ?> 
                                </div>
                            </div>
                            <div class="col-print-4">
                                <div class="col-print-6">
                                    กลุ่มการทดสอบ
                                </div>
                                <div class="col-print-6">
                                    <?= $datasets[AppConst::PARAM_TESTER]->GetTesterGrpDesc(); ?>
                                </div>

                            </div>
                        </div>
                        <div class="row-print">
                            <div class="col-print-8">
                                <div class="col-print-6">
                                    ชื่อ-นามสกุล
                                </div>
                                <div class="col-print-6">
                                    <?= $datasets[AppConst::PARAM_TESTER]->GetNameTH(); ?> <?= $datasets[AppConst::PARAM_TESTER]->GetSurnameTH(); ?> 
                                </div>
                            </div>
                            <div class="col-print-4">
                                <div class="col-print-6">
                                    เพศ <?= ($datasets[AppConst::PARAM_TESTER]->GetSex() == "M" ? "ชาย":"หญิง"); ?>   
                                </div>
                                <div class="col-print-6">
                                </div>
                            </div>
                        </div>
                        <div class="row-print">
                            <div class="col-print-4">
                                การออกกำลังกาย 
                                <?php
                                $value = "-";
                                if ($datasets[AppConst::EXAM_TYPE_EXCERCISE] != null) {
                                    $value = $datasets[AppConst::EXAM_TYPE_EXCERCISE]->GetTestValue();
                                    if ($value == "0") {
                                        $value = "ไม่เคย";
                                    } else if ($value == "1") {
                                        $value = "1-2 ครั้ง ต่อสัปดาห์";
                                    } else if ($value == "2") {
                                        $value = "2-5 ครั้ง ต่อสัปดาห์";
                                    } else {
                                        $value = "-";
                                    }
                                }
                                echo $value;
                                ?>

                            </div>
                            <div class="col-print-8">
                                <div class="col-print-5">
                                    วัน/เดือน/ปี เกิด(พ.ศ.)
                                </div>
                                <div class="col-print-7">
                                    <?php
                                    if ($datasets[AppConst::PARAM_TESTER] != null) {
                                        $dob = explode("/", $datasets[AppConst::PARAM_TESTER]->GetDOB());
                                        //print_r($dob);
                                        $year = $dob[2] + 543;
                                        $birthday = $dob[0] . "/" . $dob[1] . "/" . $year;
                                    }
                                    echo $birthday;
                                    ?>
                                    (อายุ <?= $datasets[AppConst::PARAM_TESTER]->GetAge(); ?> ปี)
                                </div>

                            </div>
                        </div>
                        <div class="row-print">
                            <div class="col-print-6">
                                <div class="col-print-6">
                                    น้ำหนัก <?= $datasets[AppConst::PARAM_TESTER]->GetWeight(); ?> กก.
                                </div>
                                <div class="col-print-6">
                                    ส่วนสูง <?= $datasets[AppConst::PARAM_TESTER]->GetHeight(); ?> ซม.
                                </div>

                            </div>
                            <div class="col-print-6">
                                <div class="col-print-6">
                                    BMI 
                                </div>
                                <div class="col-print-6">
                                    <?= $datasets[AppConst::EXAM_TYPE_BMI]->GetTestValue(); ?> kg/m<sup>2</sup>
                                </div>

                            </div>
                        </div>
                        <div class="row-print">
                            <div class="col-print-6">
                                <div class="col-print-6">
                                    <?php
                                    $value = "-";
                                    if ($datasets[AppConst::EXAM_TYPE_OMRON_BODYFAT] != null) {
                                        $value = $datasets[AppConst::EXAM_TYPE_OMRON_BODYFAT]->GetTestValue();
                                    }
                                    ?>
                                    Body Fat <?= $value ?>% ()  
                                </div>
                                <div class="col-print-6">
                                    <?php
                                    $value = "-";
                                    if ($datasets[AppConst::EXAM_TYPE_SKIN_FOLD] != null) {
                                        $value = $datasets[AppConst::EXAM_TYPE_SKIN_FOLD]->GetTestValue();
                                    }
                                    ?>
                                    Skin Fold <?= $value; ?>% ()
                                </div>

                            </div>
                            <div class="col-print-6">
                                <div class="col-print-6">
                                    ความดันโลหิต
                                </div>
                                <div class="col-print-6">
                                    <?php
                                    $value = "-";
                                    if ($datasets[AppConst::EXAM_TYPE_BP_U] != null && $datasets[AppConst::EXAM_TYPE_BP_D] != null) {
                                        $value = $datasets[AppConst::EXAM_TYPE_BP_U]->GetTestValue() . " / " . $datasets[AppConst::EXAM_TYPE_BP_D]->GetTestValue();
                                    }
                                    ?>
                                    <?= $value; ?> มม.ปรอท
                                </div>

                            </div>
                        </div>
                        <div class="row-print">
                            <div class="col-print-6">
                                <div class="col-print-6">
                                    ชีพจร
                                </div>
                                <div class="col-print-6">
                                    <?php
                                    $value = "-";
                                    if ($datasets[AppConst::EXAM_TYPE_PULSE] != null) {
                                        $value = $datasets[AppConst::EXAM_TYPE_PULSE]->GetTestValue();
                                    }
                                    ?>
                                    <?= $value; ?> ครั้ง/นาที
                                </div>


                            </div>
                            <div class="col-print-6">
                                <div class="col-print-6">
                                    รอบเอว
                                </div>
                                <div class="col-print-6">
                                    <?php
                                    $value = "-";
                                    if ($datasets[AppConst::EXAM_TYPE_WAIST_CIRC] != null) {
                                        $value = $datasets[AppConst::EXAM_TYPE_WAIST_CIRC]->GetTestValue();
                                    }
                                    ?>
                                    <?= $value; ?> ซม. 
                                </div>

                            </div>
                        </div>
                    </div>


                    <hr>
                    <br>
                    <br>
                    <?php
                    $cnt = 1;
                    foreach ($categorylist as $category) {
                        if ($cnt == 1) {
                            $str = '<div class="row-print">';
                        } else {
                            $str = '';
                        }
                        //echo $category;
                        if(!isset($datasets[$category])){
                            continue;
                        }
                        foreach ($datasets[$category] as $dataset) {
                            $gradeid = $dataset->GetGradeID();
                            if (!isset($gradeid) || $gradeid == null || $gradeid == "" || $gradeid == "-") {
                                $gradeid = "1";
                            }
                            if ($cnt == 1) {
                                $str = '<div class="row-print">';
                            } else {
                                $str = '';
                            }

                            $str .= '<div class="col-print-6">';
                            $str .= '<div class="box-wrapper">';
                            $str .= '<div class="header">';
                            $str .= '<div class="header-text">' . $appconst->getExamGroupDesc($dataset->GetExamCat()) . '</div>';
                            //$str .= '<div class="header-text">' . $dataset->GetTDescription() . '</div>';
                            $str .= '</div>'; //Close Header
                            $str .= '<div class="content">';
                            $str .= '<img src="images/result_3/' . $dataset->GetExamID() . '.png">';
                            $str .= '<div class="star">';
                            $str .= '<img src="images/result_3/star_' . $gradeid . '.png">';
                            $str .= '</div>'; //Close Star
                            $str .= '</div>'; //Close Content
                            $str .= '<div class="score">';
                            $str .= $dataset->GetTestValue() . ' '.$dataset->GetUOM();
                            $str .= '</div>'; //Close Score
                            $str .= '</div>'; //Close box
                            $str .= '</div>'; //Close column.

                            if ($cnt >= 2) {
                                $str .= '</div>'; //Close row
                                $str .= '<br>'; //Close row
                                $cnt = 1;
                            }
                            else{
                                $cnt++;
                            }
                            
                            echo $str;
                        }
                        
                    }
                    ?>
                </div>
            </div>

        </body>
        </html>
        <?php
        $html = ob_get_contents();
        ob_clean();
        return $html;
    }

}
?>