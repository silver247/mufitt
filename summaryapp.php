<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <?php
    include 'page/header.php';
    //include ('./AppManager.php');
    $appObj = new AppManager();
    $ddlGrp = $appObj->RequestDDLGrpTest();
    $ddlExam = $appObj->GetDDLExamType();
    $value = "";
    $examlist = $appObj->GetDropdownSummaryExamlist($value);
    ?>    
    <script src="resources/js/calendar-th.js"></script>
    <script>
        $(function () {
            var dateFormat = "mm/dd/yy",
            from = $("#txtDateFrom")
            .datepicker({
                defaultDate: "+1w",
                changeYear: true,
                yearRange: "c-100:c+10",
                changeMonth: true,
                numberOfMonths: 3
            })
            .on("change", function () {
                to.datepicker("option", "minDate", getDate(this));
            }),
            to = $("#txtDateTo").datepicker({
                defaultDate: "+1w",
                changeYear: true,
                yearRange: "c-100:c+10",
                changeMonth: true,
                numberOfMonths: 3
            })
            .on("change", function () {
                from.datepicker("option", "maxDate", getDate(this));
            });

            function getDate(element) {
                var date;
                try {
                    date = $.datepicker.parseDate(dateFormat, element.value);
                } catch (error) {
                    date = null;
                }

                return date;
            }
            $("#txtDateFrom").datepicker("option", "dateFormat", "dd/mm/yy");
            $("#txtDateFrom").datepicker($.datepicker.regional[ "th" ]);
            $("#txtDateTo").datepicker("option", "dateFormat", "dd/mm/yy");
            $("#txtDateTo").datepicker($.datepicker.regional[ "th" ]);
        });


    </script>
    <tr>
        <td style="width: 1024px;height: 564px; vertical-align: top; background-color: #ffffff;text-align: center;border: 1px solid #990000;border-top: none;border-bottom: none;" >
            <form name="tform1" id="tform1" onsubmit="return false;">
                <table style="width: 100%" border="0" cellpadding="0" cellspacing="0" align="center">
                    <tr>
                        <td  class="fontscreen"  style="padding-top: 10px;text-align: left"><h3>รายงานผลการทดสอบ</h3></td>
                    </tr>
                    <tr>
                        <td  class="fontscreen"  style="padding-top: 10px;text-align: left">เลือกประเภทการทดสอบ</td>
                        <td  class="fontscreen"  style="padding-top: 10px;text-align: left">

                            <select name="ddlExam" id="ddlExam">
                                <?= $examlist; ?>
                            </select>
                        </td>
                        <td  class="fontscreen"  style="padding-top: 10px;text-align: left">กลุ่มการทดสอบ</td>
                        <td  class="fontscreen"  style="padding-top: 10px;text-align: left">

                            <select name="ddlGrpTest" id="ddlGrpTest">
                                <?= $ddlGrp; ?>
                            </select>
                        </td>

                    </tr>
                    <tr>
                        <td  class="fontscreen"  style="padding-top: 10px;text-align: left">จาก</td>
                        <td  class="fontscreen"  style="padding-top: 10px;text-align: left"><input type="text" name="txtDateFrom" id="txtDateFrom"></td>
                        <td  class="fontscreen"  style="padding-top: 10px;text-align: left"">ถึง</td>
                        <td  class="fontscreen"  style="padding-top: 10px;text-align: left"><input type="text" name="txtDateTo" id="txtDateTo"></td>

                    </tr>
                    <tr>
                        <td colspan="4" style="text-align: right;padding-top: 10px">
                            <input name="do" type="hidden" value="<?= AppConst::METHOD_QUERY_SUMMARY ?>"/>
                            <img src="images/btn/back.png" style="cursor: pointer;padding-left: 10px;" onclick="back()"/>
                            <img src="images/btn/view.png" style="cursor: pointer;padding-left: 10px;" onclick="submitSummary()"/>
                        </td>
                    </tr>
                    <tr class="result" style="display: none">
                        <td colspan="4" style="text-align: center;padding-top: 10px">
                            <h4>รายงานผลการทดสอบ <span id="tabledesc"></span></h4>
                        </td>
                    </tr>
                    <tr class="result" style="display: none">
                        <td colspan="4" style="text-align: center;padding-top: 10px">
                            <table id="tblResult" style="width: 90%; margin: 0px auto;"  border="1" cellpadding="0" cellspacing="0" align="center">

                            </table>
                        </td>
                    </tr>
                </table>
            </form>
        </td>
    </tr>
    <?php
    include 'page/footer.php';
    $successval = AppConst::STATUS_SUCCESS;
    $errorval = AppConst::STATUS_ERROR;
    ?>
    <script type="text/javascript">
        var defaultContentType = "application/x-www-form-urlencoded; charset=UTF-8";
        function back() {
            window.open("landing.php", "_top");
        }
        function submitSummary() {
            //alert("Start send login");
            var descTable = $("#ddlExam option:selected").text() + " ";
            try {
                jQuery.ajax({
                    url: "AppHttpRequest.php",
                    type: "POST",
                    contentType: defaultContentType,
                    data: $("#tform1").serialize(),
                    dataType: "json",
                    error: function (transport, status, errorThrown) {
                        console.log(transport.responseText);
                        alert("error");
                    },
                    success: function (data) {
                        //var datas = JSON.parse(data);
                        if (data.MSGID === "<?= AppConst::STATUS_SUCCESS; ?>") {
                            $(".result").show();
                            var summary = "";
                            var average = "";
                            var sum_min = "";
                            var sum_max = "";

                            $.each(data.ResponseDetail, function (i, detail) {
                                if (detail.INDICATOR === "<?= AppConst::SUMMARY_TOTAL; ?>") {
                                    summary = "<tr><td width='50%' style='background-color:#666666; color: white;' rowspan='3'>จำนวนผู้เข้ารับการทดสอบ</td><td>ชาย</td><td>" + detail.MALE + " คน</td></tr><tr><td>หญิง</td><td>" + detail.FEMALE + " คน</td></tr><tr><td>รวม</td><td>" + detail.TOTAL + "คน</td></tr>";
                                } else if (detail.INDICATOR === "<?= AppConst::SUMMARY_AVERAGE; ?>") {
                                    average = "<tr><td width='50%' style='background-color:#666666; color: white;' rowspan='3'>ค่าเฉลี่ย</td><td>ชาย</td><td>" + detail.MALE + " </td></tr><tr><td>หญิง</td><td>" + detail.FEMALE + " </td></tr><tr><td>รวม</td><td>" + detail.TOTAL + "</td></tr>";
                                } else if (detail.INDICATOR === "<?= AppConst::SUMMARY_MAX; ?>") {
                                    sum_max = "<tr><td width='50%' style='background-color:#666666; color: white;' rowspan='2'>ค่าสูงสุด</td><td>ชาย</td><td>" + detail.MALE + " </td></tr><tr><td>หญิง</td><td>" + detail.FEMALE + " </td></tr>";
                                } else if (detail.INDICATOR === "<?= AppConst::SUMMARY_MIN; ?>") {
                                    sum_min = "<tr><td width='50%' style='background-color:#666666; color: white;' rowspan='2'>ค่าต่ำสุด</td><td>ชาย</td><td>" + detail.MALE + " </td></tr><tr><td>หญิง</td><td>" + detail.FEMALE + " </td></tr>";
                                }

                                //alert("Mine is " + i + "|" + detail.MALE + "|" + detail.FEMALE);


                            });
                            if ($("#ddlGrpTest").val() != 0) {
                                descTable = descTable + " กลุ่มผู้ทดสอบ " + $("#ddlGrpTest option:selected").text();
                            }
                            $("#tabledesc").empty().append(descTable);
                            var str_rdy = summary.concat(average, sum_max, sum_min);
                            $("#tblResult").empty().append(str_rdy);

                        }
                        else{
                            var str_rdy = "<tr><td>ไม่พบข้อมูล</td></tr>";
                            $("#tblResult").empty().append(str_rdy);
                        }
                    }
                });
            } catch (ex) {
                alert(ex);
            }
        }
    </script>
</html>