<?php

include_once ('./DatabaseManager.php');
include_once ('./ExamObj.php');
include_once ('./ApplicationObj.php');
include_once ('./TesterHeaderInfo.php');
include_once ('./trace.php');
include_once ('./ResultSearchDetail.php');
include_once ('./SearchResultResponse.php');
include_once ('./SearchResultResponseHeader.php');
include_once ('./PrintResponseHeader.php');
include_once ('./ResponseHeader.php');
include_once ('./SummaryDetailResponse.php');
include_once ('./AppManager.php');
include_once ('service.php');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AppbaseManager
 *
 * @author puwakitk
 */
class AppbaseManager {

    //put your code here
    private $dbObj;

    function __construct() {
        $this->dbObj = new DatabaseManager();
    }

    function RequestInsertExamResult(ExamObj $data) {
        $con = $this->dbObj->Open();
        $table = AppConst::TABLE_EXAM_RESULT;
        $result = "";
        //$sql = "INSERT INTO `$table`  VALUES (?, ?, ?, ?) ";
        $sql = "INSERT INTO `$table`  VALUES ('".$data->GetAppID()."', '".$data->GetExamID()."', '".$data->GetSeqNo()."', '".$data->GetTestValue()."') ";
        $stmt = $con->prepare($sql);
        //$stmt->bind_param("ssid", $data->GetAppID(), $data->GetExamID(), $data->GetSeqNo(), $data->GetTestValue());
        if ($stmt->execute()) {
            $stmt->close();
            $result = AppConst::STATUS_SUCCESS;
        } else {
            $result = "From function RequestInsertExamResult " . htmlspecialchars($stmt->error);
            $stmt->close();
        }


        return $result;
    }

    function RequestInsertApplication(ApplicationObj $appObj) {
        if (!$this->CheckGroupIDisExist($appObj->GetGroupTest())) {
            $res = new ResponseHeader();
            $res = $this->CreateNewGroup($appObj->GetGroupTest());
            $grpid = $res->MSGDETAIL;
            if ($res->MSGID === AppConst::STATUS_SUCCESS) {
                $appObj->SetGroupTest($grpid);
            }
        }
        $con = $this->dbObj->Open();
        $table = AppConst::TABLE_APPLICATION;
        $sql = "INSERT INTO `$table`  VALUES ('".$appObj->GetApplicationID()."', '".$appObj->GetTesterID()."', CURDATE(), '".$appObj->GetGroupTest()."')";
        $stmt = $con->prepare($sql);
        //$stmt->bind_param("sss", $appObj->GetApplicationID(), $appObj->GetTesterID(), $appObj->GetGroupTest());
        if ($stmt->execute()) {
            $stmt->close();
            $result = AppConst::STATUS_SUCCESS;
        } else {
            $result = "From function RequestInsertApplication " . htmlspecialchars($stmt->error);
            $stmt->close();
        }
        return $result;
    }

    function RequestInsertMap(MapObject $obj) {
        $con = $this->dbObj->Open();
        $table = "";
        $table = AppConst::TABLE_MAP;
        $sql = "INSERT INTO `$table`  VALUES ('".$obj->GetAppID()."', '".$obj->GetEntryUser()."', '".$obj->GetStatus()."', CURDATE(), CURDATE(),'".$obj->GetTesterID()."', '".$obj->GetIsResearch()."')";
        $stmt = $con->prepare($sql);
        //$stmt->bind_param("ssssi", $obj->GetAppID(), $obj->GetEntryUser(), $obj->GetStatus(), $obj->GetTesterID(), $obj->GetIsResearch());
        if ($stmt->execute()) {
            $stmt->close();
            $result = AppConst::STATUS_SUCCESS;
        } else {
            $result = "From function RequestInsertMap " . htmlspecialchars($stmt->error);
            $stmt->close();
        }
        return $result;
    }

    function RequestInsertTester(TesterHeaderInfo $obj) {
        $con = $this->dbObj->Open();
        $table = AppConst::TABLE_TESTER;
        //$sql = "INSERT IGNORE INTO `$table`  VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        $sql = "INSERT INTO `$table` (`testerid`, `tname`, `tsurname`, `gender`, `birthday`, `phone`, `email`) "
                . " VALUES ('".$obj->GetTesterID()."'"
                . ", '".$obj->GetNameTH()."'"
                . ", '".$obj->GetSurnameTH()."'"
                . ", '".$obj->GetSex()."'"
                . ", '".$obj->GetDOB()."'"
                . ", '".$obj->GetPhone()."'"
                . ", '".$obj->GetEmail()."') "
                . " on duplicate key update tname = '" . $obj->GetNameTH() . "',"
                . " tsurname = '" . $obj->GetSurnameTH() . "', gender = '" . $obj->GetSex() . "', "
                . " birthday = '" . $obj->GetDOB() . "', phone = '" . $obj->GetPhone() . "', "
                . " email = '" . $obj->GetEmail() . "' ";
        $stmt = $con->prepare($sql);
        //$stmt->bind_param("sssssss", $obj->GetTesterID(), $obj->GetNameTH(), $obj->GetSurnameTH(), $obj->GetSex(), $obj->GetDOB(), $obj->GetPhone(), $obj->GetEmail());
        if ($stmt->execute()) {
            $stmt->close();
            $result = AppConst::STATUS_SUCCESS;
        } else {
            $result = "From function RequestInsertTester " . htmlspecialchars($stmt->error);
            //$result = AppConst::STATUS_ERROR;
            $stmt->close();
        }

        return $result;
    }

    function RequestUpdateMapTable(MapObject $obj) {
        $con = $this->dbObj->Open();
        $table = "";
        $table = AppConst::TABLE_MAP;
        $sql = "Update `$table`  Set status = '".$obj->GetAppID()."' , completedate = CURDATE() where applicationid = '".$obj->GetStatus()."'";
        $stmt = $con->prepare($sql);
        //$stmt->bind_param("ss", $obj->GetAppID(), $obj->GetStatus());
        if ($stmt->execute()) {
            $stmt->close();
            $result = AppConst::STATUS_SUCCESS;
        } else {
            $result = "From function RequestUpdateMapTable " . htmlspecialchars($stmt->error);
            $stmt->close();
        }
        return $result;
    }

    function RequestUpdateApplication(ApplicationObj $appObj) {
        $con = $this->dbObj->Open();
        $table = AppConst::TABLE_APPLICATION;
        $sql = "Update `$table`  Set completedate = CURDATE() where applicationid = '".$appObj->GetApplicationID()."'";
        $stmt = $con->prepare($sql);
        //$stmt->bind_param("s", $appObj->GetApplicationID());
        if ($stmt->execute()) {
            $stmt->close();
            $result = AppConst::STATUS_SUCCESS;
        } else {
            $result = "From function RequestUpdateApplication " . htmlspecialchars($stmt->error);
            $stmt->close();
        }
        return $result;
    }

    function GetOxygenAstrand($hr, $load, $sex) {
        $con = $this->dbObj->Open();
        $table = AppConst::TABLE_AST_OXY;
        $sql = "select oxygen_val from `$table` where gender = '" . $sex . "' and pulserate = '" . $hr . "' and load_val = '" . $load . "'";
        //echo $sql;
        $stmt = $con->prepare($sql);
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($value);
            while ($stmt->fetch()) {
                return $value;
            }
        } else {
            $result = htmlspecialchars($stmt->error);
            $stmt->close();
            return $result;
        }
    }

    function GetMultiplyAstrand($age) {
        $con = $this->dbObj->Open();
        $table = AppConst::TABLE_AST_AGEF;
        $sql = "select factor from `$table` where age = '" . $age . "'";
        $stmt = $con->prepare($sql);
        //$stmt->bind_param("i", $age);
        //echo $sql;
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($value);
            while ($stmt->fetch()) {
                return $value;
            }
        } else {
            $result = htmlspecialchars($stmt->error);
            $stmt->close();
        }
        return $result;
    }

    function GetAstrandResult($age, $sex, $value) {
        $con = $this->dbObj->Open();
        $table = AppConst::TABLE_ASTRAND;
        $sql = "select result from `$table` where age = '" . $age . "' and value = '" . $value . "'";
        $stmt = $con->prepare($sql);
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($value);
            while ($stmt->fetch()) {
                return $value;
            }
        } else {
            $result = htmlspecialchars($stmt->error);
            $stmt->close();
        }
        return $result;
    }

    function CheckAuthentication($user, $pass) {
        $res = new ResponseHeader();
        $con = $this->dbObj->Open();
        $sql = "select userid,loginfail from user where username = '" . $user . "' and password = md5('" . $pass . "') ";
        $stmt = $con->prepare($sql);
        //echo $sql;
        //$stmt->bind_param("ss", $user, md5($pass));
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($userID, $loginFail);
            if ($stmt->num_rows <= 0 || $stmt->num_rows === null) {
                $res->MSGID = AppConst::STATUS_ERROR;
            } else {
                $res->MSGID = AppConst::STATUS_SUCCESS;
                while ($stmt->fetch()) {
                    $res->MSGDETAIL = $userID;
                    if ($loginFail === "" || $loginFail === null || $loginFail === 1) {
                        $res->MSGDETAIL2 = 1;
                    } else {
                        $res->MSGDETAIL2 = $loginFail;
                    }
                }
            }
        } else {
            $res->MSGID = AppConst::STATUS_SQL_ERROR;
            //$result = htmlspecialchars($stmt->error);
            $stmt->close();
        }
        return $res;
    }

    function CheckDuplicateLogin($userid) {
        $res = new ResponseHeader();
        $con = $this->dbObj->Open();
        $sql = "select sessionid from userlogging where userid = ? and delflag = 0";
        $stmt = $con->prepare($sql);
        $stmt->bind_param("s", $userid);
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($sessionID);
            if ($stmt->num_rows <= 0 || $stmt->num_rows === null) {
                $res->MSGID = AppConst::STATUS_SUCCESS;
            } else {
                $res->MSGID = AppConst::STATUS_ERROR;
                if ($stmt->fetch()) {
                    $res->MSGDETAIL = $sessionID;
                }
            }
            $stmt->close();
        } else {
            $res->MSGID = AppConst::STATUS_SQL_ERROR;
            $stmt->close();
        }
        return $res;
    }

    function CheckUserIDBySession($session) {
        $res = new ResponseHeader();
        $con = $this->dbObj->Open();
        $sql = "select userid from userlogging where session = ? and delflag = 0";
        $stmt = $con->prepare($sql);
        $stmt->bind_param("s", $session);
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($userID);
            if ($stmt->num_rows <= 0 || $stmt->num_rows === null) {
                $res->MSGID = AppConst::STATUS_ERROR;
            } else {
                $res->MSGID = AppConst::STATUS_SUCCESS;
                while ($stmt->fetch()) {
                    $res->MSGDETAIL = $userID;
                }
            }
            $stmt->close();
        } else {
            $res->MSGID = AppConst::STATUS_SQL_ERROR;
            $stmt->close();
        }
        return $res;
    }

    function AUTH($user, $pass) {
        $res = new ResponseHeader();
        $con = $this->dbObj->Open();
        $sql = "select userid from user join userlogging as ul on user.userid = ul.userid where username = '" . $user . "' and password = md5('" . $pass . "') and ul.delflag = 0";
        $stmt = $con->prepare($sql);
        //$stmt->bind_param("ss", $user, md5($pass));
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($userID);
            if ($stmt->num_rows <= 0 || $stmt->num_rows === null) {
                $res->MSGID = AppConst::STATUS_ERROR;
            } else {
                while ($stmt->fetch()) {
                    $res->MSGDETAIL = $userID;
                }
                $res->MSGID = AppConst::STATUS_SUCCESS;
            }
        } else {
            $res->MSGID = AppConst::STATUS_SQL_ERROR;
            //$result = htmlspecialchars($stmt->error);
            $stmt->close();
        }
        return $res;
    }

    function InsertUserLogin($userid, $session, $ip, $lang) {
        $result = new ResponseHeader();
        $con = $this->dbObj->Open();
        $table = AppConst::TABLE_USER_LOGGING;
        $sql = "insert into `$table` values (?,?,?,0,?)";
        $stmt = $con->prepare($sql);
        $stmt->bind_param("ssss", $userid, $session, $ip, $lang);
        if ($stmt->execute()) {
            $stmt->close();
            $result->MSGID = AppConst::STATUS_SUCCESS;
        } else {
            $result->MSGDETAIL = "From function InsertUserLogin " . htmlspecialchars($stmt->error);
            $result->MSGID = AppConst::STATUS_ERROR;
            $stmt->close();
        }
        return $result;
    }

    function Logout($userid, $session) {
        $result = new ResponseHeader();
        $con = $this->dbObj->Open();
        $table = AppConst::TABLE_USER_LOGGING;
        $sql = "update `$table` set delflag = 1 where userid = ? and sessionid = ?";
        $stmt = $con->prepare($sql);
        $stmt->bind_param("ss", $userid, $session);
        if ($stmt->execute()) {
            $stmt->close();
            $result->MSGID = AppConst::STATUS_SUCCESS;
        } else {
            $result->MSGDETAIL = "From function Logout " . htmlspecialchars($stmt->error);
            $result->MSGID = AppConst::STATUS_ERROR;
            $stmt->close();
        }
        return $result;
    }

    function RequestForDDLGrpTest() {
        $con = $this->dbObj->Open();
        $table = AppConst::TABLE_APPLICATION;
        $res = "";
        $sql = "select groupid,description from groups "; //Edit For Imgration Phase2
        $stmt = $con->prepare($sql);
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($value, $desc);

            while ($stmt->fetch()) {
                $res .= "<option value = '" . $value . "'>" . $desc . "</option>";
            }
        } else {
            $res .= "<option value = '" . AppConst::STATUS_SQL_ERROR . "'>" . htmlspecialchars($stmt->error) . "</option>";
            $stmt->close();
        }
        return $res;
    }

    function GetSearchResultTable($dt, $grpTest, $testerID, $name, $surname, $action) {
        $con = $this->dbObj->Open();
        $whereCondition = "";
        $responses = new SearchResultResponseHeader();

        if ($dt != "") {
            $date = str_replace('/', '-', $dt);
            $newdt = date("Y-m-d", strtotime($date));
            $whereCondition .= " where application.completedate = '" . $newdt . "' ";
        }

        if ($grpTest != "" && $grpTest != "0") {
            if ($whereCondition != "") {
                $whereCondition .= " and groups.groupid = '" . $grpTest . "' "; //Edit For phase 2 migration
            } else {
                $whereCondition .= " where groups.groupid = '" . $grpTest . "' ";
            }
        }
        if ($testerID != "") {
            if ($whereCondition != "") {
                $whereCondition .= " and tester.testerID = '" . $testerID . "' ";
            } else {
                $whereCondition .= " where tester.testerID = '" . $testerID . "' ";
            }
        }

        if ($name != "") {
            if ($whereCondition != "") {
                $whereCondition .= " and tester.tname like '%" . $name . "%' ";
            } else {
                $whereCondition .= " where tester.tname like '%" . $name . "%' ";
            }
        }

        if ($surname != "") {
            if ($whereCondition != "") {
                $whereCondition .= " and tester.tsurname like '%" . $surname . "%' ";
            } else {
                $whereCondition .= " where tester.tsurname like '%" . $surname . "%' ";
            }
        }

        $sql = "select map.applicationid,tname,tsurname, gender, applydate, groups.description ,tester.testerid, email " //Phase 2 Add column email
                . " from tester "
                . " join map on tester.testerid = map.testerid "
                . " join application on map.applicationid = application.applicationid "
                . " join groups on application.groupid = groups.groupid "
                . " ";

        $sql = $sql . $whereCondition;
        $stmt = $con->prepare($sql);
        //echo $sql;
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($appID, $name, $surname, $gender, $applydate, $testerGrp, $tid, $mail);
            if ($stmt->num_rows > 0) {
                $arr = array();
                while ($stmt->fetch()) {
                    $response = new SearchResultResponse();
                    $response->UID = $appID;
                    //$response->LINK = "newUrl.php?appID=" . $appID;
                    $response->NAME = $name;
                    $response->SURNAME = $surname;
                    $response->GENDER = $gender;
                    $response->ACTION = $action;
                    $response->APPLYDATE = $this->ConvertYMD($applydate);
                    $response->GRPTEST = $testerGrp;
                    $response->TESTERID = $tid;
                    if (!in_array($tid, $arr)) {
                        $arr[] = $tid;
                    }
                    if ($mail == null || $mail == "" || $mail == 'null') {
                        $mail = '-';
                    } else {
                        $response->TESTER_EMAIL = $mail;
                    }
                    $responses->DETAILS[] = $response;
                }
                $grpmail = $this->GetEmailListByTesterID($arr);
                $responses->TESTER_GRPMAIL = $grpmail;
                $responses->MSGID = AppConst::STATUS_SUCCESS;
            } else {
                $responses->MSGID = AppConst::STATUS_ERROR;
                $responses->MSGDETAIL = htmlspecialchars($stmt->error);
            }
        } else {
            $responses->MSGID = AppConst::STATUS_SQL_ERROR;
            $responses->MSGDETAIL = "From function GetSearchResultTable " . htmlspecialchars($stmt->error);
            $stmt->close();
        }
        //print_r($responses);
        return $responses;
    }

    function GetLatestExamByTesterID($testerID, $times) {
        $con = $this->dbObj->Open();
        $resObj = new ResponseHeader();
        $sql = "select applicationid "
                . " from map "
                . " where testerid = '" . $testerID . "' "
                . " order by applydate desc "
                . " LIMIT " . $times . " ";
        //echo $sql;
        $stmt = $con->prepare($sql);
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($appID);
            $row_cnt = $stmt->num_rows;
            $cnt = $times;
            if ($row_cnt < $times) {
                $cnt = $row_cnt;
            }

            while ($stmt->fetch()) {

                $resObj->ResponseDetail[$cnt] = $appID;
                $cnt--;
            }
            $resObj->MSGID = AppConst::STATUS_SUCCESS;
            $stmt->close();
        } else {
            $resObj->MSGID = AppConst::STATUS_SQL_ERROR;
            $resObj->MSGDETAIL = "From function GetLatestExamByTesterID " . htmlspecialchars($stmt->error);
            $stmt->close();
        }
        return $resObj;
    }

    function GetTesterDetailByAppID($appID) {
        $con = $this->dbObj->Open();
        $testerObj = new TesterHeaderInfo();
        $this->GetTesterWeightAndHeight($appID, $testerObj);
        $sql = "select tester.testerid, tname, tsurname, gender, birthday, phone, groups.groupid, groups.description , TIMESTAMPDIFF( YEAR, birthday, CURDATE( ) ) as age, email, map.isResearch "
                . " from tester "
                . " join map on tester.testerid = map.testerid "
                . " join application on map.applicationid = application.applicationid "
                . " join groups on application.groupid = groups.groupid "
                . " where map.applicationID = '" . $appID . "' ";
        //echo $sql;
        $stmt = $con->prepare($sql);
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($testerID, $tname, $tsurname, $gender, $bday, $phone, $testGrp, $testGrpDesc, $age, $email, $isResearch);

            while ($stmt->fetch()) {
                $testerObj->SetTesterID($testerID);
                $testerObj->SetNameTH($tname);
                $testerObj->SetSurnameTH($tsurname);
                $testerObj->SetSurnameTH($tsurname);
                //$testerObj->SetSurnameEN($esurname);
                //$testerObj->SetNameEN($ename);
                $testerObj->SetSex($gender);
                $dob = explode("-", $bday);
                $testerObj->SetDOB($dob[2] . "/" . $dob[1] . "/" . $dob[0]);
                $testerObj->SetPhone($phone);
                $testerObj->SetTesterGrp($testGrp);
                $testerObj->SetAge($age);
                $testerObj->SetEmail($email);
                $testerObj->SetTesterGrpDesc($testGrpDesc);
                $testerObj->SetIsResearch($isResearch);
            }
            $testerObj->SetMessageID(AppConst::STATUS_SUCCESS);
            //print_r($testerObj);
            $stmt->close();
        } else {
            $testerObj->SetMessageID(AppConst::STATUS_SQL_ERROR);
            $testerObj->SetMessageDetail("From function GetTesterDetailByAppID " . htmlspecialchars($stmt->error));
            $stmt->close();
        }
        return $testerObj;
    }

    function GetTesterWeightAndHeight($appID, TesterHeaderInfo $testerObj) {
        $con = $this->dbObj->Open();
        $sql = "select value,examid "
                . " from mapexam"
                . " where applicationid = '" . $appID . "' "
                . " and (examid = '" . AppConst::EXAM_TYPE_HEIGHT . "' or examid = '" . AppConst::EXAM_TYPE_WEIGHT . "')";
        $stmt = $con->prepare($sql);
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($value, $examid);
            while ($stmt->fetch()) {
                if ($examid === AppConst::EXAM_TYPE_HEIGHT) {
                    $testerObj->SetHeight($value);
                } else if ($examid === AppConst::EXAM_TYPE_WEIGHT) {
                    $testerObj->SetWeight($value);
                }
            }
            $stmt->close();
        }
        return $testerObj;
    }

    function GetScoreGrade(ExamObj $examObj, TesterHeaderInfo $testerObj, $age) {
        $con = $this->dbObj->Open();
        $masterTable = AppConst::TABLE_MASTER_DATA;
        $gradeTable = AppConst::TABLE_GRADE;
        if ($examObj->GetExamID() === AppConst::EXAM_TYPE_OMRON) {
            //Omron will use same score as skin fold
            $where = "where `$masterTable`.examid = '" . AppConst::EXAM_TYPE_SKIN_FOLD . "' and ";
        } else {
            $where = "where `$masterTable`.examid = '" . $examObj->GetExamID() . "' and ";
        }

        $sql = "select `$gradeTable`.grade, `$gradeTable`.tdescription, `$gradeTable`.edescription "
                . " from `$gradeTable` "
                . " join `$masterTable` on `$gradeTable`.grade = `$masterTable`.grade "
                . $where
                . " `$masterTable`.gender = '" . $testerObj->GetSex() . "' and "
                . " `$masterTable`.age_from <= '" . $age . "' and "
                . " `$masterTable`.age_to >= '" . $age . "' and "
                . " `$masterTable`.min_val <= '" . $examObj->GetTestValue() . "' and "
                . " `$masterTable`.max_val >= '" . $examObj->GetTestValue() . "' ";

        $stmt = $con->prepare($sql);

        //echo "<br>" . $sql;
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($grade, $tdesc, $edesc);
            if ($stmt->num_rows > 0) {
                while ($stmt->fetch()) {
                    $examObj->SetGradeID($grade);
                    $examObj->SetGradeTDescription($tdesc);
                    $examObj->SetGradeEDescription($edesc);
                }
                $examObj->SetMessageID(AppConst::STATUS_SUCCESS);
            } else {
                $examObj->SetMessageID(AppConst::STATUS_ERROR);
                $examObj->SetMessageDetail("From function GetScoreGrade " . htmlspecialchars($stmt->error));
            }
        } else {
            $examObj->SetMessageID(AppConst::STATUS_SQL_ERROR);
            $examObj->SetMessageDetail("From function GetScoreGrade " . htmlspecialchars($stmt->error));
        }
        $stmt->close();
        return $examObj;
    }

    function GetScoreGradeNew($appID, $examid, ExamObj $exam) {
        $response = new ResponseHeader();
        $sql = "SELECT a.value, b.grade,e.tdescription,e.edescription "
                . " FROM mapexam a "
                . " JOIN map d ON d.applicationid = a.applicationid "
                . " JOIN tester c ON c.testerid = d.testerid "
                . " LEFT JOIN MasterData b ON a.examid = b.examid "
                . " AND b.age_from <= TIMESTAMPDIFF( YEAR, c.birthday, CURDATE( ) )  "
                . " AND b.age_to >= TIMESTAMPDIFF( YEAR, c.birthday, CURDATE( ) )  "
                . " AND b.min_val <= a.value "
                . " AND b.max_val >= a.value "
                . " AND b.gender = c.gender "
                . " LEFT JOIN grade e on e.grade = b.grade "
                . " WHERE a.applicationid =  '" . $appID . "' "
                . " AND a.examid =  '" . $examid . "' "
                . " AND a.applicationid = d.applicationid "
                . " AND d.testerid = c.testerid";
        $con = $this->dbObj->Open();
        //echo "<br><br>" . $sql . "<br><br>";
        $stmt = $con->prepare($sql);
        //$stmt->bind_param("ss", $appID, $examid);
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($value, $grade, $tdesc, $edesc);
            if ($stmt->num_rows > 0) {
                while ($stmt->fetch()) {
                    $exam->SetTestValue($value);
                    if ($grade === "" || $grade === NULL) {
                        $exam->SetGradeID("-");
                    } else {
                        $exam->SetGradeID($grade);
                    }
                    if ($tdesc === "" || $tdesc === NULL) {
                        $exam->SetGradeTDescription("-");
                    } else {
                        $exam->SetGradeTDescription($tdesc);
                    }
                    if ($edesc === "" || $edesc === NULL) {
                        $exam->SetGradeEDescription("-");
                    } else {
                        $exam->SetGradeEDescription($edesc);
                    }
                }
            } else {
                $exam->SetTestValue($value);
                $exam->SetGradeID($grade);
                $exam->SetGradeTDescription($tdesc);
                $exam->SetGradeEDescription($edesc);
            }
        } else {
            $exam->SetGradeID(AppConst::STATUS_SQL_ERROR);
            $exam->SetGradeTDescription("From function GetGraphForPrintByAppID " . htmlspecialchars($stmt->error));
        }
        //echo '<pre>'; print_r($exam); echo '</pre>';
        //print_r($response);
        $stmt->close();
        return $exam;
    }

    function RequestForPrintByAppIDNew($appID) {
        $response = new ResponseHeader();
        $condition = "";
        //$condition = " and examid in (select examid from filter_table where active = 2) ";
        $exams = $this->GetMapExamByAppID($appID, $condition); //ได้ทุกการทดสอบใน app id นั้นมา
        $calculateExam = $this->GetExamCalculateList();
        $tester = new TesterHeaderInfo();
        $tester = $this->GetTesterDetailByAppID($appID);
        $bmi = $this->CalculateBMI($appID, $tester);
        //$omron = $this->CalCulateOmron($appID, AppConst::EXAM_TYPE_OMRON_WBODYFAT,  $tester);
        $omron_vis = $this->CalCulateOmron($appID, AppConst::EXAM_TYPE_OMRON_VISCERAL, $tester);
        $omron_wf = $this->CalCulateOmron($appID, AppConst::EXAM_TYPE_OMRON_WBODYFAT, $tester);
        $omron_wm = $this->CalCulateOmron($appID, AppConst::EXAM_TYPE_OMRON_WBODYMUSCLE, $tester);
        $omron_wbft = $this->CalCulateOmron($appID, AppConst::EXAM_TYPE_OMRON_WBF_TRUNK, $tester);
        $omron_wbfa = $this->CalCulateOmron($appID, AppConst::EXAM_TYPE_OMRON_WBF_ARM, $tester);
        $omron_wbfl = $this->CalCulateOmron($appID, AppConst::EXAM_TYPE_OMRON_WBF_LEG, $tester);
        $omron_wbmt = $this->CalCulateOmron($appID, AppConst::EXAM_TYPE_OMRON_WBM_TRUNK, $tester);
        $omron_wbma = $this->CalCulateOmron($appID, AppConst::EXAM_TYPE_OMRON_WBM_ARM, $tester);
        $omron_wbml = $this->CalCulateOmron($appID, AppConst::EXAM_TYPE_OMRON_WBM_LEG, $tester);
        $skinfold = $this->CalCulateGradeSkinFold($appID, $tester);
        $handGrip = $this->CalculateHandGrip($appID, $tester);
        $astrand = $this->CalCulateAstrand($appID, $tester);
        $multiStage = $this->CalculateMultiStage($appID, $tester);
        $one_leg_L = $this->CalculateOneLegBalance($appID, AppConst::EXAM_TYPE_ONE_LEG_BAL_L, $tester);
        $one_leg_R = $this->CalculateOneLegBalance($appID, AppConst::EXAM_TYPE_ONE_LEG_BAL_R, $tester);
        $legstrength = $this->CalculateLegStrength($appID, $tester);
        $starlist = $this->CalculateStarExcursion($appID, $tester);
        $backscratch = $this->CalculateBackScratch($appID, AppConst::EXAM_TYPE_BACK_STRACH, $tester);
        $SixminsWalk = $this->Calculate6MinsWalkTest($appID, $tester);
        /*
         * Disabled since phase 2 no more graph
         */
        /* if ($astrand !== null) {
          $astrand->SetExamGraph($this->GetCalculatedExamGraph($appID, AppConst::EXAM_TYPE_ASTRAND));
          }
          if ($omron !== null) {
          $omron->SetExamGraph($this->GetCalculatedExamGraph($appID, AppConst::EXAM_TYPE_OMRON));
          }
          if ($skinfold !== null) {
          $skinfold->SetExamGraph($this->GetCalculatedExamGraph($appID, AppConst::EXAM_TYPE_SKIN_FOLD));
          }
          if ($handGrip !== null) {
          $handGrip->SetExamGraph($this->GetCalculatedExamGraph($appID, AppConst::EXAM_TYPE_HANDGRIP));
          }
          if ($multiStage !== null) {
          $multiStage->SetExamGraph($this->GetCalculatedExamGraph($appID, AppConst::EXAM_TYPE_MULTISTAGE));
          }
          if ($one_leg_L !== null) {
          $one_leg_L->SetExamGraph($this->GetCalculatedExamGraph($appID, AppConst::EXAM_TYPE_ONE_LEG_BAL_L));
          }
          if ($one_leg_R !== null) {
          $one_leg_R->SetExamGraph($this->GetCalculatedExamGraph($appID, AppConst::EXAM_TYPE_ONE_LEG_BAL_R));
          }
          if ($legstrength !== null) {
          $legstrength->SetExamGraph($this->GetCalculatedExamGraph($appID, AppConst::EXAM_TYPE_LEG_STRENGTH));
          } */


        if ($omron_vis !== null) {
            $response->ResponseDetail[] = $omron_vis;
        }
        if ($omron_wf !== null) {
            $response->ResponseDetail[] = $omron_wf;
        }
        if ($omron_wm !== null) {
            $response->ResponseDetail[] = $omron_wm;
        }
        if ($omron_wbft !== null) {
            $response->ResponseDetail[] = $omron_wbft;
        }
        if ($omron_wbfa !== null) {
            $response->ResponseDetail[] = $omron_wbfa;
        }
        if ($omron_wbfl !== null) {
            $response->ResponseDetail[] = $omron_wbfl;
        }
        if ($omron_wbmt !== null) {
            $response->ResponseDetail[] = $omron_wbmt;
        }
        if ($omron_wbma !== null) {
            $response->ResponseDetail[] = $omron_wbma;
        }
        if ($omron_wbml !== null) {
            $response->ResponseDetail[] = $omron_wbml;
        }
        //print_r($skinfold);
        if ($skinfold !== null) {
            $response->ResponseDetail[] = $skinfold;
        }
        //$response->ResponseDetail[] = $handGrip;
        if ($handGrip !== null) {
            $response->ResponseDetail[] = $handGrip;
        }
        if ($astrand !== null) {
            $response->ResponseDetail[] = $astrand;
        }
        if ($bmi !== null) {
            $response->ResponseDetail[] = $bmi;
        }

        if ($multiStage !== null) {
            $response->ResponseDetail[] = $multiStage;
        }

        if ($one_leg_L !== null) {
            $response->ResponseDetail[] = $one_leg_L;
        }

        if ($one_leg_R !== null) {
            $response->ResponseDetail[] = $one_leg_R;
        }

        if ($legstrength !== null) {
            $response->ResponseDetail[] = $legstrength;
        }

        if ($starlist != null || !empty($starlist)) {
            foreach ($starlist as $key => $star) {
                $response->ResponseDetail[] = $star;
            }
        }

        if ($backscratch !== null) {
            $response->ResponseDetail[] = $backscratch;
        }

        if ($SixminsWalk !== null) {
            $response->ResponseDetail[] = $SixminsWalk;
        }

        $response->ResponseDetail[AppConst::PARAM_TESTER] = $tester;

        foreach ($exams as $exam) {
            //echo "exam id = ".$exam['examid'];
            if (in_array($exam["examid"], $calculateExam)) {
                continue;
                //echo "<br> in calculate ".$exam["examid"]."<br>";
            } else if (stripos($exam["examid"], 'type23') !== FALSE) {
                //echo "exam id = ".$exam['examid'];
                continue;
                //echo "<br> in calculate ".$exam["examid"]."<br>";
            } else {
                //echo "<br>not in calculate : examid = ".$exam["examid"]."";
                $examObj = new ExamObj();
                $examObj->SetAppID($appID);
                $examObj->SetSeqNo($exam["seqno"]);
                $examObj->SetExamID($exam["examid"]);
                $examObj->SetExamCat($exam["category"]);
                $examObj->SetExamPriority($exam["priority"]);
                $examObj = $this->GetScoreGradeNew($appID, $exam["examid"], $examObj);
                $res = $this->GetExamDescriptionByExamID($exam["examid"]);
                if ($res->MSGID === AppConst::STATUS_SUCCESS) {
                    $examObj->SetEDescription($res->MSGDETAIL2);
                    $examObj->SetTDescription($res->MSGDETAIL);
                    $examObj->SetUOM($res->MSGDETAIL3);
                }
                //$examObj = $this->GetGraphForPrintByAppID($appID, $exam["examid"], $examObj);
                //$examObj->SetExamGraph($this->GetCalculatedExamGraph($appID, $exam["examid"]));
                $examObj = $this->GetPreviousTestScore($tester->GetTesterID(), $examObj, $appID);
                $response->ResponseDetail[] = $examObj;
                $response->MSGDETAIL3 = $exam['applydate'];
            }
        }
        $response->MSGID = AppConst::STATUS_SUCCESS;
        /* echo '<pre>';
          print_r($response);
          echo '</pre>'; */
        return $response;
    }

    function GetExamCalculateList() {
        $con = $this->dbObj->Open();
        $arr = array();
        $sql = "select examid from exam where calculate = 1";
        $stmt = $con->prepare($sql);
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($value);
            while ($stmt->fetch()) {
                $arr[] = $value;
            }
        } else {
            $arr[] = AppConst::STATUS_SQL_ERROR;
        }
        $stmt->close();
        return $arr;
    }

    function RequestForPrintByAppID($appID, $do) {
        $appManager = new AppManager();
        $response = new ResponseHeader();
        //$tester = new TesterHeaderInfo();
        //$tester = $this->GetTesterDetailByAppID($appID);
        //$age = $appManager->CalCulateAge($tester->GetDOB());
        if ($do === AppConst::PARAM_PRINT_APP) {
            $condition = " and mapexam.examid in (select examid from filter_table where active = 2) ";
        } else {
            $condition = "";
        }
        //echo "<br><br>".$do."<br>".$condition."<br>";
        //$exams = $this->GetMapExamByAppID($appID, $condition);
        //$lists = $this->GetCompareChartList();
        $exams = $this->GetMapExamByAppID($appID, $condition); //ได้ทุกการทดสอบใน app id นั้นมา
        $calculateExam = $this->GetExamCalculateList();
        $tester = new TesterHeaderInfo();
        $tester = $this->GetTesterDetailByAppID($appID);
        $bmi = $this->CalculateBMI($appID, $tester);
        $omron_vis = $this->CalCulateOmron($appID, AppConst::EXAM_TYPE_OMRON_VISCERAL, $tester);
        $omron_wf = $this->CalCulateOmron($appID, AppConst::EXAM_TYPE_OMRON_WBODYFAT, $tester);
        $omron_wm = $this->CalCulateOmron($appID, AppConst::EXAM_TYPE_OMRON_WBODYMUSCLE, $tester);
        $skinfold = $this->CalCulateGradeSkinFold($appID, $tester);
        $handGrip = $this->CalculateHandGrip($appID, $tester);
        $astrand = $this->CalCulateAstrand($appID, $tester);
        $multiStage = $this->CalculateMultiStage($appID, $tester);
        $one_leg_L = $this->CalculateOneLegBalance($appID, AppConst::EXAM_TYPE_ONE_LEG_BAL_L, $tester);
        $one_leg_R = $this->CalculateOneLegBalance($appID, AppConst::EXAM_TYPE_ONE_LEG_BAL_R, $tester);
        $legstrength = $this->CalculateLegStrength($appID, $tester);
        $starlist = $this->CalculateStarExcursion($appID, $tester);
        $backscratch = $this->CalculateBackScratch($appID, AppConst::EXAM_TYPE_BACK_STRACH, $tester);
        $SixminsWalk = $this->Calculate6MinsWalkTest($appID, $tester);

        if ($omron_vis !== null) {
            $response->ResponseDetail[] = $omron_vis;
        }
        if ($omron_wf !== null) {
            $response->ResponseDetail[] = $omron_wf;
        }
        if ($omron_wm !== null) {
            $response->ResponseDetail[] = $omron_wm;
        }
        //print_r($skinfold);
        if ($skinfold !== null) {
            $response->ResponseDetail[] = $skinfold;
        }
        //$response->ResponseDetail[] = $handGrip;
        if ($handGrip !== null) {
            $response->ResponseDetail[] = $handGrip;
        }
        if ($astrand !== null) {
            $response->ResponseDetail[] = $astrand;
        }
        if ($bmi !== null) {
            $response->ResponseDetail[] = $bmi;
        }

        if ($multiStage !== null) {
            $response->ResponseDetail[] = $multiStage;
        }

        if ($one_leg_L !== null) {
            $response->ResponseDetail[] = $one_leg_L;
        }

        if ($one_leg_R !== null) {
            $response->ResponseDetail[] = $one_leg_R;
        }

        if ($legstrength !== null) {
            $response->ResponseDetail[] = $legstrength;
        }

        if ($starlist != null || !empty($starlist)) {
            foreach ($starlist as $key => $star) {
                $response->ResponseDetail[] = $star;
            }
        }

        if ($backscratch !== null) {
            $response->ResponseDetail[] = $backscratch;
        }

        if ($SixminsWalk !== null) {
            $response->ResponseDetail[] = $SixminsWalk;
        }

        $response->ResponseDetail[AppConst::PARAM_TESTER] = $tester;

        /* if ($exams[0] === AppConst::STATUS_SQL_ERROR) {
          $response->MSGID = AppConst::STATUS_ERROR;
          $response->MSGDETAIL = $exams[1];
          } else {
          foreach ($exams as $exam) {
          $len = strlen(substr($exam["examid"], 4));
          if ($len === 4) {
          //echo "<br>str len = ".strlen(substr($exam["examid"], 4));
          //echo "<br> examid = ".substr($exam["examid"], 4)."<br>";
          if (substr($exam["examid"], 4, 2) === "22") {
          //Skin fold
          $examObj = new ExamObj();
          $examObj->SetAppID($appID);
          $examObj->SetSeqNo($exam["seqno"]);
          $examObj->SetExamID($exam["examid"]);
          $examObj->SetExamCat($exam["category"]);
          $examObj->SetExamPriority($exam["priority"]);
          $res = $this->GetExamDescriptionByExamID($exam["examid"]);
          if ($res->MSGID === AppConst::STATUS_SUCCESS) {
          $examObj->SetEDescription($res->MSGDETAIL2);
          $examObj->SetTDescription($res->MSGDETAIL);
          $examObj->SetUOM($res->MSGDETAIL3);
          }
          $examObj->SetTestValue($exam["value"]);
          $examObj->SetTestDate($this->ConvertYMD($exam["applydate"]));
          /* if (in_array($exam["examid"], $lists)) {
          $examObj = $this->GetScoreGrade($examObj, $tester, $age);
          }
          $response->ResponseDetail[] = $examObj;
          } else if (substr($exam["examid"], 4, 2) === "23") {
          //Omron
          $examObj = new ExamObj();
          $examObj->SetAppID($appID);
          $examObj->SetSeqNo($exam["seqno"]);
          $examObj->SetExamID($exam["examid"]);
          $examObj->SetExamCat($exam["category"]);
          $examObj->SetExamPriority($exam["priority"]);
          $res = $this->GetExamDescriptionByExamID($exam["examid"]);
          if ($res->MSGID === AppConst::STATUS_SUCCESS) {
          $examObj->SetEDescription($res->MSGDETAIL2);
          $examObj->SetTDescription($res->MSGDETAIL);
          $examObj->SetUOM($res->MSGDETAIL3);
          }
          $examObj->SetTestValue($exam["value"]);
          $examObj->SetTestDate($this->ConvertYMD($exam["applydate"]));
          /* if (in_array($exam["examid"], $lists)) {
          $examObj = $this->GetScoreGrade($examObj, $tester, $age);
          }
          $response->ResponseDetail[] = $examObj;
          } else if (substr($exam["examid"], 4, 2) === "01") {
          //HandGrip
          } else {

          }
          } else {
          //echo "<br>str len = ".strlen(substr($exam["examid"], 4));
          //echo "<br> examid = ".$exam["examid"]."<br>";
          if ((substr($exam["examid"], 4, 2)) === "10") {
          //Astrand
          //echo "<br>  astrand <br>";
          } else {
          $examObj = new ExamObj();
          $examObj->SetAppID($appID);
          $examObj->SetSeqNo($exam["seqno"]);
          $examObj->SetExamID($exam["examid"]);
          $examObj->SetExamCat($exam["category"]);
          $examObj->SetExamPriority($exam["priority"]);
          $res = $this->GetExamDescriptionByExamID($exam["examid"]);
          if ($res->MSGID === AppConst::STATUS_SUCCESS) {
          $examObj->SetEDescription($res->MSGDETAIL2);
          $examObj->SetTDescription($res->MSGDETAIL);
          $examObj->SetUOM($res->MSGDETAIL3);
          }
          $examObj->SetTestValue($exam["value"]);
          $examObj->SetTestDate($this->ConvertYMD($exam["applydate"]));
          if (in_array($exam["examid"], $lists)) {
          $examObj = $this->GetScoreGrade($examObj, $tester, $age);
          }
          $response->ResponseDetail[] = $examObj;
          }
          }
          }

          $response->MSGID = AppConst::STATUS_SUCCESS;
          }
          //echo '<pre>'; print_r($response); echo '</pre>';
          //print_r($response);
          return $response; */

        foreach ($exams as $exam) {
            //echo "exam id = ".$exam['examid'];
            if (in_array($exam["examid"], $calculateExam)) {
                continue;
                //echo "<br> in calculate ".$exam["examid"]."<br>";
            } else if (stripos($exam["examid"], 'type23') !== FALSE) {
                //echo "exam id = ".$exam['examid'];
                continue;
                //echo "<br> in calculate ".$exam["examid"]."<br>";
            } else {
                //echo "<br>not in calculate : examid = ".$exam["examid"]."";
                $examObj = new ExamObj();
                $examObj->SetAppID($appID);
                $examObj->SetSeqNo($exam["seqno"]);
                $examObj->SetExamID($exam["examid"]);
                $examObj->SetExamCat($exam["category"]);
                $examObj->SetExamPriority($exam["priority"]);
                $examObj = $this->GetScoreGradeNew($appID, $exam["examid"], $examObj);
                $res = $this->GetExamDescriptionByExamID($exam["examid"]);
                if ($res->MSGID === AppConst::STATUS_SUCCESS) {
                    $examObj->SetEDescription($res->MSGDETAIL2);
                    $examObj->SetTDescription($res->MSGDETAIL);
                    $examObj->SetUOM($res->MSGDETAIL3);
                }
                //$examObj = $this->GetGraphForPrintByAppID($appID, $exam["examid"], $examObj);
                //$examObj->SetExamGraph($this->GetCalculatedExamGraph($appID, $exam["examid"]));
                $examObj = $this->GetPreviousTestScore($tester->GetTesterID(), $examObj, $appID);
                $response->ResponseDetail[] = $examObj;
                $response->MSGDETAIL3 = $exam['applydate'];
            }
        }
        $response->MSGID = AppConst::STATUS_SUCCESS;
        /* echo '<pre>';
          print_r($response);
          echo '</pre>'; */
        return $response;
    }

    function GetMapExamByAppID($appID, $condition) {
        $con = $this->dbObj->Open();
        $table = AppConst::TABLE_EXAM_RESULT;
        $sql = "select `$table`.examid, value, seqno, applydate, category, priority from `$table` "
                . "join map on `$table`.applicationid = map.applicationid  "
                . "join exam on `$table`.examid = exam.examid "
                . "where map.applicationid = '" . $appID . "'";
        $sql .= $condition;
        //echo $sql . "<br>";
        $rows = array();
        $result = $con->query($sql);
        if (!$result) {
            $rows[0] = AppConst::STATUS_SQL_ERROR;
            $rows[1] = $con->error();
            return $rows;
        }

        while ($row = $result->fetch_array()) {
            $rows[] = $row;
        }
        return $rows;
    }

    function GetPreviousMapExamByTesterID($testerid, $condition) {
        $con = $this->dbObj->Open();
        $table = AppConst::TABLE_EXAM_RESULT;
        $sql = "select examid, value, completedate "
                . " FROM `$table` "
                . " JOIN application ON mapexam.applicationid = application.applicationid  "
                . " where application.testerid = '" . $testerid . "' ";
        $sql .= $condition;
        $sql .= " ORDER BY completedate DESC ";
        //echo $sql . "<br>";
        $rows = array();
        $result = $con->query($sql);
        if (!$result) {
            $rows[0] = AppConst::STATUS_SQL_ERROR;
            $rows[1] = $con->error();
            return $rows;
        }

        while ($row = $result->fetch_array()) {
            $rows[] = $row;
        }
        return $rows;
    }

    function CalculateMultiStage($appID, TesterHeaderInfo $tester) {
        $appManager = new AppManager();
        $examObj = new ExamObj();
        $res = new ResponseHeader();
        $age = $appManager->CalCulateAge($tester->GetDOB());
        $condition = " AND mapexam.examid LIKE  '" . AppConst::EXAM_TYPE_MULTISTAGE . "%' ";
        $exams = $this->GetMapExamByAppID($appID, $condition);
        /*
         * For Previous 
         */
        $prev_condition = " AND completedate < ( SELECT completedate FROM application WHERE applicationid =  '" . $appID . "' ) " . $condition;
        $prev_exams = $this->GetPreviousMapExamByTesterID($tester->GetTesterID(), $prev_condition);

        $level = 0;
        $prev_lv = 0;
        $stage = 0;
        $prev_stage = 0;
        $cnt = 0;
        $prev_cnt = 0;
        $examCat = "";
        $examPri = "";
        $recentdate = "";
        $datecnt = 0;
        foreach ($exams as $exam) {
            $cnt++;
            if ($exam["examid"] === AppConst::EXAM_TYPE_MULTISTAGE_LEVEL) {
                $level = $exam["value"];
            }
            if ($exam["examid"] === AppConst::EXAM_TYPE_MULTISTAGE_STAGE) {
                $stage = $exam["value"];
            }
            $examCat = $exam["category"];
            $examPri = $exam["priority"];
        }
        foreach ($prev_exams as $prev_exam) {
            $prev_cnt++;
            if ($recentdate === "" || $recentdate === $prev_exam["completedate"]) {
                $recentdate = $prev_exam["completedate"];
                if ($prev_exam["examid"] === AppConst::EXAM_TYPE_MULTISTAGE_LEVEL) {
                    $prev_lv = $prev_exam["value"];
                }
                if ($prev_exam["examid"] === AppConst::EXAM_TYPE_MULTISTAGE_STAGE) {
                    $prev_stage = $prev_exam["value"];
                }
                $datecnt++;
            } else {
                if ($recentdate !== $prev_exam["completedate"]) {
                    break;
                }
            }
        }
        if ($cnt > 0) {
            $value = $this->GetMultistageValue($level, $stage);

            //echo "<br>".$value->ResponseDetail[0];
            $examObj->SetAppID($appID);
            $examObj->SetExamID(AppConst::EXAM_TYPE_MULTISTAGE);
            $examObj->SetSeqNo(0);
            $examObj->SetExamCat($examCat);
            $examObj->SetExamPriority($examPri);
            $res = $this->GetExamDescriptionByExamID(AppConst::EXAM_TYPE_MULTISTAGE);
            $examObj->SetGradeID("-");
            if ($res->MSGID === AppConst::STATUS_SUCCESS) {
                $examObj->SetEDescription($res->MSGDETAIL2);
                $examObj->SetTDescription($res->MSGDETAIL);
                $examObj->SetUOM($res->MSGDETAIL3);
            }

            if ($value->MSGID != AppConst::STATUS_SUCCESS) {
                $examObj->SetTestValue("-");
            } else {
                $examObj->SetTestValue(number_format($value->ResponseDetail[0], 2));
            }
            if ($prev_cnt > 0) {
                $prev_val = $this->GetMultistageValue($prev_lv, $prev_stage);
                if ($prev_val->MSGID != AppConst::STATUS_SUCCESS) {
                    $examObj->SetPreviousScore("-");
                } else {
                    $examObj->SetPreviousScore(number_format($prev_val->ResponseDetail[0], 2));
                }
            } else {
                $examObj->SetPreviousScore("-");
            }

            if ($examObj->GetTestValue() === 0 || $examObj->GetTestValue() === '-') {
                $examObj->SetGradeID("-");
            } else {
                $examObj = $this->GetScoreGrade($examObj, $tester, $age);
            }
            //$examObj = $this->GetScoreGrade($examObj, $tester, $age);
        } else {
            $examObj = NULL;
        }

        return $examObj;
    }

    function GetMultistageValue($level, $stage) {
        $con = $this->dbObj->Open();
        $res = new ResponseHeader();
        $table = AppConst::TABLE_MULTISTAGE_COMPARE;
        $sql = "select value from `$table` where level = ? and stage = ?";
        $stmt = $con->prepare($sql);
        $stmt->bind_param("ii", $level, $stage);
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($value);
            if ($stmt->num_rows > 0) {
                while ($stmt->fetch()) {
                    $res->ResponseDetail[0] = $value;
                }
                $res->MSGID = AppConst::STATUS_SUCCESS;
            } else {
                $res->MSGID = AppConst::STATUS_ERROR;
                $res->MSGDETAIL = "from GetMultistageValue ";
                $res->MSGDETAIL2 = "cannot find value";
            }
        } else {
            $res->MSGID = AppConst::STATUS_SQL_ERROR;
            $res->MSGDETAIL = "from GetMultistageValue ";
            $res->MSGDETAIL2 = htmlspecialchars($stmt->error);
            ;
        }
        $stmt->close();
        return $res;
    }

    function CalculateBMI($appID, TesterHeaderInfo $tester) {
        $examObj = new ExamObj();
        if ($tester->GetHeight() != NULL && $tester->GetHeight() != "" && $tester->GetWeight() != NULL && $tester->GetWeight() != "") {
            $age = $tester->GetAge();
            $temp_height = $tester->GetHeight() / 100;
            //echo "<br>weight = ".$tester->GetWeight()." height = ".$temp_height."<br>";
            $bmi = $tester->GetWeight() / ($temp_height * $temp_height);
            $examObj->SetAppID($appID);
            $examObj->SetExamID(AppConst::EXAM_TYPE_OMRON_BMI);
            $examObj->SetTestValue(number_format($bmi, 2));
            $examObj->SetSeqNo(0);
            $examObj->SetExamCat("examcat00");
            $examObj->SetExamPriority(0);
            $examObj->SetPreviousScore("-");
            $res = $this->GetExamDescriptionByExamID(AppConst::EXAM_TYPE_BMI);
            if ($res->MSGID === AppConst::STATUS_SUCCESS) {
                $examObj->SetEDescription($res->MSGDETAIL2);
                $examObj->SetTDescription($res->MSGDETAIL);
                $examObj->SetUOM($res->MSGDETAIL3);
            }
            $examObj = $this->GetScoreGrade($examObj, $tester, $age);
            $examObj->SetGradeTDescription($this->GetBMIScoreDesciption($examObj->GetGradeID()));
            $examObj->SetExamID(AppConst::EXAM_TYPE_BMI);
        } else {
            $examObj = NULL;
        }

        return $examObj;
    }

    function CalCulateGradeSkinFold($appID, TesterHeaderInfo $tester) {
        $appManager = new AppManager();
        $examObj = new ExamObj();
        $age = $appManager->CalCulateAge($tester->GetDOB());
        $condition = " AND mapexam.examid LIKE  '" . AppConst::EXAM_TYPE_SKIN_FOLD . "%' ";
        $exams = $this->GetMapExamByAppID($appID, $condition);
        $tmp = 0;
        $cnt = 0;
        $examCat = "";
        $examPri = "";

        foreach ($exams as $exam) {
            $cnt++;
            //echo "<br>value  = ".$exam["value"];
            $tmp += $exam["value"];
            $examCat = $exam["category"];
            $examPri = $exam["priority"];
        }

        $prev_cnt = 0;
        $prev_condition = " AND completedate < ( SELECT completedate FROM application WHERE applicationid =  '" . $appID . "' ) " . $condition;
        $prev_exams = $this->GetPreviousMapExamByTesterID($tester->GetTesterID(), $prev_condition);
        $recentdate = "";
        $datecnt = 0;
        $prev_tmp = 0;
        foreach ($prev_exams as $prev_exam) {
            $prev_cnt++;
            if ($recentdate === "" || $recentdate === $prev_exam["completedate"]) {
                $recentdate = $prev_exam["completedate"];
                $prev_tmp += $prev_exam["value"];
                $datecnt++; //day 1
            } else {
                if ($recentdate !== $prev_exam["completedate"]) {
                    break;
                }
            }
        }
        //echo "<br>cnt = ".$cnt;
        if ($cnt > 0) {
            //$sum = round($tmp / 4);
            $sum = $tmp;
            //echo "sum = ".$sum;
            $percen = $this->GetFatPercentageSkinFold($sum, $tester->GetSex(), $age);
            //echo "percen skin = ".$percen;
            if ($percen === null || $percen === "") {
                $percen = 0;
            }
            $examObj->SetAppID($appID);
            $examObj->SetExamID(AppConst::EXAM_TYPE_SKIN_FOLD);
            $examObj->SetTestValue($percen);
            $examObj->SetSeqNo(0);
            $examObj->SetExamCat($examCat);
            $examObj->SetExamPriority($examPri);
            $res = $this->GetExamDescriptionByExamID(AppConst::EXAM_TYPE_SKIN_FOLD);
            if ($res->MSGID === AppConst::STATUS_SUCCESS) {
                $examObj->SetEDescription($res->MSGDETAIL2);
                $examObj->SetTDescription($res->MSGDETAIL);
                $examObj->SetUOM($res->MSGDETAIL3);
            }
            if ($prev_cnt > 0) {
                //$prev_percent = $this->GetFatPercentageSkinFold(round($prev_tmp / 4), $tester->GetSex(), $age);
                $prev_percent = $this->GetFatPercentageSkinFold(($prev_tmp), $tester->GetSex(), $age);
                if ($prev_percent === null || $prev_percent === "") {
                    $prev_percent = 0;
                }
                if ($prev_percent === 0) {
                    $examObj->SetPreviousScore("-");
                } else {
                    $examObj->SetPreviousScore($prev_percent);
                }
            } else {
                $examObj->SetPreviousScore("-");
            }

            /* if ($percen === AppConst::STATUS_SQL_ERROR) {
              die();
              } */
            if ($examObj->GetTestValue() === 0) {
                $examObj->SetGradeID("-");
            } else {
                $examObj = $this->GetScoreGrade($examObj, $tester, $age);
            }
            //print_r($examObj);
        } else {
            $examObj = null;
        }

        return $examObj;
    }

    function CalCulateOmron($appID, $examid, TesterHeaderInfo $tester) {
        $appManager = new AppManager();
        $examObj = new ExamObj();
        $age = $appManager->CalCulateAge($tester->GetDOB());
        $condition = " AND mapexam.examid =  '" . $examid . "' ";
        $exams = $this->GetMapExamByAppID($appID, $condition);
        $tmp = 0;
        $cnt = 0;
        $examCat = "";
        $examPri = "";
        foreach ($exams as $exam) {
            $cnt++;
            //echo "<br>value  = ".$exam["value"];
            $tmp = $exam["value"];
            $examCat = $exam["category"];
            $examPri = $exam["priority"];
        }

        //echo "<br>cnt = ".$cnt;
        if ($cnt > 0) {
            $examObj->SetAppID($appID);
            $examObj->SetExamID($examid);
            if ($tmp === null || $tmp === "") {
                $tmp = 0;
            }
            $examObj->SetTestValue($tmp);
            $examObj->SetSeqNo(0);
            $examObj->SetExamCat($examCat);
            $examObj->SetExamPriority($examPri);
            /* if ($tmp === AppConst::STATUS_SQL_ERROR) {
              die();
              } */
            $res = $this->GetExamDescriptionByExamID($examid);
            if ($res->MSGID === AppConst::STATUS_SUCCESS) {
                $examObj->SetEDescription($res->MSGDETAIL2);
                $examObj->SetTDescription($res->MSGDETAIL);
                $examObj->SetUOM($res->MSGDETAIL3);
            }
            if ($examObj->GetTestValue() === 0) {
                $examObj->SetGradeID("-");
            } else {
                $examObj = $this->GetScoreGrade($examObj, $tester, $age);
            }
            //$examObj = $this->GetScoreGrade($examObj, $tester, $age);
            //print_r($examObj);
        } else {
            $examObj = null;
        }

        return $examObj;
    }

    function CalCulateAstrand($appID, TesterHeaderInfo $tester) {
        $appManager = new AppManager();
        $examObj = new ExamObj();
        $age = $appManager->CalCulateAge($tester->GetDOB());
        //echo "appid = ". $appID ." age = ".$age." Testerid = ".$tester->GetTesterID();
        $condition = " AND mapexam.examid LIKE  '" . AppConst::EXAM_TYPE_ASTRAND . "%' ";
        $exams = $this->GetMapExamByAppID($appID, $condition);
        $load = 0;
        $tmp = 0;
        $cnt = 0;
        $examCat = "";
        $examPri = "";
        $tmp_cnt = array();
        //print_r($exams);
        foreach ($exams as $exam) {
            if ($exam["examid"] === AppConst::EXAM_TYPE_ASTRAND_LOAD) {
                $load = $exam["value"];
                //echo "<br>load  = ".$load;
            } else {
                //$tmp += $exam["value"];
                //echo "<br>value  = ".$exam["value"];
                $cnt++;
                $tmp_cnt[$cnt] = $exam["value"];
            }
            $examCat = $exam["category"];
            $examPri = $exam["priority"];
        }
        //echo "app id = " . $appID . " count row = " . $cnt . "<br>";
        $prev_cnt = 0;
        $prev_row_cnt = 0;
        $prev_condition = " AND completedate < ( SELECT completedate FROM application WHERE applicationid =  '" . $appID . "' ) " . $condition;
        $prev_exams = $this->GetPreviousMapExamByTesterID($tester->GetTesterID(), $prev_condition);
        $recentdate = "";
        $datecnt = 0;
        $prev_tmp = 0;
        $prev_load = 0;
        $prev_tmpcnt = array();
        foreach ($prev_exams as $prev_exam) {
            $prev_row_cnt++;
            if ($recentdate === "" || $recentdate === $prev_exam["completedate"]) {
                $recentdate = $prev_exam["completedate"];
                if ($prev_exam["examid"] === AppConst::EXAM_TYPE_ASTRAND_LOAD) {
                    $prev_load = $prev_exam["value"];
                } else {
                    $prev_cnt++;
                    $prev_tmpcnt[$prev_cnt] = $prev_exam["value"];
                }
                $datecnt++;
            } else {
                if ($recentdate !== $prev_exam["completedate"]) {
                    break;
                }
            }
        }

        if ($cnt > 0) {
            if ($cnt === 4) {
                $tmp = $tmp_cnt[3] + $tmp_cnt[4];
            } else if ($cnt === 5) {
                $tmp = $tmp_cnt[4] + $tmp_cnt[5];
            } else if ($cnt === 6) {
                $tmp = $tmp_cnt[5] + $tmp_cnt[6];
            }
            $data1 = round($tmp / 2);
            //echo "<br>data1 = ".$data1;
            $data2 = $this->GetOxygenAstrand($data1, $load, $tester->GetSex());
            //echo "<br>data2 = ".$data2;
            if ($data2 === null || $data2 === "" || $data2 === AppConst::STATUS_SQL_ERROR) {
                $data2 = 0;
            }
            $data3 = ($data2 * 1000) / 60;
            $ageFactor = $this->GetMultiplyAstrand($age);
            //echo "<br>age factor = " . $ageFactor;
            if ($ageFactor === null || $ageFactor === "" || $ageFactor === AppConst::STATUS_SQL_ERROR) {
                $ageFactor = 0;
            }
            $data4 = $data3 * $ageFactor;

            /*
             * Prev
             */
            if ($prev_row_cnt > 0) {
                if ($prev_cnt === 4) {
                    $prev_tmp = $prev_tmpcnt[3] + $prev_tmpcnt[4];
                } else if ($cnt === 5) {
                    $prev_tmp = $prev_tmpcnt[4] + $prev_tmpcnt[5];
                } else if ($cnt === 6) {
                    $prev_tmp = $prev_tmpcnt[5] + $prev_tmpcnt[6];
                }
                $prev_data1 = round($prev_tmp / 2);
                //echo "<br>data1 = ".$prev_data1;
                $prev_data2 = $this->GetOxygenAstrand($prev_data1, $prev_load, $tester->GetSex());
                //echo "<br>data2 = ".$prev_data2;
                if ($prev_data2 === null || $prev_data2 === "" || $prev_data2 === AppConst::STATUS_SQL_ERROR) {
                    $prev_data2 = 0;
                }
                $prev_data3 = ($prev_data2 * 1000) / 60;

                $prev_data4 = $prev_data3 * $ageFactor;
                if ($prev_data4 === 0) {
                    $examObj->SetPreviousScore("-");
                } else {
                    $examObj->SetPreviousScore(number_format($prev_data4, 2));
                }
            } else {
                $examObj->SetPreviousScore("-");
            }
            /*
             * End
             */
            $examObj->SetAppID($appID);
            $examObj->SetExamID(AppConst::EXAM_TYPE_ASTRAND);
            $examObj->SetSeqNo(99);
            $examObj->SetTestValue(number_format($data4, 2));
            $examObj->SetExamCat($examCat);
            $examObj->SetExamPriority($examPri);
            //print_r($examObj);
            $res = $this->GetExamDescriptionByExamID(AppConst::EXAM_TYPE_ASTRAND);
            if ($res->MSGID === AppConst::STATUS_SUCCESS) {
                $examObj->SetEDescription($res->MSGDETAIL2);
                $examObj->SetTDescription($res->MSGDETAIL);
                $examObj->SetUOM($res->MSGDETAIL3);
            }
            //$examObj = $this->GetScoreGradeNew($appID, AppConst::EXAM_TYPE_ASTRAND, $examObj);
            if ($examObj->GetTestValue() === 0 || $examObj->GetTestValue() === '-') {
                $examObj->SetGradeID("-");
            } else {
                $examObj = $this->GetScoreGrade($examObj, $tester, $age);
            }
            //$examObj = $this->GetScoreGrade($examObj, $tester, $age);
            //echo "<br><br>After Get Grade <br><br>";
            //print_r($examObj);
        } else {
            $examObj = null;
        }
        //print_r($examObj);
        return $examObj;
    }

    function CalculateHandGrip($appID, TesterHeaderInfo $tester) {
        $appManager = new AppManager();
        $examObj = new ExamObj();
        $age = $appManager->CalCulateAge($tester->GetDOB());
        $condition = " AND (mapexam.examid =  '" . AppConst::EXAM_TYPE_HANDGRIP_L . "' or mapexam.examid =  '" . AppConst::EXAM_TYPE_HANDGRIP_R . "')";
        $exams = $this->GetMapExamByAppID($appID, $condition);
        $tmp = 0;
        $cnt = 0;
        $examCat = "";
        $examPri = "";
        foreach ($exams as $exam) {
            $cnt++;
            //echo "exam = ".$exam["value"]."<br>";
            $tmp += $exam["value"];
            $examCat = $exam["category"];
            $examPri = $exam["priority"];
        }

        $prev_cnt = 0;
        $prev_condition = " AND completedate < ( SELECT completedate FROM application WHERE applicationid =  '" . $appID . "' ) " . $condition;
        $prev_exams = $this->GetPreviousMapExamByTesterID($tester->GetTesterID(), $prev_condition);
        $recentdate = "";
        $datecnt = 0;
        $prev_tmp = 0;
        foreach ($prev_exams as $prev_exam) {
            $prev_cnt++;
            if ($recentdate === "" || $recentdate === $prev_exam["completedate"]) {
                $recentdate = $prev_exam["completedate"];
                $prev_tmp += $prev_exam["value"];
                $datecnt++; //day 1
            } else {
                if ($recentdate !== $prev_exam["completedate"]) {
                    break;
                }
            }
        }

        if ($cnt > 0) {
            //echo "temp = ".$tmp."<br>";
            $sum = $tmp / 2;
            //echo "sum ".$sum."<br>";
            //echo $tester->GetWeight();
            if ($tester->GetWeight() === NULL || $tester->GetWeight() == 0) {
                $data = 0;
                $prev_data = 0;
            } else {
                $data = $sum / $tester->GetWeight();
            }
            //echo "<br>" . "prev cnt = " . $prev_cnt . "<br>";
            if ($prev_cnt > 0) {
                $prev_sum = $prev_tmp / 2;
                $prev_data = $prev_sum / $tester->GetWeight();
                if ($prev_data === 0) {
                    $examObj->SetPreviousScore("-");
                } else {
                    $examObj->SetPreviousScore(number_format($prev_data, 2));
                }
            } else {
                $examObj->SetPreviousScore("-");
            }
            $examObj->SetAppID($appID);
            $examObj->SetExamID(AppConst::EXAM_TYPE_HANDGRIP);
            $res = $this->GetExamDescriptionByExamID(AppConst::EXAM_TYPE_HANDGRIP);
            if ($res->MSGID === AppConst::STATUS_SUCCESS) {
                $examObj->SetEDescription($res->MSGDETAIL2);
                $examObj->SetTDescription($res->MSGDETAIL);
                $examObj->SetUOM($res->MSGDETAIL3);
            }
            $examObj->SetTestValue(number_format($data, 2));
            $examObj->SetSeqNo(0);
            $examObj->SetExamCat($examCat);
            $examObj->SetExamPriority($examPri);
            if ($examObj->GetTestValue() === 0 || $examObj->GetTestValue() === '-') {
                $examObj->SetGradeID("-");
            } else {
                $examObj = $this->GetScoreGrade($examObj, $tester, $age);
            }
            //$examObj = $this->GetScoreGrade($examObj, $tester, $age);
            //print_r($examObj);
        } else {
            $examObj = null;
        }
        return $examObj;
    }

    function CalculateOneLegBalance($appID, $examid, TesterHeaderInfo $tester) {
        $appManager = new AppManager();
        $examObj = new ExamObj();
        $age = $appManager->CalCulateAge($tester->GetDOB());
        //$condition = " AND mapexam.examid =  '" . $examid . "' ";
        if ($examid == AppConst::EXAM_TYPE_ONE_LEG_BAL_L_OPEN || $examid == AppConst::EXAM_TYPE_ONE_LEG_BAL_R_OPEN) {
            $tmpexam = AppConst::EXAM_TYPE_ONE_LEG_BAL_OPEN;
        } else {
            $tmpexam = AppConst::EXAM_TYPE_ONE_LEG_BAL_CLOSE;
        }
        $condition = " AND mapexam.examid like  '%" . $examid . "%' ";
        //$condition = " AND mapexam.examid like  '%" . $tmpexam . "%' ";
        $exams = $this->GetMapExamByAppID($appID, $condition);
        $tmp = 0;
        $cnt = 0;
        $examCat = "";
        $examPri = "";
        $examid_query = "";
        foreach ($exams as $exam) {
            $cnt++;
            //echo "exam = ".$exam["value"]."<br>";
            $tmp = $exam["value"];
            $examCat = $exam["category"];
            $examPri = $exam["priority"];
            $examid_query = $exam['examid'];
        }

        /* $prev_cnt = 0;
          $prev_condition = " AND completedate < ( SELECT completedate FROM application WHERE applicationid =  '" . $appID . "' ) " . $condition;
          $prev_exams = $this->GetPreviousMapExamByTesterID($tester->GetTesterID(), $prev_condition);
          $recentdate = "";
          $datecnt = 0;
          $prev_tmp = 0;
          foreach ($prev_exams as $prev_exam) {
          $prev_cnt++;
          if ($recentdate === "" || $recentdate === $prev_exam["completedate"]) {
          $recentdate = $prev_exam["completedate"];
          $prev_tmp = $prev_exam["value"];
          $datecnt++; //day 1
          } else {
          if ($recentdate !== $prev_exam["completedate"]) {
          break;
          }
          }
          } */

        if ($cnt > 0) {
            $data = $tmp;
            //echo "<br>" . "prev cnt = " . $prev_cnt . "<br>";
            /* if ($prev_cnt > 0) {
              $prev_data = $prev_tmp;
              if ($prev_data === 0) {
              $examObj->SetPreviousScore("-");
              } else {
              $examObj->SetPreviousScore(number_format($prev_data, 2));
              }
              } else {
              $examObj->SetPreviousScore("-");
              } */
            $examObj->SetAppID($appID);
            //$examObj->SetExamID($examid_query);
            $examObj->SetExamID($tmpexam);
            $res = $this->GetExamDescriptionByExamID($examid);
            if ($res->MSGID === AppConst::STATUS_SUCCESS) {
                $examObj->SetEDescription($res->MSGDETAIL2);
                $examObj->SetTDescription($res->MSGDETAIL);
                $examObj->SetUOM($res->MSGDETAIL3);
            }
            $examObj->SetTestValue(number_format($data, 2));
            $examObj->SetSeqNo(0);
            $examObj->SetExamCat($examCat);
            $examObj->SetExamPriority($examPri);
            if ($examObj->GetTestValue() === 0 || $examObj->GetTestValue() === '-') {
                $examObj->SetGradeID("-");
            } else {
                //$examObj->SetExamID(AppConst::EXAM_TYPE_ONE_LEG_BAL);
                $examObj = $this->GetScoreGrade($examObj, $tester, $age);
                $examObj->SetExamID($examid);
            }
            //$examObj = $this->GetScoreGrade($examObj, $tester, $age);
            //print_r($examObj);
        } else {
            $examObj = null;
        }
        return $examObj;
    }

    function GetCompareChartList() {
        $con = $this->dbObj->Open();
        $arr = array();
        $sql = "select distinct(examid) from MasterData";
        $stmt = $con->prepare($sql);
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($value);
            while ($stmt->fetch()) {
                $arr[] = $value;
            }
        } else {
            $arr[] = AppConst::STATUS_SQL_ERROR;
        }
        $stmt->close();
        return $arr;
    }

    function GetFatPercentageSkinFold($data, $gender, $age) {
        $res = "";
        $table = AppConst::TABLE_SKINFOLD_SUMMARY;
        $con = $this->dbObj->Open();
        $sql = "select value from `$table` where summary = '" . $data . "' "
                . " and gender = '" . $gender . "' "
                . " and (age_from <= '" . $age . "' and age_to > '" . $age . "') ";
        $stmt = $con->prepare($sql);
        //echo "<br> ".$sql;
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($value);
            while ($stmt->fetch()) {
                $res = $value;
            }
        } else {
            $res = AppConst::STATUS_SQL_ERROR;
            $stmt->close();
        }
        return $res;
    }

    function GetSummaryByExamType($examType, $grpTest, $from, $to) {
        $responseHeader = new ResponseHeader();
        $response = new SummaryDetailResponse();
        $table = AppConst::TABLE_EXAM_RESULT;
        $hasError = FALSE;
        $con = $this->dbObj->Open();
        $whereCondition = " WHERE mapexam.examid like '%" . $examType . "%' ";
        if ($grpTest != "" && $grpTest != "0") {

            if ($whereCondition != "") {
                $whereCondition .= " and application.groupid = '" . $grpTest . "' ";
            } else {
                $whereCondition .= " where application.groupid = '" . $grpTest . "' ";
            }
        }

        if ($from != "" && $to != "" && $from != "1970-01-01" && $to != "1970-01-01") {
            if ($whereCondition != "") {
                $whereCondition .= " and (application.completedate >= '" . $from . "' and application.completedate <= '" . $to . "' )";
            } else {
                $whereCondition .= " where (application.completedate >= '" . $from . "' and application.completedate <= '" . $to . "' ) ";
            }
        } else if (($from != "" && $from != "1970-01-01" ) && ($to == "" || $to == "1970-01-01")) {
            if ($whereCondition != "") {
                $whereCondition .= " and application.completedate >= '" . $from . "' ";
            } else {
                $whereCondition .= " where application.completedate >= '" . $from . "'  ";
            }
        } else if (($from == "" || $from == "1970-01-01") && ($to != "" && $to != "1970-01-01")) {
            if ($whereCondition != "") {
                $whereCondition .= " and application.completedate <= '" . $to . "' ";
            } else {
                $whereCondition .= " where application.completedate <= '" . $to . "' ";
            }
        }


        $sql = "SELECT COUNT( DISTINCT map.applicationid ) as cnt, gender "
                . " FROM mapexam "
                . " JOIN map ON mapexam.applicationid = map.applicationid "
                . " JOIN tester ON map.testerid = tester.testerid "
                . " JOIN application on mapexam.applicationid = application.applicationid"
                . " JOIN groups on application.groupid = groups.groupid"
                . $whereCondition
                . " GROUP BY gender";
        $stmt = $con->prepare($sql);
        //echo $sql;
        $allQuery = 0;
        $cnt_male = 0;
        $cnt_female = 0;
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($value, $gender);
            if ($stmt->num_rows > 0) {
                $allQuery = $stmt->num_rows;
                while ($stmt->fetch()) {
                    if ($gender === AppConst::MALE) {
                        $cnt_male = $value;
                    } else if ($gender === AppConst::FEMALE) {
                        $cnt_female = $value;
                    } else {
                        
                    }
                }
                $cnt_all = $cnt_female + $cnt_male;
                $response->MALE = $cnt_male;
                $response->FEMALE = $cnt_female;
                $response->TOTAL = $cnt_all;
                $response->INDICATOR = AppConst::SUMMARY_TOTAL;
                $response->STATUS = AppConst::STATUS_SUCCESS;
                $stmt->close();
            } else {
                $response->STATUS = AppConst::STATUS_SQL_ERROR;
                $stmt->close();
            }
        } else {
            $response->STATUS = AppConst::STATUS_SQL_ERROR;
            $stmt->close();
        }
        if ($response->STATUS != AppConst::STATUS_SUCCESS) {
            $hasError = TRUE;
        }
        //echo "all query = ".$allQuery;
        $responseHeader->ResponseDetail[] = $response;

        if ($allQuery > 0) {
            $dataAvg = $this->GetSummaryAverage($examType);
            if ($dataAvg->STATUS != AppConst::STATUS_SUCCESS) {
                $hasError = TRUE;
            }
            $responseHeader->ResponseDetail[] = $dataAvg;

            $dataMax = $this->GetSummaryMax($examType);
            if ($dataMax->STATUS != AppConst::STATUS_SUCCESS) {
                $hasError = TRUE;
            }
            $responseHeader->ResponseDetail[] = $dataMax;

            $dataMin = $this->GetSummaryMin($examType);
            if ($dataMin->STATUS != AppConst::STATUS_SUCCESS) {
                $hasError = TRUE;
            }
            $responseHeader->ResponseDetail[] = $dataMin;
        }
        if ($hasError) {
            $responseHeader->MSGID = AppConst::STATUS_ERROR;
        } else {
            $responseHeader->MSGID = AppConst::STATUS_SUCCESS;
        }


        return $responseHeader;
    }

    function GetSummaryAverageComplex($examType) {
        $response = new SummaryDetailResponse();
        $table = AppConst::TABLE_EXAM_RESULT;
        $con = $this->dbObj->Open();
        $sql = "SELECT distinct(map.applicationid), gender "
                . "FROM mapexam "
                . "JOIN map ON mapexam.applicationid = map.applicationid "
                . "JOIN tester ON map.testerid = tester.testerid "
                . "WHERE mapexam.examid LIKE  '" . $examType . "%' "
                . "GROUP BY gender, map.applicationid";
        $stmt = $con->prepare($sql);
        //echo "<br>" . $sql;
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($appid, $gender);
            $cnt_male = 0;
            $cnt_female = 0;
            $tmp_male = 0;
            $tmp_female = 0;
            $avg_male = 0;
            $avg_female = 0;
            $avg_all = 0;
            while ($stmt->fetch()) {
                $tester = new TesterHeaderInfo();
                $tester = $this->GetTesterDetailByAppID($appid);
                $examObj = new ExamObj();
                if ($examType === AppConst::EXAM_TYPE_HANDGRIP) {
                    $examObj = $this->CalculateHandGrip($appid, $tester);
                } else if ($examType === AppConst::EXAM_TYPE_ASTRAND) {
                    $examObj = $this->CalCulateAstrand($appid, $tester);
                } else if ($examType === AppConst::EXAM_TYPE_OMRON) {
                    $examObj = $this->CalCulateOmron($appid, $tester);
                } else if ($examType === AppConst::EXAM_TYPE_SKIN_FOLD) {
                    $examObj = $this->CalCulateGradeSkinFold($appid, $tester);
                }

                if ($gender === AppConst::MALE) {
                    if ($examObj != NULL) {
                        $cnt_male++;
                        $tmp_male += $examObj->GetTestValue();
                    }
                } else if ($gender === AppConst::FEMALE) {
                    if ($examObj != NULL) {
                        $cnt_female++;
                        $tmp_female += $examObj->GetTestValue();
                    }
                } else {
                    
                }
            }
            if ($stmt->num_rows > 0) {
                $tmp_all = $tmp_female + $tmp_male;
                $cnt_all = $cnt_female + $cnt_male;

                $avg_male = $tmp_male / $cnt_male;
                if ($cnt_female > 0) {
                    $avg_female = $tmp_female / $cnt_female;
                } else {
                    $avg_female = 0;
                }
                $avg_all = $tmp_all / $cnt_all;
            }
            $response->MALE = number_format($avg_male, 2);
            $response->FEMALE = number_format($avg_female, 2);
            $response->TOTAL = number_format($avg_all, 2);
            $response->INDICATOR = AppConst::SUMMARY_AVERAGE;
            $response->STATUS = AppConst::STATUS_SUCCESS;

            $stmt->close();
        } else {
            $response->STATUS = AppConst::STATUS_SQL_ERROR;
            $stmt->close();
        }
        return $response;
    }

    function GetSummaryAverage($examType) {
        if ($examType === AppConst::EXAM_TYPE_HANDGRIP) {
            return $this->GetSummaryAverageComplex($examType);
        } else if ($examType === AppConst::EXAM_TYPE_ASTRAND) {
            return $this->GetSummaryAverageComplex($examType);
        } else if ($examType === AppConst::EXAM_TYPE_OMRON) {
            return $this->GetSummaryAverageComplex($examType);
        } else if ($examType === AppConst::EXAM_TYPE_SKIN_FOLD) {
            return $this->GetSummaryAverageComplex($examType);
        } else {
            $response = new SummaryDetailResponse();
            $con = $this->dbObj->Open();
            $sql = "SELECT value, gender "
                    . " FROM mapexam "
                    . " JOIN map ON mapexam.applicationid = map.applicationid "
                    . " JOIN tester ON map.testerid = tester.testerid "
                    . " WHERE mapexam.examid = '" . $examType . "'";
            $stmt = $con->prepare($sql);
            $cnt_male = 0;
            $cnt_female = 0;
            $tmp_male = 0;
            $tmp_female = 0;
            $avg_male = 0;
            $avg_female = 0;
            $avg_all = 0;
            if ($stmt->execute()) {
                $stmt->store_result();
                $stmt->bind_result($value, $gender);
                while ($stmt->fetch()) {
                    if ($gender === AppConst::MALE) {
                        $cnt_male++;
                        $tmp_male += $value;
                    } else if ($gender === AppConst::FEMALE) {
                        $cnt_female++;
                        $tmp_female += $value;
                    } else {
                        
                    }
                }
                if ($stmt->num_rows > 0) {
                    $tmp_all = $tmp_female + $tmp_male;
                    $cnt_all = $cnt_female + $cnt_male;

                    $avg_male = $tmp_male / $cnt_male;
                    if ($cnt_female > 0) {
                        $avg_female = $tmp_female / $cnt_female;
                    } else {
                        $avg_female = 0;
                    }
                    $avg_all = $tmp_all / $cnt_all;
                }
                $response->MALE = number_format($avg_male, 2);
                $response->FEMALE = number_format($avg_female, 2);
                $response->TOTAL = number_format($avg_all, 2);
                $response->INDICATOR = AppConst::SUMMARY_AVERAGE;
                $response->STATUS = AppConst::STATUS_SUCCESS;

                $stmt->close();
            } else {
                $response->STATUS = AppConst::STATUS_SQL_ERROR;
                $stmt->close();
            }
            return $response;
        }
    }

    function GetSummaryMaxComplex($examType) {
        $response = new SummaryDetailResponse();
        $table = AppConst::TABLE_EXAM_RESULT;
        $con = $this->dbObj->Open();
        $sql = "SELECT distinct(map.applicationid), gender "
                . "FROM mapexam "
                . "JOIN map ON mapexam.applicationid = map.applicationid "
                . "JOIN tester ON map.testerid = tester.testerid "
                . "WHERE mapexam.examid LIKE  '" . $examType . "%' "
                . "GROUP BY gender, map.applicationid";
        $stmt = $con->prepare($sql);
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($appid, $gender);
            $cnt_male = 0;
            $cnt_female = 0;
            $max_m = 0;
            $max_f = 0;
            while ($stmt->fetch()) {
                $tester = new TesterHeaderInfo();
                $tester = $this->GetTesterDetailByAppID($appid);
                $examObj = new ExamObj();
                if ($examType === AppConst::EXAM_TYPE_HANDGRIP) {
                    $examObj = $this->CalculateHandGrip($appid, $tester);
                } else if ($examType === AppConst::EXAM_TYPE_ASTRAND) {
                    $examObj = $this->CalCulateAstrand($appid, $tester);
                } else if ($examType === AppConst::EXAM_TYPE_OMRON) {
                    $examObj = $this->CalCulateOmron($appid, $tester);
                } else if ($examType === AppConst::EXAM_TYPE_SKIN_FOLD) {
                    $examObj = $this->CalCulateGradeSkinFold($appid, $tester);
                }

                if ($gender === AppConst::MALE) {
                    $cnt_male++;
                    if ($examObj->GetTestValue() > $max_m) {
                        $max_m = $examObj->GetTestValue();
                    } else {
                        $max_m = $max_m;
                    }
                } else if ($gender === AppConst::FEMALE) {
                    $cnt_female++;
                    if ($examObj->GetTestValue() > $max_f) {
                        $max_f = $examObj->GetTestValue();
                    } else {
                        $max_f = $max_f;
                    }
                } else {
                    
                }
            }
            $response->MALE = number_format($max_m, 2);
            $response->FEMALE = number_format($max_f, 2);
            $response->INDICATOR = AppConst::SUMMARY_MAX;
            $response->STATUS = AppConst::STATUS_SUCCESS;

            $stmt->close();
        } else {
            $response->STATUS = AppConst::STATUS_SQL_ERROR;
            $stmt->close();
        }
        return $response;
    }

    function GetSummaryMax($examType) {
        if ($examType === AppConst::EXAM_TYPE_HANDGRIP) {
            return $this->GetSummaryMaxComplex($examType);
        } else if ($examType === AppConst::EXAM_TYPE_ASTRAND) {
            return $this->GetSummaryMaxComplex($examType);
        } else if ($examType === AppConst::EXAM_TYPE_OMRON) {
            return $this->GetSummaryMaxComplex($examType);
        } else if ($examType === AppConst::EXAM_TYPE_SKIN_FOLD) {
            return $this->GetSummaryMaxComplex($examType);
        } else {
            $response = new SummaryDetailResponse();
            $table = AppConst::TABLE_EXAM_RESULT;
            $con = $this->dbObj->Open();
            $sql = "SELECT MAX(value), gender "
                    . " FROM mapexam "
                    . " JOIN map ON mapexam.applicationid = map.applicationid "
                    . " JOIN tester ON map.testerid = tester.testerid "
                    . " WHERE mapexam.examid = '" . $examType . "'";
            $stmt = $con->prepare($sql);

            $tmp_male = 0;
            $tmp_female = 0;
            if ($stmt->execute()) {
                $stmt->store_result();
                $stmt->bind_result($value, $gender);
                while ($stmt->fetch()) {
                    if ($gender === AppConst::MALE) {
                        $tmp_male = $value;
                    } else if ($gender === AppConst::FEMALE) {
                        $tmp_female = $value;
                    } else {
                        
                    }
                }
                $response->MALE = number_format($tmp_male, 2);
                $response->FEMALE = number_format($tmp_female, 2);
                $response->INDICATOR = AppConst::SUMMARY_MAX;
                $response->STATUS = AppConst::STATUS_SUCCESS;

                $stmt->close();
            } else {
                $response->STATUS = AppConst::STATUS_SQL_ERROR;
                $stmt->close();
            }
            return $response;
        }
    }

    function GetSummaryMinComplex($examType) {
        $response = new SummaryDetailResponse();
        $table = AppConst::TABLE_EXAM_RESULT;
        $con = $this->dbObj->Open();
        $sql = "SELECT distinct(map.applicationid), gender "
                . "FROM mapexam "
                . "JOIN map ON mapexam.applicationid = map.applicationid "
                . "JOIN tester ON map.testerid = tester.testerid "
                . "WHERE mapexam.examid LIKE  '" . $examType . "%' "
                . "GROUP BY gender, map.applicationid";
        $stmt = $con->prepare($sql);
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($appid, $gender);
            $cnt_male = 0;
            $cnt_female = 0;
            $min_f = 9999;
            $min_m = 9999;
            while ($stmt->fetch()) {
                $tester = new TesterHeaderInfo();
                $tester = $this->GetTesterDetailByAppID($appid);
                $examObj = new ExamObj();
                if ($examType === AppConst::EXAM_TYPE_HANDGRIP) {
                    $examObj = $this->CalculateHandGrip($appid, $tester);
                } else if ($examType === AppConst::EXAM_TYPE_ASTRAND) {
                    $examObj = $this->CalCulateAstrand($appid, $tester);
                } else if ($examType === AppConst::EXAM_TYPE_OMRON) {
                    $examObj = $this->CalCulateOmron($appid, $tester);
                } else if ($examType === AppConst::EXAM_TYPE_SKIN_FOLD) {
                    $examObj = $this->CalCulateGradeSkinFold($appid, $tester);
                }

                if ($gender === AppConst::MALE) {
                    $cnt_male++;
                    if ($examObj->GetTestValue() < $min_m) {
                        $min_m = $examObj->GetTestValue();
                    } else {
                        $min_m = $min_m;
                    }
                } else if ($gender === AppConst::FEMALE) {
                    $cnt_female++;
                    if ($examObj->GetTestValue() < $min_f) {
                        $min_f = $examObj->GetTestValue();
                    } else {
                        $min_f = $min_f;
                    }
                } else {
                    
                }
            }
            if ($cnt_female === 0) {
                $min_f = 0;
            }
            if ($cnt_male === 0) {
                $min_m = 0;
            }
            $response->MALE = number_format($min_m, 2);
            $response->FEMALE = number_format($min_f, 2);
            $response->INDICATOR = AppConst::SUMMARY_MIN;
            $response->STATUS = AppConst::STATUS_SUCCESS;

            $stmt->close();
        } else {
            $response->STATUS = AppConst::STATUS_SQL_ERROR;
            $stmt->close();
        }
        return $response;
    }

    function GetSummaryMin($examType) {
        if ($examType === AppConst::EXAM_TYPE_HANDGRIP) {
            return $this->GetSummaryMinComplex($examType);
        } else if ($examType === AppConst::EXAM_TYPE_ASTRAND) {
            return $this->GetSummaryMinComplex($examType);
        } else if ($examType === AppConst::EXAM_TYPE_OMRON) {
            return $this->GetSummaryMinComplex($examType);
        } else if ($examType === AppConst::EXAM_TYPE_SKIN_FOLD) {
            return $this->GetSummaryMinComplex($examType);
        } else {
            $response = new SummaryDetailResponse();
            $table = AppConst::TABLE_EXAM_RESULT;
            $con = $this->dbObj->Open();
            $sql = "SELECT MIN(value), gender "
                    . " FROM mapexam "
                    . " JOIN map ON mapexam.applicationid = map.applicationid "
                    . " JOIN tester ON map.testerid = tester.testerid "
                    . " WHERE mapexam.examid = '" . $examType . "'";
            $stmt = $con->prepare($sql);

            $tmp_male = 0;
            $tmp_female = 0;
            if ($stmt->execute()) {
                $stmt->store_result();
                $stmt->bind_result($value, $gender);
                while ($stmt->fetch()) {
                    if ($gender === AppConst::MALE) {
                        $tmp_male = $value;
                    } else if ($gender === AppConst::FEMALE) {
                        $tmp_female = $value;
                    } else {
                        
                    }
                }

                $response->MALE = number_format($tmp_male, 2);
                $response->FEMALE = number_format($tmp_female, 2);
                $response->INDICATOR = AppConst::SUMMARY_MIN;
                $response->STATUS = AppConst::STATUS_SUCCESS;

                $stmt->close();
            } else {
                $response->STATUS = AppConst::STATUS_SQL_ERROR;
                $stmt->close();
            }
            return $response;
        }
    }

    function GetDDLExamType() {
        $table = AppConst::TABLE_EXAM_MASTER;
        $table2 = AppConst::TABLE_FILTER_TABLE;
        $sql = "select examid,tdescription from `$table` where examid not "
                . "in (select examid from `$table2` where active = 1)";
        $con = $this->dbObj->Open();
        $stmt = $con->prepare($sql);
        $res = "";
        //echo $sql;
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($id, $desc);
            while ($stmt->fetch()) {
                $res .= "<option value = '" . $id . "'>" . $desc . "</option>";
            }
        } else {
            $res .= "<option value = '0'>Error no data found<option>";
        }
        return $res;
    }

    function GetExamDetailByAppID($appID) {
        $con = $this->dbObj->Open();
        $responseHeader = new ResponseHeader();
        $sql = "select examid, seqno, value, applydate "//Add select isResearch
                . " from mapexam "
                . " join map on mapexam.applicationid = map.applicationid"
                . " where map.applicationid = '" . $appID . "' ";
        //echo $sql;
        $stmt = $con->prepare($sql);
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($examID, $seqNo, $value, $dt);
            while ($stmt->fetch()) {
                $examObj = new ExamObj();
                $examObj->SetAppID($appID);
                $examObj->SetExamID($examID);
                $examObj->SetTestValue($value);
                $examObj->SetSeqNo($seqNo);
                $examObj->SetTestDate($this->ConvertYMD($dt));
                $responseHeader->ResponseDetail[$examID][$seqNo] = $examObj;
            }
            $responseHeader->MSGID = AppConst::STATUS_SUCCESS;
            $stmt->close();
        } else {
            $responseHeader->MSGID = AppConst::STATUS_SQL_ERROR;
            $responseHeader->MSGDETAIL = "From function GetExamDetailByAppID " . htmlspecialchars($stmt->error);
            $stmt->close();
        }
        return $responseHeader;
    }

    function UpdateMapExam(ExamObj $obj) {
        $con = $this->dbObj->Open();
        $table = AppConst::TABLE_EXAM_RESULT;
        $sql = "INSERT INTO `$table`  VALUES ('" . $obj->GetAppID() . "', '" . $obj->GetExamID() . "', '" . $obj->GetSeqNo() . "', '" . $obj->GetTestValue() . "') "
                . " on duplicate key update value = '" . $obj->GetTestValue() . "'";
        // $stmt->bind_param("ssis", $data->GetAppID(), $data->GetExamID(), $data->GetSeqNo(), $data->GetTestValue());
        //$sql = "update `$table` set value = ? where applicationid = ? and examid = ? and seqno = ?";
        $stmt = $con->prepare($sql);
        $res = new ResponseHeader();
        //$stmt->bind_param("dssi", $obj->GetTestValue(), $obj->GetAppID(), $obj->GetExamID(), $obj->GetSeqNo());
        if ($stmt->execute()) {
            $stmt->close();
            $res->MSGID = AppConst::STATUS_SUCCESS;
        } else {
            $res->MSGID = AppConst::STATUS_ERROR;
            $res->MSGDETAIL = "From function UpdateMapExam " . htmlspecialchars($stmt->error);
            $stmt->close();
        }
        return $res;
    }

    function UpdateTesterDetail(TesterHeaderInfo $obj) {
        $con = $this->dbObj->Open();
        $table = AppConst::TABLE_TESTER;
        $res = new ResponseHeader();
        $sql = "update `$table` set tname = '" . $obj->GetNameTH() . "', "
                . " tsurname = '" . $obj->GetSurnameTH() . "', "
                . " ename = '" . $obj->GetNameEN() . "', "
                . " esurname = '" . $obj->GetSurnameEN() . "', "
                . " gender = '" . $obj->GetSex() . "', "
                . " birthday = '" . $obj->GetDOB() . "' "
                . " where testerid = '" . $obj->GetTesterID() . "'";
        $stmt = $con->prepare($sql);
        //echo $sql;
        if ($stmt->execute()) {
            $stmt->close();
            $res->MSGID = AppConst::STATUS_SUCCESS;
        } else {
            $res->MSGID = AppConst::STATUS_ERROR;
            $res->MSGDETAIL = "From function UpdateTesterDetail " . htmlspecialchars($stmt->error);
            //$result = AppConst::STATUS_ERROR;
            $stmt->close();
        }
        return $res;
    }

    function ConvertMonth($month) {
        if ($month === "01") {
            return "มกราคม";
        } else if ($month === "02") {
            return "กุมภาพันธ์";
        } else if ($month === "03") {
            return "มีนาคม";
        } else if ($month === "04") {
            return "เมษายน";
        } else if ($month === "05") {
            return "พฤษภาคม";
        } else if ($month === "06") {
            return "มิถุนายน";
        } else if ($month === "07") {
            return "กรกฏาคม";
        } else if ($month === "08") {
            return "สิงหาคม";
        } else if ($month === "09") {
            return "กันยายน";
        } else if ($month === "10") {
            return "ตุลาคม";
        } else if ($month === "11") {
            return "พฤศจิกายน";
        } else if ($month === "12") {
            return "ธันวาคม";
        } else {
            return "รูปแบบเดือนผิด";
        }
    }

    function ConvertYMD($dt) {
        $begin = new DateTime($dt);
        $begin->format('Y-m-d');
        $res = $begin->format('d') . " ";
        $res .= $this->ConvertMonth($begin->format('m')) . " ";
        $res .= $this->ConvertYear($begin->format('Y')) . " ";
        return $res;
    }

    function ConvertYear($year) {
        return $year + 543;
    }

    function CalculateAge($dob) {
        $tz = new DateTimeZone('Asia/Bangkok');
        $age = DateTime::createFromFormat('Y-m-d', $dob, $tz)
                        ->diff(new DateTime('now', $tz))
                ->y;
        return $age;
    }

    function GetDDLAstrandLoad() {
        $res = "<option value = '0'>กรุณาเลือก</value>";
        $arrs = array(1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5);
        foreach ($arrs as $value) {
            $res .= "<option value = '" . $value . "'>" . $value . "</value>";
        }
        return $res;
    }

    function checkSession($sessionid) {
        $con = $this->dbObj->Open();
        $responseHeader = new ResponseHeader();
        $sql = "SELECT tname, tsurname ,ename , esurname"
                . " FROM userlogging   "
                . " JOIN userinfo ON userlogging.userid = userinfo.userid"
                . " WHERE userlogging.sessionid = '" . $sessionid . "' "
                . " AND userlogging.delflag = 0 ";
        //echo $sql;
        $stmt = $con->prepare($sql);
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($tname, $tsurname, $ename, $esurname);
            while ($stmt->fetch()) {
                $testObj = new TesterHeaderInfo();
                $testObj->SetNameTH($tname);
                $testObj->SetSurnameTH($tsurname);
                $testObj->SetNameEN($ename);
                $testObj->SetSurnameEN($esurname);
                $responseHeader->ResponseDetail[] = $testObj;
            }
            $responseHeader->MSGID = AppConst::STATUS_SUCCESS;
            $stmt->close();
        } else {
            $responseHeader->MSGID = AppConst::STATUS_SQL_ERROR;
            $responseHeader->MSGDETAIL = "From function checkSession " . htmlspecialchars($stmt->error);
            $stmt->close();
        }
        return $responseHeader;
    }

    function GetExamDescriptionByExamID($examid) {
        $con = $this->dbObj->Open();
        $responseHeader = new ResponseHeader();
        $sql = "select tdescription, edescription, uom "
                . " from exam "
                . " where examid = '" . $examid . "' ";
        //echo $sql;
        $stmt = $con->prepare($sql);
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($tdesc, $edesc, $uom);
            while ($stmt->fetch()) {
                $responseHeader->MSGDETAIL = $tdesc;
                $responseHeader->MSGDETAIL2 = $edesc;
                if ($uom == 'null' || $uom == null || $uom == '') {
                    $uom = '-';
                }
                $responseHeader->MSGDETAIL3 = $uom;
            }
            $responseHeader->MSGID = AppConst::STATUS_SUCCESS;
            $stmt->close();
        } else {
            $responseHeader->MSGID = AppConst::STATUS_SQL_ERROR;
            $responseHeader->MSGDETAIL = "From function GetExamDescriptionByExamID " . htmlspecialchars($stmt->error);
            $stmt->close();
        }
        return $responseHeader;
    }

    function GetGraphForPrintByAppID($appid, $examid, ExamObj $examObj) {
        /* $sql = "SELECT a.examid, TIMESTAMPDIFF( YEAR, c.birthday, CURDATE( ) ) age, b.grade "
          . " FROM mapexam a "
          . " JOIN map d ON d.applicationid = a.applicationid "
          . " JOIN tester c ON c.testerid = d.testerid "
          . " LEFT JOIN MasterData b ON a.examid = b.examid "
          . " AND b.age_from > TIMESTAMPDIFF( YEAR, c.birthday, CURDATE( ) )  "
          . " AND b.age_to <= TIMESTAMPDIFF( YEAR, c.birthday, CURDATE( ) )  "
          . " AND b.min_val >= a.value "
          . " AND b.max_val <= a.value "
          . " WHERE a.applicationid =  ? "
          . " AND a.examid =  ? "
          . " AND a.applicationid = d.applicationid "
          . " AND d.testerid = c.testerid"; */
        $sql = "SELECT c.examid,grade,count(grade) "
                . " FROM application a "
                . " JOIN  application b on a.groupid = b.groupid "
                . " JOIN tester e on b.testerid = e.testerid "
                . " JOIN  mapexam c on b.applicationid = c.applicationid "
                . " LEFT JOIN MasterData d ON c.examid = d.examid "
                . " AND d.age_from > TIMESTAMPDIFF( YEAR, e.birthday, CURDATE( ) ) "
                . " AND d.age_to <= TIMESTAMPDIFF( YEAR, e.birthday, CURDATE( ) ) "
                . " AND d.min_val >= c.value "
                . " AND d.max_val <= c.value "
                . " WHERE a.applicationid = '" . $appid . "' "
                . " AND c.examid = '" . $examid . "' "
                . " group by examid,grade"
                . " order by grade asc";
        //echo "<br>-------------------<br>" . $sql . "<br><br>";
        $con = $this->dbObj->Open();
        $stmt = $con->prepare($sql);
        // $stmt->bind_param("ss", $appid, $examid);
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($id, $grade, $cnt);
            $grade1 = 0;
            $grade2 = 0;
            $grade3 = 0;
            $grade4 = 0;
            $grade5 = 0;
            $grade6 = 0;
            $grade0 = 0;
            while ($stmt->fetch()) {
                /* $arr[$grade] = $cnt;
                  $str .= $cnt; */
                switch ($grade) {
                    case 1 : $grade1++;
                        break;
                    case 2 : $grade2++;
                        break;
                    case 3 : $grade3++;
                        break;
                    case 4 : $grade4++;
                        break;
                    case 5 : $grade5++;
                        break;
                    case 6 : $grade6++;
                        break;
                    default : $grade0++;
                }
            }
            $examObj->SetExamGraph("[" . $grade1 . "," . $grade2 . "," . $grade3 . "," . $grade4 . "," . $grade5 . "," . $grade6 . "]");
        } else {
            $examObj->SetExamGraph("[0,0,0,0,0,0]");
        }
        $stmt->close();
        //echo '<pre>'; print_r($response); echo '</pre>';
        //print_r($response);
        return $examObj;
    }

    function GetCalculatedExamGraph($appID, $trigger) {
        $rows = array();
        $rows = $this->GetAllAppIDByAppID($appID);
        $grade1 = 0;
        $grade2 = 0;
        $grade3 = 0;
        $grade4 = 0;
        $grade5 = 0;
        $grade6 = 0;
        $grade0 = 0;
        foreach ($rows as $row) {
            $testerObj = new TesterHeaderInfo();
            $testerObj = $this->GetTesterDetailByAppID($appID);
            $examObj = new ExamObj();
            //echo "app id = " . $row["applicationid"] . "<br>";
            if ($trigger === AppConst::EXAM_TYPE_ASTRAND) {
                $examObj = $this->CalCulateAstrand($row["applicationid"], $testerObj);
            } else if ($trigger === AppConst::EXAM_TYPE_SKIN_FOLD) {
                $examObj = $this->CalCulateGradeSkinFold($row["applicationid"], $testerObj);
            } else if ($trigger === AppConst::EXAM_TYPE_OMRON) {
                $examObj = $this->CalCulateOmron($row["applicationid"], $testerObj);
            } else if ($trigger === AppConst::EXAM_TYPE_HANDGRIP) {
                $examObj = $this->CalculateHandGrip($row["applicationid"], $testerObj);
            } else if ($trigger === AppConst::EXAM_TYPE_MULTISTAGE) {
                $examObj = $this->CalculateMultiStage($row["applicationid"], $testerObj);
            } else if ($trigger === AppConst::EXAM_TYPE_ONE_LEG_BAL_L || $trigger === AppConst::EXAM_TYPE_ONE_LEG_BAL_R) {
                $examObj = $this->CalculateOneLegBalance($row["applicationid"], $trigger, $testerObj);
            } else {
                if (in_array($trigger, $this->GetShowExamListInfographic())) {
                    if ($trigger === AppConst::EXAM_TYPE_ONE_LEG_BAL_L || $trigger === AppConst::EXAM_TYPE_ONE_LEG_BAL_R) {
                        $examObj->SetExamID(AppConst::EXAM_TYPE_ONE_LEG_BAL);
                        $examObj = $this->GetScoreGrade($examObj, $tester, $age);
                        //echo "<br> grade = ".$examObj->GetGradeID()."<br>";
                    } else {
                        $examObj = $this->GetScoreGradeNew($appID, $trigger, $examObj);
                    }
                }
            }
            if ($examObj != null) {
                $grade = $examObj->GetGradeID();
                /* echo "<br><br>";
                  echo '<pre>'.print_r($examObj). '</pre>'; */
                switch ($grade) {
                    case 1 : $grade1++;
                        break;
                    case 2 : $grade2++;
                        break;
                    case 3 : $grade3++;
                        break;
                    case 4 : $grade4++;
                        break;
                    case 5 : $grade5++;
                        break;
                    case 6 : $grade6++;
                        break;
                    default : $grade0++;
                }
            }
        }
        return "[" . $grade1 . "," . $grade2 . "," . $grade3 . "," . $grade4 . "," . $grade5 . "," . $grade6 . "]";
        /* echo "grade 1 = " . $grade1 . "<br>";
          echo "grade 2 = " . $grade2 . "<br>";
          echo "grade 3 = " . $grade3 . "<br>";
          echo "grade 4 = " . $grade4 . "<br>";
          echo "grade 5 = " . $grade5 . "<br>";
          echo "grade 6 = " . $grade6 . "<br>";
          echo "grade 0 = " . $grade0 . "<br>"; */
    }

    function GetAllAppIDByAppID($appID) {
        $sql = "select b.applicationid  "
                . " from application a  "
                . " join application b on a.groupid = b.groupid "
                . " where a.applicationid  = '" . $appID . "' ";
        //echo $sql;
        $con = $this->dbObj->Open();
        $result = $con->query($sql);
        if (!$result) {
            $rows[0] = AppConst::STATUS_SQL_ERROR;
            $rows[1] = $con->error();
            return $rows;
        }

        while ($row = $result->fetch_array()) {
            $rows[] = $row;
        }
        return $rows;
    }

    function GetDDLMultistageLevel() {
        $option = "";
        for ($i = 1; $i <= 21; $i++) {
            $option .= "<option value = '" . $i . "'>Level " . $i . "</option>";
        }
        return $option;
    }

    function GetDDLMultistageStage() {
        $option = "";
        for ($i = 1; $i <= 14; $i++) {
            $option .= "<option value = '" . $i . "'>Stage " . $i . "</option>";
        }
        return $option;
    }

    function GetPreviousTestScore($testerid, ExamObj $examobj, $appID) {
        $sql = "SELECT value, completedate "
                . " FROM mapexam "
                . " JOIN application ON mapexam.applicationid = application.applicationid "
                . " WHERE  completedate < (select completedate from application where applicationid = '" . $appID . "' ) "
                . " AND testerid =  '" . $testerid . "' "
                . " AND examid =  '" . $examobj->GetExamID() . "' "
                . " GROUP BY completedate "
                . " ORDER BY completedate DESC "
                . " LIMIT 0 , 1";
        //. " WHERE testerid =  ? "
        // . " AND examid =  ? "
        $con = $this->dbObj->Open();
        //echo "<br><br>" . $sql . "<br><br>";
        $stmt = $con->prepare($sql);
        //$stmt->bind_param("ss", $testerid, $examobj->GetExamID());
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($value, $completedate);
            if ($stmt->num_rows > 0) {
                while ($stmt->fetch()) {
                    $examobj->SetPreviousScore($value);
                }
            } else {
                $examobj->SetPreviousScore("-");
            }
        } else {
            $examobj->SetPreviousScore("-");
        }
        return $examobj;
    }

    function GetShowExamListInfographic() {
        $con = $this->dbObj->Open();
        $sql = "select examid from exam where showinfog = '1' ";
        $stmt = $con->prepare($sql);
        $res = array();
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($examid);
            while ($stmt->fetch()) {
                $res[] = $examid;
            }
        }
        return $res;
    }

    function GetTesterIDKey() {
        $con = $this->dbObj->Open();
        $sql = "SELECT ( SELECT COUNT( DISTINCT a.testerid ) "
                . "FROM tester a, map b "
                . "WHERE a.testerid = b.testerid "
                . "AND MONTH( b.applydate ) = MONTH( CURDATE( ) ) ) "
                . "AS subquery";
        $stmt = $con->prepare($sql);
        $res = 0;
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($key);
            while ($stmt->fetch()) {
                $res = $key;
            }
        } else {
            $res = AppConst::STATUS_ERROR;
        }
        return $res;
    }

    function UpdatePassword($userID, $password) {
        $res = new ResponseHeader();
        $sql = "update user Set password = md5('" . $password . "'), loginfail = 0 where userid = '" . $userID . "' ";
        $con = $this->dbObj->Open();
        $stmt = $con->prepare($sql);
        //$stmt->bind_param("ss", md5($password), $userID);
        if ($stmt->execute()) {
            $res->MSGID = AppConst::STATUS_SUCCESS;
        } else {
            $res->MSGID = AppConst::STATUS_ERROR;
            $res->MSGDETAIL = "from function UpdatePassword";
            $res->MSGDETAIL2 = htmlspecialchars($stmt->error);
        }
        $stmt->close();
        return $res;
    }

    function DeleteUserLogbyID($userID) {
        $result = new ResponseHeader();
        $con = $this->dbObj->Open();
        $table = AppConst::TABLE_USER_LOGGING;
        $sql = "delete from `$table` where userid = ?";
        $stmt = $con->prepare($sql);
        $stmt->bind_param("s", $userID);
        if ($stmt->execute()) {
            $stmt->close();
            $result->MSGID = AppConst::STATUS_SUCCESS;
        } else {
            $result->MSGDETAIL = "From function DeleteUserLogbyID " . htmlspecialchars($stmt->error);
            $result->MSGID = AppConst::STATUS_ERROR;
            $stmt->close();
        }
        return $result;
    }

    function GetUsernameByUserID($userID, $password) {
        $res = new ResponseHeader();
        $sql = "select username from user where userid = '" . $userID . "' and password = '" . $password . "'";
        $con = $this->dbObj->Open();
        $stmt = $con->prepare($sql);
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($username);
            while ($stmt->fetch()) {
                $res->MSGDETAIL = $username;
            }
            $res->MSGID = AppConst::STATUS_SUCCESS;
        } else {
            $res->MSGID = AppConst::STATUS_ERROR;
            $res->MSGDETAIL = "From function GetUsernameByUserID " . htmlspecialchars($stmt->error);
        }
        $stmt->close();
        return $res;
    }

    /*
     * Phase 2 Start Here
     */

    function GetExamCategory() {
        $res = array();
        //$sql = " select distinct(category) from exam where category != 'examcat999' and category != 'examcat00' ";
        $sql = " select distinct(category) from exam where category != 'examcat999' ";
        $con = $this->dbObj->Open();
        $stmt = $con->prepare($sql);
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($category);
            while ($stmt->fetch()) {
                $res[$category] = service::mapExamcategoryDescription($category);
            }
        }
        $stmt->close();
        return $res;
    }

    function GetExamDetailObject() {
        $res = new ResponseHeader();
        $categorys = $this->GetExamCategory();
        //$res->ResponseDetail[0] = $categorys;
        $con = $this->dbObj->Open();
        foreach ($categorys as $key => $value) {
            $sql = "select examid,tdescription,edescription,category from exam where category = '" . $key . "' and showinfog = '1' ";
            $stmt = $con->prepare($sql);
            if ($stmt->execute()) {
                $stmt->store_result();
                $stmt->bind_result($examid, $tdesc, $edesc, $cat);
                while ($stmt->fetch()) {
                    $examobj = new ExamObj();
                    $examobj->SetExamID($examid);
                    $examobj->SetTDescription($tdesc);
                    $examobj->SetEDescription($edesc);
                    $examobj->SetExamCat($cat);
                    $res->ResponseDetail[$key][] = $examobj;
                }
                $res->MSGID = AppConst::STATUS_SUCCESS;
            } else {
                $res->MSGID = AppConst::STATUS_ERROR;
            }
        }
        service::printr($res);
        $stmt->close();
        return $res;
    }

    function InsertExamTemplate($examid, $groupid) {
        $sql = "insert into examtemplete values ('" . $examid . "','" . $groupid . "') ";
        $con = $this->dbObj->Open();
        $stmt = $con->prepare($sql);
        $result = new ResponseHeader();
        //$stmt->bind_param("si", $examid, $groupid);
        //echo $sql;
        if ($stmt->execute()) {
            $stmt->close();
            $result->MSGID = AppConst::STATUS_SUCCESS;
        } else {
            $result->MSGID = AppConst::STATUS_ERROR;
            $result->MSGDETAIL = "From function InsertExamTemplate " . htmlspecialchars($stmt->error);
            $stmt->close();
        }
        return $result;
    }

    function GetTemplateExamByGroupID($grpid) {
        $res = new ResponseHeader();
        $con = $this->dbObj->Open();
        $sql = " select examid from examtemplete where groupid = '" . $grpid . "' ";
        $stmt = $con->prepare($sql);
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($examid);
            while ($stmt->fetch()) {
                //$examobj = new ExamObj();
                //$examobj->SetExamID($examid);
                $res->ResponseDetail[] = $examid;
            }
            $res->MSGID = AppConst::STATUS_SUCCESS;
        } else {
            $res->MSGID = AppConst::STATUS_ERROR;
        }
        $stmt->close();
        return $res;
    }

    function GetReportDataTable($datefrom, $dateto, $gender, $agefrom, $ageto, $isresearch, $grpTest) {
        $appids = $this->GetApplicationIDs($datefrom, $dateto, $gender, $agefrom, $ageto, $isresearch, $grpTest);
        if (count($appids) > 0) {
            $response = new ResponseHeader();
            $response->MSGID = AppConst::STATUS_SUCCESS;
            foreach ($appids as $appid) {
                if ($response->MSGID === AppConst::STATUS_SUCCESS) {
                    $data = $this->CalculateExamResultToDataTable($appid);
                    $response->MSGDETAIL[$appid] = $data->ResponseDetail;
                    $response->MSGID = $data->MSGID;
                }
            }
        } else {
            $response = new ResponseHeader();
            $response->MSGID = AppConst::STATUS_SQL_NODATA;
        }

        //service::printr($response);
        //$sql = "";
        return $response;
    }

    function GetExamListbyAppIDs($datefrom, $dateto, $gender, $agefrom, $ageto, $isresearch, $grpTest) {
        $appids = $this->GetApplicationIDs($datefrom, $dateto, $gender, $agefrom, $ageto, $isresearch, $grpTest);
        $infographic_examlist = $this->GetShowExamListInfographic();
        //$response = new ResponseHeader();
        $con = $this->dbObj->Open();
        $arr = array();
        if (count($appids) > 0) {
            foreach ($appids as $appid) {
                $sql = " select exam.examid, exam.tdescription, exam.edescription from mapexam "
                        . " join exam on mapexam.examid = exam.examid "
                        . " where mapexam.applicationid = '" . $appid . "' "
                        //. " and category != 'examcat00' "
                        . " order by category ";
                //echo "<br>" . $sql . "<br>";
                $stmt = $con->prepare($sql);
                if ($stmt->execute()) {
                    $stmt->store_result();
                    $stmt->bind_result($examid, $tdesc, $edesc);
                    while ($stmt->fetch()) {
                        if (!in_array($examid, $arr)) {
                            $arr[$examid] = $tdesc . "( " . $edesc . " )";
                            $arr[$examid . "Norm"] = "Norm " . $edesc;
                        }
                        $subexamlist = $this->GetSubExamlist($examid);
                        foreach ($subexamlist as $key => $value) {
                            if (!in_array($key, $arr)) {
                                $arr[$key] = $value;
                            }
                        }
                    }
                }
            }
            $stmt->close();
        }
        foreach ($infographic_examlist as $row) {
            if (!array_key_exists($row, $arr)) {
                $tmp = $this->GetExamDescriptionByExamID($row);
                $arr[$row] = $tmp->MSGDETAIL . "( " . $tmp->MSGDETAIL2 . " )";
                $arr[$row . "Norm"] = "Norm " . $tmp->MSGDETAIL2;
            }
        }
        ksort($arr);
        //$examlists = array_merge($arr,$infographic_examlist);
        return $arr;
    }

    function GetSubExamlist($examid) {
        $con = $this->dbObj->Open();
        $sql = " select exam.examid, exam.tdescription, exam.edescription "
                . " from exam "
                . " where examid like '%" . $examid . "%'"
                . " and examid != '" . $examid . "' ";
        $stmt = $con->prepare($sql);
        $arr = array();
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($exam, $tdesc, $edesc);
            while ($stmt->fetch()) {

                $arr[$exam] = $tdesc . "( " . $edesc . " )";
            }
        }
        $stmt->close();
        return $arr;
    }

    function GetApplicationIDs($datefrom, $dateto, $gender, $agefrom, $ageto, $isresearch, $grpTest) {
        if ($isresearch == '0' || $isresearch == null) {
            $researchcond = "";
        } else {
            $researchcond = " and isResearch = '" . $isresearch . "' ";
        }

        if ($gender == 'M' || $gender == 'F') {
            $gendercond = " and gender = '" . $gender . "' ";
        } else {
            $gendercond = "";
        }

        if ($agefrom == '' || $agefrom == null || $ageto == '' || $ageto == null) {
            $agecond = "";
        } else {
            $agecond = " and TIMESTAMPDIFF( YEAR, birthday, CURDATE( ) ) between '" . $agefrom . "' and '" . $ageto . "' ";
        }

        if ($grpTest == '0' || $grpTest == null) {
            $grpJoin = "";
            $grpcond = "";
        } else {
            $grpJoin = " join application on map.applicationid = application.applicationid ";
            $grpcond = " and groupid = '" . $grpTest . "' ";
        }

        $sql = "select map.applicationid from map "
                . $grpJoin
                . " join tester on map.testerid = tester.testerid"
                . $agecond
                . $gendercond
                . " where applydate between '" . $datefrom . "' and '" . $dateto . "' "
                . $researchcond
                . $grpcond;
        //echo $sql;
        $res = array();
        $con = $this->dbObj->Open();
        $stmt = $con->prepare($sql);
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($appid);
            while ($stmt->fetch()) {
                $res[] = $appid;
            }
        }
        $stmt->close();
        return $res;
    }

    function CheckGroupIDisExist($grpid) {
        $sql = "select count(groupid) from groups where groupid = '" . $grpid . "' ";
        //echo $sql;
        $con = $this->dbObj->Open();
        $stmt = $con->prepare($sql);
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($cnt);
            while ($stmt->fetch()) {
                //echo $cnt;
                if ($cnt > 0) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        return false;
    }

    function CreateNewGroup($grpid) {
        $sql = "insert into groups values (DEFAULT,'".$grpid."') ";
        //echo $sql;
        $con = $this->dbObj->Open();
        $stmt = $con->prepare($sql);
        //$stmt->bind_param("s", $grpid);
        $result = new ResponseHeader();
        if ($stmt->execute()) {

            $result->MSGID = AppConst::STATUS_SUCCESS;
            $result->MSGDETAIL = $stmt->insert_id;
            $stmt->close();
        } else {
            $result->MSGID = AppConst::STATUS_ERROR;
            $result->MSGDETAIL = "From function CreateNewGroup " . htmlspecialchars($stmt->error);
            $stmt->close();
        }
        return $result;
    }

    function CheckExamTemplateIsExist($grpid, $examid) {
        $sql = "select count(examid) from examtemplete where examid = '" . $examid . "' and groupid = '" . $grpid . "' ";
        $con = $this->dbObj->Open();
        $stmt = $con->prepare($sql);
        //echo $sql;
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($cnt);
            while ($stmt->fetch()) {
                if ($cnt > 0) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        return false;
    }

    function DeleteExamTemplate($grpid) {
        $sql = "delete from examtemplete where groupid = '" . $grpid . "' ";
        $con = $this->dbObj->Open();
        $stmt = $con->prepare($sql);
        $stmt->execute();
        $stmt->close();
        return;
    }

    /*
     * This function is copy from RequestForPrintByAppIDNew
     */

    function CalculateExamResultToDataTable($appID) {
        $response = new ResponseHeader();
        $condition = "";
        //$condition = " and examid in (select examid from filter_table where active = 2) ";
        $exams = $this->GetMapExamByAppID($appID, $condition); //ได้ทุกการทดสอบใน app id นั้นมา
        $calculateExam = $this->GetExamCalculateList();
        $tester = new TesterHeaderInfo();
        $tester = $this->GetTesterDetailByAppID($appID);
        $bmi = $this->CalculateBMI($appID, $tester);
        $omron = $this->CalCulateOmron($appID, AppConst::EXAM_TYPE_OMRON_WBODYFAT, $tester);
        /* $omron_vis = $this->CalCulateOmron($appID, AppConst::EXAM_TYPE_OMRON_VISCERAL,  $tester);
          $omron_wf = $this->CalCulateOmron($appID, AppConst::EXAM_TYPE_OMRON_WBODYFAT,  $tester);
          $omron_wm = $this->CalCulateOmron($appID, AppConst::EXAM_TYPE_OMRON_WBODYMUSCLE,  $tester); */
        $skinfold = $this->CalCulateGradeSkinFold($appID, $tester);
        $handGrip = $this->CalculateHandGrip($appID, $tester);
        $astrand = $this->CalCulateAstrand($appID, $tester);
        $multiStage = $this->CalculateMultiStage($appID, $tester);
        $one_leg_L = $this->CalculateOneLegBalance($appID, AppConst::EXAM_TYPE_ONE_LEG_BAL_L, $tester);
        $one_leg_R = $this->CalculateOneLegBalance($appID, AppConst::EXAM_TYPE_ONE_LEG_BAL_R, $tester);
        $legstrength = $this->CalculateLegStrength($appID, $tester);
        $starlist = $this->CalculateStarExcursion($appID, $tester);
        $backscratch = $this->CalculateBackScratch($appID, AppConst::EXAM_TYPE_BACK_STRACH, $tester);
        $SixminsWalk = $this->Calculate6MinsWalkTest($appID, $tester);
        //$backscratch_R = $this->CalculateBackScratch($appID, AppConst::EXAM_TYPE_BACK_STRACH_RIGHT, $tester);

        if ($starlist != null || !empty($starlist)) {
            foreach ($starlist as $key => $star) {
                $response->ResponseDetail[] = $star;
                $response->ResponseDetail[] = $this->GetExamNormalization($tester, $key);
            }
            /*
             * For Star Length Leg
             */
            $response->ResponseDetail[] = $this->GetRawTestDataAsObject(AppConst::EXAM_TYPE_STAR_LEG_LENGTH, $appID, 1)[0];
            //service::DecompressAndMerge($response->ResponseDetail, $this->GetRawTestDataAsObject(AppConst::EXAM_TYPE_STAR_LEG_LENGTH, $appID, 1));
        }

        if ($omron !== null) {
            $response->ResponseDetail[] = $omron;
            $response->ResponseDetail = service::DecompressAndMerge($response->ResponseDetail, $this->GetRawTestDataAsObject(AppConst::EXAM_TYPE_OMRON, $appID, 0));
            $response->ResponseDetail[] = $this->GetExamNormalization($tester, AppConst::EXAM_TYPE_OMRON);
            $response->ResponseDetail[] = $this->GetExamNormalization($tester, AppConst::EXAM_TYPE_OMRON_WBODYFAT);
            $response->ResponseDetail[] = $this->GetExamNormalization($tester, AppConst::EXAM_TYPE_OMRON_WBODYMUSCLE);
            $response->ResponseDetail[] = $this->GetExamNormalization($tester, AppConst::EXAM_TYPE_OMRON_VISCERAL);
        }
        //print_r($skinfold);
        if ($skinfold !== null) {
            $response->ResponseDetail[] = $skinfold;
            $response->ResponseDetail = service::DecompressAndMerge($response->ResponseDetail, $this->GetRawTestDataAsObject(AppConst::EXAM_TYPE_SKIN_FOLD, $appID, 0));
            $response->ResponseDetail[] = $this->GetExamNormalization($tester, AppConst::EXAM_TYPE_SKIN_FOLD);
        }
        //$response->ResponseDetail[] = $handGrip;
        if ($handGrip !== null) {
            $response->ResponseDetail[] = $handGrip;
            $response->ResponseDetail = service::DecompressAndMerge($response->ResponseDetail, $this->GetRawTestDataAsObject(AppConst::EXAM_TYPE_HANDGRIP, $appID, 0));
            $response->ResponseDetail[] = $this->GetExamNormalization($tester, AppConst::EXAM_TYPE_HANDGRIP);
        }
        if ($astrand !== null) {
            $response->ResponseDetail[] = $astrand;
            $response->ResponseDetail = service::DecompressAndMerge($response->ResponseDetail, $this->GetRawTestDataAsObject(AppConst::EXAM_TYPE_ASTRAND, $appID, 0));
            $response->ResponseDetail[] = $this->GetExamNormalization($tester, AppConst::EXAM_TYPE_ASTRAND);
        }
        if ($bmi !== null) {
            $response->ResponseDetail[] = $bmi;
        }

        if ($multiStage !== null) {
            $response->ResponseDetail[] = $multiStage;
            $response->ResponseDetail = service::DecompressAndMerge($response->ResponseDetail, $this->GetRawTestDataAsObject(AppConst::EXAM_TYPE_MULTISTAGE, $appID, 0));
            $response->ResponseDetail[] = $this->GetExamNormalization($tester, AppConst::EXAM_TYPE_MULTISTAGE);
        }

        if ($one_leg_L !== null) {
            $response->ResponseDetail[] = $one_leg_L;
            $response->ResponseDetail = service::DecompressAndMerge($response->ResponseDetail, $this->GetRawTestDataAsObject(AppConst::EXAM_TYPE_ONE_LEG_BAL_L, $appID, 0));
            $response->ResponseDetail[] = $this->GetExamNormalization($tester, AppConst::EXAM_TYPE_ONE_LEG_BAL_L);
        }

        if ($one_leg_R !== null) {
            $response->ResponseDetail[] = $one_leg_R;
            $response->ResponseDetail = service::DecompressAndMerge($response->ResponseDetail, $this->GetRawTestDataAsObject(AppConst::EXAM_TYPE_ONE_LEG_BAL_R, $appID, 0));
            $response->ResponseDetail[] = $this->GetExamNormalization($tester, AppConst::EXAM_TYPE_ONE_LEG_BAL_R);
        }

        if ($legstrength !== null) {
            $response->ResponseDetail[] = $legstrength;
            //Comment for correct display on 2018-01-05 by Puwakit
            //$response->ResponseDetail = service::DecompressAndMerge($response->ResponseDetail, $this->GetRawTestDataAsObject(AppConst::EXAM_TYPE_LEG_STRENGTH, $appID, 1));
            $response->ResponseDetail[] = $this->GetExamNormalization($tester, AppConst::EXAM_TYPE_LEG_STRENGTH);
        }

        if ($backscratch !== null) {
            $response->ResponseDetail[] = $backscratch;
            $response->ResponseDetail = service::DecompressAndMerge($response->ResponseDetail, $this->GetRawTestDataAsObject(AppConst::EXAM_TYPE_BACK_STRACH, $appID, 0));
            $response->ResponseDetail[] = $this->GetExamNormalization($tester, AppConst::EXAM_TYPE_BACK_STRACH);
        }

        if ($SixminsWalk !== null) {
            $response->ResponseDetail[] = $SixminsWalk;
            $response->ResponseDetail = service::DecompressAndMerge($response->ResponseDetail, $this->GetRawTestDataAsObject(AppConst::EXAM_TYPE_6M_WALK_TEST, $appID, 0));
            $response->ResponseDetail[] = $this->GetExamNormalization($tester, AppConst::EXAM_TYPE_6M_WALK_TEST);
        }


        $response->ResponseDetail[AppConst::PARAM_TESTER] = $tester;

        foreach ($exams as $exam) {
            if (in_array($exam["examid"], $calculateExam)) {
                continue;
                //echo "<br> in calculate ".$exam["examid"]."<br>";
            } else {
                //echo "<br>not in calculate : examid = ".$exam["examid"]."";
                $examObj = new ExamObj();
                $examObj->SetAppID($appID);
                $examObj->SetSeqNo($exam["seqno"]);
                $examObj->SetExamID($exam["examid"]);
                $examObj->SetExamCat($exam["category"]);
                $examObj->SetExamPriority($exam["priority"]);
                $examObj = $this->GetScoreGradeNew($appID, $exam["examid"], $examObj);
                $res = $this->GetExamDescriptionByExamID($exam["examid"]);
                if ($res->MSGID === AppConst::STATUS_SUCCESS) {
                    $examObj->SetEDescription($res->MSGDETAIL2);
                    $examObj->SetTDescription($res->MSGDETAIL);
                    $examObj->SetUOM($res->MSGDETAIL3);
                }
                //$examObj = $this->GetGraphForPrintByAppID($appID, $exam["examid"], $examObj);
                $examObj = $this->GetPreviousTestScore($tester->GetTesterID(), $examObj, $appID);
                $response->ResponseDetail[] = $examObj;
                $response->ResponseDetail[] = $this->GetExamNormalization($tester, $exam["examid"]);
            }
        }
        $response->MSGID = AppConst::STATUS_SUCCESS;
        /* echo '<pre>';
          print_r($response);
          echo '</pre>'; */
        return $response;
    }

    function GetEmailListByTesterID($idlists) {
        $con = $this->dbObj->Open();
        $ids = join("','", $idlists);
        $sql = " select distinct(email) from tester where testerid in ('$ids') ";
        $stmt = $con->prepare($sql);
        $mails = "";
        $arr = array();
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($mail);
            while ($stmt->fetch()) {
                if ($mail != '' || $mail != null || $mail != 'null') {
                    $arr[] = $mail;
                }
            }
        }
        $mails = join(";", $arr);
        $stmt->close();
        return $mails;
    }

    function GetRawTestData($examid, $appid) {
        $con = $this->dbObj->Open();
        $sql = " select examid ,value, seqno from mapexam where applicationid = '" . $appid . "' and examid like '%" . $examid . "%' ";
        //echo $sql;
        $stmt = $con->prepare($sql);
        $arr = array();
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($exam, $value, $seqno);
            while ($stmt->fetch()) {
                $arr[$appid][$exam][$seqno] = $value;
            }
        }
        $stmt->close();
        return $arr;
    }

    function GetRawTestDataAsObject($examid, $appid, $precision) {
        $condition = " and examid != '" . $examid . "'";
        $con = $this->dbObj->Open();
        $sql = " select examid ,value, seqno "
                . " from mapexam "
                . " where applicationid = '" . $appid . "' "
                . " and examid like '%" . $examid . "%' ";

        if ($precision == '0') {
            $sql .= " and examid != '" . $examid . "'";
        }

        //echo $sql . "<br>";
        $stmt = $con->prepare($sql);
        $arr = array();
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($exam, $value, $seqno);
            if ($stmt->num_rows <= 0) {
                $examObj = new ExamObj();
                $examObj->SetAppID($appid);
                $examObj->SetSeqNo($seqno);
                $examObj->SetExamID($examid);
                $examObj->SetTestValue('-');
                $res = $this->GetExamDescriptionByExamID($examid);
                if ($res->MSGID === AppConst::STATUS_SUCCESS) {
                    $examObj->SetEDescription($res->MSGDETAIL2);
                    $examObj->SetTDescription($res->MSGDETAIL);
                    $examObj->SetUOM($res->MSGDETAIL3);
                }
                //$examObj = $this->GetGraphForPrintByAppID($appID, $exam["examid"], $examObj);
                $arr[] = $examObj;
            } else {
                while ($stmt->fetch()) {
                    $examObj = new ExamObj();
                    $examObj->SetAppID($appid);
                    $examObj->SetSeqNo($seqno);
                    $examObj->SetExamID($exam);
                    $examObj->SetTestValue($value);
                    $res = $this->GetExamDescriptionByExamID($exam);
                    if ($res->MSGID === AppConst::STATUS_SUCCESS) {
                        $examObj->SetEDescription($res->MSGDETAIL2);
                        $examObj->SetTDescription($res->MSGDETAIL);
                        $examObj->SetUOM($res->MSGDETAIL3);
                    }
                    //$examObj = $this->GetGraphForPrintByAppID($appID, $exam["examid"], $examObj);
                    $arr[] = $examObj;
                    //$arr[$appid][$exam][$seqno] = $value;
                }
            }
        }
        //service::printr($arr);
        $stmt->close();
        return $arr;
    }

    function GetExamInput() {
        $categorylist = $this->GetExamCategory();
        $mainexamlist = array();
        foreach ($categorylist as $key => $value) {
            $arr = array();
            $arr = service::DecompressAndMerge($arr, $this->GetAllExamListByCategoryID($key));
            $mainexamlist[$key] = $arr;
        }
        /*
         * Only for body compositio 
         */
        $mainexamlist[AppConst::EXAM_CAT_PER][] = AppConst::EXAM_TYPE_SKIN_FOLD;
        //$mainexamlist[AppConst::EXAM_CAT_PER][] = AppConst::EXAM_TYPE_OMRON;
        //service::printr($mainexamlist);
        $subexamlist = array();
        foreach ($mainexamlist as $rows) {
            foreach ($rows as $row) {
                //service::printr($row);
                $arr = array();
                $arr = service::DecompressAndMerge($arr, $this->GetSubExam($row));
                if (!empty($arr) && ($row != 'type10' || $row != 'type17')) { //Astrands has special config and one leg balance has already show 2 infographic
                    $subexamlist[$row] = $arr;
                }
            }
        }
        $tmp = $mainexamlist;
        //service::printr($mainexamlist);
        //service::printr($subexamlist);
        foreach ($tmp as $key => $value) {//Start each Category
            foreach ($value as $row) {// Start each Test in Category
                if (array_key_exists($row, $subexamlist)) {
                    foreach ($subexamlist as $skey => $svalue) { //Start each Test
                        if ($skey == $row) {
                            foreach ($svalue as $exam) { //Start each sub Test
                                $mainexamlist[$key][] = $exam;
                                if (($delkey = array_search($row, $mainexamlist[$key])) !== false) {
                                    unset($mainexamlist[$key][$delkey]);
                                }
                            }
                        } else {
                            continue;
                        }
                    }
                } else {
                    continue;
                }
            }
        }


        //echo '---------------------------------------------------------';
        ksort($mainexamlist);
        $response = new ResponseHeader();
        foreach ($mainexamlist as $key => $value) {
            foreach ($value as $row) {
                $examobj = new ExamObj();
                $examobj->SetExamID($row);
                $res = $this->GetExamDescriptionByExamID($row);
                if ($res->MSGID === AppConst::STATUS_SUCCESS) {
                    $examobj->SetEDescription($res->MSGDETAIL2);
                    $examobj->SetTDescription($res->MSGDETAIL);
                    $examobj->SetExamCat($key);
                    $examobj->SetUOM($res->MSGDETAIL3);
                    $response->ResponseDetail[$key][] = $examobj;
                }
            }
        }
        //service::printr($response->ResponseDetail);
        return $response;
    }

    function GetAllExamListByCategoryID($catid) {
        $sql = "select examid from exam where category = '" . $catid . "' and showinfog = '1' ";
        $con = $this->dbObj->Open();
        $stmt = $con->prepare($sql);
        $arr = array();
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($examid);
            while ($stmt->fetch()) {
                if ($examid != 'type17') {
                    $arr[] = $examid;
                }
            }
        }
        $stmt->close();
        return $arr;
    }

    function GetSubExam($mainexam) {
        $con = $this->dbObj->Open();
        //$sql = "select examid,tdescription,edescription,category from exam where category = '" . $key . "' and showinfog = '1' ";
        $sql = " select examid "
                . " from exam "
                . " where examid like '%" . $mainexam . "%' "
                . " and examid != '" . $mainexam . "' "
                . " and examid != 'type1001' " //For Astrand
                . " and examid != 'type2304' "; //For BMI omron
        //echo $sql;
        $arr = array();
        $stmt = $con->prepare($sql);
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($examid);
            while ($stmt->fetch()) {
                $arr[] = $examid;
            }
        }
        $stmt->close();
        return $arr;
    }

    function CalculateLegStrength($appID, TesterHeaderInfo $tester) {
        $appManager = new AppManager();
        $examObj = new ExamObj();
        $age = $appManager->CalCulateAge($tester->GetDOB());
        $condition = " AND mapexam.examid =  '" . AppConst::EXAM_TYPE_LEG_STRENGTH . "'";
        $exams = $this->GetMapExamByAppID($appID, $condition);
        $tmp = 0;
        $cnt = 0;
        $examCat = "";
        $examPri = "";
        foreach ($exams as $exam) {
            $cnt++;
            //echo "exam = ".$exam["value"]."<br>";
            $tmp += $exam["value"];
            $examCat = $exam["category"];
            $examPri = $exam["priority"];
        }

        if ($cnt > 0) {
            //echo "temp = ".$tmp."<br>";
            //$sum = $tmp / 2;
            $sum = $tmp;
            //echo "sum ".$sum."<br>";
            //echo $tester->GetWeight();
            if ($tester->GetWeight() === NULL || $tester->GetWeight() == 0) {
                $data = 0;
            } else {
                $data = $sum / $tester->GetWeight();
            }
            //echo "<br>" . "prev cnt = " . $prev_cnt . "<br>";

            $examObj->SetAppID($appID);
            $examObj->SetExamID(AppConst::EXAM_TYPE_LEG_STRENGTH);
            $res = $this->GetExamDescriptionByExamID(AppConst::EXAM_TYPE_LEG_STRENGTH);
            if ($res->MSGID === AppConst::STATUS_SUCCESS) {
                $examObj->SetEDescription($res->MSGDETAIL2);
                $examObj->SetTDescription($res->MSGDETAIL);
                $examObj->SetUOM($res->MSGDETAIL3);
            }
            $examObj->SetTestValue(number_format($data, 2));
            $examObj->SetSeqNo(0);
            $examObj->SetExamCat($examCat);
            $examObj->SetExamPriority($examPri);
            if ($examObj->GetTestValue() === 0 || $examObj->GetTestValue() === '-') {
                $examObj->SetGradeID("-");
            } else {
                $examObj = $this->GetScoreGrade($examObj, $tester, $age);
            }
            //$examObj = $this->GetScoreGrade($examObj, $tester, $age);
            //print_r($examObj);
        } else {
            $examObj = null;
        }
        return $examObj;
    }

    function GetExamNormalization(TesterHeaderInfo $tester, $examid) {
        $con = $this->dbObj->Open();
        $sql = "select score_min, score_max "
                . " from MasterData_Normalize "
                . " where examid = '" . $examid . "' "
                . " and gender = '" . $tester->GetSex() . "' "
                . " and age_from <= '" . $tester->GetAge() . "'"
                . " and age_to >= '" . $tester->GetAge() . "' ";
        //echo $sql."<br>";
        $stmt = $con->prepare($sql);
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($min_score, $max_score);
            $examObj = new ExamObj();
            while ($stmt->fetch()) {
                $examObj->SetExamID($examid . "Norm");
                $examObj->SetTestValue($min_score . " - " . $max_score);
            }
            if ($examObj->GetExamID() == "" || $examObj->GetExamID() == null) {
                $examObj->SetExamID($examid . "Norm");
                $examObj->SetTestValue("No data found");
            }
        }
        $stmt->close();
        return $examObj;
    }

    function CalculateStarExcursion($appID, TesterHeaderInfo $tester) {
        $appManager = new AppManager();
        $age = $appManager->CalCulateAge($tester->GetDOB());
        $condition = " AND mapexam.examid like  '%" . AppConst::EXAM_TYPE_STAR . "%'";
        $exams = $this->GetMapExamByAppID($appID, $condition);
        $tmp = 0;
        $cnt = 0;
        $examCat = "";
        $examPri = "";
        $devider = 0;
        $tmpdata = array();
        $resdata = array();
        foreach ($exams as $exam) {
            if ($exam['examid'] == AppConst::EXAM_TYPE_STAR_LEG_LENGTH) {
                $devider = $exam['value'];
            } else {
                $cnt++;
                $tmpdata[$exam['examid']][0] = $exam['value'];
                $tmpdata[$exam['examid']][1] = $exam["category"];
                $tmpdata[$exam['examid']][2] = $exam["priority"];
            }
        }

        if ($cnt > 0) {
            foreach ($tmpdata as $key => $value) {
                $examObj = new ExamObj();
                $examObj->SetAppID($appID);
                $examObj->SetExamID($key);
                $res = $this->GetExamDescriptionByExamID($key);
                if ($res->MSGID === AppConst::STATUS_SUCCESS) {
                    $examObj->SetEDescription($res->MSGDETAIL2);
                    $examObj->SetTDescription($res->MSGDETAIL);
                    $examObj->SetUOM($res->MSGDETAIL3);
                }
                if ($devider == 0 || $devider == null || $devider == '') {
                    $examObj->SetTestValue(0);
                } else {
                    $precal = $value[0] / $devider;
                    $examObj->SetTestValue(number_format($precal, 2));
                }
                $examObj->SetSeqNo(0);
                $examObj->SetExamCat($value[1]);
                $examObj->SetExamPriority($value[2]);
                if ($examObj->GetTestValue() === 0 || $examObj->GetTestValue() === '-') {
                    $examObj->SetGradeID("-");
                } else {
                    $examObj = $this->GetScoreGrade($examObj, $tester, $age);
                }
                $examObj->SetTestValue($value[0]);
                $resdata[$key] = $examObj;
            }

            //$examObj = $this->GetScoreGrade($examObj, $tester, $age);
            //print_r($examObj);
        } else {
            $resdata = null;
        }
        return $resdata;
    }

    function GetMinMaxNormalization() {
        $sql = "SELECT min(score_min), max(score_max), examid FROM `MasterData_Normalize` where examid != 'type10' GROUP BY examid";
        $con = $this->dbObj->Open();
        $stmt = $con->prepare($sql);
        $res = array();
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($min_val, $max_val, $examid);
            while ($stmt->fetch()) {
                $res[$examid]['min'] = $min_val;
                $res[$examid]['max'] = $max_val;
            }
        }
        //service::printr($res);
        //echo json_encode($res);
        $stmt->close();
        return $res;
    }

    function DeleteMapExamForUpdateByAppID($appid) {
        $sql = "delete from mapexam where applicationid = '" . $appid . "' ";
        //echo $sql;
        $con = $this->dbObj->Open();
        $stmt = $con->prepare($sql);
        $stmt->execute();
        $stmt->close();
        return;
    }

    function CalculateBackScratch($appID, $examid, TesterHeaderInfo $tester) {
        $appManager = new AppManager();
        $examObj = new ExamObj();
        $age = $appManager->CalCulateAge($tester->GetDOB());
        //$condition = " AND mapexam.examid =  '" . $examid . "' ";
        $condition = " AND mapexam.examid like  '%" . $examid . "%' ";
        $exams = $this->GetMapExamByAppID($appID, $condition);
        $tmp = 0;
        $cnt = 0;
        $examCat = "";
        $examPri = "";
        $examid_query = "";
        foreach ($exams as $exam) {
            $cnt++;
            //echo "exam = ".$exam["value"]."<br>";
            $tmp = $exam["value"];
            $examCat = $exam["category"];
            $examPri = $exam["priority"];
            $examid_query = $exam['examid'];
        }


        if ($cnt > 0) {
            $data = $tmp;
            //echo "<br>" . "prev cnt = " . $prev_cnt . "<br>";

            $examObj->SetAppID($appID);
            $examObj->SetExamID($examid_query);
            $res = $this->GetExamDescriptionByExamID($examid);
            if ($res->MSGID === AppConst::STATUS_SUCCESS) {
                $examObj->SetEDescription($res->MSGDETAIL2);
                $examObj->SetTDescription($res->MSGDETAIL);
                $examObj->SetUOM($res->MSGDETAIL3);
            }
            $examObj->SetTestValue(number_format($data, 2));
            $examObj->SetSeqNo(0);
            $examObj->SetExamCat($examCat);
            $examObj->SetExamPriority($examPri);
            if ($examObj->GetTestValue() === 0 || $examObj->GetTestValue() === '-') {
                $examObj->SetGradeID("-");
            } else {
                //$examObj->SetExamID(AppConst::EXAM_TYPE_ONE_LEG_BAL);
                $examObj = $this->GetScoreGrade($examObj, $tester, $age);
                $examObj->SetExamID($examid);
            }
            //$examObj = $this->GetScoreGrade($examObj, $tester, $age);
            //print_r($examObj);
        } else {
            $examObj = null;
        }
        return $examObj;
    }

    function Calculate6MinsWalkTest($appID, TesterHeaderInfo $tester) {
        $appManager = new AppManager();
        $examObj = new ExamObj();
        $res = new ResponseHeader();
        $age = $appManager->CalCulateAge($tester->GetDOB());
        $condition = " AND mapexam.examid LIKE  '" . AppConst::EXAM_TYPE_6M_WALK_TEST_DISTANT . "%' ";
        $exams = $this->GetMapExamByAppID($appID, $condition);

        $cnt = 0;
        $examCat = "";
        $examPri = "";

        foreach ($exams as $exam) {
            $cnt++;
            //echo "exam = ".$exam["value"]."<br>";
            $tmp = $exam["value"];
            $examCat = $exam["category"];
            $examPri = $exam["priority"];
            $examid_query = $exam['examid'];
        }

        if ($cnt > 0) {
            $data = $tmp;
            //echo "<br>" . "prev cnt = " . $prev_cnt . "<br>";

            $examObj->SetAppID($appID);
            $examObj->SetExamID(AppConst::EXAM_TYPE_6M_WALK_TEST);
            $res = $this->GetExamDescriptionByExamID(AppConst::EXAM_TYPE_6M_WALK_TEST);
            if ($res->MSGID === AppConst::STATUS_SUCCESS) {
                $examObj->SetEDescription($res->MSGDETAIL2);
                $examObj->SetTDescription($res->MSGDETAIL);
                $examObj->SetUOM($res->MSGDETAIL3);
            }
            $examObj->SetTestValue(number_format($data, 2));
            $examObj->SetSeqNo(0);
            $examObj->SetExamCat($examCat);
            $examObj->SetExamPriority($examPri);
            if ($examObj->GetTestValue() == 0 || $examObj->GetTestValue() == '-') {
                $examObj->SetGradeID("-");
            } else {
                $examObj = $this->GetScoreGrade($examObj, $tester, $age);
            }
            //print_r($examObj);
        } else {
            $examObj = null;
        }

        return $examObj;
    }

    function GetSummaryExamDataList() {
        $con = $this->dbObj->Open();
        $sql = " select distinct(examid) from MasterData ";
        $stmt = $con->prepare($sql);
        $datalist = array();
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($examid);
            while ($stmt->fetch()) {
                $res = $this->GetExamDescriptionByExamID($examid);
                $datalist[$examid] = $res->MSGDETAIL;
            }
        }
        return $datalist;
        /* $showlist = $this->GetShowExamListInfographic();
          $datalist = array();
          foreach ($showlist as $each) {
          $res = $this->GetExamDescriptionByExamID($each);
          $datalist[$each] = $res->MSGDETAIL;
          }
          return $datalist; */
    }

    function GetSummaryByExamTypeNew($examType, $grpTest, $from, $to) {
        $responseHeader = new ResponseHeader();
        $responseHeader->MSGID = AppConst::STATUS_SUCCESS;
        $response = new SummaryDetailResponse();
        $con = $this->dbObj->Open();
        $exceptionalist = array(AppConst::EXAM_TYPE_SKIN_FOLD, AppConst::EXAM_TYPE_HANDGRIP,
            AppConst::EXAM_TYPE_ASTRAND, AppConst::EXAM_TYPE_MULTISTAGE,
            AppConst::EXAM_TYPE_ONE_LEG_BAL_OPEN, AppConst::EXAM_TYPE_ONE_LEG_BAL_CLOSE,
            AppConst::EXAM_TYPE_LEG_STRENGTH, AppConst::EXAM_TYPE_6M_WALK_TEST,);

        $whereCondition = " WHERE mapexam.examid like '%" . $examType . "%' ";
        if ($grpTest != "" && $grpTest != "0") {

            if ($whereCondition != "") {
                $whereCondition .= " and application.groupid = '" . $grpTest . "' ";
            } else {
                $whereCondition .= " where application.groupid = '" . $grpTest . "' ";
            }
        }

        if ($from != "" && $to != "" && $from != "1970-01-01" && $to != "1970-01-01") {
            if ($whereCondition != "") {
                $whereCondition .= " and (application.completedate >= '" . $from . "' and application.completedate <= '" . $to . "' )";
            } else {
                $whereCondition .= " where (application.completedate >= '" . $from . "' and application.completedate <= '" . $to . "' ) ";
            }
        } else if (($from != "" && $from != "1970-01-01" ) && ($to == "" || $to == "1970-01-01")) {
            if ($whereCondition != "") {
                $whereCondition .= " and application.completedate >= '" . $from . "' ";
            } else {
                $whereCondition .= " where application.completedate >= '" . $from . "'  ";
            }
        } else if (($from == "" || $from == "1970-01-01") && ($to != "" && $to != "1970-01-01")) {
            if ($whereCondition != "") {
                $whereCondition .= " and application.completedate <= '" . $to . "' ";
            } else {
                $whereCondition .= " where application.completedate <= '" . $to . "' ";
            }
        }

        $sql = "SELECT COUNT( DISTINCT map.applicationid ) as cnt, gender "
                . " FROM mapexam "
                . " JOIN map ON mapexam.applicationid = map.applicationid "
                . " JOIN tester ON map.testerid = tester.testerid "
                . " JOIN application on mapexam.applicationid = application.applicationid"
                //. " JOIN groups on application.groupid = groups.groupid"
                . $whereCondition
                . " GROUP BY gender";
        $stmt = $con->prepare($sql);
        //echo $sql;
        $allQuery = 0;
        $cnt_male = 0;
        $cnt_female = 0;
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($value, $gender);
            if ($stmt->num_rows > 0) {
                $allQuery = $stmt->num_rows;
                while ($stmt->fetch()) {
                    if ($gender === AppConst::MALE) {
                        $cnt_male = $value;
                    } else if ($gender === AppConst::FEMALE) {
                        $cnt_female = $value;
                    } else {
                        
                    }
                }
                $cnt_all = $cnt_female + $cnt_male;
                $response->MALE = $cnt_male;
                $response->FEMALE = $cnt_female;
                $response->TOTAL = $cnt_all;
                $response->INDICATOR = AppConst::SUMMARY_TOTAL;
                $response->STATUS = AppConst::STATUS_SUCCESS;
                $stmt->close();
            } else {
                $responseHeader->MSGID = AppConst::STATUS_SQL_ERROR;
                $response->STATUS = AppConst::STATUS_SQL_ERROR;
                $stmt->close();
            }
        } else {
            $responseHeader->MSGID = AppConst::STATUS_SQL_ERROR;
            $response->STATUS = AppConst::STATUS_SQL_ERROR;
            $stmt->close();
        }
        //echo "all query = ".$allQuery;
        $responseHeader->ResponseDetail[] = $response;

        try {
            if (!in_array($examType, $exceptionalist)) {
                $maxminavglist = $this->GetMinMaxAvgForSummaryApp($examType, $whereCondition);
                foreach ($maxminavglist as $each) {
                    if ($each->STATUS != AppConst::STATUS_SUCCESS) {
                        $responseHeader->MSGID = AppConst::STATUS_ERROR;
                    }
                    $responseHeader->ResponseDetail[] = $each;
                }
            } else {
                $maxminavglist = $this->GetMinMaxAvgExceptionalExamForSummaryApp($examType, $whereCondition);
                foreach ($maxminavglist as $each) {
                    if ($each->STATUS != AppConst::STATUS_SUCCESS) {
                        $responseHeader->MSGID = AppConst::STATUS_ERROR;
                    }
                    $responseHeader->ResponseDetail[] = $each;
                }
            }
        } catch (Exception $ex) {
            $responseHeader->MSGID = AppConst::STATUS_ERROR;
        }

        return $responseHeader;
    }

    function GetMinMaxAvgExceptionalExamForSummaryApp($examType, $codition) {
        $con = $this->dbObj->Open();
        $sql = " select application.applicationid from application "
                . " join mapexam on mapexam.applicationid = application.applicationid "
                . $codition;
        $stmt = $con->prepare($sql);
        //echo $sql;
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($appid);
            $resultlist = array();
            while ($stmt->fetch()) {
                $tester = new TesterHeaderInfo();
                $tester = $this->GetTesterDetailByAppID($appid);
                $examobj = new ExamObj();
                if ($examType == AppConst::EXAM_TYPE_HANDGRIP) {
                    $examobj = $this->CalculateHandGrip($appid, $tester);
                } else if ($examType == AppConst::EXAM_TYPE_ASTRAND) {
                    $examobj = $this->CalCulateAstrand($appid, $tester);
                } else if ($examType == AppConst::EXAM_TYPE_MULTISTAGE) {
                    $examobj = $this->CalculateMultiStage($appid, $tester);
                } else if ($examType == AppConst::EXAM_TYPE_ONE_LEG_BAL_OPEN) {
                    $oboL = new ExamObj();
                    $oboR = new ExamObj();
                    $oboL = $this->CalculateOneLegBalance($appid, AppConst::EXAM_TYPE_ONE_LEG_BAL_L_OPEN, $tester);
                    $oboR = $this->CalculateOneLegBalance($appid, AppConst::EXAM_TYPE_ONE_LEG_BAL_R_OPEN, $tester);
                    if ($oboL != NULL) {
                        $resultlist[$tester->GetSex()][] = $oboL->GetTestValue();
                    }
                    if ($oboR != NULL) {
                        $resultlist[$tester->GetSex()][] = $oboR->GetTestValue();
                    }
                } else if ($examType == AppConst::EXAM_TYPE_ONE_LEG_BAL_CLOSE) {
                    $obcL = new ExamObj();
                    $obcR = new ExamObj();
                    $obcL = $this->CalculateOneLegBalance($appid, AppConst::EXAM_TYPE_ONE_LEG_BAL_L_CLOSE, $tester);
                    $obcR = $this->CalculateOneLegBalance($appid, AppConst::EXAM_TYPE_ONE_LEG_BAL_R_CLOSE, $tester);
                    if ($obcL != NULL) {
                        $resultlist[$tester->GetSex()][] = $obcL->GetTestValue();
                    }
                    if ($obcR != NULL) {
                        $resultlist[$tester->GetSex()][] = $obcR->GetTestValue();
                    }
                } else if ($examType == AppConst::EXAM_TYPE_LEG_STRENGTH) {
                    $examobj = $this->CalculateLegStrength($appid, $tester);
                } else if ($examType == AppConst::EXAM_TYPE_6M_WALK_TEST) {
                    $examobj = $this->Calculate6MinsWalkTest($appid, $tester);
                } else {
                    
                }
                if ($examType != AppConst::EXAM_TYPE_ONE_LEG_BAL_CLOSE && $examType != AppConst::EXAM_TYPE_ONE_LEG_BAL_OPEN) {
                    if ($examobj != null) {
                        $resultlist[$tester->GetSex()][] = $examobj->GetTestValue();
                    }
                }
            }
            if (!isset($resultlist['M']) || $resultlist['M'] == null || empty($resultlist['F'])) {
                $resultlist['M'][] = 0;
            }
            if (!isset($resultlist['F']) || $resultlist['F'] == null || empty($resultlist['F'])) {
                $resultlist['F'][] = 0;
            }
            //service::printr($resultlist);
            $arr = array();
            /*
             * Max 
             */
            $res = new SummaryDetailResponse();
            $res->FEMALE = max($resultlist['F']);
            $res->MALE = max($resultlist['M']);
            $res->INDICATOR = AppConst::SUMMARY_MAX;
            $res->STATUS = AppConst::STATUS_SUCCESS;
            $arr[] = $res;

            /*
             * Min
             */
            $res = new SummaryDetailResponse();
            $res->FEMALE = min($resultlist['F']);
            $res->MALE = min($resultlist['M']);
            $res->INDICATOR = AppConst::SUMMARY_MIN;
            $res->STATUS = AppConst::STATUS_SUCCESS;
            $arr[] = $res;

            /*
             * AVG
             */
            $cnt_ppl = count($resultlist['F']) + count($resultlist['M']);
            $total_sum = array_sum($resultlist['F']) + array_sum($resultlist['M']);
            $res = new SummaryDetailResponse();
            $avg_total = $total_sum / $cnt_ppl;
            $res->TOTAL = number_format($avg_total, 2);
            $res->FEMALE = number_format(array_sum($resultlist['F']) / count($resultlist['F']), 2);
            $res->MALE = number_format(array_sum($resultlist['M']) / count($resultlist['M']), 2);
            $res->INDICATOR = AppConst::SUMMARY_AVERAGE;
            $res->STATUS = AppConst::STATUS_SUCCESS;
            $arr[] = $res;
        }
        return $arr;
    }

    function GetMinMaxAvgForSummaryApp($examid, $condition) {
        $con = $this->dbObj->Open();
        $sql = "select max(value), min(value), AVG(value), COUNT(mapexam.applicationid), sum(value), gender from mapexam"
                . " join map ON mapexam.applicationid = map.applicationid "
                . " join tester ON map.testerid = tester.testerid "
                . " where mapexam.applicationid in "
                . " (select application.applicationid from application "
                . " join mapexam on mapexam.applicationid = application.applicationid "
                . $condition
                . ") "
                . " and mapexam.examid = '" . $examid . "' "
                . " GROUP BY gender";
        //echo $sql;
        $total_sum = 0;
        $cnt_ppl = 0;
        $arr = array();
        $max_m = 0;
        $max_f = 0;
        $min_m = 0;
        $min_f = 0;
        $avg_m = 0;
        $avg_f = 0;
        $stmt = $con->prepare($sql);
        if ($stmt->execute()) {
            $stmt->store_result();
            $stmt->bind_result($maxval, $minval, $avg, $cnt, $sum, $gender);
            while ($stmt->fetch()) {
                $cnt_ppl += $cnt;
                $total_sum += $sum;
                if ($gender == AppConst::MALE) {
                    $max_m = number_format($maxval, 2);
                    $min_m = number_format($minval, 2);
                    $avg_m = number_format($avg, 2);
                } else {
                    $max_f = number_format($maxval, 2);
                    $min_f = number_format($minval, 2);
                    $avg_f = number_format($avg, 2);
                }
            }
            try {
                /*
                 * Max 
                 */
                $res = new SummaryDetailResponse();
                $res->FEMALE = $max_f;
                $res->MALE = $max_m;
                $res->INDICATOR = AppConst::SUMMARY_MAX;
                $res->STATUS = AppConst::STATUS_SUCCESS;
                $arr[] = $res;

                /*
                 * Min
                 */
                $res = new SummaryDetailResponse();
                $res->FEMALE = $min_f;
                $res->MALE = $min_m;
                $res->INDICATOR = AppConst::SUMMARY_MIN;
                $res->STATUS = AppConst::STATUS_SUCCESS;
                $arr[] = $res;

                /*
                 * AVG
                 */
                $res = new SummaryDetailResponse();
                $avg_total = $total_sum / $cnt_ppl;
                $res->TOTAL = number_format($avg_total, 2);
                $res->FEMALE = $avg_f;
                $res->MALE = $avg_m;
                $res->INDICATOR = AppConst::SUMMARY_AVERAGE;
                $res->STATUS = AppConst::STATUS_SUCCESS;
                $arr[] = $res;
            } catch (Exception $ex) {
                $res = new SummaryDetailResponse();
                $res->STATUS = AppConst::STATUS_ERROR;
                $arr[] = $res;
            }
        }
        //service::printr($arr);
        return $arr;
    }

    function GetBMIScoreDesciption($grade) {
        if ($grade == '1') {
            return 'ผอม';
        } else if ($grade == '2') {
            return 'สมส่วน';
        } else if ($grade == '3') {
            return 'น้ำหนักเกิน';
        } else if ($grade == '4') {
            return 'อ้วน';
        } else if ($grade == '5') {
            return 'อ้วนมาก';
        } else {
            return 'error!!';
        }
    }

}
