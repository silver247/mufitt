<?php
include_once ('./AppManager.php');
$appObj = new AppManager();
$ddlGrp = $appObj->RequestDDLGrpTest();
$table = "";
if(isset($_GET["btnSubmit"])){
    //echo "test";
    $table = $appObj->GetSearchTableByValue($_GET["txtDate"], $_GET["ddlGrpTest"], $_GET["txtTesterID"]);
}
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <form method="get" name="fsearch">
            วัน <input type="date" name="txtDate">
            กลุ่มการทดสอบ <select name="ddlGrpTest">
                <?=$ddlGrp;?>
            </select>
            หมายเลขประจำตัว <input type="text" name="txtTesterID" maxlength="10">
            <input type="submit" name="btnSubmit" value="submit">
        </form>
        
        <table border="1">
            <thead>
                <tr>
                    <td>Action</td>
                    <td>ชื่อ-นามสกุล</td>
                    <td>อายุ</td>
                    <td>กลุ่มการทดสอบ</td>
                </tr>
            </thead>
            <tbody>
                <?=$table;?>
            </tbody>
        </table>
    </body>
</html>
