var weightExam = "<?= AppConst::EXAM_TYPE_WEIGHT ?>";
var heightExam = "<?= AppConst::EXAM_TYPE_HEIGHT ?>";
var BMIExam = "<?= AppConst::EXAM_TYPE_OMRON_BMI ?>";
var validateInput = new Array();
$(function () {

    if ($(".examtype[type=hidden][value=" + weightExam + "]").length != 0) {
        weightExam = $("input[type=text]", $(".examtype[type=hidden][value=" + weightExam + "]").parent());
    //alert($(weightExam).attr("id"));
    } else {
        weightExam = $("input[name=weight]");
    }
    if ($(".examtype[type=hidden][value=" + heightExam + "]").length != 0) {
        heightExam = $("input[type=text]", $(".examtype[type=hidden][value=" + heightExam + "]").parent());
    //alert($(heightExam).attr("id"));
    } else {
        heightExam = $("input[name=height]");
    }
    if ($(".examtype[type=hidden][value=" + BMIExam + "]").length != 0) {
        BMIExam = $("input[type=text]", $(".examtype[type=hidden][value=" + BMIExam + "]").parent());
    //alert($(BMIExam).attr("id"));
    }
    setupComponents();

});

function loadValidate(){
    try {
        jQuery.ajax({
            url: "./validateInput.xml",
            type: "POST",
            contentType: defaultContentType,
            //dataType: "html",
            dataType: "xml",
            error: function (transport, status, errorThrown) {
            },
            success: function (data) {
                $("exam",data).each(function(index,obj){
                    var tmp = new Arrray(); 
                    tmp['min'] = $("min",obj).text();
                    tmp['max'] = $("max",obj).text();
                    validateInput[$(obj).attr("name")] = tmp;
                });              
            }

        });
    } catch (ex) {
        alert(ex);
    }
}
        
function back(action) {
    if (action == "<?= AppConst::METHOD_INSERT_APP ?>") {
        window.open("landing.php", "_top");
    } else if (action == "<?= AppConst::METHOD_EDIT_APP ?>") {
        window.open("searchapp.php?action=<?= AppConst::METHOD_QUERY_SEARCH_EDIT ?>", "_top");
    }
}
function setupComponents() {
    loadValidate();
    $(".astrandtest").change(function () {
        var n = 0;
        $(".astrandtest").each(function () {
            if ($(this).val() != "") {
                n++;
            }
        });
        if (n == 3) {
            if ($("#exam25val").is(":enabled") && $("#exam23val:enabled").val() == "") {
                $("#exam25val").val("");
                $("#exam25val").attr("disabled", "disabled");
                $(".examtype", $("#exam25val").parent()).attr("disabled", "disabled");
                $(".seqno", $("#exam25val").parent()).attr("disabled", "disabled");
            }
        }
        if (n == 4) {
            $("#exam25val").removeAttr("disabled");
            $(".examtype", $("#exam25val").parent()).removeAttr("disabled");
            $(".seqno", $("#exam25val").parent()).removeAttr("disabled");
            if ($("#exam26val").is(":enabled") && $("#exam25val:enabled").val() == "") {
                $("#exam26val").val("");
                $("#exam26val").attr("disabled", "disabled");
                $(".examtype", $("#exam26val").parent()).attr("disabled", "disabled");
                $(".seqno", $("#exam26val").parent()).attr("disabled", "disabled");
            }
        } else if (n == 5) {
            $("#exam26val").removeAttr("disabled");
            $(".examtype", $("#exam26val").parent()).removeAttr("disabled");
            $(".seqno", $("#exam26val").parent()).removeAttr("disabled");
        }
    });
    if ($("#testergrp option").length == 1) {
        $("#testergrpstr").show();
    } else {
        $("#testergrp").change(function () {
            if ($(this).val() == "other") {
                $("#testergrpstr").show();
            }
        });
    }
    $("#brithday").change(function () {
        if (validateBrithday()) {
            getAgeFromDOB();
        } else {
            $("#brithday").val("");
            $("#brithday").focus();
        }
    });
    if ($("#brithday").val() != "") {
        getAgeFromDOB();
    }
    if ($("input[name=do][type=hidden]").val() == "<?= AppConst::METHOD_INSERT_APP ?>") {
        $("#newapp_label").show();
    }
    $("#" + $(heightExam).attr("id") + ",#" + $(weightExam).attr("id")).change(function () {
        calBMI();
    });
}
function getAgeFromDOB() {
    $("#age").val(calculateAge($("#brithday").val(), false));
}
function disbledInput() {

    $(".exam[type=text]").each(function () {
        if ($(this).val() == "") {
            $(this).attr("disabled", "disabled");
            $(".examtype", $(this).parent()).attr("disabled", "disabled");
            $(".seqno", $(this).parent()).attr("disabled", "disabled");
        }
    });
    if ($(".exam[type=radio]:checked").length == 0) {
        $(".exam[type=radio]").each(function () {
            $(this).attr("disabled", "disabled");
            $(".examtype", $(this).parent()).attr("disabled", "disabled");
            $(".seqno", $(this).parent()).attr("disabled", "disabled");
        });
    }
}

function prepareData() {
    var n = 0;
    $(".exam[type=radio]:enabled").each(function () {
        if ($(this).is(":checked")) {
            n++;
            $(this).attr("name", "exam" + n + "val");
            $(".seqno", $(this).parent()).attr("name", "exam" + n + "seqno");
            $(".examtype", $(this).parent()).attr("name", "exam" + n + "type");
        }
    });
    $(".exam[type=text]:enabled").each(function () {
        if ($(this).val() != "") {
            n++;
            $(this).attr("name", "exam" + n + "val");
            $(".seqno", $(this).parent()).attr("name", "exam" + n + "seqno");
            $(".examtype", $(this).parent()).attr("name", "exam" + n + "type");
        }
    });
    if ($("#ddlLoad option:selected").val() != "0") {
        n++;
        $("#ddlLoad").attr("name", "exam" + n + "val");
        $("#exam34seqno").attr("name", "exam" + n + "seqno");
        $("#exam34type").attr("name", "exam" + n + "type");
    }
    if ($("#testergrp option:selected").val() == "other") {
        $("#testergrp option:selected").val($("#testergrpstr").val());
    }
    $("#maxinput").val(n);
}

function validateForm() {



}

function validateBrithday() {
    var dob = $("#brithday").val();
    var dob = dob.split("/");
    var current = new Date();
    if (dob.length != 3) {
        alert("กรุณาระบุวัน/เดือน/ปีเกิดให้ถูกต้อง");
        return false;
    } else if (dob[0].length != 2 || dob[1].length != 2 || dob[2].length != 4) {
        alert("กรุณาระบุวัน/เดือน/ปีเกิดให้ถูกต้อง");
        return false;
    } else if (eval(dob[2]) > current.getUTCFullYear()) {
        alert("กรุณาระบุวัน/เดือน/ปีเกิดให้ถูกต้อง");
        return false;
    }
    return true;
}

function changeFormatDate(dateString) {
    var date_arr = dateString.split("/");
    var date = date_arr[0];
    var month = date_arr[1];
    date_arr[0] = month;
    date_arr[1] = date;
    return date_arr.join("/");
}

function calculateAge(datestart, thialocale) {
    datestart = new Date(changeFormatDate(datestart));
    var datestop = new Date();
    var current = new Date();
    var convertFlag = false;
    if (!thialocale) {
        convertFlag = Math.abs(current.getYear() - datestart.getYear()) == 543 ? true : false;
        if (convertFlag) {
            datestart = parseDate(datestart.getDate() + "/" + (datestart.getMonth() + 1) + "/" + (datestart.getYear() - 543));
        }
        convertFlag = Math.abs(current.getYear() - datestop.getYear()) == 543 ? true : false;
        if (convertFlag) {
            datestop = parseDate(datestop.getDate() + "/" + (datestop.getMonth() + 1) + "/" + (datestop.getYear() - 543));
        }
    }
    if (thialocale) {
        convertFlag = Math.abs((current.getYear() + 543) - datestart.getYear()) == 543 ? true : false;
        if (convertFlag) {
            datestart = parseDate(datestart.getDate() + "/" + (datestart.getMonth() + 1) + "/" + (datestart.getYear() + 543));
        }
        convertFlag = Math.abs((current.getYear() + 543) - datestop.getYear()) == 543 ? true : false;
        if (convertFlag) {
            datestop = parseDate(datestop.getDate() + "/" + (datestop.getMonth() + 1) + "/" + (datestop.getYear() + 543));
        }
    }
    var day = Math.abs((datestop.getDate()) - (datestart.getDate()));
    var month = Math.abs((datestop.getMonth() + 1) - (datestart.getMonth() + 1));
    var year = Math.abs((datestop.getYear()) - (datestart.getYear()));
    return year;
}

function genDataTest() {
    $(".exam[type=text]:enabled").each(function () {
        var id = $(this).attr("id");
        id = id.replace("exam", "");
        id = id.replace("val", "");
        $(this).val(id);
    });
}

function ValidateData() {
    if ($("#tname").val().length === 0) {
        alert("กรุณากรอกชื่อ");
        $("#tname").focus();
        return false;
    }
    if ($("#tsurname").val().length === 0) {
        alert("กรุณากรอกนามสกุล");
        $("#tsurname").focus();
        return false;
    }
    if ($("#ename").val().length === 0) {
        alert("กรุณากรอกชื่อ");
        $("#ename").focus();
        return false;
    }
    if ($("#esurname").val().length === 0) {
        alert("กรุณากรอกนามสกุล");
        $("#esurname").focus();
        return false;
    }
    if ($("#height").val().length === 0) {
        alert("กรุณากรอกส่วนสูง");
        $("#height").focus();
        return false;
    }
    if ($("#weight").val().length === 0) {
        alert("กรุณากรอกน้ำหนัก");
        $("#weight").focus();
        return false;
    }
    if ($("#brithday").val().length === 0) {
        alert("กรุณากรอกวันเกิด");
        $("#brithday").focus();
        return false;
    }

    /*if (!$("input[name='tmpexam1val']:checked").val()) {
             alert("กรุณาเลือกความถี่ในการออกกำลังกาย");
             return false;
             }*/
    if (!$("input[id='exercise_0']:checked").val() && !$("input[id='exercise_1']:checked").val() && !$("input[id='exercise_2']:checked").val()) {
        alert("กรุณาเลือกความถี่ในการออกกำลังกาย");
        return false;
    }
    if (!$("input[name='gender']:checked").val()) {
        alert("กรุณาเลือกเพศ");
        return false;
    }
    if ($("#exam20val").val().length > 0) {
        if ($("#ddlLoad option:selected").val() == 0) {
            alert("กรุณาเลือก Load");
            return false;
        }
    }
    if ($("#testergrp option:selected").val() == "other") {
        if ($("#testergrpstr").val().length === 0) {
            alert("กรุณากรอกกลุ่มการทดสอบ");
            $("#testergrpstr").focus();
            return false;
        }
    } else {
        if ($("#testergrp option:selected").val() == 0) {
            alert("กรุณาเลือกกลุ่มการทดสอบ");
            return false;
        }
    }
    return true;
}

function submitData(btn) {
    var chk = ValidateData();
    if (!chk) {
        return  false;
    }
    disbledInput();
    prepareData();
    //prompt("", $("#tform1").serialize());
    try {
        jQuery.ajax({
            url: "AppHttpRequest.php",
            type: "POST",
            contentType: defaultContentType,
            data: $("#tform1").serialize(),
            //dataType: "html",
            dataType: "json",
            error: function (transport, status, errorThrown) {

                alert("error : " + errorThrown);

            },
            success: function (data) {

                if (data.result == "<?= $successval ?>") {
                    if ($(btn).attr("iname") == "insert") {
                        if (confirm("ต้องการพิมพ์ผลการทดสอบหรือไม่")) {
                            $("#appid2").val(data.appid);
                            $("#testerid2").val($("#testerid").val());
                            $("#dform1").submit();
                        } else {
                            alert("ไม่");
                        }
                    } else {
                        $("input[name=do]").val("<?= AppConst::METHOD_EDIT_APP ?>");
                        $("#appid2").append("<input type='hidden' name='appid' id='appid' value='<?= $appID ?>'/>");
                        alert("บันทึกสำเร็จ");
                    }
                } else if (data.result == "<?= $errorval ?>") {
                    alert("เกิดข้อผิดพลาดกรุณษทำรายการใหม่อีกครั้ง");
                }
            //prompt("", "success : " + data);
            }

        });
    } catch (ex) {
        alert(ex);
    }

//prompt("", $("#tform1").serialize());

}
function submitPrint() {
    $("#appid2").val($("#appid").val());
    $("#testerid2").val($("#testerid").val());
    $("#dform1").submit();
}
function prepareInsData() {
    clearForm();
    $("input[name=do]").val($("#tmpaction").val());
    $("input[name=tname][type=text]").attr("readonly", "readonly");
    $("input[name=tsurname][type=text]").attr("readonly", "readonly");
    $("input[name=ename][type=text]").attr("readonly", "readonly");
    $("input[name=esurname][type=text]").attr("readonly", "readonly");
    $("input[name=gender][type=radio]").attr("readonly", "readonly");
    $("input[name=brithday][type=text]").attr("readonly", "readonly");
    $("#newapp_label").show();
}
function clearForm() {
    $(".exam[type=text]").val("");
    $("input[name!=gender][type=radio]").removeAttr("checked");
    $("select").each(function () {
        if ($("option[value=0]", $(this)).length == 1) {
            $("option[value=0]", $(this)).attr("selected", "selected");
        } else {
            $("option", $(this)).first().attr("selected", "selected");
        }
        $("input[type=text]", $(this)).hide();
    });
    $("option", $(this)).first().attr("selected", "selected");
    $("#exam25val").attr("disabled", "disabled");
    $(".examtype", $("#exam25val").parent()).attr("disabled", "disabled");
    $(".seqno", $("#exam25val").parent()).attr("disabled", "disabled");
    $("#exam26val").attr("disabled", "disabled");
    $(".examtype", $("#exam26val").parent()).attr("disabled", "disabled");
    $(".seqno", $("#exam26val").parent()).attr("disabled", "disabled");
}
function calBMI() {
    var height = $(heightExam).val();
    var weigth = $(weightExam).val();
    var BMI = 0;
    if (height != "" && weigth != "") {
        height = eval(removeDelimiter(height, ','));
        weigth = eval(removeDelimiter(weigth, ','));
        if (height > 0) {
            BMI = weigth / ((height / 100) * (height / 100));
            BMI = formatFloating(BMI,2)
            $(BMIExam).val(BMI);
        }
    }
}
function removeComma(value) {
    value = removeDelimiter(value, ",");
    return value;
}

function removeDelimiter(value, delimiter) {
    return value.replace(delimiter, "");
}

function formatFloating(value, decimal) {
    value = value + "";
    value = removeComma(value);
    return formatDecimal(value, decimal, true);
}

function formatDecimal(value, decimal, verifydecimal) {
    var sign = "";
    var result = value + "";
    var bstr = "";
    var cstr = "";
    var i = result.indexOf("-");
    if (i >= 0) {
        sign = "-";
        result = result.substring(i + 1);
    } else {
        i = result.indexOf("+");
        if (i >= 0) {
            sign = "+";
            result = result.substring(i + 1);
        }
    }
    var astr = result;
    i = result.indexOf(".");
    if (i > 0) {
        astr = result.substring(0, i);
        bstr = result.substring(i + 1);
        cstr = result.substring(i);
    }
    var la = astr.length;
    if (la > 3) {
        var tstr = astr;
        astr = "";
        while (tstr != "") {
            la = tstr.length;
            var md = la % 3;
            if (md > 0) {
                astr += tstr.substring(0, md) + ",";
                tstr = tstr.substring(md);
            } else {
                astr += tstr.substring(0, 3);
                tstr = tstr.substring(3);
                if (tstr != "")
                    astr += ",";
            }
        }
    }
    if (verifydecimal) {
        if (decimal > 0) {
            var l = bstr.length;
            if (decimal > l) {
                var j = 0;
                for (j = l; j < decimal; j++) {
                    bstr += "0";
                }
            } else {
                bstr = bstr.substring(0, decimal);
            }
            if (astr == "")
                return "";
            return sign + astr + "." + bstr;
        } else {
            return sign + astr;
        }
    } else {
        return sign + astr + cstr;
    }
}