<?php

include_once ('./AppbaseManager.php');
include_once ('./ExamObj.php');
include_once ('./ApplicationObj.php');
include_once ('./TesterHeaderInfo.php');
include_once ('./MapObject.php');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AppManager
 *
 * @author puwakitk
 */
class AppManager {

    //put your code here
    private $obj;

    public function __construct() {
        $this->obj = new AppbaseManager();
    }

    /*
     * หลังจาก user ตกลง confirm insert data แล้วเรียก function นี้
     */

    function InsertApplication(ApplicationObj $obj) {
        return $this->obj->RequestInsertApplication($obj);
    }

    function InsertMapTable(MapObject $obj) {
        return $this->obj->RequestInsertMap($obj);
    }

    function InsertTester(TesterHeaderInfo $obj) {
        return $this->obj->RequestInsertTester($obj);
    }

    function InsertExamResult(ExamObj $examObj) {
        return $this->obj->RequestInsertExamResult($examObj);
    }

    function UpdateMapTable(MapObject $obj) {
        return $this->obj->RequestUpdateMapTable($obj);
    }

    function UpdateApplication(ApplicationObj $obj) {
        return $this->obj->RequestUpdateApplication($obj);
    }

    function CalculateSkinFold($data1, $data2, $data3, $data4) {
        return ($data1 + $data2 + $data3 + $data4) / 4;
    }

    function CalculateHandGrip($left, $right, $weight) {
        $temp = ($left + $right) / 2;
        return $temp / $weight;
    }

    function CalculateAstrand($value1, $value2, $load, $sex, $age) {
        $temp = round(($value1 + $value2) / 2);
        $oxy = $this->obj->GetOxygenAstrand($temp, $load, $sex);
        $temp2 = ($oxy * 1000) / 60;
        $multiply = $this->obj->GetMultiplyAstrand($age);
        $temp3 = $temp2 * $multiply;
    }

    function Auth($user, $pass) {
        $res = new ResponseHeader();
        $res = $this->obj->CheckAuthentication($user, $pass);
        //print_r($res);
        $firsttime = 0;
        if ($res->MSGID === AppConst::STATUS_SUCCESS) {
            $firsttime = $res->MSGDETAIL2;
            if ($firsttime === 1) {
                $c = $this->obj->DeleteUserLogbyID($res->MSGDETAIL);
                if ($c->MSGID === AppConst::STATUS_SUCCESS) {
                    $session = "ses" . $this->GetIDFromDateTime();
                    $chk2 = $this->obj->InsertUserLogin($res->MSGDETAIL, $session, $this->get_client_ip(), "TH");
                    if ($chk2->MSGID === AppConst::STATUS_SUCCESS) {
                        $res->MSGDETAIL .= "|" . $firsttime;
                        $res->MSGDETAIL2 = $session;
                        $_SESSION["sessionid"] = $session;
                    } else {
                        $res = $chk2;
                    }
                } else {
                    $res = $c;
                }
                return $res;
            } else {
                $chk = $this->obj->CheckDuplicateLogin($res->MSGDETAIL);
                //print_r($chk);
                if ($chk->MSGID === AppConst::STATUS_SUCCESS) {
                    $session = "ses" . $this->GetIDFromDateTime();
                    $chk2 = $this->obj->InsertUserLogin($res->MSGDETAIL, $session, $this->get_client_ip(), "TH");
                    //print_r($chk2);
                    if ($chk2->MSGID === AppConst::STATUS_SUCCESS) {
                        $res->MSGDETAIL2 = $session;
                        $_SESSION["sessionid"] = $session;
                    } else {
                        $res = $chk2;
                    }
                } else {
                    $res->MSGID = AppConst::STATUS_DUPLICATE_ERROR;
                    $res->MSGDETAIL = $res->MSGDETAIL . "|" . $chk->MSGDETAIL;
                    $res->MSGDETAIL2 = "ชื่อผู้ใช้คุณกำลังถูกใช้งาน คุณต้องการเข้าใช้งานแทนหรือไม่?";
                }
            }
        } else {
            $res->MSGDETAIL2 = "ชื่อ/รหัสผ่าน ผิด";
        }
        return $res;
    }

    function Logout($userid, $session) {
        return $this->obj->Logout($userid, $session);
    }

    function CheckUserIDBySession($session) {
        return $this->obj->CheckUserIDBySession($session);
    }

    function GetTesterID() {
        return "T" . $this->GetIDFromDateTime();
    }

    function GetAppID() {
        return "A" . $this->GetIDFromDateTime();
    }

    function EncMD5($value) {
        return md5($value);
    }

    function get_client_ip() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if (getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if (getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if (getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if (getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if (getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';

        $ipaddress = filter_var($ipaddress, FILTER_VALIDATE_IP);
        $ipaddress = ($ipaddress === false) ? '0.0.0.0' : $ipaddress;
        return $ipaddress;
    }

    function GetIDFromDateTime() {
        date_default_timezone_set("Asia/Bangkok");
        $t = microtime(true);
        $micro = sprintf("%06d", ($t - floor($t)) * 1000000);
        $date = new DateTime(date('y-m-d H:i:s.' . $micro, $t));
        $res = $date->format("ymdHisu");
        return $res;
    }

    function RequestDDLGrpTest() {
        $res = "";
        $res .= "<option value = '0' >---กรุณาเลือก---</option>";
        $res .= $this->obj->RequestForDDLGrpTest();
        $res .= "<option value='other'>---อื่นๆ---</option>";
        return $res;
    }

    function GetSearchTableByValue($dt, $grpTest, $testerID, $name, $surname, $action) {
        return $this->obj->GetSearchResultTable($dt, $grpTest, $testerID, $name, $surname, $action);
    }

    function GetLatestExamByTesterID($appID, $times) {
        return $this->obj->GetLatestExamByTesterID($appID, $times);
    }

    function CalCulateAge($dob) {
        //echo $dob;
        $tz = new DateTimeZone('Asia/Bangkok');
        $age = DateTime::createFromFormat('d/m/Y', $dob, $tz)
                        ->diff(new DateTime('now', $tz))
                ->y;
        return $age;
    }

    function GetScoreGrade(ExamObj $examObj, TesterHeaderInfo $testerObj) {
        $age = $this->CalCulateAge($testerObj->GetDOB());
        return $this->obj->GetScoreGrade($examObj, $testerObj, $age);
    }

    function returnBlankIfNull($obj) {
        if ($obj == null) {
            return "";
        }
        return $obj;
    }

    function GetSummaryByExamID($examType, $grpTest, $from, $to) {
        //return $this->obj->GetSummaryByExamType($examType, $grpTest, $from, $to);
        return $this->obj->GetSummaryByExamTypeNew($examType, $grpTest, $from, $to);
    }

    function GetDDLExamType() {
        return $this->obj->GetDDLExamType();
    }

    function GetTesterDetailByAppID($appID) {
        return $this->obj->GetTesterDetailByAppID($appID);
    }

    function GetExamDetailByAppID($appID) {
        return $this->obj->GetExamDetailByAppID($appID);
    }

    function UpdateTester(TesterHeaderInfo $tester) {
        return $this->obj->UpdateTesterDetail($tester);
    }

    function UpdateMapExam(ExamObj $obj) {
        return $this->obj->UpdateMapExam($obj);
    }

    function GetPrintInfoGraphicByAppID($appID, $param) {
        //return $this->obj->RequestForPrintByAppID($appID, $param);
        return $this->obj->RequestForPrintByAppIDNew($appID);
    }

    function GetPrintInfoGraphicByAppIDNew($appID) {
        return $this->obj->RequestForPrintByAppIDNew($appID);
    }

    function ConvertDateFromInput($dt) {
        $date = str_replace('/', '-', $dt);
        $newdt = date("Y-m-d", strtotime($date));
        return $newdt;
    }

    function GetDDLAstrandLoad() {
        return $this->obj->GetDDLAstrandLoad();
    }

    function checkSession($sessionid) {
        return $this->obj->checkSession($sessionid);
    }

    function GetDDLMultistageLevel() {
        $option = "<option value = '0'>กรุณาเลือก</option>";
        $payload = $this->obj->GetDDLMultistageLevel();
        return $option . $payload;
    }

    function GetDDLMultistageStage() {
        $option = "<option value = '0'>กรุณาเลือก</option>";
        $payload = $this->obj->GetDDLMultistageStage();
        return $option . $payload;
    }

    function GetShowExamListInfographic() {
        return $this->obj->GetShowExamListInfographic();
    }

    function GetTesterIDKey() {
        date_default_timezone_set('Asia/Bangkok');
        $year = date("Y");
        $month = date("m");
        $day = date("d");
        $hour = intval(date("H")) + 1;
        $hour = sprintf('%02d', $hour);
        $minute = date("i");
        $rand = mt_rand(0,9999);
        $rand = sprintf('%04d', $rand);
        $res = "";
        $key = $this->obj->GetTesterIDKey();
        $key = $key + 1;
        if ($res === AppConst::STATUS_ERROR) {
            
        } else {
            $res = "T" . $year . $month . $day . $hour . $minute . $rand;
        }
        return $res;
    }

    function ResetPassword($userID, $oldPassword, $newPassword) {
        $returnval = new ResponseHeader();
        $res = new ResponseHeader();
        //echo "user  = ".$userID." pass = ".$oldPassword;
        $res = $this->obj->GetUsernameByUserID($userID, $oldPassword);
        //print_r($res);
        $res2 = new ResponseHeader();
        if ($res->MSGID === AppConst::STATUS_SUCCESS) {
            $res2 = $this->obj->UpdatePassword($userID, $newPassword);
            $returnval = $res2;
        } else {
            $returnval = $res;
        }
        return $returnval;
    }

    function GetExamCategory() {
        $res = $this->obj->GetExamCategory(); //Return type = array
        $response = new ResponseHeader();
        $response->MSGID = AppConst::STATUS_SUCCESS;
        $response->ResponseDetail[] = $res;
        return $response;
    }

    function GetExamObject() {
        //$res = $this->obj->GetExamDetailObject();
        $res = $this->obj->GetExamInput();
        return $res;
    }

    function GetExamListbyAppIDs($datefrom, $dateto, $gender, $agefrom, $ageto, $isresearch, $grpTest) {
        return $this->obj->GetExamListbyAppIDs($datefrom, $dateto, $gender, $agefrom, $ageto, $isresearch, $grpTest);
    }

    function GetApplicationIDs($datefrom, $dateto, $gender, $agefrom, $ageto, $isresearch, $grpTest) {
        return $this->obj->GetApplicationIDs($datefrom, $dateto, $gender, $agefrom, $ageto, $isresearch, $grpTest);
    }

    function CreateTemplate($grpid, $examids) {
        $res = new ResponseHeader();
        $res->MSGID = AppConst::STATUS_SUCCESS;
        if (!$this->obj->CheckGroupIDisExist($grpid)) {
            $res = $this->obj->CreateNewGroup($grpid);
            $grpid = $res->MSGDETAIL;
        }
        $this->obj->DeleteExamTemplate($grpid);
        if ($res->MSGID === AppConst::STATUS_SUCCESS) {
            foreach ($examids as $examid) {
                if ($res->MSGID === AppConst::STATUS_SUCCESS) {
                    if (!$this->obj->CheckExamTemplateIsExist($grpid, $examid)) {
                        $res = $this->obj->InsertExamTemplate($examid, $grpid);
                    }
                } else {
                    break;
                }
            }
        }
        return $res;
    }

    function GetTemplateExamByGroupID($grpid) {
        return $this->obj->GetTemplateExamByGroupID($grpid);
    }

    function GetReportDataTable($datefrom, $dateto, $gender, $agefrom, $ageto, $isresearch, $grpTest) {
        return $this->obj->GetReportDataTable($datefrom, $dateto, $gender, $agefrom, $ageto, $isresearch, $grpTest);
    }

    function GetDDLGender() {
        $arr = array();
        $arr[0] = "เลือกทุกเพศ";
        $arr['M'] = "เพศชาย";
        $arr['F'] = "เพศหญิง";
        return $arr;
    }

    function GetDDLIsResearch() {
        $arr = array();
        $arr[0] = "เลือกทั้งหมด";
        $arr[1] = "เลือกเฉพาะข้อมูลงานวิจัย";
        $arr[2] = "เลือกข้อมูลทั่วไป";
        return $arr;
    }

    function GetDDLAge() {
        $arr = array();
        for ($i = 0; $i <= 120; $i++) {
            $arr[$i] = $i;
        }
        return $arr;
    }

    function GetRawTestData($appid, $examid) {
        return $this->obj->GetRawTestData($appid, $examid);
    }

    /* function GetApplicationIDs($datefrom, $dateto, $gender, $agefrom, $ageto, $isresearch){
      return $this->obj->GetApplicationIDs($datefrom, $dateto, $gender, $agefrom, $ageto, $isresearch);
      } */
    
    function GetMinMaxNormalization(){
        return $this->obj->GetMinMaxNormalization();
    }
    
    function DeleteMapExamForUpdateByAppID($appid){
        return $this->obj->DeleteMapExamForUpdateByAppID($appid);
    }
    
    function GetDropdownSummaryExamlist($value){
        $datalist = $this->obj->GetSummaryExamDataList();
        $str = "";
        foreach ($datalist as $key => $value){
            $option = "";
            if($value == $key){
                $option = "selected";
            }
            $str .= "<option value = '".$key."' ".$option.">".$value."</option>";
        }
        return $str;
    }
}
