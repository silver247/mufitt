<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SearchResultResponse
 *
 * @author puwakitk
 */
class SearchResultResponse {
    //put your code here
    public $UID;
    public $LINK;
    public $NAME;
    public $SURNAME;
    public $GENDER;
    public $ACTION;
    public $APPLYDATE;
    public $GRPTEST;
    public $TESTERID;
    public $TESTER_EMAIL;
}
