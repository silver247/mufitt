<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AppConst
 *
 * @author puwakitk
 */
class AppConst {

    //put your code here
    function __construct() {
        
    }

    const APP_STATUS_00 = "000";
    const APP_STATUS_10 = "100"; //ใช้้ตอนหลังจาก create table map
    const APP_STATUS_70 = "700";
    const EXAM_CAT_AGI = "examcat05";
    const EXAM_CAT_BAL = "examcat04";
    const EXAM_CAT_CAR = "examcat03";
    const EXAM_CAT_END = "examcat06";
    const EXAM_CAT_FLX = "examcat02";
    const EXAM_CAT_PER = "examcat00";
    const EXAM_CAT_STR = "examcat01";
    const EXAM_CAT_TBA = "examcat999";
    const EXAM_TYPE_6M_WALK_TEST = "type11";
    const EXAM_TYPE_6M_WALK_TEST_DISTANT = "type1101";
    const EXAM_TYPE_6M_WALK_TEST_HRPOST = "type1103";
    const EXAM_TYPE_6M_WALK_TEST_HRPRE = "type1102";
    const EXAM_TYPE_ASTRAND = "type10";
    const EXAM_TYPE_ASTRAND_LOAD = "type1001";
    const EXAM_TYPE_BACK_EXTEN = "type30";
    const EXAM_TYPE_BACK_STRACH = "type09";
    const EXAM_TYPE_BACK_STRACH_LEFT = "type0901";
    const EXAM_TYPE_BACK_STRACH_RIGHT = "type0902";
    const EXAM_TYPE_BICEP = "type06";
    const EXAM_TYPE_BMI = "type25";
    const EXAM_TYPE_BP_D = "type21";
    const EXAM_TYPE_BP_U = "type20";
    const EXAM_TYPE_EXCERCISE = "type18";
    const EXAM_TYPE_FITNESS = "type13";
    const EXAM_TYPE_HANDGRIP = "type01";
    const EXAM_TYPE_HANDGRIP_L = "type0101";
    const EXAM_TYPE_HANDGRIP_R = "type0102";
    const EXAM_TYPE_HEIGHT = "type27";
    const EXAM_TYPE_LEG_STRENGTH = "type29";
    const EXAM_TYPE_LONG_DIST_RUN = "type26";
    const EXAM_TYPE_LTEST = "type16";
    const EXAM_TYPE_MULTISTAGE = "type12";
    const EXAM_TYPE_MULTISTAGE_LEVEL = "type1201";
    const EXAM_TYPE_MULTISTAGE_STAGE = "type1202";
    const EXAM_TYPE_OMRON = "type23";
    const EXAM_TYPE_OMRON_BMI = "type2304";
    const EXAM_TYPE_OMRON_BODYAGE = "type2305";
    const EXAM_TYPE_OMRON_BODYFAT = "type2301";
    const EXAM_TYPE_OMRON_RM = "type2303";
    const EXAM_TYPE_OMRON_VISCERAL = "type2302";
    const EXAM_TYPE_OMRON_WBF_ARM = "type2308";
    const EXAM_TYPE_OMRON_WBF_LEG = "type2309";
    const EXAM_TYPE_OMRON_WBF_TRUNK = "type2307";
    const EXAM_TYPE_OMRON_WBM_ARM = "type2312";
    const EXAM_TYPE_OMRON_WBM_LEG = "type2313";
    const EXAM_TYPE_OMRON_WBM_TRUNK = "type2311";
    const EXAM_TYPE_OMRON_WBODYFAT = "type2306";
    const EXAM_TYPE_OMRON_WBODYMUSCLE = "type2310";
    const EXAM_TYPE_ONE_LEG_BAL = "type17";
    const EXAM_TYPE_ONE_LEG_BAL_L = "type1701";
    const EXAM_TYPE_ONE_LEG_BAL_L_OPEN = "type170101";
    const EXAM_TYPE_ONE_LEG_BAL_L_CLOSE = "type170102";
    const EXAM_TYPE_ONE_LEG_BAL_R = "type1702";
    const EXAM_TYPE_ONE_LEG_BAL_R_OPEN = "type170201";
    const EXAM_TYPE_ONE_LEG_BAL_R_CLOSE = "type170202";
    const EXAM_TYPE_ONE_LEG_BAL_OPEN = "type1703";
    const EXAM_TYPE_ONE_LEG_BAL_CLOSE = "type1704";
    const EXAM_TYPE_PLANK = "type07";
    const EXAM_TYPE_PULSE = "type19";
    const EXAM_TYPE_PUSH_UP = "type05";
    const EXAM_TYPE_SHUTTLE_RUN = "type14";
    const EXAM_TYPE_SIT_REACH = "type08";
    const EXAM_TYPE_SIT_UP = "type04";
    const EXAM_TYPE_SKIN_FOLD = "type22";
    const EXAM_TYPE_SKIN_FOLD_BICEP = "type2201";
    const EXAM_TYPE_SKIN_FOLD_SUBSCAP = "type2203";
    const EXAM_TYPE_SKIN_FOLD_SUPRA = "type2204";
    const EXAM_TYPE_SKIN_FOLD_TRICEP = "type2202";
    const EXAM_TYPE_STAND_BOARD_JUMP = "type02";
    const EXAM_TYPE_VERICAL_JUMP = "type03";
    const EXAM_TYPE_WAIST_CIRC = "type24";
    const EXAM_TYPE_WEIGHT = "type28";
    const EXAM_TYPE_ZIGZAG = "type15";
    const EXAM_TYPE_SIT_TO_STAND = "type31";
    const EXAM_TYPE_FLEXARM_HANG = "type33";
    const EXAM_TYPE_3MINS_STEP = "type33";
    const EXAM_TYPE_2MINS_STEP = "type34";
    const EXAM_TYPE_TTEST = "type35";
    const EXAM_TYPE_STAR = "type36";
    const EXAM_TYPE_STAR_A = "type3601";
    const EXAM_TYPE_STAR_P = "type3602";
    const EXAM_TYPE_STAR_M = "type3603";
    const EXAM_TYPE_STAR_L = "type3604";
    const EXAM_TYPE_STAR_AL = "type3605";
    const EXAM_TYPE_STAR_AM = "type3606";
    const EXAM_TYPE_STAR_PL = "type3607";
    const EXAM_TYPE_STAR_PM = "type3608";
    const EXAM_TYPE_STAR_LEG_LENGTH = "type3609";
    const FEMALE = "F";
    const MALE = "M";
    const METHOD_AUTH = "do-login";
    const METHOD_EDIT_APP = "do-edit-app";
    const METHOD_INSERT = "do-insert-app";
    const METHOD_INSERT_APP = "do-insert-app";
    const METHOD_INSERT_JAPP = "do-insert-japp";
    const METHOD_LOGOUT = "do-logout";
    const METHOD_QUERY_SEARCH_EDIT = "do-sfeit";
    const METHOD_QUERY_SEARCH_PRINT = "do-sfprint";
    const METHOD_QUERY_SUMMARY = "do-qsum";
    const METHOD_RESET_PASSWORD = "do-rpswd";
    const METHOD_CREATE_TEMPLATE = "do-create-template";
    const METHOD_VIEW_TEMPLATE = "do-view-template";
    const METHOD_QUERY_NORMALIZE = "do-query-normalize";
    const PARAM_PAS = "pass";
    const PARAM_PRINT_APP = "do-printapp";
    const PARAM_TESTER = "tester";
    const PARAM_USR = "usr";
    const PARAM_OLD_PAS = "opass";
    const PARAM_VIEW_APP = "do-viewapp";
    const STATUS_DUPLICATE_ERROR = "ecode03";
    const STATUS_ERROR = "ecode01";
    const STATUS_SQL_ERROR = "ecode02";
    const STATUS_SUCCESS = "code01";
    const STATUS_SQL_NODATA = "ecode00";
    const SUMMARY_AVERAGE = "savg";
    const SUMMARY_MAX = "smax";
    const SUMMARY_MIN = "smin";
    const SUMMARY_TOTAL = "stotal";
    const TABLE_APPLICATION = "application";
    const TABLE_AST_AGEF = "AstrandAgeFactorMaster";
    const TABLE_AST_OXY = "AstrandOxygenMaster";
    const TABLE_ASTRAND = "astrand";
    const TABLE_EXAM_MASTER = "exam";
    const TABLE_EXAM_RESULT = "mapexam";
    const TABLE_FILTER_TABLE = "filter_table";
    const TABLE_GRADE = "grade";
    const TABLE_J_APPLICATION = "japplication";
    const TABLE_MAP = "map";
    const TABLE_MASTER_DATA = "MasterData";
    const TABLE_MULTISTAGE_COMPARE = "MultistageMaster";
    const TABLE_SKINFOLD_SUMMARY = "SKINFOLD_SUMMARY";
    const TABLE_TESTER = "tester";
    const TABLE_USER_HEADER = "user";
    const TABLE_USER_LOGGING = "userlogging";

    function getExamGroupDesc($examGroup) {
        if ($examGroup === AppConst::EXAM_CAT_STR) {
            return "กำลังและความแข็งแรงของกล้ามเนื้อ";
        } else if ($examGroup === AppConst::EXAM_CAT_END) {
            return "ความทนทานของกล้ามเนื้อ";
        } else if ($examGroup === AppConst::EXAM_CAT_AGI) {
            return "ความคล่องตัว";
        } else if ($examGroup === AppConst::EXAM_CAT_BAL) {
            return "การทรงตัว";
        } else if ($examGroup === AppConst::EXAM_CAT_CAR) {
            return "ความทนทานของหัวใจ";
        } else if ($examGroup === AppConst::EXAM_CAT_FLX) {
            return "ความอ่อนตัว";
        } else if ($examGroup === AppConst::EXAM_CAT_PER) {
            return "Body Composition";
        } else{
            return "อื่นๆ ";
        }
    }

    function getBMIDesc($value) {
        if ($value == 1) {
            return "ผอม";
        } else if ($value == 2) {
            return "สมส่วน";
        } else if ($value == 3) {
            return "อ้วน";
        } else if ($value == 4) {
            return "อ้วนมาก";
        }
    }

    function getBodyFatDesc($value) {
        if ($value == 1) {
            return "ต่ำ";
        } else if ($value == 2) {
            return "ปกติ";
        } else if ($value == 3) {
            return "สูง";
        } else if ($value == 4) {
            return "สูงมาก";
        }
    }

    function getSkinfoldDesc($value) {
        if ($value == 1) {
            return "ผอม";
        } else if ($value == 2) {
            return "ค่อนข้างผอม";
        } else if ($value == 3) {
            return "สมส่วน";
        } else if ($value == 4) {
            return "ค่อนข้างอ้วน";
        } else if ($value == 5) {
            return "อ้วน";
        }
    }

}
