<html>
    <?php
    include 'page/header.php';
    ?>
    <tr>
        <td style="width: 1024px;height: 564px;margin: 0px auto;background-color: #ffffff;text-align: center;border: 1px solid #990000;border-top: none;border-bottom: none;" >
            <!--img src="images/index_02.png" width="1024" height="564" alt=""-->
            <form name="tform1" id="tform1" onsubmit="return false;">                
                <table style="width: 70%" border="0" cellpadding="0" cellspacing="0" align="center">
                    <tr>
                        <td style="width: 40%;padding-top: 20px;font-size: 20px;font-weight: bold" class="fontscreen" align="center">
                            <img src="images/contract.png" onclick="createApplication()" width="200" height="180" style="cursor: pointer"/>
                            <br/>
                            Create Application
                        </td>
                        <td style="width: 20%"></td>
                        <td style="width: 40%;padding-top: 20px;font-size: 20px;font-weight: bold" class="fontscreen" align="center">
                            <img src="images/school-material.png" onclick="editApplication()" width="200" height="180"  style="cursor: pointer"/>
                            <br/>
                            Edit Application
                        </td>
                    </tr>
                    <tr>
                        <td class="fontscreen" align="center" style="padding-top: 20px;font-size: 20px;font-weight: bold">
                            <img src="images/printer.png" onclick="printApplication()"  width="200" height="180" style="cursor: pointer"/>
                            <br/>
                            Print Application
                        </td>
                        <td>
                        </td>
                        <td class="fontscreen" align="center" style="padding-top: 20px;font-size: 20px;font-weight: bold">
                            <img src="images/office-material.png" onclick="summaryApplication()" width="200" height="180" style="cursor: pointer"/>
                            <br/>
                            Summary Application
                        </td>
                    </tr>
                    <tr>
                        <td class="fontscreen" align="center" style="padding-top: 20px;font-size: 20px;font-weight: bold">
                            <img src="images/analytics.png" width="200px" height="180px" onclick="ViewReport()" width="200" height="180" style="cursor: pointer"/>
                            <br/>
                            Report
                        </td>
                        <td>
                        </td>
                        <td class="fontscreen" align="center" style="padding-top: 20px;font-size: 20px;font-weight: bold">
                            <img src="images/check-mark.png" width="200px" height="180px" onclick="CreateTemplate()" width="200" height="180" style="cursor: pointer"/>
                            <br/>
                            Create Template
                        </td>
                    </tr>
                </table>
                <input name="do" type="hidden" value="<?=$action?>"/>
            </form>
        </td>
    </tr>
    <?php
    include 'page/footer.php';
    $successval = AppConst::STATUS_SUCCESS;
    $errorval = AppConst::STATUS_ERROR;
    ?>
    <script type="text/javascript">
        function createApplication(){
            window.open("createapp.php?seed="+Math.random(), "_top");
        }
        function editApplication(){
            window.open("searchapp.php?action=<?=AppConst::METHOD_QUERY_SEARCH_EDIT?>&seed="+Math.random(), "_top");
        }
        function printApplication(){
            window.open("searchapp.php?action=<?=AppConst::METHOD_QUERY_SEARCH_PRINT?>&seed="+Math.random(), "_top");
        }
        function summaryApplication(){
            window.open("summaryapp.php?seed="+Math.random(), "_top");
        }
        function ViewReport(){
            window.open("report.php?do=624819edf0fdf5240413bb88243028e52bcef899&seed="+Math.random(), "_blank");
        }
        function CreateTemplate(){
            window.open("createtemplete.php?seed="+Math.random(), "_top");
        }
    </script>
</html>