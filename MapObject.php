<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MapObject
 *
 * @author FREJOID
 */
class MapObject {
    //put your code here
    private $appID,$entryUsr,$status,$applyDate,$completeDate,$testerID,$isResearch;
    
    public function SetAppID($value){
        $this->appID = $value;
    }
    
    public function GetAppID(){
        return $this->appID;
    }
    
    public function SetEntryUser($value){
        $this->entryUsr = $value;
    }
    
    public function GetEntryUser(){
        return $this->entryUsr;
    }
    
    public function SetStatus($value){
        $this->status = $value;
    }
    
    public function GetStatus(){
        return $this->status;
    }
    
    public function SetApplyDate($value){
        $this->applyDate = $value;
    }
    
    public function GetApplyDate(){
        return $this->applyDate;
    }
    
    public function SetCompleteDate($value){
        $this->completeDate = $value;
    }
    
    public function GetCompleteDate(){
        return $this->completeDate;
    }
    
    public function SetTesterID($value){
        $this->testerID = $value;
    }
    
    public function GetTesterID(){
        return $this->testerID;
    }
    
    function SetIsResearch($value){
        $this->isResearch = $value;
    }
    
    function GetIsResearch(){
        return $this->isResearch;
    }
}
