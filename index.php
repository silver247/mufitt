<?php
include_once('AppConst.php');
include_once ('AppManager.php');
session_start();
$successval = AppConst::STATUS_SUCCESS;
$errorval = AppConst::STATUS_ERROR;
$duplicateval = AppConst::STATUS_DUPLICATE_ERROR;

function GetCurrentFilename($url) {
    $path = pathinfo($url);
    $filename = $path['basename'];
    $splitval = explode(".", $filename);
    return $splitval[0];
}
?>
<html>
    <head>
        <title>Phical Fitness Testing and Reconditioning</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <script type="text/javascript" src="jquery/jquery-3.1.1.min.js"></script>
        <script type="text/javascript">
            var defaultContentType = "application/x-www-form-urlencoded; charset=UTF-8";
            function sendLogin() {
                //alert("Start send login");
                try {
                    jQuery.ajax({
                        url: "AppHttpRequest.php",
                        type: "POST",
                        contentType: defaultContentType,
                        data: $("#tform1").serialize(),
                        dataType: "json",
                        error: function (transport, status, errorThrown) {
                            alert("error");
                        },
                        success: function (data) {
                            //alert(data);
                            //$(data).each(function(index,obj){                        
                            //});
                            if (data.MSGID == "<?= $successval ?>") {
                                var param = data.MSGDETAIL.split("|");
                                $("#userid").val(param[0]);
                                $("#sessionid").val(data.MSGDETAIL2);
                                if(param[1]=="1"){
                                    $("#dform1").attr("action","resetpw.php");
                                }
                                $("#dform1").submit();
                            } else if (data.MSGID == "<?= $duplicateval ?>") {
                                if (confirm(data.MSGDETAIL2)) {
                                    var param = data.MSGDETAIL.split("|");
                                    $("#userid").val(param[0]);
                                    $("#sessionid").val(param[1]);
                                    jQuery.ajax({
                                        url: "AppHttpRequest.php",
                                        type: "POST",
                                        contentType: defaultContentType,
                                        dataType: "json",
                                        data: $("#dform1").serialize()+"&do=<?= AppConst::METHOD_LOGOUT; ?>",
                                        error: function (transport, status, errorThrown) {
                                            alert("error");
                                        },
                                        success: function (data) {
                                            sendLogin();
                                        }
                                    });
                                }
                            } else if (data.MSGID == "<?= $errorval ?>") {
                                alert(data.MSGDETAIL2);
                            }
                        }
                    });
                } catch (ex) {
                    alert(ex);
                }
            }
        </script>
    </head>
    <body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
        <!-- ImageReady Slices (index.psd) -->
        <div style="text-align: center;margin: auto;width: 100%">
            <form name="tform1" id="tform1" onsubmit="return false;">
                <table id="Table_01" width="1024" height="768" border="0" cellpadding="0" cellspacing="0" align="center">
                    <tr>
                        <td colspan="3">
                            <img src="images/index_01.png" width="1024" height="166" alt=""></td>
                    </tr>
                    <tr>
                        <td rowspan="7">
                            <img src="images/index_02.png" width="726" height="503" alt=""></td>
                        <td>
                            <img src="images/index_03.png" width="180" height="158" alt=""></td>
                        <td rowspan="7">
                            <img src="images/index_04.png" width="118" height="503" alt=""></td>
                    </tr>
                    <tr>
                        <td height="40" style="background-color: #ffe0a3">
                            <?php
                            $paramname = AppConst::PARAM_USR;
                            $action = AppConst::METHOD_AUTH;
                            ?>
                            <input name="<?= $paramname ?>" type="text" id="username" style="border: 1px solid;border-color: #990000;background-color: transparent;color: #990000;height: 34"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src="images/index_06.png" width="180" height="20" alt=""></td>
                    </tr>
                    <tr>
                        <td  height="40" style="background-color: #ffe0a3">
                            <?php
                            $paramname = AppConst::PARAM_PAS;
                            ?>
                            <input name="<?= $paramname ?>" type="password" id="password"  style="border: 1px solid;border-color: #990000;background-color: transparent;color: #990000;height: 34""/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src="images/index_08.png" width="180" height="12" alt=""></td>
                    </tr>
                    <tr>
                        <td>
                            <img src="images/index_09.png" width="180" height="58" alt=""  style="cursor: pointer" onclick="sendLogin()"></td>
                    </tr>
                    <tr>
                        <td>
                            <img src="images/index_10.png" width="180" height="175" alt=""></td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <img src="images/index_11.png" width="1024" height="99" alt="">
                        </td>
                    </tr>
                </table>
                <input name="do" type="hidden" value="<?= $action ?>"/>
            </form>    
            <!-- End ImageReady Slices -->
            <form name="dform1" id="dform1" method="POST" action="landing.php">
                <input name="userid" id="userid" type="hidden" value=""/>
                <input name="sessionid" id="sessionid" type="hidden" value=""/>

            </form>
        </div>
    </body>
</html>