<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style type="text/css">
            @media print {
                footer {page-break-after: always;}

            }
            @media print {
                .info-header{
                    width: 100%;
                }
            }
            @media screen{
                .info-header{
                    width: 50%;
                }
            }
        </style>
        <script src="resources/Chartjs/Chart.js"></script>
        <script type="text/javascript" src="jquery/jquery-3.1.1.min.js"></script>
        <link rel="stylesheet" href="resources/css/appChart.css"/>
        <link rel="stylesheet" href="css/style.css"/>
        <script type="text/javascript">
            function drawInfoChart(elementid, arr_data) {
                var ctx = document.getElementById(elementid);
                var myChart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: ["1", "2", "3", "4", "5", "6"],
                        datasets: [{
                                label: '# of Votes',
                                data: arr_data,
                                backgroundColor: [
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(54, 162, 235, 0.2)',
                                    'rgba(255, 206, 86, 0.2)',
                                    'rgba(75, 192, 192, 0.2)',
                                    'rgba(153, 102, 255, 0.2)',
                                    'rgba(255, 159, 64, 0.2)'
                                ],
                                borderColor: [
                                    'rgba(255,99,132,1)',
                                    'rgba(54, 162, 235, 1)',
                                    'rgba(255, 206, 86, 1)',
                                    'rgba(75, 192, 192, 1)',
                                    'rgba(153, 102, 255, 1)',
                                    'rgba(255, 159, 64, 1)'
                                ],
                                borderWidth: 1
                            }]
                    },
                    options: {
                        legend: {
                            display: false,
                        },
                        tooltips: {
                            enabled: false
                        },
                        scales: {
                            yAxes: [{
                                    display: false,
                                    ticks: {
                                        beginAtZero: true
                                    }
                                }],
                        }
                    }
                });
            }
            /*var total = $("#hidTotal").val();
             for (var i = 0; i <= total; i++) {
             var ctx = document.getElementById("canvas" + i);
             var myChart = new Chart(ctx, {
             type: 'bar',
             data: {
             labels: ["1", "2", "3", "4"],
             datasets: [{
             label: '# of Votes',
             data: [12, 19, 3, 5],
             backgroundColor: [
             'rgba(255, 99, 132, 0.2)',
             'rgba(54, 162, 235, 0.2)',
             'rgba(255, 206, 86, 0.2)',
             'rgba(75, 192, 192, 0.2)'
             ],
             borderColor: [
             'rgba(255,99,132,1)',
             'rgba(54, 162, 235, 1)',
             'rgba(255, 206, 86, 1)',
             'rgba(75, 192, 192, 1)'
             ],
             borderWidth: 1
             }]
             },
             options: {
             legend: {
             display: false,
             },
             tooltips: {
             enabled: false
             },
             scales: {
             yAxes: [{
             display: false,
             ticks: {
             beginAtZero: true
             }
             }],
             }
             }
             });
             }*/

        </script>
    </head>
    <body>
        <?php
        include 'AppManager.php';
        include('AppConst.php');
        $appmng = new AppManager();
        $testerid = $_POST['testerid'];
        $appid = $_POST['appid'];
//echo $testerid;
        $numofexam = 6;
        $appobj = new ResponseHeader();
//echo $testerid;
        $examobj = NULL;
        $testerobj = new TesterHeaderInfo();

        $tmpresp = new ResponseHeader();
        //$tmpresp = $appmng->GetPrintInfoGraphicByAppID($appid, AppConst::PARAM_PRINT_APP);
        $tmpresp = $appmng->GetPrintInfoGraphicByAppIDNew($appid);
        $examlist = array();
        $examlist = $appmng->GetShowExamListInfographic();
        //print_r($examlist);
        /* $testerobj[] = new TesterHeaderInfo();
          $testerobj[] = $appmng->GetTesterDetailByAppID($appobj[$i]); */
        $n = 0;
        $k = 0;
        /* echo 'nam2<pre>';
          print_r($tmpresp);
          echo '</pre>'; */
        $tmpBMI = null;
        $tmpExcercise = null;
        $tmpWaistCirc = null;
        $tmpBP_D = null;
        $tmpBP_U = null;
        $tmpPulse = null;
        $tmpBodyfat = null;
        $tmpSkinfold = null;
        foreach ($tmpresp->ResponseDetail as $key => $value) {
            if ($key === AppConst::PARAM_TESTER) {
                continue;
            }
            //echo "print app[nam]" . $key;
            $exam = new ExamObj();
            $exam = $tmpresp->ResponseDetail[$key];

            if (AppConst::EXAM_TYPE_BMI == $exam->GetExamID()) {
                $tmpBMI = $exam;
            } else if (AppConst::EXAM_TYPE_EXCERCISE == $exam->GetExamID()) {
                $tmpExcercise = $exam;
            } else if (AppConst::EXAM_TYPE_WAIST_CIRC == $exam->GetExamID()) {
                $tmpWaistCirc = $exam;
            } else if (AppConst::EXAM_TYPE_PULSE == $exam->GetExamID()) {
                $tmpPulse = $exam;
            } else if (AppConst::EXAM_TYPE_BP_D == $exam->GetExamID()) {
                $tmpBP_D = $exam;
            } else if (AppConst::EXAM_TYPE_BP_U == $exam->GetExamID()) {
                $tmpBP_U = $exam;
            } else if (AppConst::EXAM_TYPE_OMRON_BODYFAT == $exam->GetExamID()) {
                $tmpBodyfat = $exam;
            } else if (AppConst::EXAM_TYPE_SKIN_FOLD == $exam->GetExamID()) {
                $tmpSkinfold = $exam;
            }
            $cat_exam = 0;
            if ($exam->GetExamCat() === AppConst::EXAM_CAT_BAL) {
                if ($exam->GetExamID() === AppConst::EXAM_TYPE_ONE_LEG_BAL_L) {
                    $cat_exam = 4;
                } else if ($exam->GetExamID() === AppConst::EXAM_TYPE_ONE_LEG_BAL_R) {
                    $cat_exam = 5;
                }
            } else {
                if ($exam->GetExamCat() === AppConst::EXAM_CAT_STR) {
                    if ($exam->GetExamID() === AppConst::EXAM_TYPE_HANDGRIP) {
                        $cat_exam = 0;
                    } else {
                        $cat_exam = 1;
                    }
                } else if ($exam->GetExamCat() === AppConst::EXAM_CAT_FLX) {
                    $cat_exam = 2;
                } else if ($exam->GetExamCat() === AppConst::EXAM_CAT_CAR) {
                    $cat_exam = 3;
                } else if ($exam->GetExamCat() === AppConst::EXAM_CAT_END) {
                    $cat_exam = 7;
                } else if ($exam->GetExamCat() === AppConst::EXAM_CAT_AGI) {
                    $cat_exam = 8;
                }
            }
            if ($exam->GetExamCat() === AppConst::EXAM_CAT_STR) {
                if ($exam->GetExamID() === AppConst::EXAM_TYPE_HANDGRIP) {
                    $examobj[$cat_exam] = $exam;
                } else {
                    if (isset($examobj[$cat_exam])) {
                        $tmp_obj = new ExamObj();
                        $tmp_obj = $examobj[$cat_exam];
                        if ($tmp_obj->GetExamPriority() < $exam->GetExamPriority()) {
                            $examobj[6] = $examobj[$cat_exam];
                            $examobj[$cat_exam] = $exam;
                        }
                    } else {
                        $examobj[$cat_exam] = $exam;
                    }
                }
            } else {
                if (isset($examobj[$cat_exam])) {
                    $tmp_obj = new ExamObj();
                    $tmp_obj = $examobj[$cat_exam];
                    if ($tmp_obj->GetExamPriority() < $exam->GetExamPriority()) {
                        $examobj[$cat_exam] = $exam;
                    }
                } else {
                    $examobj[$cat_exam] = $exam;
                }
            }
        }
        $testerobj = $tmpresp->ResponseDetail[AppConst::PARAM_TESTER];
        $appc = new AppConst();
        ?>
        <table class="table-infographic" >
            <tr>
                <td class="fontscreen" colspan="2" >
                    <table class="info-header" style=" margin-left: auto; margin-right: auto; font-size: 1em;">
                        <tr>
                            <td class="fontscreen">หมายเลขประจำตัวผู้ทดสอบ&nbsp;<?= $testerobj->GetTesterID() ?></td>
                            <td class="fontscreen">กลุ่มการทดสอบ&nbsp;<?= $testerobj->GetTesterGrp() ?></td>
                        </tr>
                        <tr>
                            <td class="fontscreen">ชื่อ&nbsp;<?= $testerobj->GetNameTH() ?></td>
                            <td class="fontscreen">นามสกุล&nbsp;<?= $testerobj->GetSurnameTH() ?></td>
                        </tr>
                        <tr>
                            <td class="fontscreen">ชื่อ(อังกฤษ)&nbsp;<?= $testerobj->GetNameEN() ?></td>
                            <td class="fontscreen">นามสกุล(อังกฤษ)&nbsp;<?= $testerobj->GetSurnameEN() ?></td>
                        </tr>
                        <tr>
                            <?php
                            $value = "";
                            if (AppConst::MALE == $testerobj->GetSex()) {
                                $value = "ชาย";
                            } else if (AppConst::FEMALE == $testerobj->GetSex()) {
                                $value = "หญิง";
                            }
                            ?>
                            <td class="fontscreen">เพศ&nbsp;<?= $value ?></td>
                            <?php
                            $dob = explode("/", $testerobj->GetDOB());
                            //print_r($dob);
                            $year = $dob[2] + 543;
                            $birthday = $dob[0] . "/" . $dob[1] . "/" . $year;
                            //echo $birthday;
                            ?>
                            <td class="fontscreen">วัน/เดือน/ปี เกิด(พ.ศ.)&nbsp;<?= $birthday ?>&nbsp;อายุ&nbsp;<?=$testerobj->GetAge();?></td>
                        </tr>
                        <tr>
                            <td class="fontscreen" colspan="2">
                                น้ำหนัก&nbsp;<?= $testerobj->GetWeight() ?>&nbsp;กก.&nbsp;&nbsp;&nbsp;
                                ส่วนสูง&nbsp;<?= $testerobj->GetHeight() ?>&nbsp;ซม.&nbsp;&nbsp;&nbsp;
                                <?php
                                $value = "-";
                                if ($tmpBMI != null) {
                                    $value = $tmpBMI->GetTestValue();
                                }
                                ?>
                                BMI&nbsp;<?= $value."".$appc->getBMIDesc($value)."" ?>&nbsp;kg/m2&nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="fontscreen">
                                <?php
                                $value = "-";
                                if ($tmpBodyfat != null) {
                                    $value = $tmpBodyfat->GetTestValue();
                                }
                                ?>
                                Body Fat&nbsp;<?= $value."% (".$appc->getBodyFatDesc($value).")" ?>
                            </td>
                            <td class="fontscreen">
                                <?php
                                $value = "-";
                                if ($tmpSkinfold != null) {
                                    $value = $tmpSkinfold->GetTestValue();
                                }
                                ?>
                                Skin Fold&nbsp;<?= $value."% (".$appc->getSkinfoldDesc($value).")" ?>
                            </td>
                        </tr>
                        <tr>
                            <?php
                            $value = "-";
                            if ($tmpExcercise != null) {
                                $value = $tmpExcercise->GetTestValue();
                                if ($value == "0") {
                                    $value = "ไม่เคย";
                                } else if ($value == "1") {
                                    $value = "1-2 ครั้ง ต่อสัปดาห์";
                                } else if ($value == "2") {
                                    $value = "2-5 ครั้ง ต่อสัปดาห์";
                                } else {
                                    $value = "-";
                                }
                            }
                            ?>
                            <td class="fontscreen" colspan="2">การออกกาลังกาย&nbsp;<?= $value ?></td>
                        </tr>
                        <tr>
                            <td class="fontscreen" colspan="2">
                                <?php
                                $value = "-";
                                if ($tmpBP_U != null && $tmpBP_D != null) {
                                    $value = $tmpBP_U->GetTestValue() . "/" . $tmpBP_D->GetTestValue();
                                }
                                ?>
                                ความดันโลหิต&nbsp;<?= $value ?>&nbsp;มม.ปรอท&nbsp;&nbsp;&nbsp;
                                <?php
                                $value = "-";
                                if ($tmpPulse != null) {
                                    $value = $tmpPulse->GetTestValue();
                                }
                                ?>
                                ชีพจร&nbsp;<?= $value ?>&nbsp;ครั้ง/นาที&nbsp;&nbsp;&nbsp;
                                <?php
                                $value = "-";
                                if ($tmpWaistCirc != null) {
                                    $value = $tmpWaistCirc->GetTestValue();
                                }
                                ?>
                                รอบเอว&nbsp;<?= $value ?>&nbsp;ซม.&nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <?php
            ?>
            <?php
            //echo "<br/>nam : " . count($examobj);
            $cnt = 0;
            $row = 0;
            $strstable = "";
            $n = 0;
            //foreach ($examobj as $key => $value) {
            for ($i = 0; $i <= 8; $i++) {
                $aExamObj = new ExamObj();
                if (!isset($examobj[$i])) {
                    continue;
                } else {
                    $aExamObj = $examobj[$i];
                }
                //$aExamObj = $value;
                if ($aExamObj->GetExamID() === AppConst::EXAM_TYPE_BMI) {
                    continue;
                } else {
                    if (!in_array($aExamObj->GetExamID(), $examlist)) {
                        continue;
                    } else {


                        //echo "count = ".$cnt."<br>";
                        //echo "count % 2= ".($cnt%2)."<br>";
                        if ($cnt % 2 === 0) {
                            echo "<tr class='table-row'><td width='50%'>";
                        } else {
                            echo "<td  width='50%'>";
                        }

                        //echo "<br/>$i:$key";
                        //echo "|$n|";

                        /* echo '<pre>';
                          print_r($aExamObj);
                          echo '</pre>'; */

                        if (($n == 0) || ($n == 2) || ($n == 4)) {
                            echo " <!--$n-->";
                            echo "<div style='clear: both;width: 100%;'>";
                        }
                        if ($row % 3 === 0 && $row != 0) {
                            //echo $row;
                            echo "<footer></footer>";
                            echo "<br><br><br>";
                        }
                        
                        ?>
                        <div class="infographic">
                            <div class="infographic-header">
                                <?= $appc->getExamGroupDesc($aExamObj->GetExamCat()) ?>
                            </div>
                            <div class="infographic-line"></div>

                            <div class="row">
                                <div class="infographic-result-box">
                                    <div class="infographic-result-header">ผลการทดสอบ</div>
                                    <div class="infographic-result-text"><?= $aExamObj->GetTestValue() ?></div>
                                </div>

                                <div class="infographic-chart">
                                    <div  class="infographic-chart-content">
                                        <canvas id="chart<?= $aExamObj->GetExamID(); ?>"></canvas>
                                        <script>drawInfoChart("<?= "chart" . $aExamObj->GetExamID(); ?>",<?= $aExamObj->GetExamGraph(); ?>)</script>
                                        <!--<canvas id="canvas<?= $cnt; ?>"></canvas>-->
                                    </div>
                                </div>
                            </div>


                            <div class="infographic-previous-result-box">
                                <div class="infographic-grade-box-header">ผลการทดสอบก่อนหน้า</div>
                                <div class="infographic-previous-result-circle">
                                    <?php
                                    $prev_score = $aExamObj->GetPrevoiusScore();
                                    if (!isset($prev_score) || $prev_score == null || $prev_score == "") {
                                        $prev_score = "-";
                                    }
                                    ?>
                                    <div class="infographic-grade-circle-text"><?= $prev_score; ?></div> 
                                </div>
                            </div>


                            <div class="infographic-grade-box">
                                <div class="infographic-grade-box-header">คะแนนการทดสอบปัจจุบัน</div>
                                <div class="infographic-grade-circle">
                                    <?php
                                    $gradeid = $aExamObj->GetGradeID();
                                    if (!isset($gradeid) || $gradeid == null || $gradeid == "") {
                                        $gradeid = "-";
                                    }
                                    ?>
                                    <div class="infographic-grade-circle-text"><?= $gradeid ?></div>
                                </div>
                            </div>

                            <div class="infographic-icon-circle">
                                <img src="images/result/<?= $aExamObj->GetExamID() ?>.png" />
                            </div>
                        </div>
                        <?php
                        if (($n == 1) || ($n == 3) || ($n == 5)) {
                            echo "</div>";
                        }
                        if ($n != 5) {
                            $n++;
                        } else {
                            $n = 0;
                        }
                        $cnt++;
                        if ($cnt % 2 === 0) {
                            $row++;
                            echo "</td></tr>";
                        } else {
                            echo "</td>";
                        }
                    }
                }
                ?>


                <?php
                /* for ($j = $n; $n <= $numofexam; $n++) {
                  if (($j == 0) || ($j == 2) || ($j == 4)) {
                  echo " <!--$j-->";
                  echo "<div style='clear: both;width: 100%;'>";
                  }

                  if (($j == 1) || ($j == 3) || ($j == 5)) {
                  echo "</div>";
                  }
                  }
                  //echo "<footer></footer>"; */

                //$strstable .= "</table>";
                //echo $strstable;
            }
            ?>
            <input type="hidden" name="hidTotal" value="<?= $cnt; ?>" id="hidTotal">
        </table>

    </body>
</html>