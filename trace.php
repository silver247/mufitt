<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of trace
 *
 * @author Nam
 */
class trace {

    //put your code here
    public $dirname = "./log";
    public $filename = "./log/server.log";

    function trace() {
        $this->checkDirectory();
        $this->checkFile();
    }

    function checkDirectory() {
        if (!file_exists($this->dirname)) {
            mkdir($this->dirname, 0770);
            //echo "The directory $this->dirname was successfully created.\n";
            exit;
        } else {
            //echo "The directory $this->dirname exists.\n";
        }
    }

    function checkFile() {
        if (!file_exists($this->filename)) {
            $fh = fopen($this->filename, 'w+');
            fclose($fh);
            //echo "The file $this->filename was successfully created.\n";
            exit;
        } else {
            //echo "The file $this->filename exists.\n";
        }
    }

    function debug($str) {
        $this->writeLog("Debug", $str);
    }

    function error($str) {
        $this->writeLog("Error", $str);
    }

    function info($str) {
        $this->writeLog("Info", $str);
    }

    function writeLog($type, $str) {
        $this->checkDirectory();
        $this->checkFile();
        $date = date_create(null, timezone_open("Asia/Bangkok"));
        $datestr = date_format($date, "Y-m-d H:i:s");
        $file = fopen($this->filename, 'a+');
        fwrite($file, "[" . $datestr . "] " . $type . " : " . $str."\n");
        fclose($file);
    }

}
