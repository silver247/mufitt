<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ApplicationObj
 *
 * @author puwakitk
 */
class ApplicationObj {
    //put your code here
    private $appID,$testerID,$completeDate,$grpTest;
    
    public function SetApplicationID($value){
        $this->appID = $value;
    }
    
    function GetApplicationID(){
        return $this->appID;
    }
    
    function SetCompleteDate($value){
        $this->completeDate = $value;
    }
    
    function GetCompleteDate(){
        return $this->completeDate;
    }

    function SetTesterID($value){
        $this->testerID = $value;
    }
    
    function GetTesterID(){
        return $this->testerID;
    }
    
    function SetGroupTest($value){
        $this->grpTest = $value;
    }
    
    function GetGroupTest(){
        return $this->grpTest;
    }
}
