<html>
    <?php
    include 'page/header.php';
    include_once 'AppManager.php';
    $appmng = new AppManager();
    $appmng->RequestDDLGrpTest();
    $action = $_GET['action'];
    ?>
    <script src="resources/js/calendar-th.js"></script>
    <script>
        $(function () {
            $("#applydate").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: "c-100:c+10"
            });
            $("#applydate").datepicker("option", "dateFormat", "dd/mm/yy");
            $("#applydate").datepicker($.datepicker.regional[ "th" ]);
        });

    </script>
    <tr>
        <td style="width: 1024px;height: 564px;vertical-align: top;background-color: #ffffff;text-align: center;border: 1px solid #990000;border-top: none;border-bottom: none;" >
            <form name="tform1" id="tform1" action="searchapp.php" method="POST">
                <table style="width: 100%" border="0" cellpadding="0" cellspacing="0" align="center">
                    <tr>
                        <td colspan="4" class="fontscreen"><h3>ค้นหาข้อมูลผู้ทำการทดสอบ</h3></td>
                    </tr>
                    <tr>
                        <?php
                        date_default_timezone_set("Asia/Bangkok");
                        $t = microtime(true);
                        $micro = sprintf("%06d", ($t - floor($t)) * 1000000);
                        $date = new DateTime(date('y-m-d H:i:s.' . $micro, $t));
                        $res = $date->format("Y-m-d");
                        ?>
                        <td class="fontscreen" style="padding-top: 10px">
                            วันที่เข้ารับการทดสอบ
                        </td>
                        <td>
                            <input type="text" name="applydate" id="applydate" size="25" />
                        </td>
                        <td class="fontscreen"  style="padding-top: 10px">
                            หมายเลขประจำตัวผู้ทดสอบ
                        </td>
                        <td>
                            <input type="text" name="testerid" id="testerid" size="25" value=""/>
                        </td>                                   
                    </tr>
                    <tr>                                    
                        <td  class="fontscreen"  style="padding-top: 10px">
                            ชื่อ
                        </td>
                        <td>
                            <input type="text" name="name" id="name" size="25"/>
                        </td>
                        <td  class="fontscreen"  style="padding-top: 10px">
                            นามสกุล
                        </td>
                        <td>
                            <input type="text" name="surname" id="surname" size="25"/>
                        </td>
                    </tr>
                    <tr>                                    
                        <td  class="fontscreen"  style="padding-top: 10px">
                            กลุ่มการทดสอบ
                        </td>
                        <td colspan="3">
                            <select name="testergrp" ><?= $appmng->RequestDDLGrpTest() ?></select>
                        </td>
                    </tr>
                    <tr>                                    
                        <td  colspan="2">
                            <input type="hidden" name="do" id="do" value="<?= $action ?>"/>
                        </td>
                        <td  colspan="2" style="text-align: right">
                            <img src="images/btn/back.png" style="cursor: pointer;padding-left: 10px;" onclick="back()"/>
                            <img src="images/btn/search.png" style="cursor: pointer;padding-left: 10px;" onclick="submitSearch()"/>
                        </td>
                    </tr>
                    <tr  class="result" style="display: none">
                        <td  colspan="4">
                            <hr/>
                        </td>
                    </tr>

                    <tr  class="result" style="display: none">
                        <td  colspan="4"  class="fontscreen">
                            <h3>ผลลัพธ์การค้นหา</h3>
                        </td>
                    <tr class="result" style="display: none;">
                        <td colspan="4"  class="fontscreen" style="padding-bottom: 1em;">
                            Email ผู้ทดสอบทั้งหมดของผลลัพธ์การค้นหา: 
                            <input type='text' id='txtemail' value = "" style="width: 300px;" readonly="true">
                            <button id="btnCopy" >คัดลอก</button>
                        </td>
                    </tr>
                    </tr>
                    <tr  class="result" style="display: none">
                        <td  class="fontscreen" colspan="4" id="result" style="text-align: center;">

                        </td>
                    </tr>
                </table>
            </form>
            <form name="dform1" id="dform1" method="POST" action="createapp.php">
                <input name="appid" id="appid" type="hidden" value=""/>
                <input name="testerid" id="testerid2" type="hidden" value=""/>

            </form>

        </td>
    </tr>

    <?php
    include 'page/footer.php';
    ?>
    <script type="text/javascript">
        var testerdata = [];
        var num_per_page = 10;
        $(function () {
            $("select[name=testergrp] option[valiue=other]").val("");
        });
        function back() {
            window.open("landing.php", "_top");
        }
        function submitEdit(appid, testerid) {
            $("#appid").val(appid);
            $("#testerid2").val(testerid);
            if ($("#do").val() == "<?= AppConst::METHOD_QUERY_SEARCH_PRINT ?>") {
                $("#dform1").attr("action", "viewapplication.php");
            }
            //prompt("",$("#dform1").serialize());
            $("#dform1").submit();
        }
        function submitSearch() {
            testerdata = [];
            $(".result").show();
            try {
                jQuery.ajax({
                    url: "AppHttpRequest.php",
                    type: "POST",
                    contentType: defaultContentType,
                    data: $("#tform1").serialize(),
                    dataType: "json",
                    error: function (transport, status, errorThrown) {
                        alert("error");
                    },
                    success: function (data) {
                        $("#result").empty();
                        var result = "<table width='100%' id='result_table' align='center' style='margin: 0px auto;'>";
                        result = result + "<thead><tr style='background-color: #666666;color: #ffffff'>";
                        result = result + "<td width='5%' class='fontscreen' style='text-align: center'>&nbsp;</td>";

                        result = result + "<td width='15%' class='fontscreen' style='text-align: center'>เลขประจำตัวผู้ทดสอบ</td>";
                        result = result + "<td width='25%' class='fontscreen' style='text-align: center'>ชื่อ-นามสกุล</td>";
                        result = result + "<td width='15%' class='fontscreen' style='text-align: center'>วันที่ทดสอบ</td>";
                        result = result + "<td width='15%' class='fontscreen' style='text-align: center'>กลุ่มทดสอบ</td>";
                        result = result + "<td width='15%' class='fontscreen' style='text-align: center'>Email</td>";
                        result = result + "<td width='10%' class='fontscreen' style='text-align: center'>&nbsp;</td>";
                        //result = result + obj.UID;//NAME SURNAME GENDER APPLYDATE GRPTEST class='fontscreen'
                        result = result + "</tr></thead>";
                        var n = 0;
                        result = result + "<tbody  style='overflow-y: scroll'>";
                        var maildata = "ไม่พบข้อมูล";
                        if (data.TESTER_GRPMAIL != null) {
                            maildata = data.TESTER_GRPMAIL;
                        }

                        var page = [];
                        $(data.DETAILS).each(function (index, obj) {
                            if (n != 0 && (n % num_per_page == 0)) {
                                testerdata[testerdata.length] = page;
                                page = [];
                            }
                            page[page.length] = obj;
                            n++;
                        });
                        //if (n <= num_per_page) {
                        testerdata[testerdata.length] = page;
                        //}
                        result = result + "</tbody></table>";
                        $("#result").append(result);
                        showSearchResult(1, maildata);
                    }
                });
            } catch (ex) {
                alert(ex);
            }
        }
        function showSearchResult(page, maildata) {
            $("#result_table tbody").empty();
            var objArray = testerdata[page - 1];
            var result = "";
            if (maildata != null) {
                $('#txtemail').val(maildata);
            }

            for (var i = 0; i < objArray.length; i++) {
                var obj = objArray[i];
                var color = '#FFFFFF';
                if (i % 2 == 1) {
                    color = '#CCCCCC';
                }
                result = result + "<tr  style='background-color: " + color + "'>";
                result = result + "<td width='5%' style='text-align: center' class='fontscreen'>" + (((page - 1) * num_per_page) + (i + 1)) + "</td>";

                result = result + "<td width='15%' style='text-align: center' class='fontscreen'>" + obj.TESTERID + "</td>";

                result = result + "<td width='25%' style='text-align: center' class='fontscreen'>" + obj.NAME + " " + obj.SURNAME + "</td>";
                result = result + "<td width='15%' style='text-align: center' class='fontscreen'>" + obj.APPLYDATE + "</td>";
                result = result + "<td width='15%' style='text-align: center' class='fontscreen'>" + obj.GRPTEST + "</td>";
                var mail = '-';
                if (obj.TESTER_EMAIL != null) {
                    mail = obj.TESTER_EMAIL;
                }
                result = result + "<td width='15%' style='text-align: center' class='fontscreen'><a href='mailto:" + mail + "'>" + mail + "</a></td>";
                var textaction = "แก้ไข";
                if ($("#do").val() == "<?= AppConst::METHOD_QUERY_SEARCH_PRINT ?>") {
                    textaction = "พิมพ์";
                }
                result = result + "<td width='10%' style='text-align: center' class='fontscreen'><a href='#' onClick=\"submitEdit('" + obj.UID + "','" + obj.TESTERID + "')\" style='text-color:#000000'>" + textaction + "</a></td>";
                //result = result + obj.UID;//NAME SURNAME GENDER APPLYDATE GRPTEST
                result = result + "</tr>";
            }
            for (var i = (num_per_page - objArray.length); i > 0; i--) {
                result = result + "<tr>";
                result = result + "<td colspan='6' style='text-align: center' class='fontscreen'>&nbsp;</td>";
                result = result + "</tr>";
            }
            result = result + "<tr>";
            result = result + "<td colspan='6' style='text-align: center' class='fontscreen'>";
            result = result + "<span onclick='showSearchResult(1)' style='cursor: pointer;padding-left: 3px'><<</span>";

            for (var i = 0; i < testerdata.length; i++) {
                if (i != 0) {
                    result = result + "&nbsp;,";
                }
                if ((i + 1) != page) {
                    result = result + "<span onclick='showSearchResult(" + (i + 1) + ")' style='cursor: pointer;padding-left: 3px'>" + (i + 1) + "</span>";
                } else {
                    result = result + "<span style='padding-left: 3px'><b>" + (i + 1) + "</b></span>";
                }
            }//tolalseachresult
            result = result + "<span onclick='showSearchResult(" + testerdata.length + ")' style='cursor: pointer;padding-left: 3px'>>></span>";
            result = result + "<td  class='fontscreen'>";
            result = result + "</tr>";
            $("#result_table tbody").append(result);
        }

        $('#btnCopy').click(function (event) {
            event.preventDefault();
            var mail = $('#txtemail').val();
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val(mail).select();
            document.execCommand("copy");
            $temp.remove();
            /*.select();
            document.execCommand("copy");*/
        });
    </script>
</html>