<?php

include_once ('AppManager.php');
include_once ('AppConst.php');
include_once ('TesterHeaderInfo.php');
include_once ('MapObject.php');
include_once ('ApplicationObj.php');
include_once ('ResponseHeader.php');
include_once ('ExamObj.php');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if ($_POST["do"] === AppConst::METHOD_AUTH) {
    AUTH($_POST[AppConst::PARAM_USR], $_POST[AppConst::PARAM_PAS]);
} else if ($_POST["do"] === AppConst::METHOD_LOGOUT) {
    //echo "dhtto = ".$_POST["do"];
    session_start();
    Logout();
    session_destroy();
} else if ($_POST["do"] === AppConst::METHOD_INSERT_APP) {
    $appID = GetApplicationID();
    $result = InsertTester();
    if ($result == AppConst::STATUS_SUCCESS) {
        $result = InsertMapTable($appID);
        if ($result == AppConst::STATUS_SUCCESS) {
            $result = InsertApplication($appID);
            if ($result == AppConst::STATUS_SUCCESS) {
                $result = InsertMapExam($appID);
                if ($result == AppConst::STATUS_SUCCESS) {
                    $result = UpdateStatusMapTable($appID);
                    if ($result == AppConst::STATUS_SUCCESS) {
                        $result = UpdateCompleteDateApplication($appID);
                        if ($result == AppConst::STATUS_SUCCESS) {
                            $res = array("result" => AppConst::STATUS_SUCCESS, "appid" => $appID);
                            echo json_encode($res);
                        }
                        else{
                            SET_RETURN_JSON($result);
                        }
                    }
                    else{
                        SET_RETURN_JSON($result);
                    }
                }
                else{
                    SET_RETURN_JSON($result);
                }
            }
            else{
                SET_RETURN_JSON($result);
            }
        }
        else{
            SET_RETURN_JSON($result);
        }
    }
    else{
        SET_RETURN_JSON($result);
    }
    
    
} else if ($_POST["do"] === AppConst::METHOD_QUERY_SEARCH_EDIT || $_POST["do"] === AppConst::METHOD_QUERY_SEARCH_PRINT) {
    GetSearchResult($_POST["do"]);
} else if ($_POST["do"] === AppConst::METHOD_QUERY_SUMMARY) {
    GetSummaryByExamType();
} else if ($_POST["do"] === AppConst::METHOD_EDIT_APP) {
    //$responseHeader = new ResponseHeader();
    $res1 = EditTester();
    $res2 = EditMapExam();
    $responseHeader = array("result" => AppConst::STATUS_SUCCESS, "appid" => $_POST["appid"]);
    /* if ($res1->MSGID !== AppConst::STATUS_SUCCESS || $res2->MSGID !== AppConst::STATUS_SUCCESS) {
      $responseHeader = array("result" => $res2->MSGID, "appid" => $_POST["appid"]);
      } else {
      $responseHeader = array("result" => AppConst::STATUS_SUCCESS, "appid" => $_POST["appid"]);
      //$responseHeader->MSGID = AppConst::STATUS_SUCCESS;
      } */
    echo json_encode($responseHeader);
} else if ($_POST["do"] === AppConst::METHOD_RESET_PASSWORD) {
    ResetPassword();
} else if ($_POST["do"] === AppConst::METHOD_CREATE_TEMPLATE) {
    CreateTemplate();
} else if ($_POST['do'] === AppConst::METHOD_VIEW_TEMPLATE) {
    GetTemplateExamByGroupID();
} else if ($_POST['do'] === AppConst::METHOD_QUERY_NORMALIZE) {
    GetMinMaxNormalization();
}

function AUTH($user, $pass) {
    $obj = new AppManager();
    $res = new ResponseHeader();
    $res = $obj->Auth($user, $pass);
    //$chk = $obj->Auth($user, $pass);
    /* if ($chk != AppConst::STATUS_ERROR && $chk != AppConst::STATUS_SQL_ERROR) {
      $res = array("result" => AppConst::STATUS_SUCCESS, "userID" => $chk);
      } else {
      $res = array("result" => AppConst::STATUS_ERROR);
      } */

    echo json_encode($res);
}

function Logout() {
    $obj = new AppManager();
    $res = new ResponseHeader();
    $res = $obj->Logout($_POST["userid"], $_POST["sessionid"]);
    echo json_encode($res);
}

function INSERT_APPLICATION() {
    
}

function SET_RETURN_JSON($msg) {
    /* if ($msg === AppConst::STATUS_ERROR) {
      $res = array("result" => AppConst::STATUS_ERROR);
      } else if ($msg === AppConst::STATUS_SQL_ERROR) {
      $res = array("result" => AppConst::STATUS_SQL_ERROR);
      } else if ($msg === AppConst::STATUS_SUCCESS) {
      $res = array("result" => AppConst::STATUS_SUCCESS);
      } */
    $res = array("result" => $msg);
    echo json_encode($res);
}

function GetDateTimeID() {
    $obj = new AppManager();
    return $obj->GetIDFromDateTime();
}

function GetApplicationID() {
    $obj = new AppManager();
    return $obj->GetAppID();
}

function InsertTester() {
    $testerObj = new TesterHeaderInfo();
    $testerObj->SetTesterID($_POST["testerid"]);
    $testerObj->SetNameTH($_POST["tname"]);
    $testerObj->SetSurnameTH($_POST["tsurname"]);
    //$testerObj->SetNameEN($_POST["ename"]);
    //$testerObj->SetSurnameEN($_POST["esurname"]);
    $date = str_replace('/', '-', $_POST["brithday"]);
    $newdt = date("Y-m-d", strtotime($date));
    $testerObj->SetDOB($newdt);
    $testerObj->SetSex($_POST["gender"]);
    if(!isset($_POST['telno'])){
        $testerObj->SetPhone('');
    }
    else{
        $testerObj->SetPhone($_POST["telno"]);
    }
    
    $testerObj->SetEmail($_POST["email"]);
    $appObj = new AppManager();
    $chk1 = $appObj->InsertTester($testerObj);

    if ($chk1 != AppConst::STATUS_SUCCESS) {
        SET_RETURN_JSON($chk1);
        exit;
    }
    return $chk1;
}

function InsertMapTable($appID) {
    $mapObj = new MapObject();
    $mapObj->SetAppID($appID);
    $mapObj->SetEntryUser($_POST['userid']);
    $mapObj->SetStatus(AppConst::APP_STATUS_10);
    $mapObj->SetTesterID($_POST["testerid"]);
    $mapObj->SetIsResearch($_POST["isResearch"]);
    $appObj = new AppManager();
    $chk1 = $appObj->InsertMapTable($mapObj);

    if ($chk1 != AppConst::STATUS_SUCCESS) {
        SET_RETURN_JSON($chk1);
        exit;
    }
    return $chk1;
}

function InsertApplication($appID) {
    $obj = new ApplicationObj();
    $obj->SetApplicationID($appID);
    $obj->SetCompleteDate("");
    $obj->SetGroupTest($_POST["testergrp"]);
    $obj->SetTesterID($_POST["testerid"]);
    $appObj = new AppManager();
    $chk1 = $appObj->InsertApplication($obj);
    if ($chk1 != AppConst::STATUS_SUCCESS) {
        SET_RETURN_JSON($chk1);
        exit;
    }
    return $chk1;
}

function InsertMapExam($appID) {
    $appObj = new AppManager();
    //echo "max val = ".$_POST["max"];
    //Insert Weight
    $data = new ExamObj();
    $data->SetAppID($appID);
    $data->SetExamID(AppConst::EXAM_TYPE_WEIGHT);
    $data->SetSeqNo(0);
    $data->SetTestValue($_POST["weight"]);
    $res = $appObj->InsertExamResult($data);
    if ($res !== AppConst::STATUS_SUCCESS) {
        SET_RETURN_JSON($res);
        exit;
    }

    //Insert Height
    $data = new ExamObj();
    $data->SetAppID($appID);
    $data->SetExamID(AppConst::EXAM_TYPE_HEIGHT);
    $data->SetSeqNo(0);
    $data->SetTestValue($_POST["height"]);
    $res = $appObj->InsertExamResult($data);
    if ($res !== AppConst::STATUS_SUCCESS) {
        SET_RETURN_JSON($res);
        exit;
    }

    //Insert Body Test
    $result = AppConst::STATUS_SUCCESS;
    for ($i = 1; $i <= $_POST["max"]; $i++) {
        if (isset($_POST["exam" . $i . "val"]) && $_POST["exam" . $i . "val"] != "") {
            if ($_POST["exam" . $i . "type"] === NULL || $_POST["exam" . $i . "type"] === "") {
                continue;
            }
            $data = new ExamObj();
            $data->SetAppID($appID);
            $data->SetExamID($_POST["exam" . $i . "type"]);
            $data->SetSeqNo($_POST["exam" . $i . "seqno"]);
            $data->SetTestValue($_POST["exam" . $i . "val"]);
            //echo "<br>i = " . $i . "ExamID = " . $data->GetExamID() . " Examval = " . $data->GetTestValue() . "<br>";
            $res = $appObj->InsertExamResult($data);
            //echo $res;
        }
        if ($res !== AppConst::STATUS_SUCCESS) {
            SET_RETURN_JSON($res);
            $result = $res;
            exit;
        }
    }
    return $result;
}

function UpdateStatusMapTable($appID) {
    $mapObj = new MapObject();
    $mapObj->SetAppID($appID);
    $mapObj->SetStatus(AppConst::APP_STATUS_70);
    $appObj = new AppManager();
    $chk1 = $appObj->UpdateMapTable($mapObj);

    if ($chk1 != AppConst::STATUS_SUCCESS) {
        SET_RETURN_JSON($chk1);
        exit;
    }
    return $chk1;
}

function UpdateCompleteDateApplication($appID) {
    $data = new ApplicationObj();
    $data->SetApplicationID($appID);
    $appObj = new AppManager();
    $chk1 = $appObj->UpdateApplication($data);
    if ($chk1 != AppConst::STATUS_SUCCESS) {
        SET_RETURN_JSON($chk1);
        exit;
    }
    return $chk1;
}

function GetSearchResult($action) {
    $appObj = new AppManager();
    //return 
    $res = $appObj->GetSearchTableByValue($_POST["applydate"], $_POST["testergrp"], $_POST["testerid"], $_POST["name"], $_POST["surname"], $action);
    /* if ($res->MSGID === AppConst::STATUS_SQL_ERROR || $res->MSGID === AppConst::STATUS_ERROR) {
      SET_RETURN_JSON($res->MSGID);
      exit;
      } else {
      echo json_encode($res);
      } */
    //print_r($res);
    echo json_encode($res);
}

function GetSummaryByExamType() {
    $appObj = new AppManager();
    $dateFrom = str_replace('/', '-', $_POST["txtDateFrom"]);
    $newFrom = date("Y-m-d", strtotime($dateFrom));
    $dateTo = str_replace('/', '-', $_POST["txtDateTo"]);
    $newTo = date("Y-m-d", strtotime($dateTo));
    $res = $appObj->GetSummaryByExamID($_POST["ddlExam"], $_POST["ddlGrpTest"], $newFrom, $newTo);
    echo json_encode($res);
    return;
}

function EditTester() {
    $testerObj = new TesterHeaderInfo();
    $testerObj->SetTesterID($_POST["testerid"]);
    $testerObj->SetNameTH($_POST["tname"]);
    $testerObj->SetSurnameTH($_POST["tsurname"]);
    $testerObj->SetNameEN($_POST["ename"]);
    $testerObj->SetSurnameEN($_POST["esurname"]);
    $date = str_replace('/', '-', $_POST["brithday"]);
    $newdt = date("Y-m-d", strtotime($date));
    $testerObj->SetDOB($newdt);
    $testerObj->SetSex($_POST["gender"]);
    //$testerObj->SetTesterGrp($_POST["grptest"]);
    $appObj = new AppManager();
    $res = $appObj->UpdateTester($testerObj);
    return $res;
}

function EditMapExam() {
    $appObj = new AppManager();
    $appObj->DeleteMapExamForUpdateByAppID($_POST["appid"]);
    InsertMapExam($_POST["appid"]);
    /* appObj = new AppManager();
      $res = new ResponseHeader();

      $data = new ExamObj();
      $data->SetAppID($_POST["appid"]);
      $data->SetExamID(AppConst::EXAM_TYPE_WEIGHT);
      $data->SetSeqNo(0);
      $data->SetTestValue($_POST["weight"]);
      $res = $appObj->UpdateMapExam($data);

      //Update Height
      $data = new ExamObj();
      $data->SetAppID($_POST["appid"]);
      $data->SetExamID(AppConst::EXAM_TYPE_HEIGHT);
      $data->SetSeqNo(0);
      $data->SetTestValue($_POST["height"]);
      $res = $appObj->UpdateMapExam($data);

      for ($i = 1; $i <= $_POST["max"]; $i++) {
      if (isset($_POST["exam" . $i . "val"]) && $_POST["exam" . $i . "val"] != "") {
      if ($_POST["exam" . $i . "type"] === NULL || $_POST["exam" . $i . "type"] === "") {
      continue;
      }
      $data = new ExamObj();
      $data->SetAppID($_POST["appid"]);
      $data->SetExamID($_POST["exam" . $i . "type"]);
      $data->SetSeqNo($_POST["exam" . $i . "seqno"]);
      $data->SetTestValue($_POST["exam" . $i . "val"]);
      //echo "<br>i = " . $i . "ExamID = " . $data->GetExamID() . " Examval = " . $data->GetTestValue() . "<br>";
      $res = $appObj->UpdateMapExam($data);
      if ($res->MSGID !== AppConst::STATUS_SUCCESS) {
      break;
      }
      }
      }
      return $res; */
}

function ResetPassword() {
    $appObj = new AppManager();
    $res = new ResponseHeader();
    $res = $appObj->ResetPassword($_POST[AppConst::PARAM_USR], $_POST[AppConst::PARAM_OLD_PAS], $_POST[AppConst::PARAM_PAS]);
    echo json_encode($res);
    return;
}

function CreateTemplate() {
    $cnt = filter_input(INPUT_POST, 'max');
    $grpid = filter_input(INPUT_POST, 'testergrp');
    $appm = new AppManager();
    $arr = array();
    for ($i = 0; $i < $cnt; $i++) {
        if (!in_array($_POST['examid'][$i], $arr)) {
            $arr[] = $_POST['examid'][$i];
        }
    }
    //service::printr($arr);
    $res = $appm->CreateTemplate($grpid, $arr);
    echo json_encode($res);
}

function GetTemplateExamByGroupID() {
    $grpid = filter_input(INPUT_POST, 'groupid');
    $appm = new AppManager();
    $res = $appm->GetTemplateExamByGroupID($grpid);
    echo json_encode($res);
}

function GetMinMaxNormalization() {
    $appm = new AppManager();
    echo json_encode($appm->GetMinMaxNormalization());
}

?>