<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Foundation for Sites</title>
        <link rel="stylesheet" href="resources/css/foundation.css">
        <link rel="stylesheet" href="resources/css/app.css">
    </head>
    <body>
        <div class="row fullscreen-bg-withImg">

            <div class="large-3 large-centered columns login-box-centering">
                <div class="login-box">
                    <div class="row collapse">
                        <div class="medium-12 columns">
                            <div class="input-group">
                                <span class="input-group-label"><i class="fi-torso-female"></i></span>
                                <input class="input-group-field" name="username" id="txtUname" type="text" placeholder="username">
                            </div>
                        </div>
                    </div>
                    <div class="row collapse">
                        <div class="medium-12 columns">
                            <div class="input-group">
                                <span class="input-group-label"><i class="fi-lock"></i></span>
                                <input class="input-group-field" name="password" id="txtPass" type="password" placeholder="password">
                            </div>
                        </div>

                    </div>
                    <div class="row collapse">
                        <div class="medium-12 columns">
                            <a href="index.php" id="do-login" class="expanded button">Login</a>
                        </div>
                    </div>
                    <div class="row collapse">
                        <div class="medium-3 columns medium-centered">
                            <div id="loadingAnimation" class="medium-12 columns hide">
                                <img src="./resource/img/ring.gif" />
                            </div>
                        </div>
                    </div>
                    <div class="row collapse">
                        <div class="medium-9 columns medium-centered">
                            <div id="errorLabel" class="medium-12 columns hide">
                                <label class="error-label">Username or password is wrong!!</label>
                            </div>
                            <?php
                            if ((isset($_GET['result'])) && $_GET['result'] == "error") {
                                ?>
                                <div id="loginErrorLabel" class="medium-12 columns">
                                    <label class="error-label"><span class="alert label"><i class="fi-x-circle">Authentication Error!!</i></span></label>
                                </div>
                                <?php
                            }
                            else if ((isset($_GET['result'])) && $_GET['result'] == "noAuth") {
                                ?>
                                <div id="loginErrorLabel" class="medium-12 columns">
                                    <label class="error-label"><span class="alert label"><i class="fi-x-circle"></i><?= "เข้าได้เฉพาะฝ่าย อรป."; ?></span></label>
                                </div>
                                <?php
                            }
                            ?>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
