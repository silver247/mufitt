<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ExamObj
 *
 * @author puwakitk
 */
class ExamObj {
    //put your code here
    private $appID, $examID, $seqNo, $testValue,$tdesc,$edesc,$gradeTDesc,$gradeEDesc,$gradeID,$messageID,$messageDesc,$testDate,$examCat,$examPriority,$examGraph,$previousScore,$uom;
    
    function SetAppID($value){
        $this->appID = $value;
    }
    
    function GetAppID(){
        return $this->appID;
    }
    
    function SetExamID($value){
        $this->examID = $value;
    }
    
    function GetExamID(){
        return $this->examID;
    }
    
    function SetSeqNo($value){
        $this->seqNo = $value;
    }
    
    function GetSeqNo(){
        return $this->seqNo;
    }
    
    function SetTestValue($value){
        $this->testValue = $value;
    }
    
    function GetTestValue(){
        return $this->testValue;
    }
    
    function SetTDescription($value){
        $this->tdesc = $value;
    }
    
    function GetTDescription(){
        return $this->tdesc;
    }
    
    function SetEDescription($value){
        $this->edesc = $value;
    }
    
    function GetEDescription(){
        return $this->edesc;
    }
    
    function SetGradeEDescription($value){
        $this->gradeEDesc = $value;
    }
    
    function GetGradeEDescription(){
        return $this->gradeEDesc;
    }
    
    function SetGradeTDescription($value){
        $this->gradeTDesc = $value;
    }
    
    function GetGradeTDescription(){
        return $this->gradeTDesc;
    }
    
    function SetGradeID($value){
        $this->gradeID = $value;
    }
    
    function GetGradeID(){
        return $this->gradeID;
    }
    
    public function SetMessageID($value){
        $this->messageID = $value;
    }
    
    public function GetMessageID(){
        return $this->messageID;
    }
    
    public function SetMessageDetail($value){
        $this->messageDesc = $value;
    }
    
    public function GetMessageDetail(){
        return $this->messageDesc;
    }
    
    public function SetTestDate($value){
        $this->testDate = $value;
    }
    
    public function GetTestDate(){
        return $this->testDate;
    }
    
    public function SetExamCat($value){
        $this->examCat = $value;
    }
    
    public function GetExamCat(){
        return $this->examCat;
    }
    
    public function SetExamPriority($value){
        $this->examPriority = $value;
    }
    
    public function GetExamPriority(){
        return $this->examPriority;
    }
    
    public function SetExamGraph($value){
        $this->examGraph = $value;
    }
    
    public function GetExamGraph(){
        return $this->examGraph;
    }
    
    public function SetPreviousScore($value){
        $this->previousScore = $value;
    }
    
    public function GetPrevoiusScore(){
        return $this->previousScore;
    }
    
    public function SetUOM($value){
        $this->uom = $value;
    }
    
    public function GetUOM(){
        return $this->uom;
    }
}