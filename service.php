<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of service
 *
 * @author puwakitk
 */
class service {
    //put your code here
    public static function printr($arr){
        echo '<pre>'; print_r($arr); echo '</pre>';
    }
    
    public static function mapExamcategoryDescription($examcat){
        $returnstr = "";
        if($examcat === 'examcat01'){
            $returnstr = "Strength (ความแข็งแรงของกล้ามเนื้อ)";
        }
        else if($examcat === 'examcat02'){
            $returnstr = "Flexibility (ความอ่อนตัว)";
        }
        else if($examcat === 'examcat03'){
            $returnstr = "Cardiovascular (ระบบหัวใจและการไหลเวียนเลือด)";
        }
        else if($examcat === 'examcat04'){
            $returnstr = "Balance (การทรงตัว)";
        }
        else if($examcat === 'examcat05'){
            $returnstr = "Agility (ความคล่องตัว)";
        }
        else if($examcat === 'examcat06'){
            $returnstr = "Endurance (ความทนทานของกล้ามเนื้อ)";
        }
        else if($examcat === 'examcat00'){
            $returnstr = "Body Composition";
        }
        else{
            $returnstr = "other (อื่นๆ)";
        }
        return $returnstr;
    }
    
    public static function ConvertObjectToDDL(array $arr, $selected){
        $str = "";
        foreach ($arr as $key => $value){
            $option = "";
            if($key == $selected){
                $option = "selected";
            }
            $str .= "<option value='".$key."' ".$option.">".$value."</option>";
        }
        return $str;
    }
    
    public static function DecompressAndMerge(array $array, array $needles){
        foreach ($needles as $needle){
            $array[] = $needle;
        }
        return $array;
    }
    
}
