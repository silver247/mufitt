<head>
    <title>Phical Fitness Testing and Reconditioning</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="resources/css/jquery-ui.min.css">
    <script type="text/javascript" src="jquery/jquery-3.1.1.min.js"></script>
    <!--<script src="resources/js/jquery-ui.js"></script>-->
    <link rel="stylesheet" href="resources/bootstrap-3.3.7-dist/css/bootstrap-theme.min.css"/>
    <link rel="stylesheet" href="resources/bootstrap-3.3.7-dist/css/bootstrap.min.css"/>
    <script src="resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="resources/datepicker/css/bootstrap-datepicker.min.css"/>
</head>
<?php
include_once('AppConst.php');
include_once ('AppManager.php');
session_start();
$filename = GetCurrentFilename($_SERVER["PHP_SELF"]);
if (isset($_POST['userid'])) {
    $_SESSION['userid'] = $_POST['userid'];
}
if (isset($_POST['sessionid'])) {
    $_SESSION['sessionid'] = $_POST['sessionid'];
}
if (!isset($_SESSION['userid']) || !isset($_SESSION['sessionid'])) {
    if ($filename !== "index") {
        header("Location: index.php");
    }
}

function GetCurrentFilename($url) {
    $path = pathinfo($url);
    $filename = $path['basename'];
    $splitval = explode(".", $filename);
    return $splitval[0];
}
?>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">    
    <script type="text/javascript">
        var defaultContentType = "application/x-www-form-urlencoded; charset=UTF-8";
        function logout() {
            jQuery.ajax({
                url: "AppHttpRequest.php",
                type: "POST",
                contentType: defaultContentType,
                data: {userid: "<?= $_SESSION["userid"]; ?>", sessionid: "<?= $_SESSION["sessionid"]; ?>", do: "<?= AppConst::METHOD_LOGOUT; ?>"},
                dataType: "json",
                error: function (transport, status, errorThrown) {
                    alert("error");
                },
                success: function (data) {
                    //alert(data);
                    //$(data).each(function(index,obj){                        
                    //});
                    if (data.MSGID == "<?= AppConst::STATUS_SUCCESS ?>") {
                        window.location.replace("index.php");
                    } else {
                        alert(data.MSGDETAIL2);
                        //alert("รหัสผ่านไม่ถูกต้อง");
                    }
                }
            });
        }
    </script>
    <div style="text-align: center;margin: auto;width: 100%">            
        <table id="Table_01" width="1024" height="0" border="0" cellpadding="0" cellspacing="0" align="center">
            <tr>
                <td class="fontscreen" style="text-align: right" colspan="3">
                    <?php
                    if (isset($_SESSION['userid']) && isset($_SESSION['sessionid'])) {
                        $appmng_tmp = new AppManager();
                        $responseHeader = new ResponseHeader();
                        $responseHeader = $appmng_tmp->checkSession($_SESSION['sessionid']);
                        $testObj = new TesterHeaderInfo();
                        if (isset($responseHeader->ResponseDetail[0])) {
                            $testObj = $responseHeader->ResponseDetail[0];
                        } else {
                            if ($filename !== "index") {
                                header("Location: index.php");
                            }
                        }
                        if ($filename !== "index") {
                            ?>
                            คุณ <?= $testObj->GetNameTH() ?> <span onclick="logout()" style="cursor: pointer">[ออกจากระบบ]</span>
                            <?php
                        }
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <img src="images/index_01.png" width="1024" height="166" alt="">
                </td>
            </tr>
        </table>