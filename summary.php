<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <?php
    include 'page/header.php';
    include ('./AppManager.php');
    $appObj = new AppManager();
    $ddlExam = $appObj->GetDDLExamType();
    ?>
    <tr>
        <td style="width: 1024px;height: 564px; vertical-align: top; background-color: #ffffff;text-align: center" >
            <table style="width: 80%" border="0" cellpadding="0" cellspacing="0" align="center">
                <tr>
                    <td>รายงานผลการทดสอบ</td>
                </tr>
                <tr>
                <form name="tform1" id="tform1" onsubmit="return false;">
                    <td>เลือกประเภทการทดสอบ</td>
                    <td>
                        <select name="ddlExam">
                            <?= $ddlExam; ?>
                        </select>
                    </td>
                    <td>
                        <input name="do" type="hidden" value="<?= AppConst::METHOD_QUERY_SUMMARY ?>"/>
                        <input type="submit" name="btnSumit" value="submit" onclick="sendLogin()">
                    </td>
                </form>
    </tr>
</table>
            <br><br>
<table id="tblResult" style="width: 80%; margin: 0px auto;"  border="1" cellpadding="0" cellspacing="0" align="center">

</table>
</td>

</tr>
<?php
include 'page/footer.php';
$successval = AppConst::STATUS_SUCCESS;
$errorval = AppConst::STATUS_ERROR;
?>
<script type="text/javascript">
    var defaultContentType = "application/x-www-form-urlencoded; charset=UTF-8";
    function sendLogin() {
        alert("Start send login");
        try {
            jQuery.ajax({
                url: "AppHttpRequest.php",
                type: "POST",
                contentType: defaultContentType,
                data: $("#tform1").serialize(),
                dataType: "json",
                error: function (transport, status, errorThrown) {
                    alert("error");
                },
                success: function (data) {
                    //var datas = JSON.parse(data);
                    if (data.MSGID === "<?= AppConst::STATUS_SUCCESS; ?>") {
                        var summary = "";
                        var average = "";
                        var sum_min = "";
                        var sum_max = "";

                        $.each(data.ResponseDetail, function (i, detail) {
                            if (detail.INDICATOR === "<?= AppConst::SUMMARY_TOTAL; ?>") {
                                summary = "<tr><td width='50%' style='background-color:#666666; color: white;' rowspan='3'>จำนวนผู้เข้ารับการทดสอบ</td><td>ชาย</td><td>" + detail.MALE + " คน</td></tr><tr><td>หญิง</td><td>" + detail.FEMALE + " คน</td></tr><tr><td>รวม</td><td>" + detail.TOTAL + "คน</td></tr>";
                            } else if (detail.INDICATOR === "<?= AppConst::SUMMARY_AVERAGE; ?>") {
                                average = "<tr><td width='50%' style='background-color:#666666; color: white;' rowspan='3'>ค่าเฉลี่ย</td><td>ชาย</td><td>" + detail.MALE + " </td></tr><tr><td>หญิง</td><td>" + detail.FEMALE + " </td></tr><tr><td>รวม</td><td>" + detail.TOTAL + "</td></tr>";
                            } else if (detail.INDICATOR === "<?= AppConst::SUMMARY_MAX; ?>") {
                                sum_max = "<tr><td width='50%' style='background-color:#666666; color: white;' rowspan='2'>ค่าสูงสุด</td><td>ชาย</td><td>" + detail.MALE + " </td></tr><tr><td>หญิง</td><td>" + detail.FEMALE + " </td></tr>";
                            } else if (detail.INDICATOR === "<?= AppConst::SUMMARY_MIN; ?>") {
                                sum_min = "<tr><td width='50%' style='background-color:#666666; color: white;' rowspan='2'>ค่าต่ำสุด</td><td>ชาย</td><td>" + detail.MALE + " </td></tr><tr><td>หญิง</td><td>" + detail.FEMALE + " </td></tr>";
                            }

                            //alert("Mine is " + i + "|" + detail.MALE + "|" + detail.FEMALE);
                        });
                        var str_rdy = summary.concat(average, sum_max, sum_min);
                        $("#tblResult").empty().append(str_rdy);

                    }
                }
            });
        } catch (ex) {
            alert(ex);
        }
    }
</script>
</html>