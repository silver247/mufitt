<html>
    <?php
    include 'page/header_report.php';
    include_once 'AppManager.php';
    include_once 'service.php';
    $appmng = new AppManager();
    $appmng->RequestDDLGrpTest();
    if (!isset($_GET['do'])) {
        header("Location: landing.php");
        die();
    }
    $do = filter_input(INPUT_GET, 'do');
    $chk = '624819edf0fdf5240413bb88243028e52bcef899';
    if ($do === $chk) {
        $from = filter_input(INPUT_GET, 'from');
        $to = filter_input(INPUT_GET, 'to');
        $g = filter_input(INPUT_GET, 'g');
        $af = filter_input(INPUT_GET, 'af');
        $at = filter_input(INPUT_GET, 'at');
        $rs = filter_input(INPUT_GET, 'rs');
        $gt = filter_input(INPUT_GET, 'gt');
        if ($from == null) {
            $date = new DateTime();
            $date->modify('-10 days');
            $from = $date->format('Y-m-d');
        }
        if ($to == null) {
            $to = date('Y-m-d');
        }
        if ($g == null) {
            $g = '0';
        }
        if ($af == null) {
            $af = '0';
        }
        if ($at == null) {
            $at = '120';
        }
        if ($rs == null) {
            $rs = '0';
        }
        if ($gt == null) {
            $gt = '0';
        }
        $result = $appmng->GetExamListbyAppIDs($from, $to, $g, $af, $at, $rs, $gt);
        $rows = $appmng->GetReportDataTable($from, $to, $g, $af, $at, $rs, $gt);
        //service::printr($result);
        //service::printr($rows);
        $examlist = array();
        $applist = $appmng->GetApplicationIDs($from, $to, $g, $af, $at, $rs, $gt);
        /*
         * Astrand
         */
        $astrandlist = array();
        foreach ($applist as $app) {
            $astrandlist[$app] = $appmng->GetRawTestData(AppConst::EXAM_TYPE_ASTRAND, $app);
        }
    } else {
        header("Location: landing.php");
        die();
    }

    //$action = $_GET['action'];href="resources/DataTables/datatables.min.css"
    ?>
    <link rel="stylesheet" type="text/css" href="resources/DataTables/datatables.min.css" />
    <br>
    <div class="row">
        <div class="col-md-12">
            <form name="tform1" method="get" class="form-inline" >
                <input type="hidden" name="do" value="<?= sha1('queryreport'); ?>">
                <div class="form-group">
                    <label for="txtDateFrom">ตั้งแต่วันที่</label>
                    <input type="text" class="form-control" name="from" id="txtDateFrom" value="<?= $from; ?>">
                </div>
                <div class="form-group">
                    <label for="txtDateTo">ถึงวันที่</label>
                    <input type="text" class="form-control" name="to" id="txtDateTo" value="<?= $to; ?>">
                </div>
                <div class="form-group">
                    <label for="g">เพศ</label>
                    <select name="g" id="ddlGender" class="form-control">
                        <?= service::ConvertObjectToDDL($appmng->GetDDLGender(), $g); ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="af">ตั้งแต่อายุ</label>
                    <select name="af" id="ddlAgeFrom" class="form-control">
                        <?= service::ConvertObjectToDDL($appmng->GetDDLAge(), $af); ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="at">ถึงอายุ</label>
                    <select name="at" id="ddlAgeTo" class="form-control">
                        <?= service::ConvertObjectToDDL($appmng->GetDDLAge(), $at); ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="rs">ประเภทข้อมูล</label>
                    <select name="rs" id="ddlResearch" class="form-control">
                        <?= service::ConvertObjectToDDL($appmng->GetDDLIsResearch(), $rs); ?>
                    </select>
                </div>
                <br>
                <br>
                <div class="form-group">
                    <label for="gt">กลุ่มการทดสอบ</label>
                    <select name="gt" id="gt" class="form-control">
                        <?= $appmng->RequestDDLGrpTest() ?>
                    </select>
                </div>
                <input type="submit" class="btn btn-primary" value="ค้นหา">
                <a href="landing.php" class="btn btn-warning" ><i class="icon-prev"></i>ย้อนกลับไปหน้าหลัก</a>
            </form>
        </div>
    </div>
    <br>
    <hr>
    <br>
    <div class="row">
        <div class="col-md-12">
            <table id="datatable" class="display table table-hover" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Action</th>
                        <th>รหัสการทดสอบ</th>
                        <th>เลขประจำตัว</th>
                        <th>ชื่อ</th>
                        <th>นามสกุล</th>
                        <th>เพศ</th>
                        <th>อายุ</th>
                        <?php
                        if ($result != null) {
                            foreach ($result as $key => $value) {
                                $examlist[] = $key;
                                echo "<th>" . $value . "</th>";
                            }
                        }
                        ?>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Action</th>
                        <th>รหัสการทดสอบ</th>
                        <th>เลขประจำตัว</th>
                        <th>ชื่อ</th>
                        <th>นามสกุล</th>
                        <th>เพศ</th>
                        <th>อายุ</th>
                        <?php
                        if ($result != null) {
                            foreach ($result as $key => $value) {
                                if (!in_array($key, $examlist)) {
                                    $examlist[] = $key;
                                }

                                echo "<th>" . $value . "</th>";
                            }
                        }
                        ?>
                    </tr>
                </tfoot>
                <tbody>
                    <?php
                    if ($rows->MSGDETAIL != null) {
                        //service::printr($examlist);
                        //service::printr($rows->MSGDETAIL);
                        foreach ($rows->MSGDETAIL as $keys => $values) {
                            //echo $keys;
                            //service::printr($values);
                            echo "<tr>"; // start row
                            $tester = new TesterHeaderInfo();
                            $rowlist = array();
                            foreach ($values as $key => $value) { //loop column
                                //service::printr($value);
                                if ($key !== "tester") {
                                    $examobj = new ExamObj();
                                    $examobj = $value;
                                    //echo $value->GetExamID(). " vs ".$value->GetAppID()."<br>";
                                    if ($examobj->GetExamID() != null) {
                                        if (in_array($examobj->GetExamID(), $examlist)) {
                                            $rowlist[$examobj->GetExamID()] = $examobj;
                                        }
                                    }
                                } else {
                                    $tester = $value;
                                }
                            }
                            //service::printr($rowlist);
                            $colarr = array();
                            foreach ($rowlist as $key => $value) {
                                foreach ($examlist as $examid) {
                                    //echo $key . "<br>";
                                    if ($examid == $key) {
                                        if (!isset($colarr[$examid]) || $colarr[$examid] == null || $colarr[$examid] == "-") {
                                            $colarr[$examid] = $value;
                                        }
                                    } else {
                                        if (!isset($colarr[$examid]) || $colarr[$examid] == null) {
                                            $colarr[$examid] = "-";
                                        }
                                    }
                                }
                            }

                            //service::printr($colarr[AppConst::EXAM_TYPE_HANDGRIP]);
                            echo "<td><a href='infographic.php?appid=" . $keys . "&testerid=" . $tester->GetTesterID() . "&trans=" . substr(md5(microtime()), rand(0, 26), 13) . "' class='btn btn-primary' target='_blank'>Info graphic</a></td>";
                            echo "<td>" . $keys . "</td>";
                            echo "<td>" . $tester->GetTesterID() . "</td>";
                            echo "<td>" . $tester->GetNameTH() . "</td>";
                            echo "<td>" . $tester->GetSurnameTH() . "</td>";
                            echo "<td>" . $tester->GetSex() . "</td>";
                            echo "<td>" . $tester->GetAge() . "</td>";
                            /* echo "<td>-</td>";
                              echo "<td>-</td>";
                              echo "<td>-</td>";
                              echo "<td>-</td>"; */
                            if ($colarr != NULL) {
                                foreach ($colarr as $col) {
                                    if ($col == "-") {
                                        echo "<td>" . $col . "</td>";
                                    } else {
                                        echo "<td>" . $col->GetTestValue() . "</td>";
                                    }
                                }
                            }
                            /* if ($rowlist != null) {
                              foreach ($rowlist as $data) {
                              if ($data == "" || $data == null) {
                              echo "<td>-</td>";
                              } else {
                              echo "<td> examid = " . $data->GetExamID() . " value = " . $data->GetTestValue() . "</td>";
                              }
                              //echo "<td> examid = " . $data->GetExamID() . " value = " . $data->GetTestValue() . "</td>";
                              }
                              } */

                            //service::printr($colarr);
                            echo "</tr>";
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <br>
    <hr>
    <br>
    <div class="row">
        <div class="col-md-12">
            <h3>จักรยานวัดงาน (ข้อมูลดิบ) </h3>
            <table id="datatableAstrand" class="display table table-hover" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>รหัสการทดสอบ</th>
                        <th>เลขประจำตัว</th>
                        <th>ชื่อ</th>
                        <th>นามสกุล</th>
                        <th>เพศ</th>
                        <th>อายุ</th>
                        <th>Load</th>
                        <th>Heart Rate 1</th>
                        <th>Heart Rate 2</th>
                        <th>Heart Rate 3</th>
                        <th>Heart Rate 4</th>
                        <th>Heart Rate 5</th>
                        <th>Heart Rate 6</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>รหัสการทดสอบ</th>
                        <th>เลขประจำตัว</th>
                        <th>ชื่อ</th>
                        <th>นามสกุล</th>
                        <th>เพศ</th>
                        <th>อายุ</th>
                        <th>Load</th>
                        <th>Heart Rate 1</th>
                        <th>Heart Rate 2</th>
                        <th>Heart Rate 3</th>
                        <th>Heart Rate 4</th>
                        <th>Heart Rate 5</th>
                        <th>Heart Rate 6</th>
                    </tr>
                </tfoot>
                <tbody>
<?php
if ($astrandlist != null && !empty($astrandlist)) {
    foreach ($rows->MSGDETAIL as $key => $value) {
        if (!isset($astrandlist[$key]) || $astrandlist[$key] == null || $astrandlist[$key] == '') {
            continue;
        }
        //service::printr($astrandlist);
        //service::printr($astrandlist[$key][$key][AppConst::EXAM_TYPE_ASTRAND_LOAD]);
        echo "<tr>"; // start row
        $tester = new TesterHeaderInfo();
        $tester = $value['tester'];
        echo "<td>" . $key . "</td>";
        echo "<td>" . $tester->GetTesterID() . "</td>";
        echo "<td>" . $tester->GetNameTH() . "</td>";
        echo "<td>" . $tester->GetSurnameTH() . "</td>";
        echo "<td>" . $tester->GetSex() . "</td>";
        echo "<td>" . $tester->GetAge() . "</td>";
        foreach ($astrandlist[$key] as $astrand) {
            if (!isset($astrand[AppConst::EXAM_TYPE_ASTRAND_LOAD][0])) {
                $load = '-';
            } else {
                $load = $astrand[AppConst::EXAM_TYPE_ASTRAND_LOAD][0];
            }
            echo "<td>" . $load . "</td>";
            for ($i = 1; $i <= 6; $i++) {

                if (!isset($astrand[AppConst::EXAM_TYPE_ASTRAND][$i]) || $astrand[AppConst::EXAM_TYPE_ASTRAND][$i] == null || $astrand[AppConst::EXAM_TYPE_ASTRAND][$i] == '') {
                    $astrand[AppConst::EXAM_TYPE_ASTRAND][$i] = '-';
                }
                echo "<td>" . $astrand[AppConst::EXAM_TYPE_ASTRAND][$i] . "</td>";
            }
        }

        echo "</tr>";
    }
}
?>
                </tbody>
            </table>
        </div>
    </div>

<?php
include 'page/footer_report.php';
?>
    <script type="text/javascript" src="resources/DataTables/datatables.min.js"></script>
    <script type="text/javascript" src="resources/DataTables/Buttons-1.3.1/js/buttons.html5.js"></script>
    <script type="text/javascript" src="resources/DataTables/JSZip-3.1.3/jszip.min.js"></script>
    <script src="resources/datepicker/js/bootstrap-datepicker.min.js"></script>
    <script src="resources/datepicker/locales/bootstrap-datepicker.th.min.js"></script>
    <script>
        $(document).ready(function () {
            $('table.display').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    //'pdfHtml5',
                    'print'
                ]
            });
            $("#txtDateFrom").datepicker({
                autoclose: true,
                language: 'th-TH',
                format: 'yyyy-mm-dd'
            });

            $("#txtDateTo").datepicker({
                autoclose: true,
                language: 'th-TH',
                format: 'yyyy-mm-dd'
            });
        });
    </script>
    <script type="text/javascript">
        var testerdata = [];
        var num_per_page = 10;
        $(function () {
            $("select[name=testergrp] option[valiue=other]").val("");
        });
        function back() {
            window.open("landing.php", "_top");
        }
        function selectExam(obj, examid) {
            var parent = $(obj).parents("tr");
            $(parent).parent().append(parent.clone());
            $(".examid", $(parent)).val(examid);
        }

    </script>
</html>