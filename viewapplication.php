<html>
    <?php
    include 'page/header.php';
    //include 'AppManager.php';
    $appmng = new AppManager();
    $testerid = $_POST['testerid'];
    $appid = $_POST['appid'];
    //echo $testerid;
    $numoftest = 3;
    $appobj = new ResponseHeader();
    //echo $testerid;
    $appobj = $appmng->GetLatestExamByTesterID($testerid, $numoftest);
    $examobj = NULL;
    $testerobj = NULL;
    for ($i = $numoftest, $n = 0; $i > 0; $i--) {
        if (isset($appobj->ResponseDetail[$i])) {
            $tmpresp = new ResponseHeader();
            $tmpresp = $appmng->GetPrintInfoGraphicByAppID($appobj->ResponseDetail[$i], AppConst::PARAM_VIEW_APP);
            //echo '<pre>'; print_r($tmpresp); echo '</pre>';
            /* $testerobj[] = new TesterHeaderInfo();
              $testerobj[] = $appmng->GetTesterDetailByAppID($appobj[$i]); */
            //echo "<br>----------in loop foreach before if -----------------";
            foreach ($tmpresp->ResponseDetail as $key => $value) {

                /* echo '<pre>'; print_r($tmpresp->ResponseDetail[$key]); echo '</pre>';
                  echo "<br>"; */
                if ($key === AppConst::PARAM_TESTER) {
                    continue;
                } else {
                    $exam = new ExamObj();
                    $exam = $tmpresp->ResponseDetail[$key];
                    $examobj[$n][$exam->GetExamID()] = $exam;
                    //echo "examobj[".$n."][".$exam->GetExamID()."] = exam";
                }
                /* if ($key != AppConst::PARAM_TESTER) {
                  $exam = new ExamObj();
                  $exam = $tmpresp->ResponseDetail[$key];
                  $examobj[$n][$exam->GetExamID()] = $exam;
                  echo "examobj[".$n."][".$exam->GetExamID()."] = exam";
                  } */
            }
            $testerobj[$n] = $tmpresp->ResponseDetail[AppConst::PARAM_TESTER];
            //echo "testerobj[$n] = " . $testerobj[$n]->GetHeight();
            $n++;
        }
    }
    $aExamObj = new ExamObj();
    $aTesterobj = new TesterHeaderInfo();
    $colorheader = "";
    $bgcolorsubheder = "#666666";
    $colorsubheder = "#ffffff";
    $bgcoloroddrow = "#cccccc";
    $coloroddrow = "#000000";
    $bgcolorevenrow = "#ffffff";
    $colorevenrow = "#000000";
    ?>
    <tr>
        <td style="width: 1024px;height: 564px;vertical-align: top;background-color: #ffffff;text-align: center;" >
            <table style="width: 80%;text-align: center" border="0" align="center">
                <tr>
                    <td style="width: 25%" class="fontscreen">ชื่อ-นามสกุล</td>
                    <td style="width: 25%" class="fontscreen">
                        <?php
                        $value = "";
                        $aTesterobj = $testerobj[0];
                        try {
                            if (isset($testerobj[0])) {
                                $value = $aTesterobj->GetNameTH();
                                $value .= " " . $aTesterobj->GetSurnameTH();
                            }
                            echo $value;
                        } catch (Exception $exc) {
                            
                        }
                        ?> 
                    </td>
                    <td style="width: 25%" class="fontscreen">เลขประจำตัวผู้ทดสอบ</td>
                    <td style="width: 25%" class="fontscreen">
                        <?php
                        $value = "";
                        try {
                            if (isset($testerid)) {
                                $value = $testerid;
                            }
                            echo $value;
                        } catch (Exception $exc) {
                            
                        }
                        ?>
                    </td>
                </tr>
            </table>
            <table style="width: 80%;text-align: center;margin-top: 20px;margin-bottom: 20px;" border="0" align="center">
                <thead>
                    <tr>
                        <th style="width: 55%;"  class="fontscreen">
                            &nbsp;
                        </th>
                        <?php
                        $typeval = AppConst::EXAM_TYPE_EXCERCISE;
                        for ($i = 0; $i < $numoftest; $i++) {
                            ?>
                            <th style="background-color: <?= $bgcolorsubheder ?>;color: <?= $colorsubheder ?>;width: 15%;"  class="fontscreen">
                                <?php
                                $value = "-";
                                try {
                                    if (isset($examobj[$i][$typeval])) {
                                        $aExamObj = $examobj[$i][$typeval];
                                        $value = $aExamObj->GetTestDate();
                                    }
                                    echo $value;
                                } catch (Exception $exc) {
                                    
                                }
                                ?>
                            </th>
                            <?php
                        }
                        ?>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan="4" class="fontscreen" style="background-color: <?= $bgcolorsubheder ?>;color: <?= $colorsubheder ?>"><h4 style="margin: 0px">ข้อมูลทดสอบ</h4></td>
                    </tr>
                    <tr>
                        <td  class="fontscreen" style="background-color: <?= $bgcolorevenrow ?>;color: <?= $colorevenrow ?>;text-align: left">น้ำหนัก&nbsp;(กก.)</td>
                        <?php
                        for ($i = 0; $i < $numoftest; $i++) {
                            ?>
                            <td class="fontscreen" style="background-color: <?= $bgcolorevenrow ?>;color: <?= $colorevenrow ?>">
                                <?php
                                $value = "-";
                                $aTesterobj = $testerobj[$i];
                                try {
                                    if (isset($testerobj[$i])) {
                                        $value = $aTesterobj->GetWeight();
                                    }
                                    echo $value;
                                } catch (Exception $exc) {
                                    
                                }
                                ?>
                            </td>
                            <?php
                        }
                        ?>
                    </tr>
                    <tr>
                        <td  class="fontscreen"  style="background-color: <?= $bgcoloroddrow ?>;color: <?= $coloroddrow ?>;text-align: left ">ส่วนสูง&nbsp;(ซม.)</td>
                        <?php
                        for ($i = 0; $i < $numoftest; $i++) {
                            ?>
                            <td class="fontscreen" style="background-color: <?= $bgcoloroddrow ?>;color: <?= $coloroddrow ?>">
                                <?php
                                $value = "-";
                                $aTesterobj = $testerobj[$i];
                                try {
                                    if (isset($testerobj[$i])) {
                                        $value = $aTesterobj->GetHeight();
                                    }
                                    echo $value;
                                } catch (Exception $exc) {
                                    
                                }
                                ?>
                            </td>
                            <?php
                        }
                        ?>
                    </tr>
                    <tr>
                        <td  class="fontscreen" style="background-color: <?= $bgcolorevenrow ?>;color: <?= $colorevenrow ?>;text-align: left ">การออกกำลังกาย</td>
                        <?php
                        for ($i = 0; $i < $numoftest; $i++) {
                            ?>
                            <td class="fontscreen" style="background-color: <?= $bgcolorevenrow ?>;color: <?= $colorevenrow ?>">

                                <?php
                                $typeval = AppConst::EXAM_TYPE_EXCERCISE;
                                $value = "-";
                                try {
                                    if (isset($examobj[$i][$typeval])) {
                                        $aExamObj = $examobj[$i][$typeval];
                                        $value = $aExamObj->GetTestValue();
                                    }
                                    echo $value;
                                } catch (Exception $exc) {
                                    
                                }
                                ?>
                            </td>
                            <?php
                        }
                        ?>
                    </tr>
                    <tr>
                        <td  class="fontscreen" style="background-color: <?= $bgcoloroddrow ?>;color: <?= $coloroddrow ?>;text-align: left ">ความดันโลหิต&nbsp;(มม. ปรอท)</td>
                        <?php
                        for ($i = 0; $i < $numoftest; $i++) {
                            ?>
                            <td class="fontscreen" style="background-color: <?= $bgcoloroddrow ?>;color: <?= $coloroddrow ?>">

                                <?php
                                $typeval = AppConst::EXAM_TYPE_BP_U;
                                $value = "-";
                                try {
                                    if (isset($examobj[$i][$typeval])) {
                                        $aExamObj = $examobj[$i][$typeval];
                                        $value = $aExamObj->GetTestValue();
                                        if (isset($examobj[$i][$typeval])) {
                                            $typeval = AppConst::EXAM_TYPE_BP_D;
                                            $aExamObj = $examobj[$i][$typeval];
                                            $value .= "/" . $aExamObj->GetTestValue();
                                        }
                                    }
                                    echo $value;
                                } catch (Exception $exc) {
                                    
                                }
                                ?>
                            </td>
                            <?php
                        }
                        ?>
                    </tr>
                    <tr>
                        <td  class="fontscreen" style="background-color: <?= $bgcolorevenrow ?>;color: <?= $colorevenrow ?>;text-align: left ">ชีพจร&nbsp;(ครั้ง)</td>
                        <?php
                        for ($i = 0; $i < $numoftest; $i++) {
                            ?>
                            <td class="fontscreen" style="background-color: <?= $bgcolorevenrow ?>;color: <?= $colorevenrow ?>">

                                <?php
                                $typeval = AppConst::EXAM_TYPE_PULSE;
                                $value = "-";
                                try {
                                    if (isset($examobj[$i][$typeval])) {
                                        $aExamObj = $examobj[$i][$typeval];
                                        $value = $aExamObj->GetTestValue();
                                    }
                                    echo $value;
                                } catch (Exception $exc) {
                                    
                                }
                                ?>
                            </td>
                            <?php
                        }
                        ?>
                    </tr>
                    <tr>
                        <td colspan="4" class="fontscreen" style="background-color: <?= $bgcolorsubheder ?>;color: <?= $colorsubheder ?>"><h4 style="margin: 0px">การทดสอบ</h4></td>
                    </tr>
                    <?php
                    $responseDetail = $appmng->GetExamCategory();
                    foreach ($responseDetail->ResponseDetail[0] as $key => $value) {
                        $responseDetail2 = $appmng->GetExamObject();
                        ?>
                        <tr>
                            <td colspan="4" class="fontscreen" style="background-color: <?= $bgcolorsubheder ?>;color: <?= $colorsubheder ?>;text-align: left"><h4 style="margin: 0px"><?= $value ?></h4></td>
                        </tr>
                        <?php
                        $num = 0;
                        foreach ($responseDetail2->ResponseDetail[$key] as $value2) {
                            $examobj_detail = new ExamObj();
                            $examobj_detail = $value2;
                            $tmpbg = "";
                            if ($num % 2 == "1") {
                                $tmpbg = $bgcoloroddrow;
                            } else {
                                $tmpbg = $bgcolorevenrow;
                            }
                            ?>
                            <tr>
                                <td  class="fontscreen" style="background-color: <?= $tmpbg ?>;color: <?= $colorevenrow ?>;text-align: left "><?= $examobj_detail->GetEDescription() ?> (<?= $examobj_detail->GetTDescription() ?>)</td>

                                <?php
                                for ($i = 0; $i < $numoftest; $i++) {
                                    ?>
                                    <td class="fontscreen" style="background-color: <?= $tmpbg ?>;color: <?= $colorevenrow ?>">

                                        <?php
                                        $typeval = $examobj_detail->GetExamID();
                                        $value = "-";
                                        try {
                                            if (isset($examobj[$i][$typeval])) {
                                                $aExamObj = $examobj[$i][$typeval];
                                                $value = $aExamObj->GetTestValue();
                                            }
                                            echo $value;
                                        } catch (Exception $exc) {
                                            
                                        }
                                        ?>
                                    </td>
                                    <?php
                                }
                                $num++;
                            }
                            ?>
                        </tr>
                        <?php
                    }
                    ?>                                                           
                </tbody>
            </table>
            <form name="dform1" id="dform1" method="GET" action="infographic.php" target="_blank">
                <input name="appid" id="appid" type="hidden" value="<?= $appid ?>"/>
                <input name="testerid" id="testerid2" type="hidden" value="<?= $testerid ?>"/>
                <input name="trans" type="hidden" value="<?= substr(md5(microtime()), rand(0, 26), 13); ?>">
            </form>
            <br/>
            <img src="images/btn/back.png" style="cursor: pointer;padding-left: 10px;" onclick="back()"/>
            <img src="images/btn/printapp.png" style="cursor: pointer;padding-left: 10px;" onclick="submitPrint()"/>
        </td>
    </tr>
    <?php
    include 'page/footer.php';
    ?>
    <script type="text/javascript">
        function submitPrint() {
            $("#dform1").submit();
        }
        function back() {
            window.open("searchapp.php?action=<?= AppConst::METHOD_QUERY_SEARCH_PRINT ?>", "_top");
        }
    </script>
</html>