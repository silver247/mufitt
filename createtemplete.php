<html>
    <?php
    include 'page/header.php';
    include_once 'AppManager.php';
    $appmng = new AppManager();
    $appmng->RequestDDLGrpTest();
    $action = $_GET['action'];
    $action = AppConst::METHOD_CREATE_TEMPLATE;
    ?>
    <script src="resources/js/calendar-th.js"></script>
    <script>
        $(function () {
            $("#applydate").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: "c-100:c+10"
            });
            $("#applydate").datepicker("option", "dateFormat", "dd/mm/yy");
            $("#applydate").datepicker($.datepicker.regional[ "th" ]);
        });

    </script>
    <tr>
        <td style="width: 1024px;height: 564px;vertical-align: top;background-color: #ffffff;text-align: center;border: 1px solid #990000;border-top: none;border-bottom: none;" >
            <form name="tform1" id="tform1" action="searchapp.php" method="POST">
                <table style="width: 100%" border="0" cellpadding="0" cellspacing="0" align="center" class="setup_exam">
                    <tr>
                        <td colspan="4" class="fontscreen"><h3>สร้างชุดการทดสอบ</h3></td>
                    </tr>
                    <tr>                                    
                        <td class="fontscreen"  style="padding-top: 10px">
                            กลุ่มการทดสอบ
                        </td>
                        <td colspan="3" class="fontscreen">
                            <select name="testergrp" id="testergrp"><?= $appmng->RequestDDLGrpTest() ?></select>&nbsp;<input type="text" name="testergrpstr" id="testergrpstr" size="10" style="display: none"/>
                        </td>
                    </tr>
                    <tr class="exam_row">                                    
                        <td colspan="3" class="fontscreen"  style="padding-top: 10px">
                            <div style="float: left">การทดสอบ :&nbsp;&nbsp;&nbsp;</div>&nbsp;<div class="dropdown" style="float: left">
                                <button class="btn btn-default dropdown-toggle fontscreen" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    โปรดระบุ
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1" style="height: 200px;width: 350px;overflow-y: scroll;">
                                    <?php
                                    $responseDetail = $appmng->GetExamCategory();
                                    foreach ($responseDetail->ResponseDetail[0] as $key => $value) {
                                        $responseDetail2 = $appmng->GetExamObject();
                                        ?>
                                        <li class="dropdown-header fontscreen" style="padding-left: 0px;font-size: 1.1em"><?= $value ?></li>
                                        <?php
                                        foreach ($responseDetail2->ResponseDetail[$key] as $value2) {
                                            $examobj = new ExamObj();
                                            $examobj = $value2;
                                            ?>
                                            <li class="fontscreen" style="padding-left: 0px;font-size: 1.1em" selectval="<?= $examobj->GetExamID() ?>"><a href="#0" onclick="selectExam(this, '<?= $examobj->GetExamID() ?>')"><?= $examobj->GetEDescription() ?> (<?= $examobj->GetTDescription() ?>)</a></li>
                                            <?php
                                        }
                                    }
                                    ?>
                                </ul>
                            </div>
                            <div style="float: left;cursor: pointer;display: none" class="removeRow" onclick="removeRow(this)">
                                &nbsp;&nbsp;
                                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                            </div>

                        </td>
                    <input type="hidden" name="examid[]" class="examid" value=""/>
                    </tr>
                    <tr class="btn_row" style="text-align: right">                                    
                        <td  colspan="4">
                            <input type="hidden" name="do" id="do" value="<?= $action ?>"/>
                            <img src="images/btn/back.png" style="cursor: pointer;padding-left: 10px;" onclick="back()"/>
                            <img src="images/btn/insert.png" style="cursor: pointer;padding-left: 10px;" onclick="submitData(this)"/>
                        </td>
                        <td  colspan="4" style="text-align: center;display: none" id="waiting_group">
                            <h3>ระบบกำลังอยู่ระหว่างบันทึกข้อมูล . . .</h3>
                        </td>
                    </tr>
                </table>
                <input name="max" id="maxinput" type="hidden" value="0"/>
                <?php
                $action = AppConst::METHOD_VIEW_TEMPLATE;
                ?>
                <input type="hidden" name="" id="do2" value="<?= $action ?>"/>
            </form>
            <form name="dform1" id="dform1" method="POST" action="createapp.php">
                <input name="appid" id="appid" type="hidden" value=""/>
                <input name="testerid" id="testerid2" type="hidden" value=""/>

            </form>

        </td>
    </tr>
    <?php
    include 'page/footer.php';
    ?>
    <script type="text/javascript">
        var testerdata = [];
        var num_per_page = 10;
        $(function () {
            $("select[name=testergrp] option[valiue=other]").val("");
            $("#testergrp").change(function () {
                if ($(this).val() == "other") {
                    $("#testergrpstr").show();
                } else {
                    searchTempplate();
                }
            });
        });
        function back() {
            window.open("landing.php", "_top");
        }
        function selectExam(obj, examid) {
            addRow(obj, examid);
        }
        function removeRow(obj) {
            var parent = $(obj).parents(".exam_row");
            $(parent).remove();
        }
        function validateGroup() {
            if ($("select[name=testergrp] option:selected").val() == "") {
                alert("กรุณาระบุกลุ่มการทดสอบ");
                return false;
            } else if ($("select[name=testergrp] option:selected").val() == "other") {
                var grpstr = $("#testergrpstr").val();
                grpstr = $.trim(grpstr);
                if (grpstr.length == 0) {
                    alert("กรุณาระบุกลุ่มการทดสอบ");
                    return false;
                }
                //testergrpstr
            }
            return true;
        }
        function validateExam() {
            if ($(".examid[value!='']", $(".exam_row")).length <= 0) {
                alert("กรุณาระบุการทดสอบ");
                return false;
            }
            return true;
        }
        function validateForm() {
            if (!validateGroup()) {
                return false;
            }
            return true;
        }
        function submitData(btn) {
            if (!validateForm()) {
                return  false;
            }
            if ($("#testergrp option:selected").val() == "other") {
                $("#testergrp option:selected").val($("#testergrpstr").val());
            }
            $("#maxinput").val($(".examid[value!='']", $(".exam_row")).length);
            //prompt("", $("#tform1").serialize());
            $(".btn_row").hide();
            $("#waiting_group").show();
            try {
                jQuery.ajax({
                    url: "AppHttpRequest.php",
                    type: "POST",
                    contentType: defaultContentType,
                    data: $("#tform1").serialize(),
                    //dataType: "html",
                    dataType: "json",
                    error: function (transport, status, errorThrown) {
                        alert("Systen terminated,Please contact administrator.");
                        $("#waiting_group").hide();
                        $(".btn_row").show();
                    },
                    success: function (data) {
                        alert("Create templete success.")
                        //prompt("", "success : " + data);
                        $("#waiting_group").hide();
                        $(".btn_row").show();
                    }

                });
            } catch (ex) {
                alert(ex);
            }

            //prompt("", $("#tform1").serialize());

        }
        function searchTempplate() {
            var action = $("#do2").val();
            var groupid = $("#testergrp option:selected").val();
            try {
                jQuery.ajax({
                    url: "AppHttpRequest.php",
                    type: "POST",
                    contentType: defaultContentType,
                    data: "do=" + action + "&groupid=" + groupid + "&seed=" + Math.random(),
                    //dataType: "html",
                    dataType: "json",
                    error: function (transport, status, errorThrown) {

                        alert("error : " + errorThrown);

                    },
                    success: function (data) {
                        var obj = $(".examid[value='']", $(".exam_row"));
                        var parent = $(obj).parents(".exam_row");
                        $(".exam_row").remove();
                        $(".btn_row").before(parent);
                        var rownum = $(".exam_row").length;
                        $("button", $(parent)).attr("id", "dropdownMenu" + rownum);
                        $("ul", $(parent)).attr("aria-labelledby", "dropdownMenu" + rownum);
                        $(data.ResponseDetail).each(function (index, value) {
                            var obj = $("li[selectval='" + value + "']", $("#dropdownMenu1").parents(".dropdown"));
                            addRow(obj, value);
                        });
                    }

                });
            } catch (ex) {
                alert(ex);
            }

            //prompt("", $("#tform1").serialize());

        }
        function addRow(obj, examid) {
            var parent = $(obj).parents(".exam_row");
            var newrow = parent.clone();
            if (!$("ul", $(parent)).attr("examselected")) {
                $(".btn_row").before(newrow);
            }
            $(".examid", $(parent)).val(examid);
            $("button", $(parent)).text($(obj).text());
            var rownum = $(".exam_row").length;
            $("button", $(parent)).attr("id", "dropdownMenu" + rownum);
            $("ul", $(parent)).attr("aria-labelledby", "dropdownMenu" + rownum);
            $("ul", $(parent)).attr("examselected", "examselected");
            $(".removeRow", $(parent)).show();
        }
    </script>
</html>