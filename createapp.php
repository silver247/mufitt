<html>



    <?php
    include 'page/header.php';



    include_once 'AppManager.php';



    $appmng = new AppManager();



    //$testid = $appmng->GetTesterID();

    $testid = $appmng->GetTesterIDKey();



    $gender_m = AppConst::MALE;



    $gender_f = AppConst::FEMALE;



    $action = AppConst::METHOD_INSERT_APP;



    $appID = $_POST['appid'];

    //echo "$appID";

    $testerobj = null;

    $examobj = null;

    if (isset($appID)) {

        $testerobj = new TesterHeaderInfo();

        $testerobj = $appmng->GetTesterDetailByAppID($appID);

        $examobj = new ResponseHeader();
        $examresult = new ResponseHeader();

        $examresult = $examobj = $appmng->GetExamDetailByAppID($appID);

        $action = AppConst::METHOD_EDIT_APP;
    }
    ?>

    <script src="resources/js/calendar-th.js"></script>

    <script>

        $(function () {

            $("#brithday").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: "c-100:c+10"

            });

            $("#brithday").datepicker("option", "dateFormat", "dd/mm/yy");

            $("#brithday").datepicker($.datepicker.regional[ "th" ]);

        });



    </script>

    <tr>



        <td style="width: 1024px;height: 564px;margin: 0px auto;background-color: #ffffff;text-align: center;border: 1px solid #990000;border-top: none;border-bottom: none;" >



            <form name="tform1" id="tform1" action="AppHttpRequest.php" method="POST" onsubmit="submitData()">



                <input name="userid" id="userid" type="hidden" value="<?= $_SESSION['userid'] ?>"/>

                <?php
                $tmpaction = AppConst::METHOD_INSERT_APP;
                ?>

                <input name="tmpaction" id="tmpaction" type="hidden" value="<?= $tmpaction ?>"/>



                <table style="width: 100%" border="0" cellpadding="0" cellspacing="0" align="center">



                    <tr>



                        <td colspan="4" class="fontscreen"><h3><u>ข้อมูลผู้ทำการทดสอบ<font color="red" id="newapp_label" style="display:none">(บันทึกการทดสอบใหม่)</font></u></h3></td>



                    </tr>   



                    <tr>



                        <td colspan="4" class="fontscreen"><p class="require-field">* หมายถึงข้อมูลที่จำเป็นต้องกรอก</p></td>



                    </tr>                    



                    <tr>



                        <td></td>



                        <td></td>



                        <td class="fontscreen">หมายเลขประจำตัวผู้ทดสอบ</td>



                        <td colspan="4" class="fontscreen">

                            <?php
                            if (isset($appID)) {

                                $testid = $testerobj->GetTesterID();

                                echo "<input type='hidden' name='appid' id='appid' value='$appID'/>";
                            }
                            ?>

                            <input type="text" name="testerid" id="testerid" readonly="readonly" size="25" style="background-color: #999999;color: #ffffff" value="<?= $testid ?>"/>



                        </td>



                    </tr>                    



                    <tr>



                        <td class="fontscreen" style="padding-top: 10px">ชื่อ<span class="require-field">*</span>&nbsp;</td>



                        <td class="fontscreen" style="padding-top: 10px">

                            <?php
                            $value = "";

                            if (isset($appID)) {

                                $testerobj = $appmng->GetTesterDetailByAppID($appID);

                                $value = $testerobj->GetNameTH();
                            }
                            ?>

                            <input type="text" name="tname" id="tname" size="25" value="<?= $value ?>"/>

                        </td>



                        <td class="fontscreen" style="padding-left: 10px;padding-top: 10px">นามสกุล<span class="require-field">*</span></td>



                        <td class="fontscreen" style="padding-top: 10px">

                            <?php
                            $value = "";

                            if (isset($appID)) {

                                $value = $testerobj->GetSurnameTH();
                            }
                            ?>

                            <input type="text" name="tsurname" id="tsurname" size="25" value="<?= $value ?>"/>

                        </td>



                    </tr>                    



                    <tr>

                        <td class="fontscreen">Email</td>



                        <td class="fontscreen">
                            <?php
                            $value = "";
                            if (isset($appID)) {
                                $value = $testerobj->GetEmail();
                            }
                            ?>
                            <input type="text" name="email" id="email" size="25" value="<?= $value ?>"/>
                        </td>
                        <td class="fontscreen" colspan="2">
                            <?php
                            $value = "";
                            if (isset($appID)) {
                                $value = $testerobj->GetIsResearch();
                                if ($value == "1") {
                                    $value = "checked='checked'";
                                }
                            }
                            ?>
                            <input type="checkbox" name="researchChk" id="researchChk" value="1"<?= $value ?>/>
                            <input type="hidden" name="isResearch" id="isResearch" value="2"/>
                            <span>ใช้เพื่องานวิจัย</span>
                        </td>
                    </tr>                    



                    <tr>



                        <td class="fontscreen">กลุ่มการทดสอบ<span class="require-field">*</span></td>



                        <td colspan="3" class="fontscreen">

                            <?php
                            $value = $appmng->RequestDDLGrpTest();

                            if (isset($appID)) {

                                $strsreach = "value = '" . $testerobj->GetTesterGrp() . "'";

                                //echo ("<!--".$strsreach."-->");

                                $value = str_replace($strsreach, $strsreach . " selected='selected' ", $appmng->RequestDDLGrpTest());

                                //echo ("<!--".$value."-->");
                            }
                            ?>

                            <select name="testergrp" id="testergrp"><?= $value ?></select>&nbsp;<input type="text" name="testergrpstr" id="testergrpstr" size="10" style="display: none"/></td>



                    </tr>                    



                    <tr>



                        <td class="fontscreen">เพศ<span class="require-field">*</span></td>



                        <td class="fontscreen">

                            <?php
                            $value = "";

                            if (isset($appID)) {

                                if ($gender_m == $testerobj->GetSex()) {

                                    $value = " checked='checked'";
                                }
                            }
                            ?>

                            <input name="gender" type="radio" id="gender_m" value="<?= $gender_m ?>"<?= $value ?>/>ชาย

                            <?php
                            $value = "";

                            if (isset($appID)) {

                                if ($gender_f == $testerobj->GetSex()) {

                                    $value = " checked='checked'";
                                }
                            }
                            ?>

                            <input name="gender" type="radio" id="gender_f" value="<?= $gender_f ?>"<?= $value ?>/>หญิง



                        </td>



                        <td class="fontscreen" style="padding-left: 10px">วัน/เดือน/ปี เกิด (ค.ศ.)<span class="require-field">*</span></td>



                        <td class="fontscreen">

                            <?php
                            $value = "";

                            if (isset($appID)) {

                                $value = $testerobj->GetDOB();
                            }
                            ?>

                            <input type="text" name="brithday" id="brithday" size="15" value="<?= $value ?>" onload="getAgeFromDOB()"/>&nbsp;อายุ&nbsp;<input type="text" name="age" id="age"  size="5"/>&nbsp;ปี</td>



                    </tr>                    



                    <tr>



                        <td class="fontscreen">น้ำหนัก<span class="require-field">*</span></td>



                        <td class="fontscreen">

                            <?php
                            $value = "";

                            if (isset($appID)) {

                                $value = $testerobj->GetWeight();
                            }
                            ?>

                            <input type="text" name="weight" id="weight" size="10" value="<?= $value ?>"/>



                            &nbsp;กก.



                        </td>



                        <td class="fontscreen" style="padding-left: 10px">ส่วนสูง<span class="require-field">*</span></td>



                        <td class="fontscreen">

                            <?php
                            $value = "";

                            if (isset($appID)) {

                                $value = $testerobj->GetHeight();
                            }
                            ?>

                            <input type="text" name="height" id="height" size="10" value="<?= $value ?>"/>



                            &nbsp;ซม.



                        </td>

                    </tr>

                    <tr>





                        <td class="fontscreen">                            

                            BMI&nbsp;(ดัชนีมวลกาย)<span class="require-field">*</span>

                        </td>



                        <td class="fontscreen">



                            <?php
                            $result = getInput($appID, $examobj, AppConst::EXAM_TYPE_OMRON_BMI, 0, 10, "&nbsp;kg/m<sup>2</sup>");

                            echo $result;
                            ?>

                        </td>

                        <td class="fontscreen" style="padding-left: 10px">



                            Waist circumference&nbsp;(รอบเอว)</td>

                        <td class="fontscreen" colspan="3">

                            <?php
                            $result = getInput($appID, $examobj, AppConst::EXAM_TYPE_WAIST_CIRC, 0, 10, "&nbsp;cm.");

                            echo $result;
                            ?>                            

                        </td>

                    </tr>



                    <tr>



                        <td class="fontscreen">การออกกำลังกาย<span class="require-field">*</span></td>



                        <td class="fontscreen" colspan="3">



                            <?php
                            $typeval = AppConst::EXAM_TYPE_EXCERCISE;

                            $value = "";

                            $aExamObj = new ExamObj();

                            if (isset($appID)) {

                                try {

                                    if (isset($examobj->ResponseDetail[$typeval][0])) {

                                        $aExamObj = $examobj->ResponseDetail[$typeval][0];
                                    }
                                } catch (Exception $exc) {
                                    
                                }
                            }
                            ?>



                            <input name="tmpexam1type" type="hidden" value="<?= $typeval ?>" class="examtype"/>

                            <input name="tmpexam1seqno" type="hidden" value="0" class="seqno"/>
                            <?php
                            $value = "";
                            if (isset($appID)) {
                                $tmpvalue = $aExamObj->GetTestValue();
                                if ($tmpvalue == "0") {
                                    $value = " checked='checked'";
                                }
                            }
                            ?>
                            <input name="tmpexam1val" type="radio" id="exercise_0" class="exam" value="0"<?= $value ?>/>ไม่เคย

                            <?php
                            $value = "";

                            if (isset($appID)) {

                                $tmpvalue = $aExamObj->GetTestValue();

                                if ($tmpvalue == "1") {

                                    $value = " checked='checked'";
                                }
                            }
                            ?>

                            <input name="tmpexam1val" type="radio" id="exercise_1" class="exam" value="1"<?= $value ?>/>1-2 ครั้ง ต่อสัปดาห์

                            <?php
                            $value = "";

                            if (isset($appID)) {

                                $tmpvalue = $aExamObj->GetTestValue();

                                if ($tmpvalue == "2") {

                                    $value = " checked='checked'";
                                }
                            }
                            ?>

                            <input name="tmpexam1val" type="radio" id="exercise_2" class="exam" value="2"<?= $value ?>/>2-5 ครั้ง ต่อสัปดาห์



                        </td>



                    </tr>                    



                    <tr>



                        <td class="fontscreen">ความดันโลหิต</td>



                        <td class="fontscreen">

                            <?php
                            $result = getInput($appID, $examobj, AppConst::EXAM_TYPE_BP_U, 0, 4, "");

                            echo $result;
                            ?>                  

                            <div style="float: left">

                                /

                            </div>

                            <?php
                            $result = getInput($appID, $examobj, AppConst::EXAM_TYPE_BP_D, 0, 4, "");

                            echo $result;
                            ?>

                            &nbsp;มม.ปรอท                                        
                        </td>
                        <td class="fontscreen">ชีพจร</td>
                        <?php
                        $result = getInput($appID, $examobj, AppConst::EXAM_TYPE_PULSE, 0, 10, "");
                        ?>
                        <td class="fontscreen"><?= $result ?></td>
                    </tr>
                    <tr>

                        <td colspan="4" class="fontscreen" style="background-color: #d30000;color: #FFFFFF">
                            <h4 style="margin: 0px">
                                การทดสอบ
                            </h4>
                        </td>

                    <tr class="exam_row">

                        <td colspan="4" class="fontscreen">
                            <div class="dropdown" style="float: left;width: 35%;vertical-align: middle">
                                <button class="btn btn-default dropdown-toggle fontscreen" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    โปรดระบุ
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1" style="height: 200px;width: 350px;overflow-y: scroll;">
                                    <?php
                                    $responseDetail = $appmng->GetExamCategory();
                                    foreach ($responseDetail->ResponseDetail[0] as $key => $value) {
                                        $responseDetail2 = $appmng->GetExamObject();
                                        ?>
                                        <li class="dropdown-header fontscreen" style="padding-left: 0px;font-size: 1.1em"><?= $value ?></li>
                                        <?php
                                        foreach ($responseDetail2->ResponseDetail[$key] as $value2) {
                                            $examobj = new ExamObj();
                                            $examobj = $value2;
                                            ?>
                                            <li class="fontscreen" style="padding-left: 0px;font-size: 1.1em" selectval="<?= $examobj->GetExamID() ?>"><a href="#0" onclick="selectExam(this, '<?= $examobj->GetExamID() ?>')"><?= $examobj->GetEDescription() ?> (<?= $examobj->GetTDescription() ?>) <?= $examobj->GetUOM() ?></a></li>
                                            <?php
                                        }
                                    }
                                    ?>
                                </ul>
                            </div>
                            <?php
                            $examtype = AppConst::EXAM_TYPE_ASTRAND;
                            ?>
                            <div class="astrand_input" style="display: none;float: left;width: 60%">
                                <table border="0" style="border: none">
                                    <tr>
                                        <td style="width: 25%;vertical-align: middle" rowspan="2" class="fontscreen">
                                            Heart Rate
                                        </td>
                                        <td style="width: 8%;text-align: center;" class="fontscreen">
                                            1
                                        </td>
                                        <td style="width: 8%;text-align: center;" class="fontscreen">
                                            2
                                        </td>
                                        <td style="width: 8%;text-align: center;" class="fontscreen">
                                            3
                                        </td>
                                        <td style="width: 8%;text-align: center;" class="fontscreen">
                                            4
                                        </td>
                                        <td style="width: 8%;text-align: center;" class="fontscreen">
                                            5
                                        </td>
                                        <td style="width: 8%;text-align: center;" class="fontscreen">
                                            6
                                        </td>

                                    </tr>
                                    <tr>
                                        <td class="astrand_1" style="text-align: center;">
                                            <input type="text" name="exam" size="4" disabled="disabled" class="exam" value=""/>
                                            <input name="examtype" type="hidden" value="<?= $examtype ?>" class="examtype" disabled="disabled"/>
                                            <input name="examseqno" type="hidden" value="1" class="seqno" disabled="disabled"/>
                                        </td>
                                        <td class="astrand_2" style="text-align: center;">
                                            <input type="text" name="exam" size="4" disabled="disabled" class="exam" value=""/>
                                            <input name="examtype" type="hidden" value="<?= $examtype ?>" class="examtype" disabled="disabled"/>
                                            <input name="examseqno" type="hidden" value="2" class="seqno" disabled="disabled"/>
                                        </td>
                                        <td class="astrand_3" style="text-align: center;">
                                            <input type="text" name="exam" size="4" disabled="disabled" class="exam" value=""/>
                                            <input name="examtype" type="hidden" value="<?= $examtype ?>" class="examtype" disabled="disabled"/>
                                            <input name="examseqno" type="hidden" value="3" class="seqno" disabled="disabled"/>
                                        </td>
                                        <td class="astrand_4" style="text-align: center;">
                                            <input type="text" name="exam" size="4" disabled="disabled" class="exam" value=""/>
                                            <input name="examtype" type="hidden" value="<?= $examtype ?>" class="examtype" disabled="disabled"/>
                                            <input name="examseqno" type="hidden" value="4" class="seqno" disabled="disabled"/>
                                        </td>
                                        <td class="astrand_5" style="text-align: center;">
                                            <input type="text" name="exam" size="4" disabled="disabled" class="exam" value=""/>
                                            <input name="examtype" type="hidden" value="<?= $examtype ?>" class="examtype" disabled="disabled"/>
                                            <input name="examseqno" type="hidden" value="5" class="seqno" disabled="disabled"/>
                                        </td>
                                        <td class="astrand_6" style="text-align: center;">
                                            <input type="text" name="exam" size="4" disabled="disabled" class="exam" value=""/>
                                            <input name="examtype" type="hidden" value="<?= $examtype ?>" class="examtype" disabled="disabled"/>
                                            <input name="examseqno" type="hidden" value="6" class="seqno" disabled="disabled"/>
                                        </td>
                                    </tr>
                                    <tr style="">
                                        <td style="padding-top: 10px" class="fontscreen">
                                            Load
                                        </td>
                                        <td colspan="6" style="padding-top: 10px" class="fontscreen">
                                            <?php
                                            $typeval = AppConst::EXAM_TYPE_ASTRAND_LOAD;
                                            $aExamObj = new ExamObj();

                                            ?>
                                            <input name="examtype"  id="exam34type" type="hidden" value="<?= $typeval ?>" class="examtype"/>

                                            <input name="examseqno"  id="exam34seqno" type="hidden" value="0" class="seqno"/>
                                            <!--
                                            <?php
                                            service::printr($examresult->ResponseDetail);
                                            ?>
                                            -->
                                            <?php
                                            $value = $appmng->GetDDLAstrandLoad();
                                            //echo ("<!--" . service::printr($examobj) . "-->");
                                            if (isset($appID)) {
                                                if (isset($examresult->ResponseDetail[$typeval][0])) {
                                                    $aExamObj = $examresult->ResponseDetail[$typeval][0];
                                                    $strsreach = "value = '" . $aExamObj->GetTestValue() . "'";
                                                    echo ("<!--".$strsreach."-->");
                                                    $value = str_replace($strsreach, $strsreach . " selected='selected' ", $appmng->GetDDLAstrandLoad());
                                                }
                                            }
                                            ?>
                                            <select name="examval" class="exam ddlLoad" disabled="disabled"><?= $value ?></select>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="textinput" style="float: left">
                                &nbsp;&nbsp;
                                <input type="text" name="exam" class="exam examid" value=""/>

                                <input name="examtype" type="hidden" value="" class="examtype"/>

                                <input name="examseqno" type="hidden" value="" class="seqno"/>
                            </div>

                            <div style="float: left;cursor: pointer;display: none" class="removeRow" onclick="removeRow(this)">
                                &nbsp;&nbsp;
                                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                            </div>
                        </td>
                    </tr>


                    <tr class="btn_row">

                        <td class="fontscreen" colspan="4" style="text-align: center;">
                            <?php
                            $tmpaction = "";
                            if ($action == AppConst::METHOD_INSERT_APP) {
                                $tmpaction = AppConst::METHOD_INSERT_APP;
                            } else if ($action == AppConst::METHOD_EDIT_APP) {
                                $tmpaction = AppConst::METHOD_EDIT_APP;
                            }
                            ?>
                            <img src="images/btn/back.png" style="cursor: pointer" onclick="back('<?= $tmpaction ?>')"/>
                            <img src="images/btn/draft.png" style="cursor: pointer;padding-left: 10px;" onclick="submitData(this)" iname="draft"/>
                            <img src="images/btn/insert.png" style="cursor: pointer;padding-left: 10px;" onclick="submitData(this)" iname="insert"/>
                            <?php
                            if ($action == AppConst::METHOD_EDIT_APP) {
                                ?>
                                <img src="images/btn/newapp.png" style="cursor: pointer;padding-left: 10px;" onclick="creatApplication()"/>
                                <?php
                            }
                            ?>
                            <img src="images/btn/clear.png" style="cursor: pointer;padding-left: 10px;" onclick="clearForm()"/>
                        </td>
                    </tr>
                    <tr id="waiting_group" style="display: none">
                        <td class="fontscreen" colspan="4" style="text-align: center;">
                            <h3>ระบบกำลังอยู่ระหว่างบันทึกข้อมูล . . .</h3>
                        </td>
                    </tr>



                </table>
                <input name="max" id="maxinput" type="hidden" value="0"/>
                <input name="do" type="hidden" value="<?= $action ?>"/>
                <?php
                $action = AppConst::METHOD_VIEW_TEMPLATE;
                ?>
                <input type="hidden" name="" id="doview" value="<?= $action ?>"/>
                <?php
                $action = AppConst::METHOD_QUERY_NORMALIZE;
                ?>
                <input type="hidden" name="" id="donormalize" value="<?= $action ?>"/>
                <?php
                if (!isset($appID)) {
                    $appID = "";
                }
                ?>
                <input name="appid" id="appid" type="hidden" value="<?= $appID ?>"/>
            </form>
            <div style="display: none">
                <?php
                $examresult;
                //var_dump($examresult->ResponseDetail);
                foreach ($examresult->ResponseDetail as $key => $value) {
                    foreach ($value as $key2 => $value2) {
                        $val = $value2->GetTestValue();
                        ?>
                        <div class="examidlist">
                            <input class="examid" type="hidden" value="<?= $key ?>"/>
                            <input class="seqno" type="hidden" value="<?= $key2 ?>"/>
                            <input class="value" type="hidden" value="<?= $val ?>"/>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>
            <form name="dform1" id="dform1" method="GET" action="infographic.php" target="_blank">
                <input name="appid" id="appid2" type="hidden" value="<?= $appID ?>"/>
                <input name="testerid" id="testerid2" type="hidden" value=""/>
                <input name="trans" id="trans" type="hidden" value="<?= substr(md5(microtime()), rand(0, 26), 13) ?>"/>
            </form>

        </td>



    </tr>



    <?php
    include 'page/footer.php';



    $successval = AppConst::STATUS_SUCCESS;



    $errorval = AppConst::STATUS_ERROR;

    function getInput($appID, $resObj, $type, $seqno, $size, $unit) {

        $value = "";

        $aExamObj = new ExamObj();

        if (isset($appID)) {

            try {

                if (isset($resObj->ResponseDetail[$type][$seqno])) {

                    $aExamObj = $resObj->ResponseDetail[$type][$seqno];

                    $value = $aExamObj->GetTestValue();
                }
            } catch (Exception $exc) {
                
            }
        }

        $id = str_replace("type", "", $type);

        $result = "";

        $result = "<div style=\"float: left\">";

        if ($seqno > 4 && $type == AppConst::EXAM_TYPE_ASTRAND) {

            $result .= "<input name=\"tmpexam" . $id . "type\" type=\"hidden\" value=\"" . $type . "\" class=\"examtype\" disabled=\"disabled\"/>";
        } else {

            $result .= "<input name=\"tmpexam" . $id . "type\" type=\"hidden\" value=\"" . $type . "\" class=\"examtype\"/>";
        }

        if ($seqno > 4 && $type == AppConst::EXAM_TYPE_ASTRAND) {

            $result .= "<input name=\"tmpexam" . $id . "seqno\" type=\"hidden\" value=\"" . $seqno . "\" class=\"seqno\" disabled=\"disabled\"/>";
        } else {

            $result .= "<input name=\"tmpexam" . $id . "seqno\" type=\"hidden\" value=\"" . $seqno . "\" class=\"seqno\"/>";
        }

        if ($seqno == 0) {

            if ($type == AppConst::EXAM_TYPE_OMRON_BMI) {

                $result .= "<input type=\"text\" name=\"tmpexam" . $id . "val\" id=\"exam" . $id . "val\" size=\"" . $size . "\" class=\"exam\"  value=\"" . $value . "\" readonly=\"readonly\" style=\"background-color: #999999;color: #ffffff\"/>$unit";
            } else {

                $result .= "<input type=\"text\" name=\"tmpexam" . $id . "val\" id=\"exam" . $id . "val\" size=\"" . $size . "\" class=\"exam\"  value=\"" . $value . "\"/>$unit";
            }
        } else {

            if ($type == AppConst::EXAM_TYPE_ASTRAND) {

                if ($seqno > 4) {

                    $result .= "<input type=\"text\" name=\"tmpexam" . $id . "val\" id=\"exam" . $id . "_" . $seqno . "val\" size=\"" . $size . "\" class=\"astrandtest exam\"  value=\"" . $value . "\" disabled=\"disabled\"/>$unit";
                } else {

                    $result .= "<input type=\"text\" name=\"tmpexam" . $id . "val\" id=\"exam" . $id . "_" . $seqno . "val\" size=\"" . $size . "\" class=\"astrandtest exam\"  value=\"" . $value . "\"/>$unit";
                }
            } else {

                $result .= "<input type=\"text\" name=\"tmpexam" . $id . "val\" id=\"exam" . $id . "_" . $seqno . "val\" size=\"" . $size . "\" class=\"exam\"  value=\"" . $value . "\"/>$unit";
            }
        }

        $result .= "</div>";

        return $result;
    }

    date_default_timezone_set('Asia/Bangkok');

    $thyear = date("Y");
    ?>



    <script type="text/javascript">

        var weightExam = "<?= AppConst::EXAM_TYPE_WEIGHT ?>";

        var heightExam = "<?= AppConst::EXAM_TYPE_HEIGHT ?>";

        var BMIExam = "<?= AppConst::EXAM_TYPE_OMRON_BMI ?>";

        var validateInput = new Array();

        var cur_thyear = <?= $thyear ?>;

        $(function () {

            if ($(".examtype[type=hidden][value=" + BMIExam + "]").length != 0) {

                BMIExam = $("input[type=text]", $(".examtype[type=hidden][value=" + BMIExam + "]").parent());

                //alert($(BMIExam).attr("id"));

            }

            setupComponents();

        });



        function loadValidate() {

            try {

                jQuery.ajax({
                    url: "./AppHttpRequest.php",
                    type: "POST",
                    contentType: defaultContentType,
                    data: "do=" + $("#donormalize").val(),
                    dataType: "json",
                    error: function (transport, status, errorThrown) {

                    },
                    success: function (data) {

                        $.each(data, function (index, obj) {

                            var tmp = new Array();

                            tmp['min'] = obj.min;

                            tmp['max'] = obj.max;

                            validateInput[index] = tmp;

                        });

                    }



                });

            } catch (ex) {

                alert(ex);

            }

        }



        function back(action) {

            if (action == "<?= AppConst::METHOD_INSERT_APP ?>") {

                window.open("landing.php", "_top");

            } else if (action == "<?= AppConst::METHOD_EDIT_APP ?>") {

                window.open("searchapp.php?action=<?= AppConst::METHOD_QUERY_SEARCH_EDIT ?>", "_top");

            }

        }

        function setupComponents() {

            loadValidate();
            /*$(".exam").change(function(){
             
             
             });*/
            $(".astrandtest").change(function () {
<?php
$id = str_replace("type", "", AppConst::EXAM_TYPE_ASTRAND);
$id = "#exam" . $id . "_";
?>
                var id = "<?= $id ?>";
                if ($(id + "1val").val() != "" && $(id + "2val").val() != "" && $(id + "3val").val() != "") {
                    if ($(id + "4val").val() == "") {
                        $(id + "5val").val("");
                        $(id + "5val").attr("disabled", "disabled");
                        $(".examtype", $(id + "5val").parent()).attr("disabled", "disabled");
                        $(".seqno", $(id + "5val").parent()).attr("disabled", "disabled");
                        $(id + "6val").val("");
                        $(id + "6val").attr("disabled", "disabled");
                        $(".examtype", $(id + "6val").parent()).attr("disabled", "disabled");
                        $(".seqno", $(id + "6val").parent()).attr("disabled", "disabled");
                    }
                    if ($(id + "4val").val() != "") {
                        $(id + "5val").removeAttr("disabled");
                        $(".examtype", $(id + "5val").parent()).removeAttr("disabled");
                        $(".seqno", $(id + "5val").parent()).removeAttr("disabled");
                    }
                    if ($(id + "5val").val() == "") {
                        $(id + "6val").val("");
                        $(id + "6val").attr("disabled", "disabled");
                        $(".examtype", $(id + "6val").parent()).attr("disabled", "disabled");
                        $(".seqno", $(id + "6val").parent()).attr("disabled", "disabled");
                    }

                    if ($(id + "5val").val() != "") {

                        $(id + "6val").removeAttr("disabled");

                        $(".examtype", $(id + "6val").parent()).removeAttr("disabled");

                        $(".seqno", $(id + "6val").parent()).removeAttr("disabled");

                    }

                } else if ($(id + "1val").val() == "" || $(id + "2val").val() == "" || $(id + "3val").val() == "") {

                    $(id + "5val").val("");

                    $(id + "5val").attr("disabled", "disabled");

                    $(".examtype", $(id + "5val").parent()).attr("disabled", "disabled");

                    $(".seqno", $(id + "5val").parent()).attr("disabled", "disabled");



                    $(id + "6val").val("");

                    $(id + "6val").attr("disabled", "disabled");

                    $(".examtype", $(id + "6val").parent()).attr("disabled", "disabled");

                    $(".seqno", $(id + "6val").parent()).attr("disabled", "disabled");

                }

            });

            if ($("#testergrp option").length == 1) {

                $("#testergrpstr").show();

            } else {

                $("#testergrp").change(function () {

                    if ($(this).val() == "other") {

                        $("#testergrpstr").show();

                    }
                    searchTempplate();
                });

            }

            $("#researchChk").click(function () {
                if ($(this).is(":checked")) {
                    $("#isResearch").val("1");
                } else {
                    $("#isResearch").val("2");
                }
            });

            $("#brithday").change(function () {
                if (validateBrithday()) {
                    getAgeFromDOB();
                } else {
                    $("#brithday").val("");
                    $("#brithday").focus();
                }
            });

            $("#age").change(function () {
                if (!isNaN($(this).val())) {
                    var tmpyear = cur_thyear - Math.floor(eval($(this).val()));
                    $("#brithday").val("01/01/" + tmpyear);
                } else {
                    $(this).val("");
                    $(this).focus();
                    alert("กรุณากรอกค่าอายุเป็นตัวเลขเท่านั้น");
                }
            });

            if ($("#brithday").val() != "") {
                getAgeFromDOB();
            }
            if ($("input[name=do][type=hidden]").val() == "<?= AppConst::METHOD_INSERT_APP ?>") {
                $("#newapp_label").show();
            }
            $("#height,#weight").change(function () {
                calBMI();
            });

            /*if($("#height").val()!="" && $("#weight").val()!=""){
             
             calBMI();
             
             }*/
            retriveRow();

        }
        function retriveRow() {
            if ($(".examidlist").length != 0) {
                var obj = $(".examtype[value='']", $(".exam_row"));
                var parent = $(obj).parents(".exam_row");
                $(".exam_row").remove();
                $(".btn_row").before(parent);
                var rownum = $(".exam_row").length;
                $("button", $(parent)).attr("id", "dropdownMenu" + rownum);
                var newastrand = true;
                $("ul", $(parent)).attr("aria-labelledby", "dropdownMenu" + rownum);
                $(".examidlist").each(function (index, examobj) {
                    var examid = $(".examid", $(this)).val();
                    var seqno = $(".seqno", $(this)).val();
                    var val = $(".value", $(this)).val();
                    var obj = $("li[selectval='" + examid + "']", $("#dropdownMenu1").parents(".dropdown"));

                    if (examid != "<?= AppConst::EXAM_TYPE_ASTRAND ?>") {
                        var newrow = addRow(obj, examid);
                        $(".exam", $(newrow)).val(val);
                        $(".examtype", $(newrow)).val(examid);
                        $(".seqno", $(newrow)).val(seqno);
                    } else {
                        var newtemprow = "";
                        if ($(".exam[value!=''][type=text]", $(".astrand_input")).length == 0 && newastrand) {
                            var newrow = addRow(obj, examid);
                            newastrand = false;
                            $(".seqno[value='" + seqno + "']", $(newrow)).parents(".astrand_input").attr("id", "myastrand");
                            newtemprow = $(".seqno[value='" + seqno + "']", $(newrow)).parent();
                        } else {
                            newtemprow = $(".seqno[value='" + seqno + "']", $("#myastrand")).parent();
                        }
                        if ($(newtemprow).length != 0) {
                            $(".exam", $(newtemprow)).val(val);
                            $(".examtype", $(newtemprow)).val(examid);
                            $(".seqno", $(newtemprow)).val(seqno);
                        }
                    }
                });
                $("input", $(".seqno", $("#myastrand")).parent()).removeAttr("disabled");
            }
        }

        function getAgeFromDOB() {

            $("#age").val(calculateAge($("#brithday").val(), false));

        }

        function disbledInput() {



            $(".exam[type=text]").each(function () {

                if ($(this).val() == "") {

                    $(this).attr("disabled", "disabled");

                    $(".examtype", $(this).parent()).attr("disabled", "disabled");

                    $(".seqno", $(this).parent()).attr("disabled", "disabled");

                }

            });

            if ($(".exam[type=radio]:checked").length == 0) {

                $(".exam[type=radio]").each(function () {

                    $(this).attr("disabled", "disabled");

                    $(".examtype", $(this).parent()).attr("disabled", "disabled");

                    $(".seqno", $(this).parent()).attr("disabled", "disabled");

                });

            }

            $("select[class~='exam']").each(function () {

                if ($(this).val() == "0") {

                    $(this).attr("disabled", "disabled");

                    $(".examtype", $(this).parent()).attr("disabled", "disabled");

                    $(".seqno", $(this).parent()).attr("disabled", "disabled");

                }

            });

        }

        function prepareData() {

            var n = 0;

            $(".exam[type=radio]:enabled").each(function () {

                if ($(this).is(":checked")) {

                    n++;

                    $(this).attr("name", "exam" + n + "val");

                    $(".seqno", $(this).parent()).attr("name", "exam" + n + "seqno");

                    $(".examtype", $(this).parent()).attr("name", "exam" + n + "type");

                }

            });

            $(".exam[type=text]:enabled").each(function () {

                if ($(this).val() != "") {

                    n++;

                    $(this).attr("name", "exam" + n + "val");

                    $(".seqno", $(this).parent()).attr("name", "exam" + n + "seqno");

                    $(".examtype", $(this).parent()).attr("name", "exam" + n + "type");

                }

            });

            $("select[class~='exam']").each(function () {

                if ($(this).val() != "" && !$(this).is(":disabled")) {

                    n++;

                    $(this).attr("name", "exam" + n + "val");

                    $(".seqno", $(this).parent()).attr("name", "exam" + n + "seqno");

                    $(".examtype", $(this).parent()).attr("name", "exam" + n + "type");

                }

            });

            /*if ($("#ddlLoad option:selected").val() != "0") {
             
             n++;
             
             $("#ddlLoad").attr("name", "exam" + n + "val");
             
             $("#exam34seqno").attr("name", "exam" + n + "seqno");
             
             $("#exam34type").attr("name", "exam" + n + "type");
             
             }*/

            if ($("#testergrp option:selected").val() == "other") {

                $("#testergrp option:selected").val($("#testergrpstr").val());

            }

            $("#maxinput").val(n);

        }

        function validateForm() {
            if ($("#tname").val().length === 0) {
                alert("กรุณากรอกชื่อ");
                $("#tname").focus();
                return false;
            }
            if ($("#tsurname").val().length === 0) {
                alert("กรุณากรอกนามสกุล");
                $("#tsurname").focus();
                return false;
            }
            if ($("#height").val().length === 0) {
                alert("กรุณากรอกส่วนสูง");
                $("#height").focus();
                return false;
            }
            if ($("#weight").val().length === 0) {
                alert("กรุณากรอกน้ำหนัก");
                $("#weight").focus();
                return false;
            }
            if ($("#brithday").val().length === 0) {
                alert("กรุณากรอกวันเกิด");
                $("#brithday").focus();
                return false;
            }
            /*if (!$("input[name='tmpexam1val']:checked").val()) {             
             alert("กรุณาเลือกความถี่ในการออกกำลังกาย");             
             return false;             
             }*/
            if (!$("input[id='exercise_0']:checked").val() && !$("input[id='exercise_1']:checked").val() && !$("input[id='exercise_2']:checked").val()) {
                alert("กรุณาเลือกความถี่ในการออกกำลังกาย");
                return false;
            }
            if (!$("input[name='gender']:checked").val()) {
                alert("กรุณาเลือกเพศ");
                return false;
            }

<?php
$id = str_replace("type", "", AppConst::EXAM_TYPE_BP_D);
$id = "#exam" . $id . "val";
?>
            var bp_d = "<?= $id ?>";
<?php
$id = str_replace("type", "", AppConst::EXAM_TYPE_BP_U);
$id = "#exam" . $id . "val";
?>
            var bp_u = "<?= $id ?>";
            if ($(bp_d).val() != "" && $(bp_u).val() == "") {
                $(bp_u).focus();
                alert("กรุณากรอกค่าความดันโลหิต");
                return false;
            } else if ($(bp_u).val() != "" && $(bp_d).val() == "") {
                alert("กรุณากรอกค่าความดันโลหิต");
                $(bp_d).focus();
                return false;
            }
            if ($("#testergrp option:selected").val() == "other") {
                if ($("#testergrpstr").val().length === 0) {
                    alert("กรุณากรอกกลุ่มการทดสอบ");
                    $("#testergrpstr").focus();
                    return false;
                }
            } else {
                if ($("#testergrp option:selected").val() == 0) {
                    alert("กรุณาเลือกกลุ่มการทดสอบ");
                    return false;
                }
            }
            return true;
        }



        function validateBrithday() {

            var dob = $("#brithday").val();

            var dob = dob.split("/");

            var current = new Date();

            if (dob.length != 3) {

                alert("กรุณาระบุวัน/เดือน/ปีเกิดให้ถูกต้อง");

                return false;

            } else if (dob[0].length != 2 || dob[1].length != 2 || dob[2].length != 4) {

                alert("กรุณาระบุวัน/เดือน/ปีเกิดให้ถูกต้อง");

                return false;

            } else if (eval(dob[2]) > current.getUTCFullYear()) {

                alert("กรุณาระบุวัน/เดือน/ปีเกิดให้ถูกต้อง");

                return false;

            }

            return true;

        }



        function changeFormatDate(dateString) {

            var date_arr = dateString.split("/");

            var date = date_arr[0];

            var month = date_arr[1];

            date_arr[0] = month;

            date_arr[1] = date;

            return date_arr.join("/");

        }



        function calculateAge(datestart, thialocale) {

            datestart = new Date(changeFormatDate(datestart));

            var datestop = new Date();

            var current = new Date();

            var convertFlag = false;

            if (!thialocale) {

                convertFlag = Math.abs(current.getYear() - datestart.getYear()) == 543 ? true : false;

                if (convertFlag) {

                    datestart = parseDate(datestart.getDate() + "/" + (datestart.getMonth() + 1) + "/" + (datestart.getYear() - 543));

                }

                convertFlag = Math.abs(current.getYear() - datestop.getYear()) == 543 ? true : false;

                if (convertFlag) {

                    datestop = parseDate(datestop.getDate() + "/" + (datestop.getMonth() + 1) + "/" + (datestop.getYear() - 543));

                }

            }

            if (thialocale) {

                convertFlag = Math.abs((current.getYear() + 543) - datestart.getYear()) == 543 ? true : false;

                if (convertFlag) {

                    datestart = parseDate(datestart.getDate() + "/" + (datestart.getMonth() + 1) + "/" + (datestart.getYear() + 543));

                }

                convertFlag = Math.abs((current.getYear() + 543) - datestop.getYear()) == 543 ? true : false;

                if (convertFlag) {

                    datestop = parseDate(datestop.getDate() + "/" + (datestop.getMonth() + 1) + "/" + (datestop.getYear() + 543));

                }

            }

            var day = Math.abs((datestop.getDate()) - (datestart.getDate()));

            var month = Math.abs((datestop.getMonth() + 1) - (datestart.getMonth() + 1));

            var year = Math.abs((datestop.getYear()) - (datestart.getYear()));

            return year;

        }



        function genDataTest() {

            $(".exam[type=text]:enabled").each(function () {

                var id = $(this).attr("id");

                id = id.replace("exam", "");

                id = id.replace("val", "");

                $(this).val(id);

            });

        }



        function submitData(btn) {
            if (!validateForm()) {
                return  false;
            }
            if ($(btn).attr("iname") == "insert") {

                disbledInput();

            }

            prepareData();

            //prompt("", $("#tform1").serialize());

            try {
                $(".btn_row").hide();
                $("#waiting_group").show();
                jQuery.ajax({
                    url: "AppHttpRequest.php",
                    type: "POST",
                    contentType: defaultContentType,
                    data: $("#tform1").serialize(),
                    //dataType: "html",

                    dataType: "json",
                    error: function (transport, status, errorThrown) {
                        alert("Systen terminated,Please contact administrator.");
                        $("#waiting_group").hide();
                        $(".btn_row").show();
                    },
                    success: function (data) {
                        if (data.result == "<?= $successval ?>") {

                            if ($(btn).attr("iname") == "insert") {

                                if (confirm("ต้องการพิมพ์ผลการทดสอบหรือไม่")) {

                                    $("#appid2").val(data.appid);

                                    $("#testerid2").val($("#testerid").val());

                                    $("#dform1").submit();

                                } else {
                                    //window.open("landing.php", "_top");
                                }
                            } else {

                                $("input[name=do]").val("<?= AppConst::METHOD_EDIT_APP ?>");
                                $("#appid2").val(data.appid);
                                $("#appid").val(data.appid);

                            }
                            $("input:disabled").removeAttr("disabled");

                            alert("บันทึกสำเร็จ");

                        } else {

                            alert("เกิดข้อผิดพลาดกรุณาทำรายการใหม่อีกครั้ง");

                        }
                        $("#waiting_group").hide();
                        $(".btn_row").show();
                        //prompt("", "success : " + data);

                    }



                });

            } catch (ex) {

                alert(ex);

            }



            //prompt("", $("#tform1").serialize());



        }

        function submitPrint() {

            $("#appid2").val($("#appid").val());

            $("#testerid2").val($("#testerid").val());

            $("#dform1").submit();

        }

        function prepareInsData() {

            clearForm();

            $("input[name=do]").val($("#tmpaction").val());

            $("input[name=tname][type=text]").attr("readonly", "readonly");

            $("input[name=tsurname][type=text]").attr("readonly", "readonly");

            $("input[name=ename][type=text]").attr("readonly", "readonly");

            $("input[name=esurname][type=text]").attr("readonly", "readonly");

            $("input[name=gender][type=radio]").attr("readonly", "readonly");

            $("input[name=brithday][type=text]").attr("readonly", "readonly");

            $("#newapp_label").show();

        }

        function clearForm() {

            $(".exam[type=text]").val("");

            $("input[name!=gender][type=radio]").removeAttr("checked");

            $("select[id!='testergrp']").each(function () {

                if ($("option[value='0']", $(this)).length == 1) {

                    $("option[value='0']", $(this)).attr("selected", "selected");

                } else {

                    $("option", $(this)).first().attr("selected", "selected");

                }

                $("input[type=text]", $(this)).hide();

            });

            $("option", $(this)).first().attr("selected", "selected");

            $("#exam25val").attr("disabled", "disabled");

            $(".examtype", $("#exam25val").parent()).attr("disabled", "disabled");

            $(".seqno", $("#exam25val").parent()).attr("disabled", "disabled");

            $("#exam26val").attr("disabled", "disabled");

            $(".examtype", $("#exam26val").parent()).attr("disabled", "disabled");

            $(".seqno", $("#exam26val").parent()).attr("disabled", "disabled");

        }

        function calBMI() {

            var height = $("#height").val();

            var weigth = $("#weight").val();

            var BMI = 0;

            if (height != "" && weigth != "") {

                height = eval(removeDelimiter(height, ','));

                weigth = eval(removeDelimiter(weigth, ','));

                if (height > 0) {

                    BMI = weigth / ((height / 100) * (height / 100));

                    BMI = formatFloating(BMI, 2)

                    $(BMIExam).val(BMI);

                }
            }
        }
        function removeComma(value) {
            value = removeDelimiter(value, ",");
            return value;
        }
        function removeDelimiter(value, delimiter) {
            return value.replace(delimiter, "");
        }
        function formatFloating(value, decimal) {
            value = value + "";
            value = removeComma(value);
            return formatDecimal(value, decimal, true);
        }
        function formatDecimal(value, decimal, verifydecimal) {
            var sign = "";
            var result = value + "";
            var bstr = "";
            var cstr = "";
            var i = result.indexOf("-");
            if (i >= 0) {
                sign = "-";
                result = result.substring(i + 1);
            } else {
                i = result.indexOf("+");
                if (i >= 0) {
                    sign = "+";
                    result = result.substring(i + 1);
                }
            }
            var astr = result;
            i = result.indexOf(".");
            if (i > 0) {
                astr = result.substring(0, i);

                bstr = result.substring(i + 1);

                cstr = result.substring(i);
            }
            var la = astr.length;
            if (la > 3) {
                var tstr = astr;
                astr = "";
                while (tstr != "") {
                    la = tstr.length;
                    var md = la % 3;
                    if (md > 0) {
                        astr += tstr.substring(0, md) + ",";
                        tstr = tstr.substring(md);
                    } else {
                        astr += tstr.substring(0, 3);
                        tstr = tstr.substring(3);
                        if (tstr != "")
                            astr += ",";
                    }
                }
            }
            if (verifydecimal) {
                if (decimal > 0) {
                    var l = bstr.length;
                    if (decimal > l) {
                        var j = 0;
                        for (j = l; j < decimal; j++) {
                            bstr += "0";
                        }
                    } else {
                        bstr = bstr.substring(0, decimal);
                    }
                    if (astr == "")
                        return "";
                    return sign + astr + "." + bstr;
                } else {
                    return sign + astr;
                }
            } else {
                return sign + astr + cstr;
            }
        }
        function searchTempplate() {
            var action = $("#doview").val();
            var groupid = $("#testergrp option:selected").val();
            try {
                jQuery.ajax({
                    url: "AppHttpRequest.php",
                    type: "POST",
                    contentType: defaultContentType,
                    data: "do=" + action + "&groupid=" + groupid + "&seed=" + Math.random(),
                    //dataType: "html",
                    dataType: "json",
                    error: function (transport, status, errorThrown) {

                        alert("error : " + errorThrown);

                    },
                    success: function (data) {
                        var obj = $(".examtype[value='']", $(".exam_row"));
                        var parent = $(obj).parents(".exam_row");
                        $(".exam_row").remove();
                        $(".btn_row").before(parent);
                        var rownum = $(".exam_row").length;
                        $("button", $(parent)).attr("id", "dropdownMenu" + rownum);
                        $("ul", $(parent)).attr("aria-labelledby", "dropdownMenu" + rownum);
                        $(data.ResponseDetail).each(function (index, value) {
                            var obj = $("li[selectval='" + value + "']", $("#dropdownMenu1").parents(".dropdown"));
                            addRow(obj, value);
                        });
                    }

                });
            } catch (ex) {
                alert(ex);
            }

            //prompt("", $("#tform1").serialize());

        }
        function addRow(obj, examid) {
            if ($(obj).length == 0) {
                return;
            }
<?php
$id = AppConst::EXAM_TYPE_ASTRAND;
?>
            if ($(".examtype[value='" + examid + "']").length != 0 && examid != "<?= $id ?>") {
                return;
            }
            var parent = $(obj).parents(".exam_row");
            var newrow = parent.clone();
            if (!$("ul", $(parent)).attr("examselected")) {
                $(".btn_row").before(newrow);
            }
            $(".astrand_input", $(parent)).remove();
            if (examid == "<?= $id ?>") {
                $(".textinput", $(parent)).before($(".astrand_input", $(".examtype[value='']", $(".exam_row")).parents(".exam_row")).clone());
                $(".textinput", $(parent)).hide();
                $(".astrand_input", $(parent)).show();
                for (var i = 1; i <= 6; i++) {
                    $("input", $(".astrand_" + i)).removeAttr("disabled");
                }
                $("input", $(".ddlLoad", $(parent)).parent()).removeAttr("disabled");
                $(".ddlLoad", $(parent)).removeAttr("disabled");
                $(parent).addClass("selectedastrand");
            } else {
                $(".textinput", $(parent)).show();
                //$(".astrand_input", $(parent)).hide();
                //$("input", $(".astrand_input")).attr("disabled", "disabled");
                $(".examtype", $(parent)).val(examid);
                $(".seqno", $(parent)).val("0");
            }
            $("button", $(parent)).text($(obj).text());
            var rownum = $(".exam_row").length;
            $("button", $(parent)).attr("id", "dropdownMenu" + rownum);
            $("ul", $(parent)).attr("aria-labelledby", "dropdownMenu" + rownum);
            $("ul", $(parent)).attr("examselected", "examselected");
            $(".removeRow", $(parent)).show();
            return parent;
        }
        function selectExam(obj, examid) {
            addRow(obj, examid);
        }
        function removeRow(obj) {
            var parent = $(obj).parents(".exam_row");
            $(parent).remove();
        }
        function creatApplication() {
            $(".selectedastrand").remove();
            $("#myastrand").parents(".exam_row").remove();
            searchTempplate();
            $("input[name=do]").val("<?= AppConst::METHOD_INSERT_APP ?>");
            $("#appid2").val("");
            $("#appid").val("");
        }
    </script>

    <style type="text/css">

        table tr td[class~="fontscreen"]{

            padding-top: 5px;

            padding-bottom : 5px;

        }

    </style>

</html>
