<html>
    <?php
    include 'page/header.php';
    ?>
    <tr>
        <td style="width: 1024px;height: 564px;margin: 0px auto;background-color: #ffffff;text-align: center;border: 1px solid #990000;border-top: none;border-bottom: none;" >
            <!--img src="images/index_02.png" width="1024" height="564" alt=""-->
            <form name="tform1" id="tform1" onsubmit="return false;">                
                <table style="width: 70%" border="0" cellpadding="0" cellspacing="0" align="center">
                    <tr>
                        <td height="40" style="width: 40%;padding-top: 10px" class="fontscreen resetfont">
                            รหัสผ่านเก่า
                        </td>
                        <td height="40" style="padding-top: 10px ">
                            <?php
                            $paramname = AppConst::PARAM_OLD_PAS;
                            $action = AppConst::METHOD_RESET_PASSWORD;
                            ?>
                            <input name="<?= $paramname ?>" type="password" id="oldpassword" style="border: 1px solid;border-color: #990000;background-color: transparent;color: #990000;height: 34"/>
                        </td>
                    </tr>
                    <tr>
                        <td height="40" style="width: 40%;padding-top: 10px " class="fontscreen resetfont">
                            รหัสผ่านใหม่
                        </td>
                        <td height="40" style="padding-top: 10px ">
                            <?php
                            $paramname = AppConst::PARAM_PAS;
                            ?>
                            <input name="<?= $paramname ?>" type="password" id="newpassword" style="border: 1px solid;border-color: #990000;background-color: transparent;color: #990000;height: 34"/>
                        </td>
                    </tr>             
                    <tr>
                        <td height="40" style="width: 40%;padding-top: 10px " class="fontscreen resetfont">
                            ยืนยันรหัสผ่านใหม่
                        </td>
                        <td height="40" style="padding-top: 10px ">
                            <?php
                            $paramname = AppConst::PARAM_PAS;
                            ?>
                            <input name="tmp" type="password" id="con_password" style="border: 1px solid;border-color: #990000;background-color: transparent;color: #990000;height: 34"/>
                            <?php
                            $paramname = AppConst::PARAM_USR;
                            $userid = $_SESSION['userid'];
                            ?>
                            <input name="<?= $paramname ?>" type="hidden" id="userid" value="<?=$userid?>"/>

                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: right">
                            <img src="images/btn/submit.png" style="cursor: pointer;padding-left: 10px;" onclick="submitReset()"/>
                        </td>
                    </tr>
                </table>
                <input name="do" type="hidden" value="<?=$action?>"/>
            </form>
        </td>
    </tr>
    <?php
    include 'page/footer.php';
    $successval = AppConst::STATUS_SUCCESS;
    $errorval = AppConst::STATUS_ERROR;
    ?>
    <script type="text/javascript">
        function submitReset(){
            try {
                if($("#newpassword").val()!=$("#con_password").val()){
                    alert("กรุณายืนยันรหัสผ่านให้ถูกต้อง");
                    return;
                }
                jQuery.ajax({
                    url: "AppHttpRequest.php",
                    type: "POST",
                    contentType: defaultContentType,
                    data: $("#tform1").serialize(),
                    dataType: "json",
                    error: function (transport, status, errorThrown) {
                        alert("error");
                    },
                    success: function (data) {
                        //alert(data);
                        //$(data).each(function(index,obj){                        
                        //});
                        if (data.MSGID == "<?= $successval ?>") {
                            window.open("landing.php");
                        } else if (data.MSGID == "<?= $errorval ?>") {
                            alert("กรุณาระบุรหัสผ่านเก่าให้ถูกต้อง");
                        }
                    }
                });
            } catch (ex) {
                alert(ex);
            }
    }
    </script>
    <style type="text/css">
        .resetfont{
            font-size: 1em;
            color: #990000;
        }
    </style>
</html>