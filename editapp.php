<html>
    <?php
    include 'page/header.php';
    include 'AppManager.php';
    $appmng = new AppManager();
    $appobj = $appmng->GetExamDetailByAppID("A161217233104483320");
    $testerObj = new TesterHeaderInfo();
    $testerObj = $appobj->TesterDetail;
    $gender_m = AppConst::MALE;
    $gender_f = AppConst::FEMALE;
    ?>
    <tr>
        <td style="width: 1024px;height: 564px;margin: 0px auto;background-color: #ffffff;text-align: center" >
            <form name="tform1" id="tform1" action="AppHttpRequest.php" method="POST" onsubmit="submitData()">
                <input name="userid" id="userid" type="hidden" value="<?= $_SESSION['userid'] ?>"/>
                <table style="width: 80%" border="0" cellpadding="0" cellspacing="0" align="center">
                    <tr>
                        <td colspan="4" class="fontscreen"><b>ข้อมูลผู้ทำการทดสอบ<b></td>
                                    </tr>                    
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td class="fontscreen">หมายเลขประจำตัวผู้ทดสอบ</td>
                                        <td colspan="4" class="fontscreen">
                                            <input type="text" name="testerid" id="testerid" readonly="readonly" size="25" style="background-color: #999999;color: #ffffff" value="<?=$testerObj->GetTesterID() ?>"/>
                                        </td>
                                    </tr>                    
                                    <tr>
                                        <td class="fontscreen" style="padding-top: 10px">ชื่อ&nbsp;</td>
                                        <td style="padding-top: 10px"><input type="text" name="tname" id="tname" size="25" value="<?=$appmng->returnBlankIfNull($testerObj->GetNameTH())?>"/></td>
                                        <td class="fontscreen" style="padding-left: 10px;padding-top: 10px">นามสกุล</td>
                                        <td style="padding-top: 10px"><input type="text" name="tsurname" id="tsurname" size="25" value="<?=$appmng->returnBlankIfNull($testerObj->GetSurnameTH())?>"/></td>
                                    </tr>                    
                                    <tr>
                                        <td class="fontscreen">ชื่อ(อังกฤษ)</td>
                                        <td><input type="text" name="ename" id="ename" size="25" value="<?=$appmng->returnBlankIfNull($testerObj->GetNameEN())?>"/></td>
                                        <td class="fontscreen" style="padding-left: 10px">นามสกุล(อังกฤษ)</td>
                                        <td><input type="text" name="esurname" id="esurname" size="25" value="<?=$appmng->returnBlankIfNull($testerObj->GetSurnameEN())?>"/></td>
                                    </tr>                    
                                    <tr>
                                        <td class="fontscreen">กลุ่มการทดสอบ</td>
                                        <td colspan="3"><select name="testergrp" ><?= $appmng->RequestDDLGrpTest() ?></select></td>
                                    </tr>                    
                                    <tr>
                                        <td class="fontscreen">เพศ</td>
                                        <td class="fontscreen">
                                            <input name="gender" type="radio" id="gender_m" value="<?= $gender_m ?>"/>ชาย
                                            <input name="gender" type="radio" id="gender_f" value="<?= $gender_f ?>"/>หญิง
                                        </td>
                                        <td class="fontscreen" style="padding-left: 10px">วัน/เดือน/ปี เกิด (ค.ศ.)</td>
                                        <td class="fontscreen"><input type="text" name="brithday" id="brithday" size="25" value="<?=$appmng->returnBlankIfNull($testerObj->GetDOB())?>"/></td>
                                    </tr>                    
                                    <tr>
                                        <td class="fontscreen">น้ำหนัก</td>
                                        <td class="fontscreen">
                                            <input type="text" name="weight" id="weight" size="10"  value="<?=$appmng->returnBlankIfNull($testerObj->GetWeight())?>"/>
                                            &nbsp;กก.
                                        </td>
                                        <td class="fontscreen" style="padding-left: 10px">ส่วนสูง</td>
                                        <td class="fontscreen">
                                            <input type="text" name="height" id="height" size="10" value="<?=$appmng->returnBlankIfNull($testerObj->GetHeight())?>"/>
                                            &nbsp;ซม.
                                        </td>
                                    </tr>                    
                                    <tr>
                                        <td class="fontscreen">การออกกาลังกาย</td>
                                        <td class="fontscreen" colspan="3">
                                            <?php
                                            $typeval = AppConst::EXAM_TYPE_EXCERCISE;
                                            ?>
                                            <input name="tmpexam1type" type="hidden" value="<?= $typeval ?>" class="examtype"/>
                                            <input name="tmpexam1seqno" type="hidden" value="" class="seqno"/>
                                            <input name="tmpexam1val" type="radio" id="exercise_0" class="exam" value="0"/>ไม่เคย
                                            <input name="tmpexam1val" type="radio" id="exercise_1" class="exam" value="1"/>1-2 ครั้ง ต่อสัปดาห์
                                            <input name="tmpexam1val" type="radio" id="exercise_2" class="exam" value="2"/>2-5 ครั้ง ต่อสัปดาห์
                                        </td>
                                    </tr>                    
                                    <tr>
                                        <td class="fontscreen">ความดันโลหิต</td>
                                        <td class="fontscreen">
                                            <?php
                                            $typeval = AppConst::EXAM_TYPE_BP_U;
                                            ?>
                                            <div>
                                                <input name="tmpexam2type" type="hidden" value="<?= $typeval ?>" class="examtype"/>
                                                <input name="tmpexam2seqno" type="hidden" value="" class="seqno"/>
                                                <input type="text" name="tmpexam2val" id="exam2val" size="4" class="exam"/>
                                            </div>
                                            /
                                            <?php
                                            $typeval = AppConst::EXAM_TYPE_BP_D;
                                            ?>
                                            <div>
                                                <input name="tmpexam3type" type="hidden" value="<?= $typeval ?>" class="examtype"/>
                                                <input name="tmpexam3seqno" type="hidden" value="" class="seqno"/>
                                                <input type="text" name="tmpexam3val" id="exam3val" size="4" class="exam"/>
                                            </div>
                                            &nbsp;มม.ปรอท                                        
                                        </td>
                                        <td class="fontscreen" style="padding-left: 10px">ชีพจร</td>
                                        <td class="fontscreen">
                                            <?php
                                            $typeval = AppConst::EXAM_TYPE_PULSE;
                                            ?>
                                            <input name="tmpexam24type" type="hidden" value="<?= $typeval ?>" class="examtype"/>
                                            <input name="tmpexam24seqno" type="hidden" value="" class="seqno"/>
                                            <input type="text" name="tmpexam24val" id="exam24val" size="4" class="exam"/>
                                            &nbsp;ครั้ง/นาที
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4"><hr></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="fontscreen"><b>Body Composition</b></td>
                                    </tr>
                                    <tr>
                                        <td class="fontscreen" colspan="4">Waist circumference &nbsp;&nbsp;
                                            <?php
                                            $typeval = AppConst::EXAM_TYPE_WAIST_CIRC;
                                            ?>
                                            <input name="tmpexam4type" type="hidden" value="<?= $typeval ?>" class="examtype"/>
                                            <input name="tmpexam4seqno" type="hidden" value="" class="seqno"/>
                                            <input type="text" name="tmpexam4val" id="exam4val" size="25" class="exam"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="fontscreen"><b>Skin fold</b></td>
                                    </tr>
                                    <tr>
                                        <td class="fontscreen">Biceps</td>
                                        <td>
                                            <?php
                                            $typeval = AppConst::EXAM_TYPE_SKIN_FOLD_BICEP;
                                            ?>
                                            <input name="tmpexam29type" type="hidden" value="<?= $typeval ?>" class="examtype"/>
                                            <input name="tmpexam29seqno" type="hidden" value="" class="seqno"/>
                                            <input type="text" name="tmpexam29val" id="exam29val" size="25" class="exam"/>
                                        </td>
                                        <td class="fontscreen" style="padding-left: 10px">Triceps</td>
                                        <td>
                                            <?php
                                            $typeval = AppConst::EXAM_TYPE_SKIN_FOLD_TRICEP;
                                            ?>
                                            <input name="tmpexam30type" type="hidden" value="<?= $typeval ?>" class="examtype"/>
                                            <input name="tmpexam30seqno" type="hidden" value="" class="seqno"/>
                                            <input type="text" name="tmpexam30val" id="exam30val" size="25" class="exam"/>
                                        </td>
                                    </tr>   
                                    <tr>
                                        <td class="fontscreen">Subscapular</td>
                                        <td>
                                            <?php
                                            $typeval = AppConst::EXAM_TYPE_SKIN_FOLD_SUBSCAP;
                                            ?>
                                            <input name="tmpexam31type" type="hidden" value="<?= $typeval ?>" class="examtype"/>
                                            <input name="tmpexam31seqno" type="hidden" value="" class="seqno"/>
                                            <input type="text" name="tmpexam31val" id="exam31val" size="25" class="exam"/>
                                        </td>
                                        <td class="fontscreen" style="padding-left: 10px">Supra iliac</td>
                                        <td>
                                            <?php
                                            $typeval = AppConst::EXAM_TYPE_SKIN_FOLD_SUPRA;
                                            ?>
                                            <input name="tmpexam32type" type="hidden" value="<?= $typeval ?>" class="examtype"/>
                                            <input name="tmpexam32seqno" type="hidden" value="" class="seqno"/>
                                            <input type="text" name="tmpexam32val" id="exam32val" size="25" class="exam"/>
                                        </td>
                                    </tr>   
                                    <tr>
                                        <td colspan="4" class="fontscreen"><b>Omron</b></td>
                                    </tr>
                                    <tr>
                                        <td class="fontscreen">Body fat</td>
                                        <td>
                                            <?php
                                            $typeval = AppConst::EXAM_TYPE_OMRON_BODYFAT;
                                            ?>
                                            <input name="tmpexam33type" type="hidden" value="<?= $typeval ?>" class="examtype"/>
                                            <input name="tmpexam33seqno" type="hidden" value="" class="seqno"/>
                                            <input type="text" name="tmpexam33val" id="exam33val" size="25" class="exam"/>
                                        </td>
                                        <td class="fontscreen" style="padding-left: 10px">Visceral fat</td>
                                        <td>
                                            <?php
                                            $typeval = AppConst::EXAM_TYPE_OMRON_VISCERAL;
                                            ?>
                                            <input name="tmpexam34type" type="hidden" value="<?= $typeval ?>" class="examtype"/>
                                            <input name="tmpexam34seqno" type="hidden" value="" class="seqno"/>
                                            <input type="text" name="tmpexam34val" id="exam34val" size="25" class="exam"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fontscreen">RM</td>
                                        <td>
                                            <?php
                                            $typeval = AppConst::EXAM_TYPE_OMRON_RM;
                                            ?>
                                            <input name="tmpexam35type" type="hidden" value="<?= $typeval ?>" class="examtype"/>
                                            <input name="tmpexam35seqno" type="hidden" value="" class="seqno"/>
                                            <input type="text" name="tmpexam35val" id="exam35val" size="25" class="exam"/>
                                        </td>
                                        <td class="fontscreen" style="padding-left: 10px">BMI</td>
                                        <td>
                                            <?php
                                            $typeval = AppConst::EXAM_TYPE_OMRON_BMI;
                                            ?>
                                            <input name="tmpexam36type" type="hidden" value="<?= $typeval ?>" class="examtype"/>
                                            <input name="tmpexam36seqno" type="hidden" value="" class="seqno"/>
                                            <input type="text" name="tmpexam36val" id="exam36val" size="25" class="exam"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fontscreen">Body age</td>
                                        <td colspan="3">
                                            <?php
                                            $typeval = AppConst::EXAM_TYPE_OMRON_BODYAGE;
                                            ?>
                                            <input name="tmpexam37type" type="hidden" value="<?= $typeval ?>" class="examtype"/>
                                            <input name="tmpexam37seqno" type="hidden" value="" class="seqno"/>
                                            <input type="text" name="tmpexam37val" id="exam37val" size="25" class="exam"/>
                                        </td>                                        
                                    </tr>
                                    <tr>
                                        <td class="fontscreen">%Whole body fat</td>
                                        <td>
                                            <?php
                                            $typeval = AppConst::EXAM_TYPE_OMRON_WBODYFAT;
                                            ?>
                                            <input name="tmpexam39type" type="hidden" value="<?= $typeval ?>" class="examtype"/>
                                            <input name="tmpexam39seqno" type="hidden" value="" class="seqno"/>
                                            <input type="text" name="tmpexam39val" id="exam39val" size="25" class="exam"/>
                                        </td>
                                        <td class="fontscreen" style="padding-left: 10px">Trunk</td>
                                        <td>
                                            <?php
                                            $typeval = AppConst::EXAM_TYPE_OMRON_WBF_TRUNK;
                                            ?>
                                            <input name="tmpexam38type" type="hidden" value="<?= $typeval ?>" class="examtype"/>
                                            <input name="tmpexam38seqno" type="hidden" value="" class="seqno"/>
                                            <input type="text" name="tmpexam38val" id="exam38val" size="25" class="exam"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fontscreen">Leg</td>
                                        <td>
                                            <?php
                                            $typeval = AppConst::EXAM_TYPE_OMRON_WBF_LEG;
                                            ?>
                                            <input name="tmpexam41type" type="hidden" value="<?= $typeval ?>" class="examtype"/>
                                            <input name="tmpexam41seqno" type="hidden" value="" class="seqno"/>
                                            <input type="text" name="tmpexam41val" id="exam41val" size="25" class="exam"/>
                                        </td>
                                        <td class="fontscreen" style="padding-left: 10px">Arm</td>
                                        <td>
                                            <?php
                                            $typeval = AppConst::EXAM_TYPE_OMRON_WBF_ARM
                                            ?>
                                            <input name="tmpexam40type" type="hidden" value="<?= $typeval ?>" class="examtype"/>
                                            <input name="tmpexam40seqno" type="hidden" value="" class="seqno"/>
                                            <input type="text" name="tmpexam40val" id="exam40val" size="25" class="exam"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fontscreen">%Whole body muscle</td>
                                        <td>
                                            <?php
                                            $typeval = AppConst::EXAM_TYPE_OMRON_WBODYMUSCLE;
                                            ?>
                                            <input name="tmpexam42type" type="hidden" value="<?= $typeval ?>" class="examtype"/>
                                            <input name="tmpexam42seqno" type="hidden" value="" class="seqno"/>
                                            <input type="text" name="tmpexam42val" id="exam42val" size="25" class="exam"/>
                                        </td>
                                        <td class="fontscreen" style="padding-left: 10px">Trunk</td>
                                        <td>
                                            <?php
                                            $typeval = AppConst::EXAM_TYPE_OMRON_WBM_TRUNK;
                                            ?>
                                            <input name="tmpexam43type" type="hidden" value="<?= $typeval ?>" class="examtype"/>
                                            <input name="tmpexam43seqno" type="hidden" value="" class="seqno"/>
                                            <input type="text" name="tmpexam43val" id="exam43val" size="25" class="exam"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fontscreen">Leg</td>
                                        <td>
                                            <?php
                                            $typeval = AppConst::EXAM_TYPE_OMRON_WBM_LEG;
                                            ?>
                                            <input name="tmpexam44type" type="hidden" value="<?= $typeval ?>" class="examtype"/>
                                            <input name="tmpexam44seqno" type="hidden" value="" class="seqno"/>
                                            <input type="text" name="tmpexam44val" id="exam44val" size="25" class="exam"/>
                                        </td>
                                        <td class="fontscreen" style="padding-left: 10px">Arm</td>
                                        <td>
                                            <?php
                                            $typeval = AppConst::EXAM_TYPE_OMRON_WBM_ARM
                                            ?>
                                            <input name="tmpexam45type" type="hidden" value="<?= $typeval ?>" class="examtype"/>
                                            <input name="tmpexam45seqno" type="hidden" value="" class="seqno"/>
                                            <input type="text" name="tmpexam45val" id="exam45val" size="25" class="exam"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="fontscreen"><b>Power and Strength</b></td>
                                    </tr>
                                    <tr>
                                        <td class="fontscreen">Handgrip</td>
                                        <td class="fontscreen">
                                            <?php
                                            $typeval = AppConst::EXAM_TYPE_HANDGRIP_L;
                                            ?>
                                            ซ้าย&nbsp;
                                            <input name="tmpexam27type" type="hidden" value="<?= $typeval ?>" class="examtype"/>
                                            <input name="tmpexam27seqno" type="hidden" value="" class="seqno"/>
                                            <input type="text" name="tmpexam27val" id="exam27val" size="7" class="exam"/>
                                            กก.&nbsp;&nbsp;ขวา&nbsp;
                                            <?php
                                            $typeval = AppConst::EXAM_TYPE_HANDGRIP_R;
                                            ?>
                                            <input name="tmpexam28type" type="hidden" value="<?= $typeval ?>" class="examtype"/>
                                            <input name="tmpexam28seqno" type="hidden" value="" class="seqno"/>
                                            <input type="text" name="tmpexam28val" id="exam28val" size="7" class="exam"/>
                                            กก.
                                        </td>
                                        <td class="fontscreen" style="padding-left: 10px">Standing board jump</td>
                                        <td>
                                            <?php
                                            $typeval = AppConst::EXAM_TYPE_STAND_BOARD_JUMP;
                                            ?>
                                            <input name="tmpexam6type" type="hidden" value="<?= $typeval ?>" class="examtype"/>
                                            <input name="tmpexam6seqno" type="hidden" value="" class="seqno"/>
                                            <input type="text" name="tmpexam6val" id="exam6val" size="25" class="exam"/>
                                        </td>
                                    </tr>                    
                                    <tr>
                                        <td class="fontscreen">Vertical jump</td>
                                        <td>
                                            <?php
                                            $typeval = AppConst::EXAM_TYPE_VERICAL_JUMP
                                            ?>
                                            <input name="tmpexam7type" type="hidden" value="<?= $typeval ?>" class="examtype"/>
                                            <input name="tmpexam7seqno" type="hidden" value="" class="seqno"/>
                                            <input type="text" name="tmpexam7val" id="exam7val" size="25" class="exam"/>
                                        </td>
                                        <td class="fontscreen" style="padding-left: 10px">Sit up</td>
                                        <td>
                                            <?php
                                            $typeval = AppConst::EXAM_TYPE_SIT_UP
                                            ?>
                                            <input name="tmpexam8type" type="hidden" value="<?= $typeval ?>" class="examtype"/>
                                            <input name="tmpexam8seqno" type="hidden" value="" class="seqno"/>
                                            <input type="text" name="tmpexam8val" id="exam8val" size="25" class="exam"/>
                                        </td>
                                    </tr>                    
                                    <tr>
                                        <td class="fontscreen">Push up</td>
                                        <td colspan="3">
                                            <?php
                                            $typeval = AppConst::EXAM_TYPE_PUSH_UP
                                            ?>
                                            <input name="tmpexam9type" type="hidden" value="<?= $typeval ?>" class="examtype"/>
                                            <input name="tmpexam9seqno" type="hidden" value="" class="seqno"/>
                                            <input type="text" name="tmpexam9val" id="exam9val" size="25" class="exam"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="fontscreen"><b>Muscle endurance</b></td>
                                    </tr>
                                    <tr>
                                        <td class="fontscreen">Bicep curl</td>
                                        <td>
                                            <?php
                                            $typeval = AppConst::EXAM_TYPE_BACK_STRACH;
                                            ?>
                                            <input name="tmpexam10type" type="hidden" value="<?= $typeval ?>" class="examtype"/>
                                            <input name="tmpexam10eqno" type="hidden" value="" class="seqno"/>
                                            <input type="text" name="tmpexam10val" id="exam10val" size="25" class="exam"/>
                                        </td>
                                        <td class="fontscreen" style="padding-left: 10px">Plank</td>
                                        <td>
                                            <?php
                                            $typeval = AppConst::EXAM_TYPE_PLANK
                                            ?>
                                            <input name="tmpexam11type" type="hidden" value="<?= $typeval ?>" class="examtype"/>
                                            <input name="tmpexam11seqno" type="hidden" value="" class="seqno"/>
                                            <input type="text" name="tmpexam11val" id="exam11val" size="25" class="exam"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="fontscreen"><b>Flexibility</b></td>
                                    </tr>
                                    <tr>
                                        <td class="fontscreen">Sit and Reach</td>
                                        <td>
                                            <?php
                                            $typeval = AppConst::EXAM_TYPE_SIT_REACH
                                            ?>
                                            <input name="tmpexam12type" type="hidden" value="<?= $typeval ?>" class="examtype"/>
                                            <input name="tmpexam12eqno" type="hidden" value="" class="seqno"/>
                                            <input type="text" name="tmpexam12val" id="exam12val" size="25" class="exam"/>
                                        </td>
                                        <td class="fontscreen" style="padding-left: 10px">Back scratch</td>
                                        <td>
                                            <?php
                                            $typeval = AppConst::EXAM_TYPE_BACK_STRACH;
                                            ?>
                                            <input name="tmpexam13type" type="hidden" value="<?= $typeval ?>" class="examtype"/>
                                            <input name="tmpexam13seqno" type="hidden" value="" class="seqno"/>
                                            <input type="text" name="tmpexam13val" id="exam13val" size="25" class="exam"/>
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td colspan="4" class="fontscreen"><b>Agility and balance</b></td>
                                    </tr>                   
                                    <tr>
                                        <td class="fontscreen">Shuttle run</td>
                                        <td>
                                            <?php
                                            $typeval = AppConst::EXAM_TYPE_SHUTTLE_RUN;
                                            ?>
                                            <input name="tmpexam16type" type="hidden" value="<?= $typeval ?>" class="examtype"/>
                                            <input name="tmpexam16seqno" type="hidden" value="" class="seqno"/>
                                            <input type="text" name="tmpexam16val" id="exam16val" size="25" class="exam"/>
                                        </td>
                                        <td class="fontscreen" style="padding-left: 10px">Zig zag</td>
                                        <td>
                                            <?php
                                            $typeval = AppConst::EXAM_TYPE_ZIGZAG;
                                            ?>
                                            <input name="tmpexam17type" type="hidden" value="<?= $typeval ?>" class="examtype"/>
                                            <input name="tmpexam17seqno" type="hidden" value="" class="seqno"/>
                                            <input type="text" name="tmpexam17val" id="exam17val" size="25" class="exam"/>
                                        </td>
                                    </tr>                    
                                    <tr>
                                        <td class="fontscreen">L-Test</td>
                                        <td>
                                            <?php
                                            $typeval = AppConst::EXAM_TYPE_LTEST;
                                            ?>
                                            <input name="tmpexam18type" type="hidden" value="<?= $typeval ?>" class="examtype"/>
                                            <input name="tmpexam18seqno" type="hidden" value="" class="seqno"/>
                                            <input type="text" name="tmpexam18val" id="exam18val" size="25" class="exam"/>
                                        </td>
                                        <td class="fontscreen" style="padding-left: 10px">One leg balance</td>
                                        <td>
                                            <?php
                                            $typeval = AppConst::EXAM_TYPE_ONE_LEG_BAL;
                                            ?>
                                            <input name="tmpexam19type" type="hidden" value="<?= $typeval ?>" class="examtype"/>
                                            <input name="tmpexam19seqno" type="hidden" value="" class="seqno"/>
                                            <input type="text" name="tmpexam19val" id="exam19val" size="25" class="exam"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="fontscreen"><b>Cardiovascular System</b></td>
                                    </tr>
                                    <tr>
                                        <td class="fontscreen">6 minute walk test</td>
                                        <td>
                                            <?php
                                            $typeval = AppConst::EXAM_TYPE_WALK_TEST
                                            ?>
                                            <input name="tmpexam14type" type="hidden" value="<?= $typeval ?>" class="examtype"/>
                                            <input name="tmpexam14seqno" type="hidden" value="" class="seqno"/>
                                            <input type="text" name="tmpexam14val" id="exam14val" size="25" class="exam"/>
                                        </td>
                                        <td class="fontscreen" style="padding-left: 10px">Multistage fitness testing</td>
                                        <td>
                                            <?php
                                            $typeval = AppConst::EXAM_TYPE_MULTISTAGE;
                                            ?>
                                            <input name="tmpexam15type" type="hidden" value="<?= $typeval ?>" class="examtype"/>
                                            <input name="tmpexam15seqno" type="hidden" value="" class="seqno"/>
                                            <input type="text" name="tmpexam15val" id="exam15val" size="25" class="exam"/>
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td class="fontscreen">Astrand</td>
                                        <td colspan="3">
                                            <?php
                                            $typeval = AppConst::EXAM_TYPE_ASTRAND;
                                            ?>
                                            <table>
                                                <tr>
                                                    <td align="center">
                                                        1
                                                    </td>
                                                    <td align="center">
                                                        2
                                                    </td>
                                                    <td align="center">
                                                        3
                                                    </td>
                                                    <td align="center">
                                                        4
                                                    </td>
                                                    <td align="center">
                                                        5
                                                    </td>
                                                    <td align="center">
                                                        6
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        <input name="tmpexam20type" type="hidden" value="<?= $typeval ?>" class="examtype"/>
                                                        <input name="tmpexam20seqno" type="hidden" value="1" class="seqno"/>
                                                        <input type="text" name="tmpexam20val" id="exam20val" size="4" class="astrandtest exam"/>
                                                    </td>
                                                    <td align="center">
                                                        <input name="tmpexam21type" type="hidden" value="<?= $typeval ?>" class="examtype"/>
                                                        <input name="tmpexam21seqno" type="hidden" value="2" class="seqno"/>
                                                        <input type="text" name="tmpexam21val" id="exam21val" size="4" class="astrandtest exam"/>
                                                    </td>
                                                    <td align="center">
                                                        <input name="tmpexam22type" type="hidden" value="<?= $typeval ?>" class="examtype"/>
                                                        <input name="tmpexam22seqno" type="hidden" value="3" class="seqno"/>
                                                        <input type="text" name="tmpexam22val" id="exam22val" size="4" class="astrandtest exam"/>
                                                    </td>
                                                    <td align="center">
                                                        <input name="tmpexam23type" type="hidden" value="<?= $typeval ?>" class="examtype"/>
                                                        <input name="tmpexam23seqno" type="hidden" value="4" class="seqno"/>
                                                        <input type="text" name="tmpexam23val" id="exam23val" size="4" class="astrandtest exam"/>
                                                    </td>
                                                    <td align="center">
                                                        <input name="tmpexam25type" type="hidden" value="<?= $typeval ?>" disabled="disabled" class="examtype"/>
                                                        <input name="tmpexam25seqno" type="hidden" value="5" class="seqno" disabled="disabled"/>
                                                        <input type="text" name="tmpexam25val" id="exam25val" size="4"  class="exam" disabled="disabled"/>
                                                    </td>
                                                    <td align="center">
                                                        <input name="tmpexam26type" type="hidden" value="<?= $typeval ?>" disabled="disabled" class="examtype"/>
                                                        <input name="tmpexam26seqno" type="hidden" value="6" class="seqno" disabled="disabled"/>
                                                        <input type="text" name="tmpexam26val" id="exam26val" size="4"  class="exam" disabled="disabled"/>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fontscreen" colspan="4">
                                            <input type="submit" name="submit" value="submit"/></td>
                                        </td>
                                    </tr>
                                    </table>
                                    <input name="max" id="maxinput" type="hidden" value="0"/>
                                    <input name="do" type="hidden" value="<?= $action ?>"/>
                                    </form>
                                    </td>
                                    </tr>
                                    <?php
                                    include 'page/footer.php';
                                    $successval = AppConst::STATUS_SUCCESS;
                                    $errorval = AppConst::STATUS_ERROR;
                                    ?>
                                    <script type="text/javascript">
                                        var defaultContentType = "application/x-www-form-urlencoded; charset=UTF-8";
                                        $(function () {
                                            setupComponents();
                                        });
                                        function setupComponents() {
                                            $(".astrandtest").change(function () {
                                                var n = 0;
                                                $(".astrandtest").each(function () {
                                                    if ($(this).val() != "") {
                                                        n++;
                                                    }
                                                });
                                                if (n == 3) {
                                                    if ($("#exam25val").is(":enabled") && $("#exam23val:enabled").val() == "") {
                                                        $("#exam25val").val("");
                                                        $("#exam25val").attr("disabled", "disabled");
                                                        $(".examtype", $("#exam25val").parent()).attr("disabled", "disabled");
                                                        $(".seqno", $("#exam25val").parent()).attr("disabled", "disabled");
                                                    }
                                                }
                                                if (n == 4) {
                                                    $("#exam25val").removeAttr("disabled");
                                                    $(".examtype", $("#exam25val").parent()).removeAttr("disabled");
                                                    $(".seqno", $("#exam25val").parent()).removeAttr("disabled");
                                                    if ($("#exam26val").is(":enabled") && $("#exam25val:enabled").val() == "") {
                                                        $("#exam26val").val("");
                                                        $("#exam26val").attr("disabled", "disabled");
                                                        $(".examtype", $("#exam26val").parent()).attr("disabled", "disabled");
                                                        $(".seqno", $("#exam26val").parent()).attr("disabled", "disabled");
                                                    }
                                                } else if (n == 5) {
                                                    $("#exam26val").removeAttr("disabled");
                                                    $(".examtype", $("#exam26val").parent()).removeAttr("disabled");
                                                    $(".seqno", $("#exam26val").parent()).removeAttr("disabled");
                                                }
                                            });
                                        }
                                        function disbledInput() {
                                            $(".exam[type=text]").each(function () {
                                                if ($(this).val() == "") {
                                                    $(this).attr("disabled", "disabled");
                                                    $(".examtype", $(this).parent()).attr("disabled", "disabled");
                                                    $(".seqno", $(this).parent()).attr("disabled", "disabled");
                                                }
                                            });
                                            if ($(".exam[type=radio]:checked").length == 0) {
                                                $(".exam[type=radio]").each(function () {
                                                    $(this).attr("disabled", "disabled");
                                                    $(".examtype", $(this).parent()).attr("disabled", "disabled");
                                                    $(".seqno", $(this).parent()).attr("disabled", "disabled");
                                                });
                                            }
                                        }
                                        function prepareData() {
                                            var n = 0;
                                            $(".exam[type=ratio]:enabled").each(function () {
                                                if ($(this).is(":checked")) {
                                                    n++;
                                                    $(this).attr("name", "exam" + n + "val");
                                                    $(".seqno", $(this).parent()).attr("name", "exam" + n + "seqno");
                                                    $(".examtype", $(this).parent()).attr("name", "exam" + n + "type");
                                                }

                                            });
                                            $(".exam[type=text]:enabled").each(function () {
                                                n++;
                                                $(this).attr("name", "exam" + n + "val");
                                                $(".seqno", $(this).parent()).attr("name", "exam" + n + "seqno");
                                                $(".examtype", $(this).parent()).attr("name", "exam" + n + "type");

                                            });
                                            $("#maxinput").val(n);
                                        }
                                        function submitData() {
                                            disbledInput();
                                            prepareData();
                                            prompt("", $("#tform1").serialize());
                                            /*try {
                                             jQuery.ajax({
                                             url: "AppHttpRequest.php",
                                             type: "POST",
                                             contentType: defaultContentType,
                                             data: $("#tform1").serialize(),
                                             dataType: "html",
                                             error: function (transport, status, errorThrown) {
                                             alert("error : " + errorThrown);
                                             },
                                             success: function (data) {
                                             if (data.result == "<?= $successval ?>") {
                                             if (confirm("ต้องการพิมพ์ผลการทดสอบหรือไม่")) {
                                             alert("ใช่");
                                             } else {
                                             alert("ไม่");
                                             }
                                             } else if (data.result == "<?= $errorval ?>") {
                                             alert("เกิดข้อผิดพลาดกรุณษทำรายการใหม่อีกครั้ง");
                                             }
                                             //prompt("", "success : " + data);
                                             }
                                             });
                                             } catch (ex) {
                                             alert(ex);
                                             }*/
                                            //prompt("", $("#tform1").serialize());
                                        }
                                    </script>
                                    </html>