<div data-sticky-container>
    <div class="sticky" id="sticky-topbar" data-sticky data-margin-top="0" style="width:100%;">
        <div class="top-bar" data-magellan>
            <div class="top-bar-left">
                <ul class="menu">
                    <li class="menu-text">ระบบบันทึกข้อมูลสุขภาพ</li>
                </ul>
            </div>
            <div class="top-bar-right">
                <ul class="menu">
                    <li><a href="landing.php" class="header-link current">หน้าหลัก</a></li>
                    <li><a href="logout.php" class="header-link">ออกจากระบบ</a></li>

                </ul>
            </div>
        </div>
    </div>
</div>