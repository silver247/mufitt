<footer class="footer-landing row expanded bg-green">
    <div class="medium-9 medium-centered column">
        <div class="footer-box">
            <div class="row">
                <div class="medium-12 columns">    
                    <p class="centered">วิทยาลัยวิทยาศาสตร์และเทคโนโลยีการกีฬา มหาวิทยาลัยมหิดล</p>
                    <hr>
                    <p class="centered">เลขที่ 999 ถ.พุทธมณฑล สาย 4 ต.ศาลายา อ.พุทธมณฑล จ.นครปฐม 73171 โทร : 0-2441-4295 Fax : 0-2889-3693</p>
                </div>
            </div>
        </div>
    </div>
</footer>