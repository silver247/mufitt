<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Foundation for Sites</title>
        <link rel="stylesheet" href="resources/css/foundation.css">
        <link rel="stylesheet" href="resources/css/app.css">
    </head>
    <body>
        <?php include ('./view/header.php'); ?>
        <br/>
        <div class="row">
            <div class="medium-12 columns">
                <div class="row medium-12">
                    <div class="column">
                        <nav aria-label="You are here:" role="navigation">
                            <ul class="breadcrumbs">
                                <li><a href="index.php">หน้าแรก</a></li>
                                <li>
                                    <span class="show-for-sr">Current: </span> บันทึกข้อมูลการทดสอบ
                                </li>
                                <!--<li class="disabled">Gene Splicing</li>-->

                            </ul>
                        </nav>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="medium-12 columns">
                        <h4>บันทึกข้อมูลการทดสอบ</h4>
                    </div>
                </div>
                <hr>
                <form method="post" action="">
                    <div class="row">
                        <div class="medium-6 columns">
                            <div class="medium-4 columns">
                                <div align="right">ชื่อ</div> 
                            </div>
                            <div class="medium-8 columns">
                                <input type="text" name="txtName" id="txtName" required placeholder="กรุณากรอกชื่อ">
                            </div>
                        </div>
                        <div class="medium-6 columns">
                            <div class="medium-4 columns">
                                <div align="right">นามสกุล</div>
                            </div>
                            <div class="medium-8 columns">
                                <input type="text" name="txtLastname" id="txtLastname" required placeholder="กรุณากรอกนามสกุล">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="medium-6 columns">
                            <div class="medium-4 columns">
                                <div align="right">น้ำหนัก</div>
                            </div>
                            <div class="medium-8 columns">
                                <input type="text" name="txtWeight" id="txtWeight" required placeholder="กรุณาน้ำหนัก">
                            </div>
                        </div>
                        <div class="medium-6 columns">
                            <div class="medium-4 columns">
                                <div align="right">ส่วนสูง</div>
                            </div>
                            <div class="medium-8 columns">
                                <input type="text" name="txtHeight" id="txtHeight" required placeholder="กรุณาส่วนสูง">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="medium-6 columns">
                            <div class="medium-4 columns">
                                <div align="right">ผลการทดสอบ 1</div>
                            </div>
                            <div class="medium-8 columns">
                                <input type="text" name="txtExam1" id="txtExam1" required placeholder="กรุณากรอกผลการทดสอบ">
                            </div>
                        </div>
                        <div class="medium-6 columns">
                            <div class="medium-4 columns">
                                <div align="right">ผลการทดสอบ 2</div>
                            </div>
                            <div class="medium-8 columns">
                                <input type="text" name="txtExam2" id="txtExam2" required placeholder="กรุณากรอกผลการทดสอบ">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="medium-6 columns">
                            <div class="medium-4 columns">
                                <div align="right">ผลการทดสอบ 3</div>
                            </div>
                            <div class="medium-8 columns">
                                <input type="text" name="txtExam3" id="txtExam3" required placeholder="กรุณากรอกผลการทดสอบ">
                            </div>
                        </div>
                        <div class="medium-6 columns">
                            <div class="medium-4 columns">
                                <div align="right">ผลการทดสอบ 4</div>
                            </div>
                            <div class="medium-8 columns">
                                <input type="text" name="txtExam4" id="txtExam4" required placeholder="กรุณากรอกผลการทดสอบ">
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="medium-6 medium-centered columns">
                            <div class="medium-6 columns">
                                <a href="index.php" class="alert expanded button">ย้อนกลับ</a>
                            </div>
                            <div class="medium-6 columns">
                                <input type="submit" class="expanded success button" value="ดำเนินการต่อ" name="btnSubmit">
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>

        <?php
        include ('view/footerBottom.php');
        ?>
        <script src="resources/js/vendor/jquery.js"></script>
        <script src="resources/js/vendor/what-input.js"></script>
        <script src="resources/js/vendor/foundation.js"></script>
        <script src="resources/js/app.js"></script>
    </body>
</html>
