<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Foundation for Sites</title>
        <link rel="stylesheet" href="resources/css/foundation.css">
        <link rel="stylesheet" href="resources/css/app.css">
    </head>
    <body>
        <?php include ('./view/header.php'); ?>
        <br/>
        <div class="row">
            <div class="medium-12 columns">
                <div class="row medium-12">
                    <div class="column">
                        <nav aria-label="You are here:" role="navigation">
                            <ul class="breadcrumbs">
                                <li><a href="index.php">หน้าแรก</a></li>
                                <li>
                                    <span class="show-for-sr">Current: </span> แก้ไขข้อมูลการทดสอบ
                                </li>
                                <!--<li class="disabled">Gene Splicing</li>-->
                                
                            </ul>
                        </nav>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="medium-12 columns">
                        <h4>แก้ไขข้อมูลทดสอบ</h4>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="medium-6 columns">
                        <div class="medium-4 columns">
                            <div align="right">ชื่อ</div> 
                        </div>
                        <div class="medium-8 columns">
                            <input type="text" name="txtName" id="txtName" required placeholder="กรุณากรอกชื่อ">
                        </div>
                    </div>
                    <div class="medium-6 columns">
                        <div class="medium-4 columns">
                            <div align="right">นามสกุล</div>
                        </div>
                        <div class="medium-8 columns">
                            <input type="text" name="txtLastname" id="txtLastname" required placeholder="กรุณากรอกนามสกุล">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="medium-6 columns">
                        <div class="medium-4 columns">
                            <div align="right">เลขบัตรประชาชน</div>
                        </div>
                        <div class="medium-8 columns">
                            <input type="text" name="txtName" id="txtName" required placeholder="กรุณากรอกชื่อ">
                        </div>
                    </div>
                    <div class="medium-6 columns">
                        <div class="medium-4 columns">
                            <div align="right">เลขแบบฟอร์ม</div>
                        </div>
                        <div class="medium-8 columns">
                            <input type="text" name="txtLastname" id="txtLastname" required placeholder="กรุณากรอกนามสกุล">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <?php
        include ('view/footerBottom.php');
        ?>
        <script src="resources/js/vendor/jquery.js"></script>
        <script src="resources/js/vendor/what-input.js"></script>
        <script src="resources/js/vendor/foundation.js"></script>
        <script src="resources/js/app.js"></script>
    </body>
</html>
