<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TesterHeaderInfo
 *
 * @author FREJOID
 */
class TesterHeaderInfo {
    //put your code here
    private $testerID,$nameTH,$nameEN,$surnameTH,$surnameEN,$dob,$sex,$keyTest
            ,$weight,$height,$exercise,$phone,$pressure,$pulse,$cardID,$messageID,$messageDetail,$age,$email, $grpDesc,$isResearch;
    
    public function SetTesterID($value){
        $this->testerID = $value;
    }
    
    public function GetTesterID(){
        return $this->testerID;
    }
    
    public function SetNameTH($value){
        $this->nameTH  = $value;
    }
    
    public function GetNameTH(){
        return $this->nameTH;
    }
    
    public function SetNameEN($value){
        $this->nameEN  = $value;
    }
    
    public function GetNameEN(){
        return $this->nameEN;
    }
    
    public function SetSurnameTH($value){
        $this->surnameTH  = $value;
    }
    
    public function GetSurnameTH(){
        return $this->surnameTH;
    }
    
    public function SetSurnameEN($value){
        $this->surnameEN  = $value;
    }
    
    public function GetSurnameEN(){
        return $this->surnameEN;
    }
    
    public function SetDOB($value){
        $this->dob  = $value;
    }
    
    public function GetDOB(){
        return $this->dob;
    }
    
    public function SetSex($value){
        $this->sex  = $value;
    }
    
    public function GetSex(){
        return $this->sex;
    }
    
    public function SetTesterGrp($value){
        $this->keyTest  = $value;
    }
    
    public function GetTesterGrp(){
        return $this->keyTest;
    }
    
    public function SetWeight($value){
        $this->weight  = $value;
    }
    
    public function GetWeight(){
        return $this->weight;
    }
    
    public function SetHeight($value){
        $this->height  = $value;
    }
    
    public function GetHeight(){
        return $this->height;
    }
    
    public function SetExcercise($value){
        $this->exercise  = $value;
    }
    
    public function GetExcercise(){
        return $this->exercise;
    }
    
    public function SetPhone($value){
        $this->phone  = $value;
    }
    
    public function GetPhone(){
        return $this->phone;
    }
    
    public function SetBloodPressure($value){
        $this->pressure  = $value;
    }
    
    public function GetBloodPressure(){
        return $this->pressure;
    }
    
    public function SetPulse($value){
        $this->pulse  = $value;
    }
    
    public function GetPulse(){
        return $this->pulse;
    }
    
    public function SetCardID($value){
        $this->cardID  = $value;
    }
    
    public function GetCardID(){
        return $this->cardID;
    }
    
    public function SetMessageID($value){
        $this->messageID = $value;
    }
    
    public function GetMessageID(){
        return $this->messageID;
    }
    
    public function SetMessageDetail($value){
        $this->messageDetail = $value;
    }
    
    public function GetMessageDetail(){
        return $this->messageDetail;
    }
    
    public function SetAge($value){
        $this->age = $value;
    }
    
    public function GetAge(){
        return $this->age;
    }
    
    public function SetEmail($value){
        $this->email = $value;
    }
    
    public function GetEmail(){
        return $this->email;
    }
    
    public function SetTesterGrpDesc($value){
        $this->grpDesc = $value;
    }
    
    public function GetTesterGrpDesc(){
        return $this->grpDesc;
    }
    
    public function SetIsResearch($value){
        $this->isResearch = $value;
    }
    
    public function GetIsResearch(){
        return $this->isResearch;
    }
}

?>

